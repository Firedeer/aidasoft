<?php

return [

    'throttle' => 'Demasiados intentos de inicio de sesión. Inténtalo de nuevo en :seconds segundos.',

    'roles' => [
        'superadmin' => 'Super administrador',
        'admin' => 'Administrador',
        'editor' => 'Editor',
        'user' => 'Usuario',
    ],

];