	<div class="form-row">
	<div class="form-group col-md-12">
	<div class="form-group col-md-3">
	<div class="custom-control custom-radio custom-control-inline">
	{!! Form::label('lblnatural', 'Persona Natural', ['class' => 'custom-control-label']) !!}
	{!! Form::radio('id_tipo_cliente', '2' , true,['onchange' => 'hidden_control(2);']) !!}
	
	</div>
	</div>
	<div class="form-group col-md-3">
	<div class="custom-control custom-radio custom-control-inline">

	{!! Form::label('lbljuridica', 'Persona Juridica', ['class' => 'custom-control-label']) !!}
	{!! Form::radio('id_tipo_cliente', '1' , false,['onchange' => 'hidden_control(1);']) !!}
	</div>

	</div>
	<div class="form-group col-md-2">

	<label for="inputCity">Activo</label>
      <div class="togglebutton">
	  
		<label>
		<input type="checkbox" id="chk_active" name="chk_active" 
		@if(isset($provider->active))
		@if($provider->active)
        checked="checked"
		@endif
		@else
		checked="checked"
        @endif
		>
		</label>
		</div>
	</div>
	
</div>	
    <div class="form-group col-md-12">
		{!! Form::label('lblnom_empresa', 'Nombre') !!}
      {!! Form::text('Nombre', old('Nombre'), ['class' => 'form-control', 'placeholder' => 'Nombre', 'autofocus']) !!}
    </div>
	@if(isset($provider->id_tipo_cliente))
		@if($provider->id_tipo_cliente==2)
		<div class="form-group col-md-12" id='empresa_natural'>


		{!! Form::label('lblnom_empresa', 'Empresa') !!}

		{!! Form::select('empresa_id', \TradeMarketing\Clients::listing(), old('empresa_id'), ['class'=>'selectpicker','data-live-search'=>'true', 'placeholder' => '--']) !!}
		{!! $errors->first('empresa_id', '<p class="text-danger">:message</p>') !!}
		</div>	
	
		@endif
		@else
			<div class="form-group col-md-12" id='empresa_natural'>


    {!! Form::label('lblnom_empresa', 'Empresa') !!}

	{!! Form::select('empresa_id', \TradeMarketing\Clients::listing(), old('empresa_id'), ['class'=>'selectpicker','data-live-search'=>'true', 'placeholder' => '--']) !!}
	{!! $errors->first('empresa_id', '<p class="text-danger">:message</p>') !!}
	</div>	
	@endif

	<hr/>

	   <div class="form-group col-md-6">
      {!! Form::label('lblnom_empresa', 'Direccion 1') !!}
	  {!! Form::text('direccion1', old('direccion1'), ['class' => 'form-control', 'placeholder' => '1234 Main St', 'autofocus']) !!}
    </div>
	   <div class="form-group col-md-6">
      {!! Form::label('lblnom_empresa', 'Direccion 2') !!}
      {!! Form::text('direccion2', old('direccion2'), ['class' => 'form-control', 'placeholder' => '5678 Main St', 'autofocus']) !!}
    </div>
	
	
	
	
	
	
	
	

	
	
	
    <div class="form-group col-md-4">
		{!! Form::label('lblnom_empresa', 'Pais') !!}
		{!! Form::select('id_pais', $Listapaises, old('id_pais'), ['class'=>'selectpicker','data-live-search'=>'true', 'placeholder' => '--']) !!}
		{!! $errors->first('id_pais', '<p class="text-danger">:message</p>') !!}
    </div>
    <div class="form-group col-md-4">
      {!! Form::label('lblnom_empresa', 'Estado') !!}
      {!! Form::text('estado', old('estado'), ['class' => 'form-control', 'placeholder' => 'Saragosa', 'autofocus']) !!}
    </div>
  </div>
	<div class="form-row">
    <div class="form-group col-md-2">
      {!! Form::label('lblnom_empresa', 'Codigo postal') !!}
      {!! Form::text('codigo_postal', old('codigo_postal'), ['class' => 'form-control', 'placeholder' => '32576', 'autofocus']) !!}
    </div>
     <div class="form-group col-md-2">
      {!! Form::label('lblnom_empresa', 'Telefono') !!}
      {!! Form::text('telefono', old('telefono'), ['class' => 'form-control', 'placeholder' => '000000000', 'autofocus']) !!}
    </div>
	</div>
		<div class="form-row">
    <div class="form-group col-md-4">
      {!! Form::label('lblnom_empresa', 'Ruc') !!}
      {!! Form::text('Ruc', old('ruc'), ['class' => 'form-control', 'placeholder' => '000-000000', 'autofocus']) !!}
    </div>
    <div class="form-group col-md-4">
      {!! Form::label('lblnom_empresa', 'Email') !!}
      {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => '123@hiod.com', 'autofocus']) !!}
    </div>
	<div class="form-group col-md-4">
      {!! Form::label('lblnom_empresa', 'Pagina web') !!}
      {!! Form::text('pagina_web', old('pagina_web'), ['class' => 'form-control', 'placeholder' => 'www.something.com', 'autofocus']) !!}
    </div>
	</div>
