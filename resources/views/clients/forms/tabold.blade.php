	<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Contactos</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="notas-tab" data-toggle="tab" href="#notas" role="tab" aria-controls="notas" aria-selected="false">Notas</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="sales_purchase" data-toggle="tab" href="#sales_purchases" role="tab" aria-controls="sales_purchases" aria-selected="false">Ventas y Facturas</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
   <div class="col-md-12">

        <a href="#" class="btn btn-primary mdb">
         <i class="material-icons md-18">save</i> Agregar
        </a>

    </div>
  
  
  </div>
    <!--               NOTAS                  -------------->
  <div class="tab-pane fade" id="notas" role="tabpanel" aria-labelledby="notas-tab">
  
  <div class="md-form">
  <i class="material-icons">
	speaker_notes</i>
  <textarea id="form10" class="md-textarea form-control" rows="3"></textarea>
  
</div>
  
  </div>
  
  
  
  <!--               VENTAS Facturas-------------->
  <div class="tab-pane fade" id="sales_purchases" role="tabpanel" aria-labelledby="sales_purchase">
	<div class="form-row">
	
	
	
	<div class="form-group col-md-3">
	<div class="custom-control custom-radio custom-control-inline">
	{!! Form::label('lblnatural', 'Persona Natural', ['class' => 'custom-control-label']) !!}
	{!! Form::radio('id_tipo_cliente', '2' , true,['onchange' => 'hidden_control(2);']) !!}
	
	</div>
	</div>
	<div class="form-group col-md-3">
	<div class="custom-control custom-radio custom-control-inline">

	{!! Form::label('lbljuridica', 'Persona Juridica', ['class' => 'custom-control-label']) !!}
	{!! Form::radio('id_tipo_cliente', '1' , false,['onchange' => 'hidden_control(1);']) !!}
	</div>

	</div>
	
	<div class="form-group col-md-12" >
	<div class="form-group col-md-9" >
	
	<label class="custom-control-label" for="defaultInline2"><h3>Ventas</h3></label>
	<div class="custom-control custom-checkbox">
	
	
	<input type="checkbox" class="custom-control-input" id="chk_cliente" onchange="isClient(true);">
	
	{!! Form::checkbox('Isclient', '1', true,['onchange' => 'isClient(true);']) !!}
	{!! Form::label('lblcliente', 'Es cliente', ['class' => 'custom-control-label']) !!}
	
	</div>
	</div>	
	

	<div class="form-group col-md-3">
	<label class="custom-control-label" for="defaultInline2"><h3>Compras</h3></label>
	<div class="custom-control custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="chk_proveedor">
  <label class="custom-control-label" for="chk_proveedor">Es proveedor</label>
	</div>
	</div>	
	</div>
	
	
	
	
	
	</div>
	<div class="form-row" id="es_cliente" style="visibility:hidden;">
	   <div class="form-group col-md-4">
      <label for="txt_rol">Sucursal</label>
	
	<select class="selectpicker" data-live-search="true">
	<option data-tokens="ketchup mustard">Hot Dog, Fries and a Soda</option>
	<option data-tokens="frosting"> Crear nueva</option>
	</select>
	</div >

	
	<div class="form-group col-md-4">
	 <a href="" class="btn pull-right"><i class="material-icons"> add_circle_outline</i>Crear sucursal</a>
	</div>

	<div class="form-group col-md-4">

	<a href="" class="btn pull-right"><i class="material-icons"> edit</i>Editar sucursal</a></div>
	</div >
	<div class="form-row">
	<br/>
	 <div class="form-group col-md-12">
      <label for="txt_usuario">Referencia</label>
      <input type="text" class="form-control" id="txt_referencia" name="txt_referencia" placeholder="0000000000" >
    </div>
    </div>
	 </div>
	</div>
  </div>
</div>