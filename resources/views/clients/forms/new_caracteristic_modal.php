<!-- Modal -->
<div class="modal fade" id="brandModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva Marca</h4>
            </div>
            <div class="modal-body">

                <form action="{!! route('brand.store') !!}" class="form-horizontal row" method="POST"
                      id="new-brand">

                    @include('brands.forms.general')

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb" role="save">Guardar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="sucursaleModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva Categoria</h4>
            </div>
            <div class="modal-body">

                <form action="{!! route('category.store') !!}" class="form-horizontal row" method="POST"
                      id="new-category">

                    @include('clients.forms.new_sucursal')

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb" role="save">Guardar</button>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="measurementModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva Unidad de Medida</h4>
            </div>
            <div class="modal-body">

                <form action="{!! route('measurement.store') !!}" class="form-horizontal row" method="POST"
                      id="new-measurement">

                    @include('measurements.forms.general')

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb" role="save">Guardar</button>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="attributeModal" tabindex="-1" role="dialog">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo Atributo</h4>
                <p class="help-block">Puedes editar en la seccion de Attributos</p>
            </div>

            <div class="modal-body">

                <form action="{!! route('attribute.store') !!}" class="form-horizontal row"
                      method="POST"
                      id="new-attribute">


                    <div class="form-group mdb">
                        {!!Form::label('description', 'Descripción', ['class' => 'control-label col-sm-2'])!!}

                        <div class="col-sm-10">

                            {!! Form::text('description', old('description'), ['class' => 'form-control', 'autocomplete' => 'off', 'autofocus']) !!}
                        </div>
                    </div>
                </form>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb" role="save">Guardar</button>
            </div>
        </div>
    </div>
</div>



<script>
    function showModal(modal, element, selected) {

        selected || ( selected = false );

        var button = modal.find('.modal-footer button[role="save"]');

        var form = modal.find('form')[0];

        $(form).submit(function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
        });

        $(button).click(function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            var ajax = save(form);

            ajax.success(function (response) {

                console.log($(element));

                var selectOption = $(element).append('<option value="' + response.id + '">' + response.description + '</option>');

                if (selected) {
                    selectOption[0].lastChild.selected = true;
                }

                form.reset();
                modal.modal('hide')

            }).error(function (xhr, status) {
                if (xhr.status == 422) {
                    errors(xhr.responseJSON, modal);
                }
            })
        });

        modal.find('.modal-body input').focus();
    }


    function save(form) {
        return $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            dataType: 'json',
            error: function (xhr, status) {
                console.log(xhr)
            }
        });
    }

    function errors(json, modal) {
        modal.find('.text-danger').remove();

        $.each(json, function (item, message) {

            var p = document.createElement('P');

            p.appendChild(document.createTextNode(message));
            p.setAttribute('class', 'text-danger');

            var elem = modal.find('#' + item + '')[0];


            if (elem) {
                var parent = elem.closest('.form-group');
                parent.classList.add('has-error');

                $(parent).insertAfterNode(p, elem);
            }
        });
    }

    $('#brandModal').on('show.bs.modal', function (event) {
        showModal($(this), '#brand_id', true);
    });


    $('#sucursaleModel').on('show.bs.modal', function (event) {
        showModal($(this), '#category_id', true);
    });


    $('#measurementModal').on('show.bs.modal', function (event) {
        showModal($(this), '#measurement_unit_id', true);
    });


    $('#attributeModal').on('show.bs.modal', function (event) {
        showModal($(this), 'select.attr');
    });
</script>
