	<div class="form-row">

	
	<div class="form-group col-md-12" >
	<div class="form-group col-md-9" >
	
	<label class="custom-control-label" for="defaultInline2"><h3>Ventas</h3></label>
	<div class="custom-control custom-checkbox">
	
	
	{!! Form::checkbox('chk_cliente', '1',isset($provider->chk_cliente) ? old($provider->chk_cliente):true,['id'=>'chk_cliente','onchange' => 'isClient(true);']) !!}
	{!! Form::label('lblcliente', 'Es cliente', ['class' => 'custom-control-label']) !!}
	
	</div>
	</div>	
	

	<div class="form-group col-md-3">
	<label class="custom-control-label" for="defaultInline2"><h3>Compras</h3></label>
	<div class="custom-control custom-checkbox">
	{!! Form::checkbox('chk_provider', '1',isset($provider->chk_provider) ?  old($provider->chk_provider):true ) !!}
	{!! Form::label('lblprovider', 'Es proveedor', ['class' => 'custom-control-label']) !!}
	</div>
	</div>	
	</div>
	
	
	
	
	
	</div>
	
	
			<div class="form-row" id="es_cliente" >
	  
		    <div class="form-group @if($errors->has('sucursal_id')) has-error @endif">

        {!!Form::label('sucursal_id', 'Sucursal (*)', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::select('sucursal_id', $ListaSucursales, old('sucursal_id'), ['class' => 'form-control', 'placeholder'=>'-- ninguno --'])!!}

            {!! $errors->first('sucursal_id', '<p class="text-danger">:message</p>')  !!}
            <br>
        </div>
    </div>


	@if(!isset($provider->id))
	<div class="form-group col-md-4">
	  <button type="button" class="btn btn-link" data-toggle="modal" data-target="#sucursaleModel">
      <i class="material-icons md-18">add_circle_outline</i> Crear sucursal
       </button>
	</div>
	@endif

	@if(!isset($provider->sucursal_id)&& isset($provider->id))
	<div class="form-group col-md-4">
	  <button type="button" class="btn btn-link" data-toggle="modal" data-target="#sucursaleModel">
      <i class="material-icons md-18">add_circle_outline</i> Crear sucursal
       </button>
	</div>
	@endif
	@if(isset($provider->sucursal_id)&& isset($provider->id))
	<div class="form-group col-md-4">
	  <button type="button" class="btn btn-link" data-toggle="modal" data-target="#editsucursaleModel">
      <i class="material-icons md-18">add_circle_outline</i> Editar sucursal
       </button>
	</div>
	@endif
	
	<div class="form-row">
	<br/>
	 <div class="form-group col-md-12">
      <label for="txt_usuario">Referencia</label>
	   {!! Form::text('txt_referencia', old('txt_referencia'), ['class' => 'form-control', 'placeholder' => '0000000000', 'autofocus','id'=>'txt_referencia']) !!}
    </div>
    </div>
	
	
@section('extra_scriptBody')
<script>

$(document).ready(function() {
	
var elem = document.getElementById('es_cliente');
		  // Get the checkbox
		var checkBox = document.getElementById("chk_cliente");
		if (checkBox.checked == true){
		elem.style.visibility='visible';
		}
		else 
		{
		elem.style.visibility='hidden';
		}
});

</script>



    @include('clients.forms.sucursales')
    <script src="{!! asset('assets/js/send_to_ajax.js') !!}"></script>
    <script src="{!! asset('assets/js/demo_textarea_handler.js') !!}"></script>

		<script>
        // Confirmar que hubo algun cambio  en la tabla
        $(document).delegate('#tbl-atrributes tbody', 'DOMSubtreeModified', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            var rows = Array.prototype.slice.call(this.getElementsByTagName('tr'));

            rows.forEach(function (items, key) {

                var elements = Array.prototype.slice.call(items.querySelectorAll('.name_handler'));

                elements.forEach(function (item) {

                    var nameAttr = item.getAttribute('name');

                    var split = nameAttr.split("]");

                    // var split_2 = nameAttr.split("[")
                    var newName = split[0].split("[")[0] + '[' + key + ']' + split[1] + '][]';

                    // change attribute name
                    item.setAttribute('name', newName);
                });
            });
        });
    </script>
	
@endsection