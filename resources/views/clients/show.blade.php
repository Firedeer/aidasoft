@extends('layout_principal')

@section('extra_scriptHead')

@endsection

@section('page_title')
    <span class="hidden-sm hidden-md hidden-lg">Perfil</span>
@endsection

@section('notify_icon_bar')
    <a href="{{url('order/create/purchases')}}">
        <i class="icon material-icons">note_add</i>
    </a>

@endsection

@section('container')

    <?php setlocale(LC_TIME, "es");
    ?>

    <div class="col-md-8">
        <div class="row-sm">

            <div class="panel">
                <div class="panel-heading">
                    {{-- <a href="" class=""><i class="material-icons">add_shopping_cart</i></a> --}}
                    <a href="{!! route('admin.provider.edit', $provider->id) !!}" class="btn btn-primary btn-xs mdb pull-right">
                        <i class="material-icons">edit</i>
                        <span class="hidden-xs"> Editar</span>
                    </a>

                    <h2>{!! $provider->fullname !!}</h2>

                </div>

                <div class="panel-body">


                    <p><strong>Nombre:</strong> {!!$provider->fullname !!}</p>
                    <p><strong>Direccion: </strong>{!!$provider->address!!}</p>
                    <p><strong>Telefono: </strong>{!!$provider->phone!!}</p>
                    <p><strong>Email: </strong>{!!$provider->email!!}</p>
                    <p><strong>Compañia: </strong>{!!$provider->company !!}</p>
                    <p><strong>Tipo de proveedor: </strong>
                        <span class="tags">{!!$provider->type!!}</span>
                    </p>


                    <div class="col-xs-12 divider text-center hidden">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 emphasis">
                                <h2><strong>{!!count($purchases)!!}</strong></h2>
                                <p>
                                    <small>Compras</small>
                                </p>
                            </div>
                            {{--<div class="col-xs-4 col-sm-4 emphasis">--}}
                                {{--<h2><strong>245</strong></h2>--}}
                                {{--<p>--}}
                                    {{--<small>Following</small>--}}
                                {{--</p>--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-4 col-sm-4 emphasis">--}}
                                {{--<h2><strong>43</strong></h2>--}}
                                {{--<p>--}}
                                    {{--<small>Snippets</small>--}}
                                {{--</p>--}}

                            {{--</div>--}}
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel">
            <div class="panel-heading">
                <h3>Ultimas compras</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="list-group">

                        @if(count($purchases) <= 0 )
                            <p class="text-center">
                                No se encontraron compras para este proveedor.
                                <br>
                                Presiona el boton
                                <i class="material-icons">add_shopping_cart</i>
                                para hacer una compra
                            </p>
                        @endif

                        @foreach($purchases as $purchase)

                            <a href="/order/purchases/{{ $purchase->id }}" class="list-group-item">
                                <div class="row-action-primary">
                                    <i class="material-icons">description</i>
                                </div>
                                <div class="row-content">

                                    <h4 class="list-group-item-heading">{{ $purchase->order }}
                                        <small class="date-format">
                                            {!! \Carbon\Carbon::parse( $purchase->updated_at )->formatLocalized('%b %d') !!}
                                        </small>
                                    </h4>

                                    <p class="list-group-item-text">
                                        Creado: {!! \Carbon\Carbon::parse( $purchase->created_at )->formatLocalized('%d de %B') !!} </p>

                                </div>
                                <!-- .row-content -->
                            </a>
                            <!-- .list-group-item -->
                            <div class="separator"></div>

                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection