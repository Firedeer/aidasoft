@extends('layout_principal')


@section('container')

    {!! Form::model($order, ['route' => ['order.update', $order->id ], 'method' => 'PUT', 'class' => 'form-horizontal form-condensed', 'id' => 'form-order']) !!}


    <section class="col-md-10 col-md-offset-1" id="order-panel">

        <div class="panel">

            <div class="panel-heading">

                <div class="pull-right">
                    <ul class="nav nav-pills">
                        <li role="presentation" class="active" href="#transfer" aria-controls="transfer"
                            role="tab"
                            data-toggle="tab">
                            <a href="#">
                                <i class="material-icons md-18">&#xE915;</i>
                                <span class="hidden-xs">{!! trans('app.attributes.order') !!}</span>
                            </a>
                        </li>
                        <li role="presentation" href="#tracking" aria-controls="tracking" role="tab"
                            data-toggle="tab">
                            <a href="#">
                                <i class="material-icons md-18">&#xE889;</i>
                                <span class="hidden-xs">{!! trans('app.attributes.tracking') !!}</span>
                            </a>
                        </li>
                    </ul>
                </div>


                @if($order->ordered_at)
                    @include('partials.contact-chip',  [
                    'role' => $order->fromUser->role->description,
                    'userName' => $order->fromUser->fullName,
                    'shortName' => $order->fromUser->nameProfile,
                    'time' => $order->ordered_at,
                    'message' =>   'envió un pedido.'
                    ])
                    @else

                        @include('partials.contact-chip',  [
                        'role' => $order->createdBy->role->description,
                        'userName' => $order->createdBy->fullName,
                        'shortName' => $order->createdBy->nameProfile,
                        'time' => $order->created_at
                        ])
                @endif

                <h1>
                    <small>{!! trans('app.attributes.order') !!}</small>
                    <br>{!! isset($order->order_num)? $order->order_num : '' !!}
                </h1>
            </div>


            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="transfer">

                    <div class="panel-body">
                        <div class="row">

                            @include('orders.partials.order_general')

                        </div>
                    </div>

                    <div class="panel-body">

                        <legend>{!! trans('app.attributes.articles') !!}</legend>

                        <div class="row-sm">
                            @if($order->editing)

                                @include('orders.partials.table_editing')
                            @else

                                @include('orders.partials.table',[ 'authorized' => $order->for_user_id])
                            @endif
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-sm-12">

                            @if($order->editing)
                                @include('partials.observation_field')
                            @else
                                @include('partials.observation_field', ['textarea' => isset($order->observation)? $order->observation : ''])
                            @endif
                        </div>
                    </div>

                </div>
                <!--.tabpanel-->
                <div role="tabpanel" class="tab-pane" id="tracking">

                    <div class="panel-body">

                        <legend class="pull-left">{!! trans('app.attributes.tracking') !!}</legend>

                        @if(count($order->tracking))
                            <table class="table table-bordered small">
                                <thead>
                                <tr>
                                    <th>{!! trans('app.attributes.date') !!}</th>
                                    <th>{!! trans('app.attributes.warehouse') !!}</th>
                                    <th>{!! trans('app.attributes.long_description') !!}</th>
                                </tr>
                                </thead>
                                <tbody>


                                @foreach($order->tracking as $tracking)
                                    <tr>
                                        <td>{!! $tracking->made_at !!}</td>
                                        <td>{!! $tracking->warehouse !!}</td>
                                        <td>{!! $tracking->description !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="clearfix"></div>
                            <p class="text-muted">No se ha registrado ningún movimiento para este Pedido.</p>
                        @endif
                    </div>

                </div>
                <!--.tabpanel-->
            </div>
            <!-- .tab-content -->


        </div>
    </section>

    {!! Form::close() !!}
@endsection


@section('extra_scriptBody')


    @if($order->isDraft())

        @include('partials.date_script')


        @include('orders.partials.scripts')

    @endif


    <script>
        $('.delete-order').on('click', function (event) {
            event.preventDefault();
            var route = $(this).attr('href');

            swal({
                    title: "¿Estas seguro que quieres eliminar este pedido?",
                    text: "¡No podrás recuperar este pedido!",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancelar",
                    confirmButtonText: "Si, Quiero Eliminarlo!",
                    closeOnConfirm: true
                },
                function () {

                    var $get = $.getJSON(route);

                    $get.done(function (response) {
                        if (response.message_html) {

                            $('#order-panel').html(response.message_html);
                        }
                    });
                    $get.always(function (response) {

                        var html = '';

                        if (response.alert) {
                            html = response.alert;
                        } else {
                            html = response.responseJSON.alert;
                        }

                        var body = document.getElementsByTagName('body')[0];
                        body.insertAdjacentHTML('afterbegin', html)

                        // actualiza el estado del carrito en el menu
                        refreshCart();
                    });
                });
        });
    </script>

@endsection



@section('header')

    <div class="col-md-12">
        <div class="row">
            <div class="navbar">

                @include('orders.partials.step_state', ['status' => $order->status])


                <div class="clearfix"></div>

                <div class="container-fluid">

                    <div class="navbar-btn">
                        @include('orders.partials.action_controls')

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('order.index') !!}">Pedidos</a></li>
        <li class="active">{!! isset($order->order_num)? $order->order_num : '#XXXX' !!}</li>
    </ol>
@endsection