<table id="table_" hidden>
    <tr>
        <td>
            {!! Form::text('article', '') !!}
        </td>
        <td>
            {!! Form::text('measurement_unit', '') !!}
        </td>
        <td>
            {!! Form::text('quantity', '' ) !!}
        </td>
        <td>
        </td>
        <td>
            <button type="button" class="btn btn-xs btn-danger mdb remove-row" title="Quitar">
                <i class="material-icons md-18">&#xE872;</i>
            </button>
        </td>
    </tr>

    <tr class="editing">
        <td>
            {!! Form::select('items[][article]', $articleList, [], ['class' => 'form-control name_handler article', 'placeholder' => 'Selecciona un artículo de la lista'] ) !!}
        </td>

        <td class="active">
            <span class="internal_reference"></span>
            {!! Form::hidden('items[][internal_reference]', '', ['class' => 'internal_reference name_handler'] ) !!}
        </td>
        <td class="active">
            <span class="measurement_unit"></span>
            {!! Form::hidden('items[][measurement_unit]', '', ['class' => 'measurement_unit name_handler'] ) !!}
        </td>
        <td>
            {!! Form::text('items[][quantity]', '', ['class' => 'form-control quantity name_handler'] ) !!}
        </td>
        <td class="active">
        </td>
        <td>
            <button type="button" class="btn btn-xs btn-danger mdb remove-row" title="Quitar">
                <i class="material-icons md-18">&#xE872;</i>
            </button>
        </td>
    </tr>
</table>



<script>
    $('#add').click(function () {

        var row = $('#table_').find('tr.editing')[0];

        var draft = row.cloneNode(true);

        var tbody = $('#table-orders').find('tbody');

        draft.classList.add('editing');

        tbody.append(draft);
    });


    $('#table-orders').delegate('.remove-row', 'click', function () {
        var row = this.closest('tr');
        row.remove();
    });


    $('#table-orders').delegate('select.article', 'change', function (event) {
        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();

        var tr = event.target.closest('tr');
        var article = $(this).val();

        var measurement_unit = $(tr).find('td .measurement_unit');
        var internal_reference = $(tr).find('td .internal_reference');

        if (article) {

            $.getJSON('{!! url('article/find') !!}' + '/' + article, function (json) {

                measurement_unit[0].innerHTML = json.measurement_unit;
                measurement_unit[1].value = json.measurement_unit;
                internal_reference[0].innerHTML = json.internal_reference;
                internal_reference[1].value = json.internal_reference;
            });
        } else {
            measurement_unit[0].innerHTML = '';
            measurement_unit[1].value = '';
            internal_reference[0].innerHTML = '';
            internal_reference[1].value = '';
        }
    });
</script>


<script>
    // Confirmar que hubo algun cambio  en la tabla
    $(document).delegate('#table-orders tbody', 'DOMSubtreeModified', function (event) {
        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();

        var rows = Array.prototype.slice.call(this.getElementsByTagName('tr'));

        rows.forEach(function (items, key) {

            var elements = Array.prototype.slice.call(items.querySelectorAll('.name_handler'));

            elements.forEach(function (item) {

                var nameAttr = item.getAttribute('name');

                var split = nameAttr.split("]");

                // var split_2 = nameAttr.split("[")
                var newName = split[0].split("[")[0] + '[' + key + ']' + split[1] + ']';

                // change attribute name
                item.setAttribute('name', newName);
            });
        });
    });
</script>



