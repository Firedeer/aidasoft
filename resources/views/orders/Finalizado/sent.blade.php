@extends('layout_principal')

@section('container')

    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">


                <table id="table-orders-sent" class="table table-bordered table-condensed text-middle small">
                    <thead class="hidden-xs">
                    <tr>
                        <th>{!! trans('app.attributes.order_num') !!}</th>
                        <th>{!! trans('app.attributes.date') !!}</th>
                        <th>{!! trans('app.attributes.warehouse') !!}</th>
                        <th>{!! trans('app.attributes.required_at') !!}</th>
                        <th>{!! trans('app.attributes.status') !!}</th>
                        <th>{!! trans('app.attributes.user') !!}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class="text-uppercase">
                    @foreach($orders as  $order)

                      @include('orders.partials.order_row')
                    @endforeach
                    </tbody>
                </table>


            </div>
        </div>
    </div>
@endsection

@section('extra_scriptBody')
    @include('partials.dataTables_script')
    <script>
        $(document).ready(function () {
            $('#table-orders-sent').dataTableConfig({
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"orderable": false}
                ],
            });
        });
    </script>
@endsection

@section('header')

    <div class="col-md-12">
        @include('partials.dataTables_filter', ['table' => 'table-orders-sent'])
    </div>

    @include('orders.partials.navbar')
@endsection



@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('order.index') !!}">{!! trans('app.attributes.orders') !!}</a></li>
        <li class="active">{!! trans('app.attributes.sent') !!}</li>
    </ol>
@endsection

{{--<tr>--}}

    {{--<td>--}}
        {{--<a href="{!! route('order.show', $order->id) !!}">{!! $order->order_num !!}</a>--}}
    {{--</td>--}}

    {{--<td title="{!! $order->ordered_at !!}" data-order="{!! $order->sortOrderedAt() !!}">--}}
        {{--{!! $order->orderedAt() !!}--}}
    {{--</td>--}}

    {{--<td>{!! isset($order->warehouse_destination)? $order->warehouseDestination() : '' !!}</td>--}}

    {{--<td title="{!! $order->require_date !!}" data-order="{!! $order->sortRequiredAt() !!}">--}}
        {{--{!! $order->requiredAt() !!}--}}
    {{--</td>--}}

    {{--<td>--}}
        {{--@include('partials.status', ['status' => $order->status])--}}
    {{--</td>--}}

    {{--<td>{!! $order->forUser->fullName !!}</td>--}}


    {{--<td width="80">--}}
        {{--<div class="pull-right">--}}

            {{--@if(!$order->isCanceled() && !$order->isComplete())--}}
                {{--<a href="{!! route('order.cancel', $order->id) !!}" class="btn btn-xs btn-default"--}}
                   {{--title="Cancelar">--}}
                    {{--<!-- clear -->--}}
                    {{--<i class="material-icons md-18">&#xE14C;</i>--}}
                {{--</a>--}}
            {{--@endif--}}

        {{--</div>--}}
    {{--</td>--}}
{{--</tr>--}}
