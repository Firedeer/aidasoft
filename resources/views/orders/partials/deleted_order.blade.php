<div class="alert alert-default alert-dismissible text-muted" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>

    <i class="material-icons">&#xE88F;</i>
    El pedido <strong>{!! $order->order_num !!}</strong> ha sido eliminado eliminado!
</div>
