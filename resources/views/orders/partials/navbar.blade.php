<!-- Nav tabs -->
<!-- style="display:flex; justify-content:center; border-bottom:0" -->
<ul id="tab" class="nav nav-pills tabs-flex" role="tablist">

    <li role="presentation" class="{!! URL::current() == route('order.index') ? 'active' : ''  !!}">
        <a href="{!! route('order.Pendientes.index') !!}">Pendientes</a>
    </li>

    <li role="presentation" class="{!! URL::current() == route('order.inbox') ? 'active' : ''  !!}">
        <a href="{!! route('order.Transito.index') !!}">Por confirmar</a>
    </li>

    <li role="presentation" class="{!! URL::current() == route('order.sent') ? 'active' : ''  !!}">
        <a href="{!! route('order.Finalizado.index') !!}">Finalizados</a>
    </li>

    <li role="presentation" class="{!! URL::current() == route('order.drafts') ? 'active' : ''  !!}">
        <a href="{!! route('order.Cancelados.index') !!}">Cancelados</a>
    </li>
</ul>