<tr>
    <td>
        <a href="{!! route('order.show', $order->id) !!}">{!! $order->order_num !!}</a>
    </td>

    <td title="{!! $order->ordered_at !!}" data-order="{!! $order->sortOrderedAt() !!}">
        {!! $order->orderedAt() !!}
    </td>

    <td>{!! isset($order->warehouse_origen)? $order->warehouseOrigen() : '' !!}</td>

    <td title="{!! $order->require_date !!}" data-order="{!! $order->sortRequiredAt() !!}">
        {!! $order->requiredAt() !!}
    </td>

    <td>
        @include('partials.status', ['status' => $order->status])
    </td>

    <td>{!! $order->fromUser->fullName !!}</td>


    <td width="80">
        <div class="pull-right">
            @if($order->status == 'draft')

                <a href="{!! route('order.show', $order->id) !!}" class="btn btn-xs btn-default" title="Editar">
                    <i class="material-icons md-18">&#xE254;</i>
                </a>


                <a href="" class="btn btn-xs btn-default" title="Eliminar">
                    <i class="material-icons md-18">&#xE872;</i>
                </a>
            @endif

        </div>
    </td>
</tr>
