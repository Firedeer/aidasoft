<table id="table-orders" class="table table-bordered table-striped small">
    <thead>
    <tr>
        <th>{!! trans('app.attributes.description') !!}</th>
        <th class="hidden-xs">{!! trans('app.attributes.internal_reference') !!}</th>
        <th class="hidden-xs">{!! trans('app.attributes.measurement_unit') !!}</th>
        <th>{!! trans('app.attributes.quantity_requested') !!}</th>
        <th>{!! trans('app.attributes.pending') !!}</th>
        {{--<th>{!! trans('app.attributes.available') !!}</th>--}}
        <th>{!! trans('app.attributes.status') !!}</th>
    </tr>
    </thead>

    <tbody class="text-uppercase">
    @foreach($order->details as $key => $item)
        <tr>
            <td>
                {!! isset($item->article_id) ? $item->article->description : '' !!}

                @if(isset($order->status_id) && $order->status_id != 1)
                    {!! Form::hidden('items['.$key.'][article_id]', isset($item->article_id) ?  $item->article_id : 0) !!}
                @endif
            </td>

            <td class="hidden-xs">
                {!! isset($item->article) ? $item->article->internal_reference : '' !!}
            </td>

            <td class="hidden-xs">
                {!!  isset($item->article)? $item->article->measurementUnit : '' !!}
            </td>

            <td>
                {!! isset($item->quantity) ? $item->quantity : '' !!}
            </td>

            <td>
                {!! isset($item->pending) ? $item->pending : '' !!}
            </td>

            {{--<td>--}}
                {{--@if(isset($order->status_id) && $order->status_id != 1)--}}

                    {{--@if(isset($order->for_user_id) && ($order->for_user_id == auth()->user()->id  || auth()->user()->authRole('admin') ) )--}}

                        {{--<span class="text-capitalize">--}}
                            {{--{!! isset($item->article_id) ? $item->article->availableToTransfer($item->quantity, $order->warehouse_destination) : 0 !!}--}}
                            {{--{!! ' / ' !!}--}}
                            {{--{!! isset($item->article_id) ?  $item->article->availableByWarehouse($order->warehouse_destination) : 0  !!}--}}
                            {{--{!! ' (en stock)' !!}--}}
                        {{--</span>--}}

                        {{--@if($order->for_user_id === auth()->user()->id)--}}

                            {{--{!! Form::hidden('items['.$key.'][quantity]', isset($item->article_id) ? $item->article->availableToTransfer($item->quantity, $order->warehouse_destination) : 0, ['class' => 'form-control']) !!}--}}

                        {{--@endif--}}
                    {{--@endif--}}
                {{--@endif--}}
            {{--</td>--}}

            <td>

                <div class="progress" style="margin-bottom: 0px">
                    <div class="progress-bar"
                         role="progressbar"
                         aria-valuenow="{!! $item->status !!}"
                         aria-valuemin="0"
                         aria-valuemax="100"
                         style="width: {!! $item->status !!}%;"
                         data-toggle="tooltip"
                         data-placement="bottom" title="{!! $item->status !!}%">
                        %
                    </div>
                </div>
                <span class="text-muted">{!! $item->status !!}%</span>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>