@if($order->status_id=="2")
<tr>

    <td data-sort="{!! $order->sortOrderedAt() !!}">
        <a href="{!! route('order.show', $order->id) !!}">{!! $order->order_num !!}    </a>
    </td>

    <td title="{!! $order->ordered_at !!}">
        {!! $order->orderedAt() !!}
    </td>

    <td>
        @if($order->warehousesId($order->warehouse_origen) == $order->warehousesId($order->warehouse_destination))
            {!! $order->warehouseDestination() !!}
            <i class="material-icons md-18">&#xE0B2;</i>
            <i class="material-icons md-18">&#xE0B5;</i>
        @elseif($order->warehousesId($order->warehouse_destination))
            {!! $order->warehouseOrigen() !!}
            <i class="material-icons md-18">&#xE0B5;</i>

        @elseif($order->warehousesId($order->warehouse_origen))

            {!! $order->warehouseDestination() !!}
            <i class="material-icons md-18">&#xE0B2;</i>
        @endif
    </td>


    <td title="{!! $order->require_date !!}" data-sort="{!! $order->sortRequiredAt() !!}">
        {!! $order->requiredAt() !!}
    </td>

    <td>
        @include('partials.status', ['status' => $order->status ])
    </td>

    <td>
        {{--        @if($order->warehousesId($order->warehouse_origen) == $order->warehousesId($order->warehouse_destination))--}}

        {{--PARA: {!! $order->forUser->fullName !!}--}}
        {{--@elseif($order->warehousesId($order->warehouse_destination))--}}

        {!! $order->fromUser->fullName !!}
        {{--@elseif($order->warehousesId($order->warehouse_origen))--}}
        {{--PARA: {!! $order->forUser->fullName !!}--}}
        {{--@endif--}}
    </td>

    <td width="80">

        <div class="pull-right">
            @if(!$order->isCanceled() && !$order->isComplete())
                <a href="{!! route('order.cancel', $order->id) !!}" class="btn btn-xs btn-default"
                   title="Cancelar">
                    <!-- clear -->
                    <i class="material-icons md-18">&#xE14C;</i>
                </a>
            @endif
        </div>

    </td>
  
</tr>
  @endif