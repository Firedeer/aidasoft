@extends('layout_principal')

@section('container')
    <section class="col-md-12">
        <div class="panel">
            <div class="panel-body">

                <table id="table-orders-draft" class="table table-bordered table-condensed text-middle small">
                    <thead>
                    <tr>
                        <th>{!! trans('app.attributes.order_num') !!}</th>
                        <th>{!! trans('app.attributes.date') !!}</th>
                        <th>{!! trans('app.attributes.warehouse') !!}</th>
                        <th>{!! trans('app.attributes.required_at') !!}</th>
                        <th>{!! trans('app.attributes.status') !!}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class="text-uppercase">
                    @foreach($orders as  $order)
                        <tr>
                            <td data-sort="{!! $order->sortCreatedAt() !!}">
                                <a href="{!! route('order.show', $order->id) !!}">{!! $order->order_num !!}</a>
                            </td>

                            <td title="{!! $order->created_at !!}">
                                {!! $order->createdAt() !!}
                            </td>

                            <td>{!! isset($order->warehouse_destination)? $order->warehouseDestination() : '' !!}</td>


                            <td title="{!! $order->require_date !!}">
                                {!! $order->requiredAt() !!}
                            </td>

                            <td>
                                @include('partials.status', ['status' => $order->status ])
                            </td>

                            <td width="80">

                                <div class="pull-right">
                                    @if($order->isDraft())

                                        <a href="{!! route('order.show', $order->id) !!}" class="btn btn-xs btn-default"
                                           title="Editar">
                                            <i class="material-icons md-18">&#xE254;</i>
                                        </a>
                                    @endif

                                    <a href="" class="btn btn-xs btn-default" title="Eliminar">
                                        <i class="material-icons md-18">&#xE872;</i>
                                    </a>
                                </div>

                            </td>
                        </tr>


                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </section>
@endsection

@section('extra_scriptBody')
    @include('partials.dataTables_script')
    <script>
        $(document).ready(function () {
            $('#table-orders-draft').dataTableConfig({
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"orderable": false}
                ]
            });
        });
    </script>
@endsection

@section('header')

    <div class="col-md-12">
        @include('partials.dataTables_filter', ['table' => 'table-orders-draft'])
    </div>

    @include('orders.partials.navbar')

@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('order.index') !!}">{!! trans('app.attributes.orders') !!}</a></li>
        <li class="active">{!! trans('app.attributes.drafts') !!}</li>
    </ol>
@endsection