@extends('layout_principal')


@section('extra_scriptHead')

    <!-- Bootstrap Bootstrap-select Pluging CSS -->
    <link rel="stylesheet" href="{{asset('/bootstrap/bootstrap-select/css/bootstrap-select.css')}}">
    <!-- Bootstrap Bootstrap-select Pluging JS -->
    <script src="{{url('/bootstrap/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <link rel="stylesheet" href="{{asset('jquery.flexdatalist/jquery.flexdatalist.css')}}">

@endsection

@section('page_title')
    Nueva empresa
@endsection

@section('notify_icon_bar')
    <a href="javascript:void(0)">
        <i class="icon material-icons">show_chart</i>
    </a>
@endsection

@section('container')

    <div class="col-md-10 col-md-offset-1">
        <div class="row-sm">
            <div class="panel">

                <div class="panel-heading">
                    <h3>Nuevo empresa</h3>
                </div>

                {!! Form::open( ['route' => ['admin.provider.store'], 'method' => 'POST']) !!}

                <div class="col-md-12">

                    <a href="{!! route('admin.provider.index') !!}" class="btn pull-right">
                        <i class="material-icons md-18">list</i> Ir a la lista
                    </a>

                    <button type="reset" class="btn btn-default btn-sm mdb">Cancelar
                    </button>

                    <button type="submit" class="btn btn-primary mdb">
                        <i class="material-icons md-18">save</i> Guardar
                    </button>

                </div>

                <div class="panel-body">
                    @include('providers.forms.general')
                </div>
                <!-- .panel-body -->

                {!! Form::close() !!}
            </div>
            <!-- .panel -->
        </div>
        <!-- .row-sm -->
    </div>
    <!-- .col offset-->

@endsection

@section('extra_scriptBody')

    <script src="{{ asset('jquery.flexdatalist/jquery.flexdatalist.js') }}"></script>
@endsection