@extends('layout_principal')


@section('extra_scriptHead')

    {{--<link rel="stylesheet" href="{{asset('jquery.flexdatalist/jquery.flexdatalist.css')}}">--}}
@endsection


@section('container')
    <div class="col-md-10 col-md-offset-1">
        <div class="row-sm">
            <div class="panel">

                <div class="panel-heading">
                    <h3>Editar Proveedor</h3>
                </div>

                {!! Form::model($provider,['route' => ['admin.provider.update', $provider->id], 'method' => 'PUT', 'role' => 'update']) !!}
                <div class="col-md-12">

                    <a href="{!! route('admin.provider.index') !!}" class="btn pull-right">
                        <i class="material-icons md-18">list</i> Ir a la lista
                    </a>

                    <a href="{!! URL::previous() !!}" class="btn btn-default btn-sm mdb"> Atras
                    </a>

                    <button type="submit" class="btn btn-primary mdb">
                        <i class="material-icons md-18">save</i> Guardar
                    </button>

                </div>


                <div class="panel-body">

                    @include('providers.forms.general')
                </div>
                <!-- .panel-body -->

                {!! Form::close() !!}

            </div>
            <!-- .panel -->
        </div>
        <!-- .row-sm -->
    </div>
    <!-- .col offset-->

@endsection

@section('extra_scriptBody')
{{--    <script src="{{ asset('jquery.flexdatalist/jquery.flexdatalist.js') }}"></script>--}}
@endsection