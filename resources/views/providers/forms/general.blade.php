





<div class="col-md-6">

    <div class="form-group mdb @if($errors->has('firstname')) has-error @endif">
        {!! Form::label('firstname', 'Nombre', ['class' => 'control-label col-md-2']) !!}

        <div class="col-md-10">
            {!! Form::text('firstname', old('firstname'), ['class' => 'form-control', 'placeholder' => 'Nombre', 'autofocus']) !!}

            {!! $errors->first('firstname', '<p class="text-danger">:message</p>')  !!}
        </div>
    </div>


    <div class="form-group mdb @if($errors->has('lastname')) has-error @endif">
        {!! Form::label('lastname', 'Apellido', ['class' => 'control-label col-md-2']) !!}

        <div class="col-md-10">
            {!! Form::text('lastname', old('lastname'), ['class' => 'form-control', 'placeholder' => 'Apellido']) !!}

            {!! $errors->first('lastname', '<p class="text-danger">:message</p>')  !!}
        </div>
    </div>

    <div class="form-group mdb @if($errors->has('company')) has-error @endif">
        {!! Form::label('company', 'Compañia', ['class' => 'control-label col-md-2']) !!}

        <div class="col-md-10">
            {!! Form::text('company', old('company'), ['class' => 'form-control', 'placeholder' => 'Compañia, empresa o negocio']) !!}

            {!! $errors->first('company', '<p class="text-danger">:message</p>')  !!}
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group mdb @if($errors->has('type')) has-error @endif">
        {!! Form::label('type', 'Tipo de proveedor', ['class' => 'control-label col-md-2']) !!}

        <div class="col-md-10">
            {!! Form::select('type', config('enums.provider_types'), null, ['class' => 'form-control', 'placeholders' => 'Selecciona un tipo de proveedor']) !!}

            {!! $errors->first('type', '<p class="text-danger">:message</p>')  !!}
        </div>
    </div>

    <div class="form-group mdb @if($errors->has('address')) has-error @endif">
        {!! Form::label('address', 'Dirección', ['class' => 'control-label col-md-2']) !!}

        <div class="col-md-10">
            {!! Form::text('address', old('address'), ['class' => 'form-control']) !!}
            {{--        {!! Form::datalist('address', config('enums.provider_types'), old('address'), ['class' => 'form-control flexdatalist', 'placeholder' => 'Dirección', 'data-min-length' => '1']) !!}--}}

            {!! $errors->first('address', '<p class="text-danger">:message</p>')  !!}
        </div>
    </div>

    <div class="form-group mdb @if($errors->has('phone')) has-error @endif">
        {!! Form::label('phone', 'Teléfono', ['class' => 'control-label col-md-2']) !!}

        <div class="col-md-10">
            {!! Form::text('phone', old('phone'), ['class' => 'form-control', 'placeholder' => 'Teléfono']) !!}

            {!! $errors->first('phone', '<p class="text-danger">:message</p>')  !!}
        </div>
    </div>

    <div class="form-group mdb @if($errors->has('email')) has-error @endif">
        {!! Form::label('email', 'Email', ['class' => 'control-label col-md-2']) !!}

        <div class="col-md-10">
            {!! Form::email('email', old('phone'), ['class' => 'form-control', 'placeholder' => 'Correo electrónico']) !!}

            {!! $errors->first('email', '<p class="text-danger">:message</p>')  !!}
        </div>
    </div>
</div>

