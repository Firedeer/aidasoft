@extends('layout_principal')

@section('extra_scriptHead')
    @include('partials.dataTables_script')
@endsection

@section('page_title')
    <span class="hidden-sm hidden-md hidden-lg">Proveedores</span>
@endsection



@section('container')

    <div class="col-md-12">

        <div class="panel">

            <div class="panel-body">

                <table class="table table-bordered table-condensed text-middle small" id="providers-list">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Contacto</th>
                        <th>Tipo</th>
                        <th>Cant. Compras</th>
                        <th>Ult. compra</th>
                        <th width="80"></th>
                    </tr>
                    </thead>
                    <tbody class="text-uppercase">
                    @include('providers.partials.providers')
                    </tbody>
                </table>

            </div>
            <!-- .panel-body -->
        </div>
        <!-- .panel -->

    </div>
    <!-- .col -->

@endsection

@section('extra_scriptBody')

    {!! Form::open(['route' => ['admin.provider.destroy', ':PROVIDER_ID'], 'method' => 'DELETE', 'id' => 'form-delete']) !!}
    {!! Form::close() !!}

    <!-- Modal -->
    <div class="modal fade" id="deleteProviderModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="myModalLabel">Eliminar Proveedor</h4>
                </div>
                <div class="modal-body text-center">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary mdb" role="delete" data-id=""
                            onclick="deleteProvider(this)">
                        Eliminar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function deleteProvider(button) {

            var modal = button.closest('.modal');
            var id = button.getAttribute('data-id');
            var row = $('#providers-list').find('tr[data-provider-id="' + id + '"]')[0];

            var form = $('#form-delete');
            var route = form.attr('action').replace(':PROVIDER_ID', id);

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: route,
                type: 'DELETE',
                dataType: 'json',
                beforeSend: function () {
                    var alerts = Array.prototype.slice.call(document.querySelectorAll('.alert.flash'));

                    alerts.forEach(function (item) {
                        item.remove();
                    });

                    $(modal).modal('hide');
                },
                error: function (xhr, status) {
                    console.log(xhr)
                },
                success: function (response) {

                    $(row).fadeOut();

                    var body = document.getElementsByClassName('container-wrapper')[0];
                    body.insertAdjacentHTML('afterbegin', response.alert);
                }
            });
        }


        $('#deleteProviderModal').on('show.bs.modal', function (event) {

            var provider = $(event.relatedTarget);
            var name = provider.data('name');
            var id = provider.data('id');

            var modal = $(this);

            modal.find('.modal-title').html('¿Estas seguro de eliminar a <strong>' + name + '</strong>?');
            var button = modal.find('.modal-footer button[role="delete"]');

            button.attr('data-id', id);
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#providers-list').dataTableConfig({
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"orderable": false}
                ]
            });
        });
    </script>
@stop

@section('header')


    <div class="col-md-12">
        <div class="row">
            <div class="navbar">
                <div class="container-fluid">

                    <div class="navbar-btn">

                        <div class="row-sm">

                            <a class="btn" href="{!! route('admin.provider.create') !!}">
                                <!-- add -->
                                <i class="material-icons">&#xE145;</i>Nueva Empresa
                            </a>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! url('/') !!}">Inicio</a></li>
        <li class="active">Clientes y Proveedores</li>
    </ol>
@endsection
