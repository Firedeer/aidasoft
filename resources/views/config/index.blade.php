@extends('layout_principal')

@section('extra_scriptHead')
{{-- <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap-vertical-tabs/v1.2.1/bootstrap-vertical-tabs.css') }}"> --}}
@endsection

@section('container')

<div class="col-md-12">
	<div class="row">
		<div class="panel panel-default">

			<!-- Nav tabs -->
			<ul class="nav nav-pills " role="tablist">
				<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Administracion de usuarios</a></li>
				<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
				<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
				<li role="presentation" class="pull-right"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
			</ul>

			<div class="panel-body">
				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="home">
						<div class="row">
							<div class="col-md-6">
								<table class="table vertical-text-center">
									<thead>			
										<th colspan="2" class="text-right">
											<!-- Standard button -->
											<button type="button" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus"></span> Nuevo Usuario</button>
										</th>
									</thead>
									<tbody>
										@foreach($users as $users)
										<tr>
											<td colspan="2">
												<div class="row-sm">
													<div class="list-group">
														<div class="list-group-item">

															<div class="row-picture">
																<img class="circle" src="http://lorempixel.com/56/56/people/" alt="icon">
															</div>

															<div class="row-content" style="width: auto !important">
																<a href="user/{{ $users->id }}">
																	<h4 class="list-group-item-heading">{{ $users->nombre }}</h4>	
																</a>	      	
																<p class="list-group-item-text"> {{ $users->EMAIL }} </p>
															</div>

															<div class="pull-right" style="margin-top: 20px">
																<ul class="btn-option-table">
																	<li class="show">
																		<a  role="button" data-toggle="modal" data-target="#myModal" ><span class="glyphicon glyphicon-certificate"></span></a>
																	</li>
																	<li class="edit">
																		<a href=""  role="button" data-toggle="tooltip" data-placement="top" title="Editar" data-original-title="Editar"><span class="glyphicon glyphicon-edit"></span></a>
																	</li>
																	<li class="delete">
																		<a href=""  role="button" data-toggle="tooltip" data-placement="top" title="Eliminar" data-original-title="Eliminar"><span class="glyphicon glyphicon-trash"></span></a>
																	</li>
																</ul>
															</div>

														</div>
														{{-- <div class="list-group-separator"></div>  --}}
													</div>
													<!-- .list-group -->
												</div> 	
												<!-- .row-sm -->
											</td>
										</tr>									
										@endforeach
									</tbody>
								</table>	
							</div>
							<!-- .col -->				
						</div>
						<!-- .row -->
					</div>
					<!-- .tabpanel -->
					<div role="tabpanel" class="tab-pane fade" id="profile">B...</div>
					<!-- .tabpanel -->
					<div role="tabpanel" class="tab-pane fade" id="messages">C...</div>
					<!-- .tabpanel -->
					<div role="tabpanel" class="tab-pane fade" id="settings">D...</div>
					<!-- .tabpanel -->
				</div>
				<!-- .tab-content -->

			</div>
			<!-- .panel-body -->
		</div>
		<!-- .panel -->
	</div>
	<!-- .row -->
</div>
<!-- .col -->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog  modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Nuevo Usuario</h4>
			</div>
			{!! Form::open(array('id' => 'form-user','class' => 'form-horizontal')) !!}		
			<div class="modal-body">

				{{-- FORM include CSRF TOKEN--}}
				@include('admin.users.form.create')


			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button id="registrar" type="button" class="btn btn-raised btn-primary">Registrar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection

@section('extra_scriptBody')
<script>
	$('#myTabs a').click(function (e) {
		e.preventDefault()
		$(this).tab('show')
	})

	$('#myModal').on('shown.bs.modal', function () {
		$('input[name="name"]').focus()
	})

	$('#registrar').click(function(){

		var dataUser = new FormData(document.getElementById("form-user"));
		var token = $('input[name=_token]').val();
		var route = '/user';

		$.ajax({
			url: route,
			headers: {
				'X-CSRF-TOKEN': token
			},
			type: 'POST',
			dataType: 'json',
			data: dataUser,
			cache: false,
			contentType: false,
			processData: false,
			success: function (res) {
        	// console.log(res)
        }, error: function(res){
        	console.log(res)

        	$.each(res.responseJSON, function (item, message) {
        		document.getElementById(item + 'HelpBlock').innerHTML = message;
        		var padre = $('#' + item + 'HelpBlock').parent();
        		padre.parent().addClass('has-error');
        	});
        }	
    })
	})

</script>
@endsection