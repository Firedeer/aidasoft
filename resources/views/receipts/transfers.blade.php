@extends('layout_principal')

@section('container')
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">


                <table class="table table-bordered table-condensed small">
                    @foreach($transfers as $key => $transfer)
                    <tr>
                        <td>Fecha de Transferencia</td>
                        <td><a href="{!! route('transfer.show', $transfer->id) !!}">{!! $transfer->transfer_num !!}</a></td>
                        <td>{!! $transfer->subject !!}</td>
                        <td>{!! $transfer->warehouseOrigen !!}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection