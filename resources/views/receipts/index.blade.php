@extends('layout_principal')

@section('container')

    <div class="col-md-12">

        <ul class="list-group">
            @foreach($receipts as $receipt)
                <li class="list-group-item">
                    Codigo: {!! $receipt->receipt_number !!}<br>
                    Destino: {!! $receipt->sucursal->DESCR_SUCURSAL !!}<br>
                    Usuario:{!! $receipt->user->agent_id !!}<br>
                    Observaciones: {!! $receipt->observation !!}<br>
                    Articulos Recibidos:  {!! $receipt->receiptDetails->count() !!} cantidad: {!! $receipt->receiptDetails->sum('quantity') !!}<br>
                    Fecha: {!! $receipt->created_at !!}<br>

                    Proveedor: {!! $receipt->purchase->provider->firstname !!}
                    <br>

                    Usuario: {!! $receipt->purchase->user->firstname !!}
                </li>
            @endforeach
        </ul>

    </div>

@endsection
