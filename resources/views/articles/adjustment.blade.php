<div class="col-md-6">	
	<div class="row-sm">
		<div class="panel">	
			<div class="panel-body">

				<div class="page-header">
					<h3>Movimiento</h3>
				</div>

				<form action="/movement/article" method="POST" class="form-horizontal"> 
					<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">

						<label for="" class="col-md-2 control-label">Fecha</label>
						<div class="col-md-5">
							<input type="text" name="date" class="form-control datepicker">
						</div>
					</div>

					<div class="form-group">
						<label for="" class="col-md-2 control-label">Bodega</label>
						<div class="col-md-10">
							<select name="bodega" id="bodega_out" class="form-control"></select>
						</div>
					</div>

					<div class="form-group">

						<label for="" class="col-md-2 control-label">Movimiento</label>
						<div class="col-md-10">
							<select name="movement" id="movement" class="form-control"></select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Afecta</label>

						<div class="col-md-10">
							<div class="radio radio-primary ">
								<label>
									<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
									Ninguno
								</label>
							</div>
							<div class="radio radio-primary ">
								<label>
									<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
									Cliente
								</label>
							</div>
							<div class="radio radio-primary ">
								<label>
									<input type="radio" name="optionsRadios" id="transferCheck" value="option3">
									Transferir a bodega
								</label>
							</div>
						</div>
					</div>	

					<div id="transferOption" class="well hidden">
						<div class="form-group">
							<label for="user" class="col-md-2 control-label">Para</label>
							<div class="col-md-10">
								<select name="user" id="user" class="form-control" title="Usuarios"></select>
							</div>
						</div>
						<div class="form-group">
							<label for="bodega_in" class="col-md-2 control-label">Bodega</label>
							<div class="col-md-10">
								<select name="bodega_in" id="bodega_in" class="form-control"></select>
							</div>
						</div>
					</div>

					<div id="providerOption" class="well hidden">
						<div class="form-group">
							<label for="provider" class="col-md-2 control-label">Proveedor</label>
							<div class="col-md-10">
								<select name="provider" id="" class="form-control"></select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="" class="col-md-2 control-label">Articulo</label>
						<div class="col-md-10">
							<div class="row">
								<div class="col-xs-9 col-sm-10">
									<select name="article" id="article" data-live-search="true" class="form-control"></select>
								</div>
								<div class="col-xs-3 col-sm-2">
									<div class="row">
										<button type="button" class="btn btn-info btn-raised" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus"></span>
										</button>  
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">

						<label for="" class="col-md-2 control-label">Cantidad</label>
						<div class="col-md-3">

							<input type="text" name="quantity" class="form-control">
						</div>
					</div>

					<div class="form-group">

						<label for="" class="col-md-2 control-label">Costo unitario</label>
						<div class="col-md-3">

							<input type="text" name="unit_cost" class="form-control" placeholder="Costo por unidad">	
						</div>
					</div>

					<input class="btn btn-primary btn-raised pull-right" type="submit">
				</form>
			</div>
		</div>	
	</div>
	</div>