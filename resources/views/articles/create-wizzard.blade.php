@extends('layout_principal') 

@section('extra_scriptHead')
<!-- Bootstrap DatePiker Pluging CSS -->
<link rel="stylesheet" href="{{ asset('bootstrap/bootstrap-datetimepicker/css/datepicker.css')}}">
<!-- Bootstrap DatePiker Pluging JS -->
<script src="{{ asset('bootstrap/bootstrap-datetimepicker/js/bootstrap-datepicker.js')}}"></script>


<!-- Bootstrap Select Pluging -->
<link rel="stylesheet" href="{{ asset('bootstrap/bootstrap-select/css/bootstrap-select.css')}}">
<script src="{{ asset('bootstrap/bootstrap-select/js/bootstrap-select.js')}}"></script>

<!--  Pluging CSS -->
<link rel="stylesheet" href="{{ asset('snackbarjs/snackbar.css')}}">
<!-- JQuery Validate Pluging JS -->
<script type="text/javascript" src="{{ asset('jqueryValidation/1.15.1/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('jqueryValidation/1.15.1/jquery.validate.form.js') }}"></script>


<link rel="stylesheet" href="{{ asset('croppie/croppie.css')}}" />
<script src="{{ asset('croppie/croppie.js') }}"></script>

<style>
	.alert .icon span{
		/*border-radius: 50%;*/
		font-size: 46px;
		height: 56px;
		margin: auto;
		min-width: 56px;
		width: 56px;
		padding: 0;
		/*overflow: hidden;*/
		text-align: center;
		/* box-shadow: 0 1px 1.5px 0 rgba(0, 0, 0, 0.12), 0 1px 1px 0 rgba(0, 0, 0, 0.24); */
		position: relative;
		line-height: normal;
		display: inline-block;
	}

</style>
@endsection 

@section('container')        

<div class="col-sm-12 col-md-12 col-lg-12">
	<div class="row-xs">
		@if(Session::has('msj'))

		<!-- ALERT -->
		<div class="alert alert-success" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong><h3>Felicidades!</h3></strong> 
			
			<div class="icon">
				<span class="glyphicon glyphicon-thumbs-up"></span>{{ Session::get('msj') }}
			</div></br>
			Puedes agregar más detalles 
			<a href="/article/create/detail/{{ Session::get('article') }}/{{ Session::get('bodega') }}" class="btn btn-warning btn-link">aqui</a>
		</div>
		<!--.alert-->
		
		<a href="/article/create" class="btn btn-success btn-raised">Agregar nuevo</a>
		<a href="/article/{{ Session::get('id') }}" class="btn btn-warning btn-raised">ver</a> 
		<a href="/bodega/{{ Session::get('bodega') }}" class="btn btn-warning btn-raised">ir a la bodega</a> 
		@else

		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong><br><br>
			@foreach ($errors->all() as $error)
			{{ $error }} <br>
			@endforeach  
		</div>
		@endif

		<div class="panel panel-primary x-panel">

			<div class="panel-heading">
				<h3>Nuevo Articulo</h3>
			</div>
			<!--.panel-heading-->
			<div class="panel-body" style="min-height: 70vh">


<!-- 
       <div class="col-md-5">
        <div class="demo"></div>
        <div id="result-image" onChange="onFileSelected(event)"></div>
      </div>
      <input type="text" class="basic-width">
      <input type="text" class="basic-height">
      <button type="button" class="basic-result">result</button> -->



      <!-- FORM -->
      <form enctype="multipart/form-data" id="form-article" class="form-horizontal" action="/article" method="POST">
      	<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
      	<input id="image" type="file" name="image" class="hidden" value='{{ old('image') }}'/>

      	<!--Form Wizard-->
      	<div class="__wizard">
      		<ul class="progressbar hidden">
      			<li class="active"></li>
      			<li></li>
      			<li></li>
      			<li></li>
      			<li></li>
      		</ul>

      		<div class="wizard-container">

      			<!--STEP 1-->
      			<fieldset class="step">
      				<div class="col-md-4 col-lg-2">
      					<div class="well">
      						<div class="">

      							<div class="__image_container-preview center-block">
      								<div id="btn-cargar" class="__img-preview">
      									<div id="spin" class="load-rotate" hidden><span class="glyphicon glyphicon-refresh"></span></div>
      									<span class="glyphicon glyphicon-picture"></span>
      									{{-- <i class="fa fa-image" aria-hidden="true"></i> --}}
      								</div>
      								<a id="btn-quitar" class="img-remove"></a>
      							</div>
      						</div>
      					</div>  
      					<!-- <a id="btn-quitar" class="btn btn-danger btn-load" style="display: none;">Quitar imagen</a>-->                    
      				</div>
      			</fieldset>
      			<!-- .step -->   

      			<!--STEP 2-->
      			<fieldset class="step">
      				<div class="row-xs">
      					<legend>Generales</legend>
      				</div>
      				<div class="col-md-6">  
      					<div class="row-xs">
      						@include('admin.forms.general')
      					</div>
      				</div>
      				<!-- .col -->
      			</fieldset>
      			<!-- .step -->

      			<!-- STEP 3 -->
      			<fieldset class="step">
      				<div class="row-xs">
      					<legend>Almacenamiento</legend>
      				</div>
      				<div class="col-md-6">    
      					<div class="row">
      						<div class="col-sm-12">
      							<div class="row-xs">
      								@include('form-help.detail_article_bodega')
      							</div>
      						</div>
      						<!-- .col -->
      					</div>
      					<!-- .row -->
      				</div>
      				<!-- .col -->
      			</fieldset>
      			<!-- .step --> 

      			<!-- STEP 4 -->
      			<fieldset class="step">
      				<div class="row-xs">
      					<legend>Unidades de medidas</legend>
      				</div>
      				<div class="col-md-6">
      					<div class="row-xs">
      						<div class="col-md-12">
      							{{-- <div class="row"> --}}
      							@include('form-help.measurement_units') 
      							{{-- </div> --}}
      						</div>
      					</div>
      				</div>
      			</fieldset>
      			<!-- .step -->

      			<!-- STEP 5 -->
      			<fieldset class="step hidden">
      				<div class="row-xs">
      					<legend>Devoluciones al proveedor</legend>
      				</div>
      				{{-- <div class="col-md-6">  --}}
      				{{-- <div class="row-xs">       --}}
      				@include('form-help.devolutions')        
      				{{-- </div>         --}}
      				{{-- </div>      --}}
      				<!-- .col -->
      			</fieldset>
      			<!-- .step --> 

      		</div>
      		<!--.wizard-container-->
      	</div>
      	<!-- form .wizard-->       
      	<button id="btnSubmit" type="submit" hidden="">Enviar</button>  
      </form>
      <!--form-->   
  </div>
  <!--.panel-body-->

  <div class="panel-footer">
  	<div class="row">
  		<div class="col-md-12">
          {{-- <button id="btn-previous" class="btn btn-default btn-lg pull-left hidden"><span class="glyphicon glyphicon-menu-left"></span> <span class="hidden-xs hidden-sm"> Anterior</span></button>
          <button id="btn-next" class="btn btn-raised btn-primary btn-lg" onClick="$('#btnSubmit').click()">Siguiente <span class="glyphicon glyphicon-menu-right"></span></button>  --}}
          <button type="botton" class="btn btn-primary btn-raised btn-lg" onClick="document.getElementById(btnSubmit)">Enviar</button>
      </div>
  </div>
  <!--.row -->
</div>
<!--.panel-footer-->
</div>
<!--.panel-->
@endif

</div>
<!--.row-->
</div>
<!--.col-->





@include('alerts.stackebar')

@endsection 

@section('extra_scriptBody')

<!--  -->
<script src="{{ asset('assets/js/rgtr_article.js')}}"></script>
<script src="{{ asset('assets/js/load_select_rgtr_article.js')}}"></script>
<script src="{{ asset('assets/js/demo_imagen.js')}}"></script>
<script src="{{ asset('assets/js/demo_form_wizard.js')}}"></script>

<!--  Snackbar Pluging JS -->
<script src="{{ asset('snackbarjs/snackbar.min.js')}}"></script>
<!--  -->
<script type="text/javascript">
	$(document).ready(function(){ 
    // class();
    // brands();
    bodegas('bodega');    

    $('.datepicker').datepicker({
    	format: 'yyyy/mm/dd'
    });
});
</script>

<script>
	/*RADIO PAKAGE*/    
	$("input:radio[name=pakageOption]").click(function () {  
		if($('input:radio[name=pakageOption]:checked').val() == 2 ){
			$('#pakageDetaills').removeClass('hidden');
			$('label[for="quantity"').addClass('sr-only');
			$('label[for="box"]').removeClass('sr-only')
		} else {
			$('#pakageDetaills').addClass('hidden');
			$('label[for="quantity"').removeClass('sr-only');
			$('label[for="box"]').addClass('sr-only')
		}
	});
</script>

@endsection