<table class="table table-striped table-condensed table-hover text-middle">
    <thead>
    <tr>
        <th>Articulo</th>
        <th class="hidden-xs">Codigo</th>
        <th class="hidden-xs">Marca</th>
        <th class="hidden-xs">Categoria</th>
        <th width="45px">Stock</th>
        <th class="hidden-xs">Pendiente</th>
        <th class="hidden-xs" width="50"></th>
    </tr>
    </thead>
    <tbody class="text-uppercase ">
    @foreach($articles as $article)
        <tr>
            <td>
                <div class="media">
                    <div class="media-left">
                        <a href="{!! url('article/'.$article->id) !!}">


                        @if( $article->image == null )
                            <img src="{{ asset('assets/img/trade.jpg') }}" width="45px"
                                 class="media-object img-rounded">
                        @else
                            <img src="{{ asset('assets/img/'.$article->image ) }}" width="45px"
                                 class="media-object img-rounded">
                        @endif
                        </a>
                    </div>
                    <div class="media-body">
                        <a href="{!! url('article/'.$article->id) !!}">
                            <strong>{!! $article->description !!}</strong>
                        </a>
                        <br>
                        <small>{!! $article->name !!}</small>
                    </div>
                </div>
            </td>

            <td class="hidden-xs">
                <span class="serial-code">{!! $article->serial_code !!}</span>
            </td>

            <td class="hidden-xs">{!! $article->brand !!}</td>

            <td class="hidden-xs">
                {!! $article->category !!}
            </td>

            <td>
                @if($article->stock == 0)
                    <label class="text-danger" style="width: 80px;">
                        <span class="glyphicon glyphicon-arrow-down"></span> {!! $article->stock !!}
                    </label>
                @elseif($article->stock <= $article->min_stock)
                    <label class="text-warning" style="width: 80px;">
                        <span class="glyphicon glyphicon-arrow-down"></span> {!! $article->stock !!}
                    </label>
                @elseif($article->stock >= $article->max_stock )
                    <label class="text-success" style="width: 80px;">
                        <span class="glyphicon glyphicon-arrow-up"></span> {!! $article->stock !!}
                    </label>
                @else
                    <label class="text-danger" style="width: 80px;">{!! $article->stock !!}
                    </label>
                @endif
            </td>

            <td class="hidden-xs">
                {!! $article->pendeing !!}
            </td>

            <td class="hidden-xs">

                <div class="btn-group">
                    <a href="{!! url('article/'.$article->id.'/edit') !!}"
                       class="btn btn-xs btn-default mdb">
                        <i class="material-icons">mode_edit</i>
                    </a>
                </div>

            </td>

        </tr>
    @endforeach


    </tbody>
</table>



