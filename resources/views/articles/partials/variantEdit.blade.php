{!! Form::model($article, ['route' => ['article.update', $article->id], 'method' => 'PUT',  'role' => 'update' ] ) !!}

{!! Form::hidden('article_master_id', old('article_master_id')) !!}


<img @if(isset($article->image))
     src="{!! asset('assets/img/'.$article->image) !!}"
     @else
     src="{!! asset('assets/img/trade.jpg') !!}"
     @endif
     class="media-object img-raunded" width="150px"
>

<div class="form-group mdb">

    <div class="togglebutton">
        <label>Activo
            <input type="checkbox"
                   name="active"
                   @if( $article->active == 1 || old('active') == 1) checked="checked" @endif
            >
        </label>
    </div>
</div>


<div class="form-group mdb @if($errors->has('description')) has-error @endif">

    {!! Form::label('description', 'Descripción', ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null, [
    'class' => 'form-control',
    'placeholder' => 'Descripción del articulo',
    'rows' => '3',
    'maxlength' => '255'
    ]) !!}

    @if($errors->has('description'))
        <p class="text-danger">{!! $errors->first('description') !!}</p>
    @endif

</div>
<!--.form-group -->


{{--<div class="form-group mdb">--}}
{{--{!! Form::label('article_master_id', 'Principal', ['class' => 'control-label']) !!}--}}
{{--{!! Form::select('article_master_id', \TradeMarketing\ArticleMaster::listing(), old('article_master_id'), [--}}
{{--'class' => 'form-control'--}}
{{--]) !!}--}}

{{--</div>--}}


<div class="row-sm">
    <table class="table table-condensed table-striped">
        <thead>
        <tr>
            <th width="30">Características</th>
            <th></th>
            <th width="5" class="text-right">
                <button type="button" class="btn btn-link btn-xs mdb pull-right" role="addAttibute">
                    <span class="glyphicon glyphicon-plus"></span>
                    <span class="hidden-xs">Agregar</span>
                </button>
            </th>
        </tr>
        </thead>
        <tbody>
        @if(isset($attributeValues))
            @foreach( $attributeValues as $value )

                <tr>
                    <td>
                        {!! $value->attribute->description !!}
                        {!! Form::hidden('attribute[id][]', $value->attribute->id) !!}

                    </td>
                    <td>
                        {!! $value->description !!}
                        {!! Form::hidden('attribute[value_id][]', $value->id) !!}
                    </td>
                    <td></td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>


{!! Form::submit('Guardar Cambios', ['class' => 'btn btn-raised mdb pull-right']) !!}
{!! Form::close() !!}