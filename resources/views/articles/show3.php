@extends('layout_principal')

@section('extra_scriptHead')
@endsection

@section('container')

    <section class="col-md-3">

        <div class="media">
            <div class="media-left">
                <a href="#">
                    <img @if(isset($article->image))
                         src="{!! asset('assets/img/'.$article->image) !!}"
                         @else
                         src="{!! asset('assets/img/trade.jpg') !!}"
                         @endif class="media-object img-rounded" width="125px">
                </a>
            </div>
            <div class="media-body">
                <h3 class="media-heading">{!! $article->description !!}
                    <small>{!! $article->master->name !!}</small>
                </h3>

                <p>Código:
                    <span class="serial-code">{!! $article->internal_reference !!}</span>
                </p>

                <p>Categoría:
                    @if(isset($article->master->category->description))
                        <strong>{!! $article->master->category->description !!}</strong>
                    @endif
                </p>

                <P>Marca:
                    @if(isset($article->master->brand->description))
                        <strong>{!! $article->master->brand->description !!}</strong>
                    @endif
                </p>
            </div>

            <div class="media-right">
                <a href="{!! url('article/'.$article->id.'/edit') !!}" class="btn btn-fab btn-fab-sm mdb">
                    <i class="material-icons">mode_edit</i>
                </a>

                Stock
                <strong>{!! $article->view->stock !!}</strong>


                Pendiente
                <strong>{!! $article->view->pendeing !!}</strong>

            </div>
        </div>

    </section>
    <!-- .col -->




    <section class="col-md-9">

        <div class="panel">

            <div class="panel-body">

            </div>


            <!-- Nav tabs -->
            <ul class="nav nav-pills tabs-flex" role="tablist">
                <li role="presentation" class="active">
                    <a href="#tap1" aria-controls="tap1" role="tab" data-toggle="tab">Articulos</a>
                </li>
                <li role="presentation">
                    <a href="#tap2" aria-controls="tap2" role="tab" data-toggle="tab">Movimientos</a>
                </li>
            </ul>


            <div class="panel-body">

            </div>
        </div>

    </section>



    <div class="col-md-4">
        <div class="panel">
            <div class="panel-body">

                <div id="form-attributes">

                    Caracteristicas
                    <input type="hidden" name="attribute_master_id" value="{!! $article->article_master_id !!}">
                    @if(isset($attributeValues) && $attributeValues != '' )

                        @foreach( $attributeValues as $attrValue )

                            <div class="form-group">
                                <label class="control-label">{!! $attrValue->attribute->description !!}</label>
                                <div class="radio radio-primary">
                                    @foreach( \DB::table('tmk_articles')
                                            ->join('tmk_variants', 'tmk_variants.article_id', '=', 'tmk_articles.id')
                                            ->join('tmk_attribute_values', 'tmk_attribute_values.id', '=', 'tmk_variants.attribute_value_id')
                                            ->where('tmk_articles.article_master_id', $article->article_master_id)
                                            ->where('tmk_attribute_values.attribute_id', $attrValue->attribute->id)
                                            ->select('tmk_attribute_values.description', 'tmk_attribute_values.id')
                                            ->groupBy('tmk_attribute_values.description', 'tmk_attribute_values.id')
                                            ->get() as $key => $value )


                                        <label>
                                            <input type="radio"
                                                   class="hidden attrRadios"
                                                   name="attrRadios{!!  $attrValue->attribute->id  !!}"
                                                   value="{!! $value->id !!}"

                                                   @if( $attrValue->id ==  $value->id )

                                                   checked="checked"

                                                    @endif
                                            >{!! $value->description !!}
                                        </label>


                                    @endforeach
                                </div>
                            </div>
                        @endforeach

                    @endif
                </div>

            </div>
            <!--.panel-body-->
        </div>
    </div>


    <div class="col-md-4">

        <table class="table">
            <thead>
            <tr>
                <td>Bodega</td>
                <td width="40">Stock</td>
            </tr>
            </thead>
            <tbody>
            @foreach($article->articleByWarehouses as $warehouse)
                <tr>
                    <td>
                        {!! $warehouse->warehouse->description !!}
                    </td>
                    <td>{!! $warehouse->entry !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>


    <div class="col-md-12">
        <div class="panel">
            <table class="table">
                <thead>
                <tr class="text-uppercase">
                    <th colspan="2">Articulos Relacionados</th>
                    <th width="100">Stock</th>
                </tr>
                </thead>
                <tbody>
                @foreach($variants as $variant)
                    <tr>
                        <td width="30">
                            <a href="#">
                                <img @if(isset($article->image))
                                     src="{!! asset('assets/img/'.$article->image) !!}"
                                     @else
                                     src="{!! asset('assets/img/trade.jpg') !!}"
                                     @endif class="media-object img-rounded" width="30px">
                            </a>
                        </td>
                        <td><a href="{!! url('article/'.$variant->id) !!}">{!! $variant->description !!}</a></td>
                        <td>1000</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection


@section('extra_scriptBody')

    <script src="{{ asset('assets/js/demo_color_convert.js') }}"></script>
    {{--    <script src="{{ asset('assets/js/charts/article_bodegas.js') }}"></script>--}}
    <script src="{{ asset('assets/js/movements.js') }}"></script>


    <script>

        $(document).ready(function (e) {
            e.preventDefault;
            var article = window.location.pathname.split('article/')[1];
            var route = '/article/bodegas/' + article;
            var datos = $('#tb_article_bodegas');

            var active;
            var a = [];
            var _json = '';

            $.get(route, function (res) {

                $(res).each(function (key, value) {

                    // if (value.active == 1) {
                    //   active = '<span class="label label-info">Activo</span>';
                    // } else {
                    //   active = '<span class="label label-default">Inactivo</span>';
                    // }
                    key += 1;

                    datos.append("<tr><td>" + key + "</td>" +
                        // "<td>" + value.article +"</td>"+
                        "<td><a href='/bodega/" + value.bodega_id + "'>" + value.bodega + "</a>" + "</td>" +
                        "<td>" + value.initial + "</td>" +
                        "<td>" + value.min_stock + "</td>" +
                        "<td>" + value.stock + "</td>");
                });

                //
                $(res).each(function (key, value) {
                    _json += '{ "label": "' + value.bodega + '",' +
                        '"highlight": "rgba(' + ConvertToRGB(value.colour) + ',0.6)",' +
                        '"color": "' + value.colour + '",' +
                        '"value": "' + value.stock + '" }';

                    if (key < (res.length - 1)) {
                        _json += ',';
                    }

                });
                //
                a[0] = _json;
                chart_article_bodega(a);
            });

        });


    </script>


    <script>
        $(document).ready(function () {
            show_movements(window.location.pathname.split('article/')[1])
        });
    </script>



    <script>
        $(document).ready(function () {
            // Basic hovercard

            $(document).delegate('.basic-hovercard', 'mouseenter mouseleave', function (event) {

                $(this).popover({
                    html: true,
                    trigger: 'manual',
                    placement: function (context, source) {
                        var get_position = $(source).position();
                        if (get_position.left > 515) {
                            return "left";
                        } else if (get_position.left < 515) {
                            return "right";
                        } else if (get_position.top < 110) {
                            return "bottom";
                        } else {
                            return "top";
                        }
                    },
                    content: function () {
                        return $('.basic-content').html();
                    }
                }).on("click", function (e) {
                    e.preventDefault();
                }).on("mouseenter", function () {
                    var _this = this;
                    $(this).popover("show");
                    $(this).siblings(".popover").on("mouseleave", function () {
                        $(_this).popover('hide');
                    });
                }).on("mouseleave", function () {
                    var _this = this;
                    setTimeout(function () {
                        if (!$(".popover:hover").length) {
                            $(_this).popover("hide")
                        }
                    }, 100);
                });
            })

        });
    </script>


    <script scr="{!! asset('assets/js/send_to_ajax.js')!!}"></script>

    <script>

        $('input.attrRadios').click(function () {

            var data = $('#form-attributes input').serializeArray();
            console.log(data);
            $.ajax({
                url: 'article/',
                headers: {'X-CSRF-TOKEN': '{!! csrf_token() !!}'},
                type: 'GET',
                data: {data: data},
                dataType: 'json',
                success: function (res) {

                    location.href = '/article/' + res.article_id;
                    // console.log(res);
                },
                error: function (err) {
                    console.log(err);
                }
            });

        });
    </script>

@endsection
