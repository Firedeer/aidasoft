@extends('layout_principal')

@section('extra_scriptHead')

    <link rel="stylesheet" href="{{ asset('sweetalert/sweetalert.css')}}"/>
    <script src="{{ asset('sweetalert/sweetalert-dev.js') }}"></script>


    <link rel="stylesheet" href="{{ asset('croppie/croppie.css')}}"/>
    <script src="{{ asset('croppie/croppie.js') }}"></script>
@endsection


@section('container')

    <div class="col-md-10 col-md-offset-1">
        <div class="row-sm">

            <div class="panel">

                <div class="panel-heading">
                    <h2>Editar variante</h2>
                </div>

                <div class="panel-body">

                   @include('articles.partials.variantEdit')

                </div>
            </div>
        </div>
    </div>
    <!-- .col -->
@endsection



@section('extra_scriptBody')
    <script>

        $(document).ready(function () {
            $('button[role="addAttibute"]').click(function (event) {
                event.preventDefault();

                $.get('/article/attribute/add', function (res) {

                    $('.table').append(res);
                });
            });

            $(document).delegate('button[role="buttonDelete"]', 'click', function (event) {
                event.preventDefault();

                var row = event.target.closest('tr');

                row.remove();
            });
        });

        $(document).delegate('select[name="attribute[id][]"]', 'change', function (event) {

            var row = event.target.closest('tr');
            var select = $(row).find('select[name="attribute[value_id][]"]')[0];

            $(select).empty();

            if (event.target.value) {
                $.get('/attribute/' + event.target.value + '/value', function (response, status) {

                    response.forEach(function (element, index) {

                        $(select).append('<option value="' + element.id + '">' + element.description + '</option>');
                    });
                });
            }
        });
    </script>

    <script src="{!! asset('assets/js/send_to_ajax.js') !!}"></script>

    <script>
        $(document).ready(function () {
            var form = $('form[role="update"]');

            $(form).sendToAjax({
                method: $(form).find('input[name="_method"]').val(),
                token: $(form).find('input[name="_token"]').val(),
                route: $(form)[0].action,
//                success: function(res){
//                    swal(res);
//                },
//                error: function(err){
//                    var erros = $(form).find('.text-danger');
//                    erros.remove();
//
//                    $.each(err.responseJSON, function (item, message) {
//
//                        var p = document.createElement('P');
//
//                        p.appendChild(document.createTextNode(message));
//                        p.setAttribute('class', 'text-danger');
//
//                        var elem = $(form).find('*[name="description"]')[0];
//                        var parent = elem.parentNode;
//
//                        parent.classList.add('has-error');
//
//                        $(parent).insertAfter(p, elem);
//
//                    });
//                }
            });
        });

    </script>
@endsection



