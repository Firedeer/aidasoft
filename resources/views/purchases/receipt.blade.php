@extends('layout_principal')


@section('container')

<style type="text/css">
    tr.active td:hover{ 
        box-shadow: none !important;
    }
</style>
    
    <div class="col-md-10 col-md-offset-1">
        <div class="row-sm">

            <div class="panel">

                <div class="panel-body">
                    <h1>
                        <small>Compra</small>
                        <br> {!! $purchase->order !!}
                    </h1>
                </div>

                {!! Form::open(['route' => ['purchase.confirm', $purchase->id ], 'method' => 'POST', 'id' => 'form-purchase']) !!}

                <input type="checkbox" name="confirm_reception" class="hidden" id="confirm_reception"
                       onclick="document.getElementById('form-purchase').submit();">


                <div class="panel-body">

                    <div class="form-horizontal form-condensed">


                        <div class="col-sm-6">

                            <div class="form-group mdb has-feedback @if($errors->has('require_date')) has-error @endif">
                                {!! Form::label('require_date', 'Fecha de Entrega', ['class' => 'control-label col-md-3']) !!}

                                <div class="col-md-9">
                                    <div class="form-control">{!! $purchase->require_date !!}</div>
                                </div>
                            </div>


                            <div class="form-group mdb @if($errors->has('warehouse_id')) has-error @endif">
                                {!! Form::label('warehouse_id', 'Bodega de  destino', ['class' => 'control-label col-md-3']) !!}

                                <div class="col-md-9">
                                    <div class="form-control">{!! $purchase->warehouse->description !!}</div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-sm-6">
                            <div class="form-group mdb @if($errors->has('provider_id')) has-error @endif">
                                {!! Form::label('provider_id', 'Proveedor', ['class' => 'control-label col-md-3']) !!}

                                <div class="col-md-9">
                                    <div class="form-control">{!! isset($proveedor->Nombre)? $proveedor->Nombre : '' !!}</div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- .form-horizontal -->

                </div>
                <!-- .panel-body -->

                <div class="panel-body">

                    <div class="row-sm">

                        <table id="table-purchase-details"
                               class="article table table-bordered table-control small">
                            <thead>
                            <tr>
                                <th>{!! trans('app.attributes.description') !!}</th>

                                <th>{!! trans('app.attributes.internal_reference') !!}</th>

                                <th>{!! trans('app.attributes.quantity') !!}</th>

                                <th>{!! trans('app.attributes.pending') !!}</th>
                                <th class="cell-unit-cost">{!! trans('app.attributes.unit_cost') !!}</th>

                                <th class="cell-quantity-receipt">{!! trans('app.attributes.received') !!}</th>
                            </tr>
                            </thead>

                            <tbody class="text-uppercase">


                            @foreach( $purchase->details as $key => $item )


                                @if( $item->pending > 0 )
                                <tr>
                                    <td class="cell-description active">
                                        {!! $item->article->description !!}
                                        {!! Form::hidden('items['.$key.'][article]', old('items.'.$key.'.article') | $item->article_id ) !!}
                                    </td>

                                    <td class="cell-internal-reference active">{!! $item->article->internal_reference !!}</td>

                                    <td class="cell-quantity active">{!! $item->quantity !!}</td>

                                    <td class="cell-pending active">
                                        {!! $item->pending !!}
                                        {!! Form::hidden('items['.$key.'][pending]', old('items.'.$key.'.pending') | $item->pending, ['class' => 'input-pending'] ) !!}
                                    </td>

                                    <td class="cell-unit-cost active">
                                        {!! $item->unit_cost !!}
                                    </td>

                                    
                                    <td class="cell-quantity-receipt">
 
                                        <span class="text-quantity-receipt">

                                            @if(old('items.'.$key.'.quantity'))
                                                {!! old('items.'.$key.'.quantity') !!}
                                            @else
                                                {!! $item->pending !!}
                                            @endif
                                        </span>

               <div class="form-group @if($errors->has('items.'.$key.'.quantity')) has-error @endif">
                {!! Form::text('items['.$key.'][quantity]',
                                old('items.'.$key.'.quantity') | $item->pending,
                                            ['class' => 'form-control input-quantity']) !!}

                {!! $errors->first('items.'.$key.'.quantity', '<p class="text-danger">:message</p>') !!}
                </div>
                                    </td>
                                   

                                </tr>

                                 @else 
                                    <tr class="active">
                                        <td>{!! $item->article->description !!}
                                        
                                        </td>

                                        <td>{!! $item->article->internal_reference !!}</td>

                                        <td>{!! $item->quantity !!}</td>

                                        <td>
                                            {!! $item->pending !!}
                                        </td>

                                        <td class="text-right">
                                            {!! $item->unit_cost !!}
                                        </td>

                                        <td>{!!  $item->pending  !!}
                                        </td>

                                    </tr>       
                         
                                @endif
                            @endforeach
                            </tbody>


                            <tfoot>
                            <tr class="total">
                                <td colspan="5" class="cell-total">TOTAL $</td>
                                <td colspan="2" class="cell-total">{!! $purchase->total !!}</td>
                            </tr>
                            </tfoot>


                        </table>

                    </div>
                    <!-- .row-sm -->
                </div>
                <!-- .panel-body -->

                <div class="panel-body">
                    @include('partials.observation_field', ['textarea' =>  $purchase->observation])
                </div>
                <!-- .panel-body -->

                {!! form::close() !!}

            </div>
            <!-- .panel -->
        </div>
    </div>
@endsection




@section('extra_scriptBody')

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                        Las cantidades recibidas no coinciden con las solicidadas en la compra.
                    </h4>
                </div>
                <div class="modal-body">


                    <h2>Ha indicado que su mercancia esta incompleta....</h2>

                    <p class="text-muted"> Puedes procesar tu compra parcialmente si recibiras los articulos restantes
                        en otro momento;
                        de lo contrario indica <strong>Confirmar completo</strong> para procesar la compra actual como
                        completo.
                    </p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary mdb" onclick="confirmReception(true)">Confirmar
                        completo
                    </button>
                    <button type="button" class="btn btn-primary mdb" onclick="confirmReception()">Confirmar Parcial
                    </button>
                    <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            $('#btn-confirm').click(function (event) {

                var tbody = $('#table-purchase-details tbody tr');
                var i = 0;

                tbody.each(function (key, item) {
                    var pending = parseInt($(item).find('.cell-pending .input-pending').val());
                    var quantity = parseInt($(item).find('.cell-quantity-receipt .input-quantity').val());

                    if (quantity < pending && quantity >= 0) {
                        i++;
                    }
                });

                if (i > 0) {

                    $('#myModal').modal('show');

                } else if (i == 0) {
                    confirmReception(true);
                }
            });
        });


        function confirmReception(confirm_all) {

            confirm_all = (confirm_all) ? confirm_all : false;

            var form = $('#form-purchase');

            $('#myModal').modal('hide');

            if (confirm_all) {

                $('#confirm_reception').click()
                form.submit();
            } else {

                form.submit();
            }
        }
    </script>



    <script>

        function inArray(value, array) {

            if ($.inArray(value, array) != -1) {
                return true;
            }

            return false;
        }

        var only = [5];

        $('#table-purchase-details').on('click', 'tbody td', function () {

            if (inArray(this.cellIndex, only)) {
                displayForm($(this));
            }
        });


        function displayForm(cell) {

            var column = cell.attr('class').split(" ")[0],
                id = cell.closest('tr').attr('id'),
                cellWidth = cell.css('width'),//obtiene el ancho de la celda para el estilo de ancho del campo de entrada
                cellHeight = cell.css('height'),//obtiene el alto de la celda para el estilo de ancho del campo de entrada
                cellText = $(cell.find(column.replace('cell', '.text'))),//obtiene el texto actual de la celda para el campo de entrada
                prevContent = cellText.text();//almacena el valor anteror


            //borra el texto actual de la celda, mantiene la celda con el ancho actual
            cellText.html('');
            cell.css('width', cellWidth);

            var controls = cell.find('.form-control');
            var control;
            var select2;

            if (controls.length > 1) {
                control = $(controls[0]);
                select2 = $(controls[1]);
                select2.css({'width': cellWidth, 'height': cellHeight, 'display': 'block'});
                control.select();

            } else {
                control = $(controls[0]);
                control.css({'width': cellWidth, 'height': cellHeight, 'display': 'block'}).select();
            }

            //desactiva el listener en la celda individual una vez hecho clic
            cell.on('click', function () {
                return false
            });

            //on keypress within td
            cell.on('keydown', function (event) {
                if (event.keyCode == 13) {//13 == enter

                    cellText.text(control.val());
                    control.css('display', 'none');

                    if (select2 != null)
                        select2.css('display', 'none');


                } else if (event.keyCode == 27) {//27 == escape

                    cellText.text(prevContent);//vuelve al valor original
                    control.val(prevContent);//vuelve al valor original
                    cell.off('click'); //reactivar edición
                    control.css('display', 'none');

                    if (select2 != null)
                        select2.css('display', 'none');
                }
            });


            control.blur(function () {

                cell.off('click'); //reactivar edición
                control.css('display', 'none');

                if (select2 != null) {

                    if (control[0].options[control.val()])
                        cellText.text(control[0].options[control.val()].text);

                    select2.css('display', 'none');

                } else {
                    cellText.text(control.val());
                }
            });


            control.on('change', function (event) {
                event.preventDefault();
                cell.off('click'); //reactivar edición

                var text;

                if (select2 != null) {

                    if (control[0].options[control.val()]) {
                        text = control[0].options[control.val()].text;//revert to original value
                    }
                    select2.css('display', 'none');

                } else {
                    text = control.val();
                }

                cellText.text(text);
            });
        }


        $('#table-purchase-details').on('mouseenter', 'tbody td', function () {

            if (inArray(this.cellIndex, only)) {
                $(this).css({
                    'box-shadow': '0 0 1px 0 rgba(0, 0, 0, 0.5), 0 0 1px 0 rgba(0, 0, 0, 0.5)',
                    'cursor': 'pointer',
                    'z-index': '1'
                });
            }
        }).on('mouseleave', 'tbody td', function () {
            $(this).css({
                'box-shadow': 'none'
            });
        });


        $('textarea.form-control').on('keyup', function () {
            var actual_height = parseInt(this.style.height);

            if (actual_height <= (this.scrollHeight)) {
                this.style.height = "5px";
                this.style.height = (this.scrollHeight) + "px";
            }
        });
    </script>


@endsection





@section('header')

    <div class="col-md-12">
        <div class="row">
            <div class="navbar">

                @include('purchases.forms.step_state',  ['status' => $purchase->status ])

                <div class="clearfix"></div>

                <div class="container-fluid">

                    <div class="navbar-btn">


{{--                        @include('purchases.forms.control_actions')--}}

                        <a href="{!! route('purchase.show', $purchase->id) !!}"  class="btn">
                            <i class="material-icons md-18">&#xE5C4;</i>Atras
                        </a>

                        <button type="button" id="btn-confirm" class="btn">Confirmar
                        </button>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('purchase.index') !!}">Compras</a></li>
        <li class="active">{!! $purchase->order !!}</li>
    </ol>
@endsection
