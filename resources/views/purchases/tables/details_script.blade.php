<script>
    // Trae la lista de costos de un articulo , al seleccionar articulo en lista
    $(document).delegate('select.article_id', 'change', function (event) {

        var itemId = $(this).val();
        var row = event.target.closest('tr');
        var internal_reference = $(row).find('td.cell-internal-reference')[0];
        var span = $(internal_reference).find('.text-internal-reference')[0];
        var input = $(internal_reference).find('.input-internal-reference')[0];

        span.innerHTML = '';
        input.value = '';
        var urls = '{{ route("article.findById", ":id") }}';
		urls = urls.replace(':id', itemId);
      
        $.get(urls, function (response) {
        
            span.innerHTML = response.internal_reference;
            input.value = response.internal_reference;

            var unit_cost = $(row).find('td.cell-unit-cost');
            var quantity = $(row).find('td.cell-quantity');

            unit_cost.find('.text-unit-cost').text(response.avg_unit_cost);
            unit_cost.find('.input-unit-cost').val(response.avg_unit_cost);

            quantity.find('.input-quantity').focus();




        });
    });


    $(document).ready(function () {
        $('#add').click(function (event) {

            var button = $(this);

            button.button('loading');
            urls='{!! route('get.select') !!}';
            
            $.get(urls, function (html) {
              
                var tbody = $('#table-quotation').find('tbody');
                var num = document.getElementById("table-quotation").rows.length;
                
                html = html.replace('span:', 'span['+(num-1)+'][quantity]');
                html = html.replace('input:', 'items['+(num-1)+'][quantity]');
                html = html.replace('key:', (num-1));
                
                

                tbody.append(html);
                
                var last_row = tbody.find('tr').last();
                
                var select = $(last_row).find('select.form-control').select2({
                    placeholder: 'Selecciona un artículo'
                });
              
                var select2 = $(last_row).find('.select2.form-control');
                select2.css('display', 'block');

            }).always(function(){
                button.button('reset');
            });
        });
    });


    $('table#table-quotation tbody').delegate('input', 'change', function () {
        __calculate();
    });


    // Calcular el total de los articulos agregados a la tabla
    function __calculate() {

        var tbody = $('#table-quotation').find('tbody');
        var rows = tbody.find('tr');
        var cellTotal = $('.cell-total');
        var inputTotal = $(cellTotal).find('.input-total')[0];
        var spanTotal = $(cellTotal).find('.text-total')[0];
        var totalAmount = 0.00;
        var amount = 0.00;

        rows.each(function (i, item) {
            var cellQuantity = $(item).find('.cell-quantity');
            var cellUnitCost = $(item).find('.cell-unit-cost');
            var cellAmount = $(item).find('.cell-amount');
            var quantity = parseFloat($(cellQuantity).find('.input-quantity')[0].value);
            var unitCost = parseFloat($(cellUnitCost).find('.input-unit-cost')[0].value);
            var inputAmount = $(cellAmount).find('.input-amount')[0];
            var spanAmount = $(cellAmount).find('.text-amount')[0];

            amount = quantity * unitCost;

            if (!isNaN(amount)) {
                inputAmount.value = parseFloat(amount).toFixed(2);
                spanAmount.textContent = parseFloat(amount).toFixed(2);
            } else {
                amount = 0.00;
                inputAmount.value = parseFloat(amount).toFixed(2);
                spanAmount.textContent = parseFloat(amount).toFixed(2);
            }

            totalAmount += amount;

        });

        inputTotal.value = parseFloat(totalAmount).toFixed(2);
        spanTotal.textContent = parseFloat(totalAmount).toFixed(2);
    }

    // Confirmar que hubo algun cambio  en la tabla
    $(document).on('DOMSubtreeModified', 'table#table-quotation tbody', function (event) {

        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();

        var rows = Array.prototype.slice.call(this.getElementsByTagName('tr'));

        rows.forEach(function (items, key) {

            var elements = Array.prototype.slice.call(items.querySelectorAll('.name_handler'));

            elements.forEach(function (item) {

                var nameAttr = item.getAttribute('name');

                var split = nameAttr.split("]");

                var newName = split[0].split("[")[0] + '[' + key + ']' + split[1] + ']';

                // change attribute name
                item.setAttribute('name', newName);
            });
        });

        __calculate()
    });


    $(document).delegate('.remove', 'click', function (event) {
        event.preventDefault();

        var row = event.target.closest('tr');

        $(row).remove().fadeOut();

        __calculate();
    });


    $('#table-quotation').on('change', '.input-unit-cost', function () {

        var unit_cost = this.value;
        this.value = parseFloat(unit_cost).toFixed(2);
    });

    function Model_series(key,id)
    {
        var largo=key.name.length;
        var total=largo-10;
        var elemento=key.name.substring(0,total)+'[internal_reference]';
        var cantida_name=key.name.substring(0,total)+'[quantity]';
        var internal=document.getElementsByName(elemento);
        var numero_item=key.name.substring(6,key.name.length - 11);
        var referencia=internal[0].value;
        var tipo_gestion;
        var urls = '{{ route("article.referencia", ":id") }}';
		urls = urls.replace(':id', referencia);
      
        $.get(urls, function (response) {
            tipo_gestion=response.gestion;
            sessionStorage.setItem("tipo_gestion", tipo_gestion);
            if(tipo_gestion=='1')
            {
                horaA = new Date();
                cantidad=key
                cantidad.readOnly = true;
               
                
                temp=document.getElementById('num_doc_temp').value;
                document.getElementById("titulo").innerHTML=temp;
                document.getElementById("item_id").innerHTML=numero_item;
                sessionStorage.setItem("referencia_item", referencia);
                var urls2 = '{{ route("article.findseries", ":id/:refer/:num_item") }}';
		            urls2 = urls2.replace(':id', referencia);
                    urls2 = urls2.replace(':refer', temp);
                    urls2 = urls2.replace(':num_item', numero_item);
                   
                    $.get(urls2, function (response) {

                        if(response.serie ==null)
                        {
                            urls='{!! route("article.findseriesall",":id/:refer/:num_item") !!}';
                            urls = urls.replace(':id', referencia);
                            urls = urls.replace(':refer', temp);
                            urls = urls.replace(':num_item', numero_item);
                            document.getElementById('create_modal_label').innerHTML="Crear series"
                            $.get(urls, function (html) {
                                
                                var tbody = $('#modal_create');
                                tbody.empty();
                                html = html.replace(':t1', 'T-create');
                                document.getElementById('type_modal').innerHTML='T-create'
                                
                                tbody.append(html);
                                addrow();
                                var last_row = tbody.find('tr').last();
                                create_series=document.getElementById('create_id_serial');
                                create_series.click(); 
                                })

                        }
                        else{
                            urls='{!! route("article.findseriesall",":id/:refer/:num_item") !!}';
                            urls = urls.replace(':id', referencia);
                            urls = urls.replace(':refer', temp);
                            urls = urls.replace(':num_item', numero_item);
                            document.getElementById('edit_modal_label').innerHTML="Agregar o Editar series"
                            $.get(urls, function (html) {
                               
                                var tbody = $('#modal_edit');
                                tbody.empty();
                                html = html.replace(':t1', 'T-edit');
                                
                                document.getElementById('type_modal').innerHTML='T-edit'
                                tbody.append(html);
                                var last_row = tbody.find('tr').last();
                                obj=document.getElementById('edit_id_serial');
                                obj.click(); 
                                })
                            }
                        
                    })
            }
            else if(tipo_gestion=='2')
            {
                horaA = new Date();
                cantidad=key
                cantidad.readOnly = true;
               
                
                temp=document.getElementById('num_doc_temp').value;
               // document.getElementById("titulo").innerHTML=temp;
                document.getElementById("item_id").innerHTML=numero_item;
                sessionStorage.setItem("referencia_item", referencia);
                var urls2 = '{{ route("lotes.findlotes", ":id/:refer/:num_item") }}';
		            urls2 = urls2.replace(':id', referencia);
                    urls2 = urls2.replace(':refer', temp);
                    urls2 = urls2.replace(':num_item', numero_item);
                    
                    $.get(urls2, function (response) {
                       
                        if(response.lote ==null)
                        {
                            urls='{!! route("lotes.findlotesall",":id/:refer/:num_item") !!}';
                            urls = urls.replace(':id', referencia);
                            urls = urls.replace(':refer', temp);
                            urls = urls.replace(':num_item', numero_item);
                            
                            document.getElementById('create_modal_label').innerHTML="Crear lotes"
                            $.get(urls, function (html) {
                                
                                var tbody = $('#modal_create');
                                tbody.empty();
                                html = html.replace(':t1', 'T-create');
                                document.getElementById('type_modal').innerHTML='T-create'
                                
                                tbody.append(html);
                                addrow();
                                var last_row = tbody.find('tr').last();
                                create_series=document.getElementById('create_id_serial');
                                create_series.click(); 
                                })

                        }
                        else{
                            
                           
                            urls='{!! route("lotes.findlotesall",":id/:refer/:num_item") !!}';
                            urls = urls.replace(':id', referencia);
                            urls = urls.replace(':refer', temp);
                            urls = urls.replace(':num_item', numero_item);
                            document.getElementById('edit_modal_label').innerHTML="Editar lotes"
                            $.get(urls, function (html) {
                               
                                var tbody = $('#modal_edit');
                                tbody.empty();
                                html = html.replace(':t1', 'T-edit');
                                
                                document.getElementById('type_modal').innerHTML='T-edit'
                                tbody.append(html);
                                var last_row = tbody.find('tr').last();
                                obj=document.getElementById('edit_id_serial');
                                obj.click(); 
                                })
                            }
                        
                    })


            }
           else
           {
            horaA = new Date();
                cantidad=key
                cantidad.readOnly = false;  
           }
 



        });

    }



    
</script>
