<tr>
    <td class="cell-description active">
        {!! $item->article->fullDescription !!}
        {!! Form::hidden('items['.$key.'][article]', old('items.'.$key.'.article') | $item->article_id ) !!}
    </td>

    <td class="cell-internal-reference active">{!! $item->article->internal_reference !!}</td>

    <td class="cell-quantity active">{!! $item->quantity !!}</td>

    <td class="cell-pending active">
        {!! $item->pending !!}
        {!! Form::hidden('items['.$key.'][pending]', old('items.'.$key.'.pending') | $item->pending, ['class' => 'input-pending'] ) !!}
    </td>

    <td class="cell-unit-cost active">
        {!! $item->unit_cost !!}
    </td>

    <td class="cell-quantity-receipt">

        <span class="text-quantity-receipt">
            {!!  $item !!}
        </span>

        <div class="form-group @if($errors->has('items.'.$key.'.quantity')) has-error @endif">
            {!! Form::text('items['.$key.'][quantity]',
            old('items.'.$key.'.quantity') | $item->pending,
            ['class' => 'form-control input-quantity']) !!}

            {!! $errors->first('items.'.$key.'.quantity', '<p class="text-danger">:message</p>') !!}
        </div>
    </td>

</tr>