<div class="form-group">

	{!! Form::label('_article', 'Articulo', [ 'class' => 'control-label']) !!}

	{!! Form::select('_article', $articleList,  null, 
		[ 
		'class' => 'form-control',
		'placeholder' => 'Selecciona un articulo de la lista',
		] ) 
	!!}
</div>

<div class="form-group">

	{!! Form::label('_quantity', 'Cantidad', [ 'class' => 'control-label']) !!}

	{!! Form::text('_quantity', old('_quantity'), 
		[ 
		'class' => 'form-control',
		'placeholder' => 'Cantidad de articulos', 
		'autocomplete' => 'off' 
		] ) 
	!!}
</div>

<div class="form-group">

	{!! Form::label('_unit_cost', 'Costo unitario', [ 'class' => 'control-label']) !!}
	
	{!! Form::text('_unit_cost', old('_unit_cost'), 
		[ 
		'class' => 'form-control',
		'placeholder' => 'Costo por unidad', 
		'autocomplete' => 'off' 
		] ) 
	!!}
</div>

<div class="form-group">

	{!! Form::label('_tax', 'Impuesto', [ 'class' => 'control-label ']) !!}

	{!! Form::select(
		'_tax', config('enums.taxes'),  0,  
		[
		'class' => 'form-control'
		] ) 
	!!}
</div>