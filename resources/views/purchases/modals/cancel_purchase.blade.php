<!-- Modal -->
<div id="cancelPurchaseModal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="modalLabel">

    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cancelar la compra</h4>
            </div>

            <div class="modal-body">
                {!! Form::open(['route' => ['purchase.cancel', ':PURCHASE_ID'], 'method' => 'POST', 'id' => 'form-cancel-purchase']) !!}
                {!! Form::close() !!}

                <h3>¿Estas seguro de cancelar la compra?</h3>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" id="add-discount" class="btn btn-primary mdb"
                        onclick="document.getElementById('form-cancel-purchase').submit()">Aceptar
                </button>
            </div>

        </div>

    </div>
</div>
<!-- .modal -->


<script>
    $('#cancelPurchaseModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var purchase = button.data('purchase');
        var modal = $(this);
        var form = $(modal).find('#form-cancel-purchase');
        var route = form.attr('action').replace(':PURCHASE_ID', purchase);

        form.attr('action', route);

//            modal.find('.modal-title').text('New message to ' + recipient)
//            modal.find('.modal-body input').val(recipient)
    })
</script>