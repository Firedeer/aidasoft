@extends('layout_principal')

@section('container')

    <div class="col-md-10 col-md-offset-1">
        <div class="panel">

            <div class="panel-heading">
                <div class="pull-right">
                    @include('partials.status', ['status' => $purchase->status ])
                </div>

                <h1>
                    <small>{!! trans('app.attributes.purchase') !!}</small>
                    <br>
                    {!! $purchase->order !!}
                </h1>
            </div>

            {!! Form::model($purchase, ['route'=>['purchase.update', $purchase->id], 'method'=>'PUT', 'id' => 'form-purchase', 'class' => 'form-horizontal form-condensed']) !!}

            <div class="panel-body">
                <div class="row">
                    @include('purchases.forms.heading')
                </div>
            </div>
            <!-- .panel-body -->

            <div class="panel-body">

                <legend>{!! trans('app.attributes.articles') !!}</legend>

                <div class="row-sm">
                    @include('purchases.tables.table')
                </div>
            </div>


            <div class="panel-body">
                <div class="col-md-12">
                    @include('partials.observation_field')
                </div>
            </div>
            <!-- .panel-body -->

            {!!Form::close()!!}


        </div>


    </div>

@endsection


@section('extra_scriptBody')

    @include('partials.select2_script')
    <script>
        $(document).ready(function () {
            $('#provider_id, #warehouse_id').select2().focus(function () {
                $(this).select2('focus');
            });

            $('.table select.form-control').select2({
                placeholder: ''
            });
        });
    </script>

    @include('partials.date_script')

    @include('purchases.tables.details_script')

    @include('purchases.modals.cancel_purchase')

@endsection




@section('header')
    <div class="col-md-12">
        <div class="row">
            <div class="navbar" style="background-color: transparent">
                <div class="container-fluid">

                    <div class="navbar-btn">
					@include('purchases.forms.control_actions')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('purchase.index') !!}">Compras</a></li>
        <li class="active">Editar {!! $purchase->order !!}</li>
    </ol>
@endsection

