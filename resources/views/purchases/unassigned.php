



<table class="table table-sm">
	<thead>
		<tr class="text-uppercase">
			<th>Desc<span class="hidden-xs">ripción</span></th>
			<th>Recibido</th>
			<th id="_unassigned">Por asignar</th>
			<th>	
				<a href="" class="btn btn-primary btn-raised btn-xs">Todo</a>
			</th>
		</tr>
	</thead>
	<tbody>
		@foreach($unassigned as $unassigned)
		<tr>
			<td>{!!$unassigned->article->description !!}
				{!!Form::hidden('article[]', $unassigned->article->id)!!}
				{!!Form::hidden('detail_id[]', $unassigned->id)!!}
			</td>
			<td>{!!$unassigned->received!!}</td>
			<td headers="_unassigned">{!!$unassigned->unassigned!!}</td>
			<td>
				@if($unassigned->unassigned > 0)
				<button name="btnModal" class="btn btn-primary btn-raised btn-xs">Asig<span class="hidden-xs">nar</span>
				</button>
				@else
				<i class="material-icons text-success">check_circle</i>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Modal title</h4>
			</div>
			<div class="modal-body">
				{!!Form::open(['url' => '', 'method' => 'POST', 'id'=>'form-assign'])!!}

				<div class="form-group">
					{!!Form::label('quantity', 'Cantidad')!!}
					{!!Form::text('quantity', null, ['class'=>'form-control'])!!}
					{!!Form::hidden('article_id')!!}
					{!!Form::hidden('purchase_detail_id')!!}
				</div>

				<div class="form-group">
					{!!Form::label('bodega', 'Cantidad')!!}
					{!! Form::select('bodega', $bodegas, old('bodega'), ['class' => 'selectpicker'])!!}
				</div>

				{!!Form::close()!!}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary" id="done">Aceptar</button>
			</div>
		</div>
	</div>
</div>


<script>
	$(document).ready(function(){

		$("button[name='btnModal']").click(function(event){

			var form = $("form#form-assign");
			var tr = $(event.target).closest('tr');
			var unssigned = tr.find("td[headers='_unassigned']");

			var quantity = form.find("input[name='quantity']");
			quantity.val(parseFloat(unssigned.text()));

			quantity.focus();

			var article = form.find("input[name='article_id']");
			article.val(tr.find("input[name='article[]']").val());

			var detail = form.find("input[name='purchase_detail_id']");
			detail.val(tr.find("input[name='detail_id[]']").val());

			$("#myModal").modal({backdrop: 'static'});

		});
	});

	$('#myModal').on('show.bs.modal', function (event) {

		// event.preventDefault();
		event.stopImmediatePropagation();
		var button = $(event.target).find("button#done");
		var token = $("input[name='_token']").val();
		var form = $("#form-assign");
		var modal = $(this);

		form.submit(function(event){ event.preventDefault(); });
		button.click(function(event){

			$.ajax({
				url: '/order/purchase/received',
				headers: {'X-CSRF-TOKEN': token},
				data: form.serialize(),
				type:'POST',
				dataType: 'json',
				success: function(res){
					console.log(res);
					modal.modal('hide');
				},
				errors: function(err){
					console.log(err);
				}
			});

		});

		console.log(modal)


	});

</script>