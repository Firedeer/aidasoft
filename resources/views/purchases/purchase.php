@extends('layout_principal') 

@section('extra_scriptHead')
<!---->
@endsection 

@section('container')

<div class="col-md-12">

	<div class="panel">

		<div class="panel-body">

			@foreach( $purchase as $purchase )
			<input type="hidden" name="purchase_id" id="purchase_id" value="{{ $purchase->purchase_id }}">
			
			<h1>
				<span class="glyphicon glyphicon-list-alt"></span> Orden de Compra #{{ $purchase->purchase_order }}
				<small class="pull-right">Fecha: <?php echo date_format( date_create( $purchase->date  ) , 'd F Y'); ?></small>
			</h1>

			<address>
				<strong>Telefónica Móviles Panamá (Movistar), SA.</strong>
				<br>Ave. Central Santiago, Veraguas
				<br>Panamá
				<br>Telefono: (507) 999-8888
				<br>Email: santiago@movistar.com
			</address>

			<address>
				<strong>{{ $purchase->company }}</strong>
				<br>{{ $purchase->address }}
				<br>Panamá
				<br>Telefono: {{ $purchase->phone }}
				<br>Email: {{ $purchase->email }}
			</address>

			<address>
				<strong>{{ $purchase->provider_name }}</strong>
				<br>{{ $purchase->provider_address }}
				<br>Panamá
				<br>Telefono: {{ $purchase->provider_email }}
				<br>Email: {{ $purchase->provider_phone }}
			</address>

			@endforeach

			<table class="table table-striped">
				<thead>
					<tr class="text-uppercase">
						<td class="text-nowrap">Cant.</td>
						<td class="text-nowrap">Articulo</td>
						<td class="text-nowrap">Codigo</td>
						<td class="text-nowrap text-right">Costo/U.</td>
						<td class="text-nowrap text-right">Subtotal</td>
					</tr>
				</thead>
				<tbody>                   
					@foreach( $articles as $article )
					<tr class="dato" >
						<td>{{ $article->quantity }}</td>
						<td><a href="/article/{{ $article->article_id }}">{{ $article->name }}</a></td>
						<td class="text-uppercase"><dt>{{ $article->barcode }}</dt></td>
						<td class="text-right">{{ $article->unit_cost }}</td>
						<td class="text-right">{{ $article->amount  }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>  

			<div class="col-xs-12 col-sm-6 col-md-4 pull-right">
				<div class="row">
					
					<p class="lead">Monto</p>
					
					<table class="table">
						<tbody>
							<tr>
								<th style="width:50%">Subtotal:</th>
								<td class="text-right">{{ $purchase->subtotal }}</td>
							</tr>
							<tr>
								<th style="width:50%">Impuesto:</th>
								<td class="text-right">{{ $purchase->tax }}</td>
							</tr>
							<tr>
								<th style="width:50%">Descuento:</th>
								<td class="text-right">{{ $purchase->discount }}</td>
							</tr>
							<tr>
								<th>Total:</th>
								<td class="total text-right">{{ $purchase->total }}</td>
							</tr>	
						</tbody>
					</table>
					
				</div>
			</div>
			<!--.col -->

		</div>
		<!-- .panel-body -->
	</div>
	<!-- .panel -->
</div>
<!--.col -->
@endsection