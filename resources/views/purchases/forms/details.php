
<div class="col-sm-4">

	<div class="form-group mdb">

		{!!Form::label('provider', 'Proveedor', ['class' => 'control-label']) !!}


		<select id="provider" name="provider" class="selectpicker" data-live-search="true" title="Proveedor">

			@foreach($providers as $provider)

			<option 
			value="{!! $provider->id !!}" 
			title="{!! $provider->firstname !!}" 
			data-content="
			<div class='__select-data-content'>
				<div class='__heading'>
					<span class='__title'>{!! $provider->firstname !!}</span>
				</div>
				<div class='__body'>
					{!! $provider->phone !!}<br>
					{!! $provider->email !!}<br>
					{!! $provider->address !!}
				</div>
			</div>" 

			@if( old('provider') and old('provider')  == $provider->id )

			selected='selected'

			@elseif( isset($order->provider_id) )

			@if( $order->provider_id == $provider->id and !old('provider') )
			selected='selected' 
			@endif

			@endif

			></option>

			@endforeach

		</select>

		@if ($errors->has('provider'))

		<p class="text-danger">{!!$errors->first('provider')!!}</p>

		@endif


	</div>
</div>



<div class="col-sm-4">

	<div class="form-group mdb has-feedback">

		{!! Form::label('require_date', 'Fecha de Entrega', ['class' => 'control-label']) !!}
		{!! Form::text('require_date', old('require_date'), ['class' => 'form-control datepicker', 'placeholder' => 'yyyy-mm-dd', 'autocomplete' => 'off']) !!}

		<span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>

		@if ($errors->has('require_date'))

		<p class="text-danger">{!!$errors->first('require_date')!!}</p>

		@endif

	</div>


	<div class="form-group mdb">

		{!!Form::label('sucursal', 'Lugar de destino', ['class' => 'control-label']) !!}

		<select id="sucursal" name="sucursal" class="selectpicker" data-live-search="true" title="Sucursales">
			@foreach($sucursales as $sucursal)

			<option 
			value="{{ $sucursal->id }}" 
			title="{{ $sucursal->sucursal }}"
			data-content="
			<div class='__select-data-content'>
				<div class='__heading'>
					<span class='__title'>{{ $sucursal->sucursal }}</span>
				</div>
				<div class='__body'>
					{{ $sucursal->region }}
				</div>
			</div>" 

			@if( old('sucursal') and old('sucursal')  == $sucursal->id )

			selected="selected"

			@elseif( isset($order->require_place_id)  ) 
			@if( $order->require_place_id == $sucursal->id  and !old('sucursal'))
			selected="selected" 
			@endif
			@endif 

			></option>

			@endforeach

		</select>

		@if ($errors->has('sucursal'))

		<p class="text-danger">{!!$errors->first('sucursal')!!}</p>

		@endif

	</div>

</div>

