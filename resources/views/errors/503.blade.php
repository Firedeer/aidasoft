<!--<html>
	<head>
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 72px;
				margin-bottom: 40px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
                <h1>503</h1>
				<div class="title">Be right back.</div>
			</div>
		</div>
	</body>
</html>
-->
<!doctype html>
<title>Styde - En Mantenimiento</title>
<style>
  body { text-align: center; padding: 150px; }
  h1 { font-size: 50px; }
  body { font: 20px Helvetica, sans-serif; color: #333; }
  article { display: block; text-align: left; width: 650px; margin: 0 auto; }
</style>

<article>
    <h1>&iexcl;Regresaremos pronto!</h1>
    <div>
        <p>Pedimos disculpas por los incovenientes causados pero estamos trabajando en cosas interesantes.&iexcl;Pronto estaremos en línea de nuevo!</p>
        <p>&mdash; El equipo</p>
    </div>
</article>
