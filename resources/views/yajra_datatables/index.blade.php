@extends('layout_principal')

@section('extra_scriptHead')



    <!-- css DataTables-1.10.11-->
    <link rel="stylesheet" href="http://127.0.0.1:8000/dataTables/1.10.11/css/dataTables.bootstrap.css">

    <script src="http://127.0.0.1:8000/dataTables/1.10.11/js/jquery.dataTables.js"></script>

    <script src="http://127.0.0.1:8000/dataTables/1.10.11/js/dataTables.bootstrap.js"></script>

@endsection


@section('container')


    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">


                <section class="col-md-12">

                    <div class="page-header">
                        <h3>Inventario</h3>
                    </div>

                    {!! Form::model(Request::all(), [ 'url' => '/inventory', 'role' => 'search', 'method' => 'GET']) !!}

                    <div class="form-inline">

                        <div class="input-group" style="width: 100%">

                            {!! Form::select('filter', config('enums.searchFilter'), 'all', [ 'class' => 'form-control', 'id'=> 'filter']) !!}
                            {{--                {!! Form::select('param', [], null, [ 'class' => 'form-control', 'placeholder' =>'Debe seleccionar un filtro', 'id'=> 'param']) !!}--}}
                            {!! Form::text('param', '', [ 'class' => 'form-control', 'placeholder' =>'Debe seleccionar un filtro', 'id'=> 'param', 'list' => 'browsers']) !!}

                            {{--<input list="browsers" name="param">--}}

                            <datalist id="browsers">

                            </datalist>


                            {!! Form::submit('Filtrar', ['class' => 'btn btn-primary btn-raised mdb']) !!}
                        </div>


                    </div>

                    {!! Form::close() !!}
                </section>
                <div class="row-sm">

                    <table id="example" class="table table-bordered table-hover text-middle" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Artuculo</th>
                            <th>Codigo</th>
                            <th>Marca</th>
                            <th>Categoria</th>
                            <th>Bodega</th>
                            <th>Unidad</th>
                            <th>Stock</th>
                            <th>Stock Minimo</th>
                            <th>Stock Maximo</th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {


            $('#param').keyup(function () {
                $('#example_filter input[type="search"]').val(this.value).keyup();
                console.log(this.value);
            });


            var token = '{!! csrf_token() !!}';

            var oTable = $('#example').DataTable({
                "processing": true,
                "serverSide": true,
                "displayLength": 50,
                "ajax": {
                    url: '{!! route('yajra.datatables.list') !!}',
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'POST',
                    data: function (d) {
                        d.filter = $('select[name="filter"]').val();
                        d.param = $('input[name="param"]').val();
                    },
                    error: function (xhr, error, thrown) {
                       console.log(thrown)
                    }
                },
                "columns": [
                    {data: 'article', name: 'article'},
                    {data: 'serial_code', name: 'serial_code', className: 'hidden-xs'},
                    {data: 'brand', name: 'brand', className: 'hidden-xs'},
                    {data: 'category', name: 'category', className: 'hidden-xs'},
                    {data: 'warehouse', name: 'warehouse'},
                    {data: 'measurement_unit', name: 'measurement_unit', className: 'hidden-xs'},
                    {data: 'stock', name: 'stock', width: '20'},
                    {data: 'min_stock', name: 'min_stock', className: 'hidden-xs'},
                    {data: 'max_stock', name: 'max_stock', className: 'hidden-xs'}
                ],
                "language": {
                    "decimal": "",
                    "emptyTable": "No hay datos disponibles en la tabla",
                    "info": "Viendo _START_ a _END_ de _TOTAL_ entradas",
                    "infoEmpty": "Viendo 0 a 0 de 0 entradas",
                    "infoFiltered": "(filtered from _MAX_ total entries)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Cantidad _MENU_ ",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontraron registros coincidentes",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "<span class='glyphicon glyphicon-chevron-right'></span>",
                        "previous": "<span class='glyphicon glyphicon-chevron-left'></span>"
                    },
                    "aria": {
                        "sortAscending": ": activar para ordenar la columna ascendente",
                        "sortDescending": ": activar para ordenar la columna descendente"
                    }
                }
            });


            $('form[role="search"]').on('submit', function (e) {
                e.preventDefault();
                oTable.draw();
            });

        });
    </script>

    <script src="{!! asset('assets/js/inventory_filter.js') !!}"></script>
@endsection