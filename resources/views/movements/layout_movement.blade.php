
@if(Session::has('msj'))
<div class="alert alert-success" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong> <h3>Felicidades!</h3></strong> 
	<div class="icon">
		<span class="glyphicon glyphicon-thumbs-up"></span>
	</div>
	<p>{{ Session::get('msj') }}</p>		
</div>
@endif

<div class="col-md-12">
	<div class="row">
		
		<a href="/article/movement/purchase" class="btn btn-link btn-sm active">Compra</a>
		<a href="/devolution/purchase" class="btn btn-link btn-sm">Devolución</a>
		<a href="/orders" class="btn btn-link btn-sm">Pedido</a>
		<a href="/transfers" class="btn btn-link btn-sm">Transferencia</a>
		<a href="/order/purchases" class="btn btn-link btn-sm">Orden de Compra</a>
		
	</div>
</div>	
