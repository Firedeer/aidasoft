<script>
    $(document).ready(function () {

        $('#add').click(function () {
           
            var button = $(this);

            button.button('loading');

            $.get('{!! route('movement.warehouse.add.article.render', $warehouse_id) !!}', function (html) {
                var tbody = $('#table-articles').find('tbody');
                var num = document.getElementById("table-articles").rows.length;                
                html = html.replace('span:', 'span['+(num-1)+'][quantity]');
                html = html.replace('input:', 'items['+(num-1)+'][quantity]');
                html = html.replace('key:', (num-1));
                tbody.append(html);

                var last_row = tbody.find('tr').last();

                var select = $(last_row).find('select.form-control').select2({
                    placeholder: 'Selecciona un artículo'
                });

                var select2 = $(last_row).find('.select2.form-control');
                select2.css('display', 'block');

            }).always(function(){
                button.button('reset');
            });
        });
    });


    $('#table-articles').delegate('.remove-row', 'click', function () {
        var row = this.closest('tr');
        row.remove();
    });

    $('#table-articles').on('change', 'select.article', function (event) {
        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();


        var tr = event.target.closest('tr');
        var article = $(this).val();

        var measurement_unit = $(tr).find('td.cell-measurement-unit');
        var internal_reference = $(tr).find('td.cell-internal-reference');
        var available_quntity = $(tr).find('td.cell-available');

        if (article) {

            var route = '{!! route('warehouse.article',  ['warehouse' => $warehouse_id, 'article' => ':ARTICLE_ID']) !!}';

            $.getJSON(route.replace(':ARTICLE_ID', article), function (json) {

                console.log(json);

                measurement_unit.find('.text-measurement-unit').html(json.measurement_unit);
                measurement_unit.find('input.measurement_unit').val(json.measurement_unit);
                internal_reference.find('.text-internal-reference').html(json.internal_reference);
                internal_reference.find('input.internal_reference').val(json.internal_reference);
                available_quntity.find('.text-available').html(json.available_quantity);
                available_quntity.find('input.available').val(json.available_quantity);
            });
        } else {
            measurement_unit.find('.text-measurement-unit').html('');
            measurement_unit.find('input.measurement_unit').val('');
            internal_reference.find('.text-internal-reference').html('');
            internal_reference.find('input.internal_reference').val('');
        }
    });



    // Confirmar que hubo algun cambio  en la tabla
    $('#table-articles').on('DOMSubtreeModified', 'tbody', function (event) {
        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();

        var rows = Array.prototype.slice.call(this.getElementsByTagName('tr'));

        rows.forEach(function (items, key) {

            var elements = Array.prototype.slice.call(items.querySelectorAll('.name-handler'));

            elements.forEach(function (item) {

                var nameAttr = item.getAttribute('name');

                var split = nameAttr.split("]");

                // var split_2 = nameAttr.split("[")
                var newName = split[0].split("[")[0] + '[' + key + ']' + split[1] + ']';

                // change attribute name
                item.setAttribute('name', newName);
            });
        });
    });
</script>

<!-- MODULO DE ENTRADA DE SRIES"-->
<script>
function Model_series(key,id)
{

    var largo=key.name.length;
    var total=largo-10;
    var elemento=key.name.substring(0,total)+'[internal_reference]';
    var cantida_name=key.name.substring(0,total)+'[quantity]';
    var internal=document.getElementsByName(elemento);
    var numero_item=key.name.substring(6,key.name.length - 11);
    var referencia=internal[0].value;
    var tipo_gestion;
    var urls = '{{ route("article.referencia", ":id") }}';
		urls = urls.replace(':id', referencia);
        $.get(urls, function (response) {
            tipo_gestion=response.gestion;
            
            sessionStorage.setItem("tipo_gestion", tipo_gestion);
            if(tipo_gestion=='1')
            {
                horaA = new Date();
                cantidad=key
                cantidad.readOnly = true;
                temp=document.getElementById('num_doc_temp').value;
                document.getElementById("titulo").innerHTML=temp;
                document.getElementById("item_id").innerHTML=numero_item;
                sessionStorage.setItem("referencia_item", referencia);
                var urls2 = '{{ route("article.findseries", ":id/:refer/:num_item") }}';
		            urls2 = urls2.replace(':id', referencia);
                    urls2 = urls2.replace(':refer', temp);
                    urls2 = urls2.replace(':num_item', numero_item);
                    $.get(urls2, function (response) {

                        if(response.serie ==null)
                        {
                            urls='{!! route("article.findseriesall",":id/:refer/:num_item") !!}';
                            urls = urls.replace(':id', referencia);
                            urls = urls.replace(':refer', temp);
                            urls = urls.replace(':num_item', numero_item);
                            $.get(urls, function (html) {
                                document.getElementById('create_modal_label').innerHTML="Crear series"
                                var tbody = $('#modal_create');
                                tbody.empty();
                                html = html.replace(':t1', 'T-create');
                                document.getElementById('type_modal').innerHTML='T-create'
                                
                                tbody.append(html);
                                addrow();
                                var last_row = tbody.find('tr').last();
                                create_series=document.getElementById('create_id_serial');
                                create_series.click(); 
                                })

                        }
                        else{
                            urls='{!! route("article.findseriesall",":id/:refer/:num_item") !!}';
                            urls = urls.replace(':id', referencia);
                            urls = urls.replace(':refer', temp);
                            urls = urls.replace(':num_item', numero_item);
                            document.getElementById('create_modal_label').innerHTML=" Agregar o Editar series"
                            $.get(urls, function (html) {
                               
                                var tbody = $('#modal_edit');
                                tbody.empty();
                                html = html.replace(':t1', 'T-edit');
                                
                                document.getElementById('type_modal').innerHTML='T-edit'
                                tbody.append(html);
                                var last_row = tbody.find('tr').last();
                                obj=document.getElementById('edit_id_serial');
                                obj.click(); 
                                })
                            }
                        
                    })

            }
            else if(tipo_gestion=='2')
            {
                horaA = new Date();
                cantidad=key
                cantidad.readOnly = true;
               
                
                temp=document.getElementById('num_doc_temp').value;
               // document.getElementById("titulo").innerHTML=temp;
                document.getElementById("item_id").innerHTML=numero_item;
                sessionStorage.setItem("referencia_item", referencia);
                var urls2 = '{{ route("lotes.findlotes", ":id/:refer/:num_item") }}';
		            urls2 = urls2.replace(':id', referencia);
                    urls2 = urls2.replace(':refer', temp);
                    urls2 = urls2.replace(':num_item', numero_item);
                    console.log(urls2);
                    $.get(urls2, function (response) {
                       
                        if(response.lote ==null)
                        {
                            urls='{!! route("lotes.findlotesall",":id/:refer/:num_item") !!}';
                            urls = urls.replace(':id', referencia);
                            urls = urls.replace(':refer', temp);
                            urls = urls.replace(':num_item', numero_item);
                            
                            document.getElementById('create_modal_label').innerHTML="Crear lotes"
                            $.get(urls, function (html) {
                                
                                var tbody = $('#modal_create');
                                tbody.empty();
                                html = html.replace(':t1', 'T-create');
                                document.getElementById('type_modal').innerHTML='T-create'
                                
                                tbody.append(html);
                                addrow();
                                var last_row = tbody.find('tr').last();
                                create_series=document.getElementById('create_id_serial');
                                create_series.click(); 
                                })

                        }
                        else{
                            
                           
                            urls='{!! route("lotes.findlotesall",":id/:refer/:num_item") !!}';
                            urls = urls.replace(':id', referencia);
                            urls = urls.replace(':refer', temp);
                            urls = urls.replace(':num_item', numero_item);
                            document.getElementById('edit_modal_label').innerHTML="Editar lotes"
                            $.get(urls, function (html) {
                               
                                var tbody = $('#modal_edit');
                                tbody.empty();
                                html = html.replace(':t1', 'T-edit');
                                
                                document.getElementById('type_modal').innerHTML='T-edit'
                                tbody.append(html);
                                var last_row = tbody.find('tr').last();
                                obj=document.getElementById('edit_id_serial');
                                obj.click(); 
                                })
                            }
                        
                    })


            }
            else
            {
                horaA = new Date();
                cantidad=key
                cantidad.readOnly = false;
            }
        });
}


</script>