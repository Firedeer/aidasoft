<tr>
    <td class="cell-article">
        <span class="text-article">
           {!! isset($articleList[old('items.'.$key.'.article')])
             ? $articleList[old('items.'.$key.'.article')]
             : '' !!}
        </span>

        <div class="form-group @if($errors->has('items.'.$key.'.article')) has-error @endif">
            {!! Form::select(
            'items['.$key.'][article]',
             $articleList,
             old('items.'.$key.'.article'),
            [
            'class' => 'form-control article name-handler',
            'placeholder' => 'Selecciona un articulo de la lista'
            ]) !!}

            {!! $errors->first('items.'.$key.'.article', '<p class="text-danger">:message</p>') !!}
        </div>
    </td>


    <td class="cell-internal-reference col-xs-1 active">
        <span class="text-internal-reference">{!! old('items.'.$key.'.internal_reference') !!}</span>

        {!! Form::hidden('items['.$key.'][internal_reference]', old('items.'.$key.'.internal_reference'), ['class' => 'internal_reference name-handler'] ) !!}
    </td>


    <td class="cell-measurement-unit col-xs-1 active">
        <span class="text-measurement-unit">{!!  old('items.'.$key.'.measurement_unit') !!}</span>

        {!! Form::hidden('items['.$key.'][measurement_unit]', old('items.'.$key.'.measurement_unit'), ['class' => 'measurement_unit name-handler'] ) !!}
    </td>


    <td class="cell-quantity">
        <span class="text-quantity" id="span:">{!! old('items.'.$key.'.quantity') !!}</span>

        <div class="form-group @if($errors->has('items.'.$key.'.quantity')) has-error @endif">
            {!! Form::text('items['.$key.'][quantity]', old('items.'.$key.'.quantity'), ['class' => 'form-control name-handler', 'onFocus'=>'Model_series(this,key:)','id'=>'input:']) !!}

            {!! $errors->first('items.'.$key.'.quantity', '<p class="text-danger">:message</p>') !!}
     </div>
    </td>

    <td class="cell-available col-xs-1 active">
        <span class="text-available">{!! old('items.'.$key.'.available') !!}</span>

        <div class="form-group @if($errors->has('items.'.$key.'.available')) has-error @endif">
            {!! Form::hidden('items['.$key.'][available]', old('items.'.$key.'.available'), ['class' => 'available name-handler']) !!}

            {!! $errors->first('items.'.$key.'.available', '<p class="text-danger">:message</p>') !!}
        </div>
    </td>


    <td width="40">
        <button type="button" class="btn btn-xs btn-danger mdb remove-row">
            <i class="material-icons md-18">&#xE872;</i>
        </button>
    </td>
</tr>