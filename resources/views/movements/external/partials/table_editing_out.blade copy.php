<button style='visibility:hidden' type="button" class="btn btn-link" data-toggle="modal" data-target="#createmodelserie">
      <i class="material-icons md-18" id="create_id_serial" name="create_id_serial" value="prueba" >add_circle_outline</i> Editar sucursal
</button>


<button style='visibility:hidden' type="button" class="btn btn-link" data-toggle="modal" data-target="#editmodelserie">
      <i class="material-icons md-18" id="edit_id_serial" name="edit_id_serial" value="prueba" >add_circle_outline</i> Editar sucursal
</button>

@if($errors->first('items'))
    <div class="text-danger small">
        {!! $errors->first('items', '<p><i class="material-icons md-18">error_outline</i> :message</p>')  !!}
    </div>
@endif




<table class="table table-condensed table-bordered table-control text-top small" id="table-articles">
    <thead>
    <tr>
        <th>{!! trans('app.attributes.description') !!}</th>
        <th>{!! trans('app.attributes.internal_reference') !!}</th>
        <th>{!! trans('app.attributes.measurement_unit') !!}</th>
        <th>{!! trans('app.attributes.quantity') !!}</th>
        <th>{!! trans('app.attributes.available') !!}</th>
        <th></th>
    </tr>
    </thead>
    <tbody class="text-uppercase"> 
    @if(old('items'))
        @foreach(old('items') as $key => $item)

            @include('movements.external.partials.table_row')

        @endforeach
    @endif
    </tbody>
</table>

<button type="button" id="add" class="btn btn-xs btn-link pull-left" title="Agrega un artículo"
        data-loading-text="Agregando...">
    <i class="material-icons">add</i> Agregar
</button>

<script>

    function inArray(value, array) {

        if ($.inArray(value, array) != -1) {
            return true;
        }

        return false;
    }

    var only = [0, 3];

    $('#table-articles').on('click', 'tbody td', function () {
      
        if (inArray(this.cellIndex, only)) {
            displayForm($(this));
        }
    });


    function displayForm(cell) {

        var column = cell.attr('class').split(" ")[0],
            id = cell.closest('tr').attr('id'),
            cellWidth = cell.css('width'),//obtiene el ancho de la celda para el estilo de ancho del campo de entrada
            cellHeight = cell.css('height'),//obtiene el alto de la celda para el estilo de ancho del campo de entrada
            cellText = $(cell.find(column.replace('cell', '.text'))),//obtiene el texto actual de la celda para el campo de entrada
            prevContent = cellText.text();//almacena el valor anteror

        //borra el texto actual de la celda, mantiene la celda con el ancho actual
        cellText.html('');
        cell.css('width', cellWidth);

        var controls = cell.find('.form-control');
        var control;
        var select2;

        if (controls.length > 1) {
            control = $(controls[0]);
            select2 = $(controls[1]);
            select2.css({'width': cellWidth, 'height': cellHeight, 'display': 'block'});
            control.select();

        } else {
            control = $(controls[0]);
            control.css({'width': cellWidth, 'height': cellHeight, 'display': 'block'}).select();
        }


        //desactiva el listener en la celda individual una vez hecho clic
        cell.on('click', function () {
            return false
        });

        //on keypress within td
        cell.on('keydown', function (event) {
            if (event.keyCode == 13) {//13 == enter

                cellText.text(control.val());
                control.css('display', 'none');

                if (select2 != null)
                    select2.css('display', 'none');


            } else if (event.keyCode == 27) {//27 == escape

                cellText.text(prevContent);//vuelve al valor original
                control.val(prevContent);//vuelve al valor original
                cell.off('click'); //reactivar edición
                control.css('display', 'none');

                if (select2 != null)
                    select2.css('display', 'none');
            }
        });


        control.blur(function () {

            cell.off('click'); //reactivar edición
            control.css('display', 'none');

            if (select2 != null) {

                if (control[0].options[control.val()])
                    cellText.text(control[0].options[control.val()].text);

                select2.css('display', 'none');

            } else {
                cellText.text(control.val());
            }
        });


        control.on('change', function (event) {
            event.preventDefault();
            cell.off('click'); //reactivar edición
            
            var text;

            if (select2 != null) {

                if (control[0].options[control[0].selectedIndex]) {
                    text = control[0].options[control[0].selectedIndex].innerText ;//revert to original value
                }
  
                select2.css('display', 'none');

            } else {
                text = control.val();
            }

            cellText.text(text);
        });
    }


    $('#table-articles')
        .on('mouseenter', 'tbody td', function () {
            
            if (inArray(this.cellIndex, only)) {
                $(this).css({
                    'box-shadow': '0 0 1px 0 rgba(0, 0, 0, 0.5), 0 0 1px 0 rgba(0, 0, 0, 0.5)',
                    'cursor': 'pointer',
                    'z-index': '1'
                });
            }
        }).on('mouseleave', 'tbody td', function () {
        $(this).css({
            'box-shadow': 'none'
        });
    });


    $('textarea.form-control').on('keyup', function () {
        var actual_height = parseInt(this.style.height);

        if (actual_height <= (this.scrollHeight)) {
            this.style.height = "5px";
            this.style.height = (this.scrollHeight) + "px";
        }
    });

    $('#createmodelserie').on('show.bs.modal', function (event) {
        showModal($(this), '#items[0][internal_reference]', true);

    });
    $('#editmodelserie').on('show.bs.modal', function (event) {
        showModal($(this), '#items[0][internal_reference]', true);
        
    });
    function ocultar_modal_create()
    {

        var num = document.getElementById("T-create").rows.length;


        var items = document.getElementById("table-articles").rows.length;
        var n=document.getElementById("item_id").innerHTML;
        var referencia=document.getElementById("titulo").innerHTML;



        var span='span['+n+'][quantity]';
        var input='items['+n+'][quantity]';
        

        document.getElementById(input).value=(num-1);
        document.getElementById(span).innerHTML=(num-1);
        var referencia_item=sessionStorage.getItem("referencia_item");
        var urls = '{{ route("movement.create_series_out", ":id/:item_id/:internal_reference") }}';
        urls = urls.replace(':id', referencia);
        urls = urls.replace(':item_id', n);
        urls = urls.replace(':internal_reference', referencia_item);
     
        var series= {};
        var series_acumulada=[];
        var items= {};

        for (var i = 1; i < (num); i++) 
        {
           
         
                    var inp=document.getElementById('num_doc_temp['+i+']');
                    var obj = { 
                            id:i,
                            serie: inp.value
    	                    };

                    series_acumulada.push(obj);
                   
                   
                    
        }
        
       
        var series_json = JSON.stringify(series_acumulada);


                 $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: "post",
                    url: urls,
                    data: series_json,
                    crossDomain: true,
                    cache: false,
                    beforeSend: function() 
                    {
                        
                    },
                    success: function(data) 
                    {
                      
                       
                    },
                    error: function (jqXHR, exception) {
                        console.log( "Error con el servidor: "+exception);
                        console.log( "Error con el servidor: "+jqXHR.status );
        } 
                
            
                });


                         

     
       
                $('#T-create').empty();
       
        $('#createmodelserie').modal('hide');
       
    }
    function ocultar_modal_edit()
    {

        var num = document.getElementById("T-edit").rows.length;


        var items = document.getElementById("table-articles").rows.length;
        var n=document.getElementById("item_id").innerHTML;
        var referencia=document.getElementById("titulo").innerHTML;



        var span='span['+n+'][quantity]';
        var input='items['+n+'][quantity]';
        

        document.getElementById(input).value=(num-1);
        document.getElementById(span).innerHTML=(num-1);
        var referencia_item=sessionStorage.getItem("referencia_item");
        var urls = '{{ route("movement.edit_series_out", ":id/:item_id/:internal_reference") }}';
        urls = urls.replace(':id', referencia);
        urls = urls.replace(':item_id', n);
        urls = urls.replace(':internal_reference', referencia_item);
     
        var series= {};
        var series_acumulada=[];
        var items= {};
            
        for (var i = 1; i < num; i++) 
        {
           
  
         
                    var inp=document.getElementById('num_doc_temp['+i+']');
                    
                   
                    var obj = { 
                            id:i,
                            serie: inp.value
    	                    };

                    series_acumulada.push(obj);
                    
                   
                   
                   
                    
        }
        
       
        var series_json = JSON.stringify(series_acumulada);

                 $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: "post",
                    url: urls,
                    data: series_json,
                    crossDomain: true,
                    cache: false,
                    beforeSend: function() 
                    {
                        
                    },
                    success: function(data) 
                    {
                      
                       
                    },
                    error: function (jqXHR, exception) {
                        console.log( "Error con el servidor: "+exception);
                        console.log( "Error con el servidor: "+jqXHR.status );
                    } 
                });
        $('#T-edit').empty();
        $('#editmodelserie').modal('hide');
       
    }
</script>


<div style="display:none">
<span id='type_modal'></span>
<span id='item_id'></span>
<span id='internal_refer'></span>

</div>


<!-- Modal create -->
<div class="modal fade" id="createmodelserie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">

                Crear salidas de series
                <span id="titulo" style="display:none"></span></h4>
            </div>
              <div class="modal-body" id="modal_create">
              
             
              
             

         
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb" onclick="ocultar_modal_create()">Guardar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal edit-->
<div class="modal fade" id="editmodelserie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">

                Agregar o editar salidas de series
                <span id="titulo" style="display:none"></span></h4>
            </div>
              <div class="modal-body" id="modal_edit">
             

 
             

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb" onclick="ocultar_modal_edit()">Guardar</button>
            </div>
        </div>
    </div>
</div>