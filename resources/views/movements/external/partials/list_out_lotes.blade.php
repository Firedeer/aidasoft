<style>
body{
  padding:20px 20px;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px; 
  color:#ccc;
}
</style>
<script>
function checkIt(evt) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        status = "This field accepts numbers only."
        return false
    }
    status = ""
    return true
}
function cantidad_disponible(id,cant)
{
  
  //console.log('input_cantidad_out['+id+']')
var cant_def=document.getElementById('input_cantidad_out['+id+']').value;

if(cant_def>cant)
{
  var opcion = confirm("Solo tiene disponible :"+cant+" para salidas desea sacar la totalidad");
  
  if (opcion == true) {
     document.getElementById('input_cantidad_out['+id+']').value=cant;
	} else {
    document.getElementById('input_cantidad_out['+id+']').value="";
	}

}

}
$(document).ready(function() {
  $(".search").keyup(function () {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
    
  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });
    
  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });

  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text();

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();}
		  });
});
</script>

<div class="form-group ">
    <input type="text" class="search form-control" placeholder="Buscar lote">
</div>
<span class="counter pull-right"></span>
<table  id="table_lote" class="table table-hover table-bordered results">
  <thead>
    <tr>
    <th>.</th>
    <th>#</th>
    <th>Lotes</th>
    <th>Cantidad salida</th>
    <th>Cantidad en stock</th>
    <th></th>
    </tr>
    <tr class="warning no-result">
      <td colspan="4"><i class="fa fa-warning"></i> No result</td>
    </tr>
  </thead>
  <tbody>

@foreach($series as $key => $lotes)

    <tr>
    <td>

    </td>
    <td >{!!Form::label('lote_id['.$lotes->row.']', isset($lotes->id)?($lotes->id):'',['id'=>'lote_id['.$lotes->row.']'] )!!}</td>
<td >{!!Form::label('lote_num['.$lotes->id.']', isset($lotes->lote)?($lotes->lote):'',['id'=>'lote_num['.$lotes->id.']'] )!!}
</td>
<td >{!! Form::text('input_cantidad_out['.$lotes->id.']',isset($lotes->cantidad_out)?($lotes->cantidad_out):'' , ['class' => 'form-control input-pending','id'=>'input_cantidad_out['.$lotes->id.']','tabindex'=>'1','onfocusout'=>'cantidad_disponible('.$lotes->id.','.$lotes->cantidad.');','onKeyPress'=>'return checkIt(event)'] ) !!}</td>
    <td >{!!$lotes->cantidad!!}</td>
    <td style="color:transparent">{!!$lotes->num_doc_temp!!}</td>
    </tr>
@endforeach
</tbody>
</table>