@extends('layout_principal')

@section('container')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel">

            <div class="panel-heading">

                <div class="pull-right text-right">
                    <br>
                    <address>
                        <strong>{!! trans('app.attributes.telefonica')!!}</strong><br>
                        <span class="small">{!! $external->warehouse->sucursalName !!}</span><br>
                        <span class="small">{!! $external->warehouse->regionName !!}</span><br>
                    </address>
                </div>

                <h1>
                    <small>{!! trans('app.attributes.external_movement_'.$external->movement) !!}</small>
                    <br>{!! $external->external_movement_num !!}
                </h1>
            </div>


            <div class="panel-body">

                <div class="form-horizontal form-condensed">

                    <div class="col-sm-6">
                        <div class="form-group mdb @if($errors->has('agent')) has-error @endif">
                            {!! Form::label('agent', trans('app.attributes.supervisor'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-9">
                                <div class="form-control">{!! $external->agentName !!}</div>

                            </div>
                        </div>


                        <div class="form-group mdb @if($errors->has('agent')) has-error @endif">
                            {!! Form::label('date', trans('app.attributes.external_agent'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-9">
                                <div class="form-control">{!! config('enums.external_agents')[$external->external_agent_id ] !!}</div>

                            </div>
                        </div>


                        <div class="form-group mdb @if($errors->has('agent')) has-error @endif">
                            {!! Form::label('warehouse', 'Bodega', ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-9">
                                <div class="form-control">{!! $external->warehouse->description !!}</div>

                            </div>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <div class="form-group mdb @if($errors->has('agent')) has-error @endif">
                            {!! Form::label('date', 'Fecha', ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-9">
                                <div class="form-control">{!! $external->createdAt() !!}</div>

                            </div>
                        </div>



                        <div class="form-group mdb @if($errors->has('agent')) has-error @endif">
                            {!! Form::label('user', 'Usuario', ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-9">
                                <div class="form-control">{!! $external->createdBy->fullName !!}</div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="panel-body">

                <legend>{!! trans('app.attributes.articles') !!}</legend>

                <table class="table table-striped table-bordered text-middle small" id="table-out">
                    <thead>
                    <tr>
                        <th>{!! trans('app.attributes.description') !!}</th>
                        <th>{!! trans('app.attributes.internal_reference') !!}</th>
                        <th>{!! trans('app.attributes.measurement_unit') !!}</th>
                        <th>{!! trans('app.attributes.quantity') !!}</th>
                    </tr>
                    </thead>
                    <tbody>


                    @if(isset($external->details))
                        @foreach($external->details as $key => $item)

                            <tr>
                                <td>
                                    {!! isset($item->article)? $item->article->fullDescription : '' !!}
                                </td>
                                <td>
                                    {!! isset($item->article)? $item->article->internal_reference : '' !!}
                                </td>
                                <td>
                                    {!! isset($item->article)? $item->article->measurement_unit : ''!!}
                                </td>

                                <td>
                                    {!! $item->quantity !!}
                                </td>
                            </tr>

                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>

            <div class="panel-body">
                <div class="col-md-12">
                    @include('partials.observation_field', ['textarea' => $external->observation])
                </div>


            </div>
        </div>
    </div>
@endsection





@section('breadcrumb')

    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('warehouse.show', $external->warehouse_id) !!}">{!! trans('app.attributes.warehouse') !!}</a></li>
        <li class="active">{!! $external->external_movement_num !!}</li>
    </ol>
@endsection