@extends('app')

@section('container')

    <div class="container-fluid " style="min-height: 100vh">
        <div class="">
            <div class="col-sm-12">

                <div class="center-block" style="margin-top: 5em; max-width:400px;">

                    <h1 class="text-center"><img src="{{ asset('/assets/logo/aidasoft.png')}}" alt="">
                   </h1>

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            @foreach ($errors->all() as $error)
                                <small>{!! $error  !!}</small><br>
                            @endforeach
                        </div>
                    @endif


                    {!! Form::model(Request::only('username'), ['route' => ['login'], 'method' => 'POST', 'class' => 'form-horizontal', 'role' => 'form']) !!}

                    <div class="form-group form-group-lg has-feedback">

                        {!! Form::label('username', 'Nombre de Usuario', ['class' => 'col-md-3 control-label sr-only']) !!}

                        <div class="col-md-12">

                            {!! Form::hidden('confirmation_token', Session::has('confirmation_token')? Session::get('confirmation_token') : old('confirmation_token')  ) !!}

                            {!! Form::text('username', Session::has('username_confirm')? Session::get('username_confirm') : old('username'), ['class' => 'form-control', 'autofocus' => 'on', 'placeholder' => 'NOMBRE DE USUARIO']) !!}


                            <span class="glyphicon glyphicon-user form-control-feedback" aria-hidden="true"></span>
                        </div>
                    </div>


                    <div class="form-group form-group-lg has-feedback">
                        {!! Form::label('password', 'Contraseña', ['class' => 'col-md-3 control-label sr-only']) !!}

                        <div class="col-md-12">
                            {!! Form::password('password', ['class' => 'form-control', 'autofocus' => 'on', 'autocomplete' => 'off', 'placeholder' => 'CONTRASEÑA']) !!}

                            <span class="glyphicon glyphicon-lock form-control-feedback" aria-hidden="true"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    {{-- <input type="checkbox" name="remember"> Recordarme --}}
                                </label>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-12">
                            {!! Form::submit('Acceder', ['class' => 'btn btn-raised btn-primary btn-lg btn-block mdb']) !!}

                     <!--        <a class="btn btn-link" href="{{ url('/password/email') }}">¿Olvidaste tu contraseña?</a> -->
                    </br>
                     </br>
                     <a>           Compatible con Chrome,Firefox,Opera o Safari.</a>
                        </div>
                    </div>

                    {!! Form::close() !!}


                </div>

            </div>
        </div>
    </div>
@endsection
