

@extends('app')

@section('container')
<div class="container-fluid " >
	<div class="row">
		<div class="col-md-6 col-lg-4 col-md-offset-3 col-lg-offset-4" style="margin-top: 2em">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><h2>Confirmar</h2></div>
				<div class="panel-body">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong>
						</div>
					@endif

					
					@if(Session::has('message'))
						<div class="alert alert-info">
							<h4><strong>Hola!</strong> {{ Session::get('message') }} </h4>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login/confirm') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						{{-- <div class="form-group">
							<label class="col-md-3 control-label">E-Mail</label>
							<div class="col-md-8">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div> --}}

						@foreach($userData as $usuario)
							{{-- <div class="form-group">
								<label class="col-md-3 control-label">Nombre de Usuario</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="username" disabled="" value="">
								</div>
							</div> --}}
						<input type="hidden" name="username" value="{{ $usuario->username }}">
						@endforeach

  
						<div class="form-group">
							<label class="col-md-3 control-label">Contraseña</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="password" autofocus>
							</div>
						</div>

						{{-- 	<div class="form-group">
								<div class="col-md-6 col-md-offset-3">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="remember"> Recordarme
										</label>
									</div>
								</div>
							</div>
 --}}
						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<button type="submit" class="btn btn-raised btn-primary">Entrar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
