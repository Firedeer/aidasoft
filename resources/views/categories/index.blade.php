@extends('layout_principal')


@section('container')
    <div class="col-md-12">
        <div class="panel">
            <table class="table table-bordered table-condensed text-middle small">
                @foreach($categories as $category)
                    <tr>
                        <td>
                            <a href="{!! route('category.show', $category->id) !!}">{!! $category->description !!}</a>
                        </td>

                        <td width="80">
                            <a href="{!! route('category.edit', $category->id) !!}" class="btn btn-xs btn-default"
                               title="Editar">
                                <i class="material-icons md-18">mode_edit</i>
                            </a>

                            <a class="btn btn-xs btn-default" title="Eliminar">
                                <i class="material-icons md-18">delete</i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection


{{--<td width="40">--}}
{{--<div class="checkbox checkbox-mini">--}}
{{--<label>--}}
{{--<input type="checkbox">--}}
{{--</label>--}}
{{--</div>--}}
{{--</td>--}}

{{--<td>--}}
{{--<div class="togglebutton">--}}
{{--<label>--}}
{{--<input type="checkbox" checked>--}}
{{--</label>--}}
{{--</div>--}}
{{--</td>--}}