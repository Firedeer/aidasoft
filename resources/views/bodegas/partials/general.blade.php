<div class="form-group mdb">
    {!! Form::label('description', 'Descripción', ['class' => 'control-label']) !!}
    {!! Form::text('description', old('description'), [ 'class' => 'form-control']) !!}
</div>

<div class="form-group mdb">
    {!! Form::label('sucursal_id', 'Sucursal', ['class' => 'control-label']) !!}
    {!! Form::select('sucursal_id', \TradeMarketing\Models\Sucursal::listing(), old('sucursal_id'), [ 'class' => 'form-control'] ) !!}
</div>

<div class="form-group mdb">
    {!! Form::label('colour', 'Color', ['class' => 'control-label']) !!}
    {!! Form::color('colour', old('colour'), [ 'class' => 'form-control']) !!}
</div>