@foreach($bodegas as $bodega)
    <tr>
        <td>
            <a href="{!! url('warehouse/admin', $bodega->id ) !!}">{!! $bodega->description !!}
            </a>

            @if($bodega->main)
                <i class="material-icons" style="color:#ffc107;">star</i>
            @endif
        </td>

        <td>{!! $bodega->sucursal->DESCR_SUCURSAL !!}</td>

        <td>{!! $bodega->sucursal->region->DESCR_REGION !!}</td>

        <td>{!! $bodega->stock->sum('stock') !!}</td>

        <td width="40">
            <div class="togglebutton">
                <label>
                    <input type="checkbox" name="active"
                           @if(!$bodega->deleted_at)
                           checked="checked"
                            @endif>
                </label>
            </div>
        </td>

        <td width="40">
            <button class="btn btn-sm mdb" data-bodega_id="{!! $bodega->id !!}" data-toggle="modal"
                    data-target="#editBodega" data-backdrop="static">
                <span class="glyphicon glyphicon-pencil"></span>
            </button>
        </td>
    </tr>
@endforeach