<table class="table table-bordered small">

    <thead>
    <tr>
        <th>{!! trans('app.attributes.date') !!}</th>
        <th>{!! trans('app.attributes.warehouse') !!}</th>
        <th>{!! trans('app.attributes.description') !!}</th>
    </tr>
    </thead>
    @foreach($transfer->tracking as $tracking)
        <tr>
            <td>{!! $tracking->made_at !!}</td>
            <td>{!! $tracking->warehouse !!}</td>
            <td>{!! $tracking->description !!}</td>
        </tr>
    @endforeach
</table>



