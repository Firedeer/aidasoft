@foreach($transfer->details as $key => $detail)

    @if($detail->pending($transfer->order_id))

        <tr data-detail="{!! $detail->id !!}">
            <td class="cell-article active">{!! $detail->article->description !!}

                {!! Form::hidden('items['.$key.'][article]',
                old('items.'.$key.'.article') | $detail->article_id,
                [ 'class' => 'name_handler', 'autocomplete' => 'off']) !!}
            </td>

            <td class="cell-internal_reference active hidden-xs">{!! $detail->article->internal_reference !!}</td>

            <td class="cell-measurement-unit active hidden-xs">
                {!! $detail->article->measurementUnit !!}
            </td>

            <td class="active">{!! $detail->pending($transfer->order_id) !!}</td>

            <td class="cell-stock active hidden-xs">

                {!! old('items.'.$key.'.stock') ? old('items.'.$key.'.stock')  : $detail->stock($transfer->bodega_id)  !!}

                {!! Form::hidden(
                'items['.$key.'][stock]',
                $detail->stock($transfer->bodega_id) | old('items.'.$key.'.stock'),
                [ 'class' => 'name_handler']) !!}
            </td>

            <td class="cell-available active">
                {!! old('items.'.$key.'.available') ? old('items.'.$key.'.available') : $detail->article->availableByWarehouse($transfer->bodega_id) !!}

                {!! Form::hidden('items['.$key.'][available]',
                $detail->article->availableByWarehouse($transfer->bodega_id) | old('items.'.$key.'.available'),
                [ 'class' => 'name_handler']) !!}

            </td>

            <td class="cell-quantity col-sm-1">

                <span class="text-quantity">
                    {!! isset($detail->quantity)
                    ? $detail->quantity
                    : $detail->pending($transfer->order_id) | old('items.'.$key.'.quantity') !!}
                </span>

                <div class="form-group @if($errors->has('items.'.$key.'.quantity')) has-error @endif">

                    {!! Form::textarea('items['. $key .'][quantity]',
                    isset($detail->quantity)
                    ? $detail->quantity
                    : $detail->pending($transfer->order_id) | old('items.'.$key.'.quantity'),
                    [ 'class' => 'form-control name_handler', 'autocomplete' => 'off', 'autofocus'])  !!}

                    {!! $errors->first('items.'.$key.'.quantity', '<p class="text-danger">:message</p>')  !!}
                </div>
            </td>

            <td width="40">
                <a href="#" class="btn text-danger" role="delete-row">
                    <i class="material-icons md-18">delete</i>
                </a>
            </td>
        </tr>

    @endif

@endforeach