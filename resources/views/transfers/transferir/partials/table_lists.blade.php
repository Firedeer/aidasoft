<tbody id="app-container" class="text-uppercase">


@foreach($transfers as $transfer)
@if($transfer->status_id=='12')

    <tr>

        <td width="100" title="{!! $transfer->transferred_at !!}">
            {!! isset($transfer->transferred_at) ? $transfer->transferredAt() : '' !!}
        </td>

        <td>
            <a href="{!! route('transfer.show', $transfer->id) !!}">{!! $transfer->transfer_num !!}</a>
        </td>


        <td class="hidden-xs">
            {!! $transfer->transfer_reference !!}
        </td>

        <td>
            {!! $transfer->warehouse_origen !!}
            <i class="material-icons md-18">&#xE915;</i>
            {!! $transfer->warehouse_destination !!}
        </td>

        <td>
            @include('partials.status', ['status' => $transfer->status])
        </td>

        <td class="hidden-xs">
            @if(isset($transfer->transferred_by))

                <span hidden>{!! $transfer->transferredBy->fullName !!}</span>

                @include('partials.contact-chip',  [
                       'role' => $transfer->transferredBy->role->description,
                       'shortName' => $transfer->transferredBy->nameProfile,
                       'mini' => true
                       ])
            @endif
        </td>
    </tr>

@endif
@endforeach

</tbody>
