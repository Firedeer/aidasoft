@extends('layout_principal')

@section('container')

    <section class="col-md-10 col-md-offset-1">

        <div class="panel">

            <div class="panel-heading">


                <div class="pull-right">
                    <ul class="nav nav-pills">
                        <li role="presentation" class="active" href="#transfer" aria-controls="transfer"
                            role="tab"
                            data-toggle="tab">
                            <a href="#">
                                <i class="material-icons md-18">&#xE915;</i>
                                <span class="hidden-xs">{!! trans('app.attributes.transfer') !!}</span>
                            </a>
                        </li>
                        <li role="presentation" href="#tracking" aria-controls="tracking" role="tab"
                            data-toggle="tab">
                            <a href="#">
                                <i class="material-icons md-18">&#xE889;</i>
                                <span class="hidden-xs">{!! trans('app.attributes.tracking') !!}</span>
                            </a>
                        </li>
                    </ul>
                </div>


                @if($transfer->transferred_at)
                    @include('partials.contact-chip',  [
                    'role' => $transfer->transferredBy->role->description,
                    'userName' => $transfer->transferredBy->fullName,
                    'shortName' => $transfer->transferredBy->nameProfile,
                    'time' => $transfer->transferred_at,
                    'message' => ' inicio una transferencia.',
                    ])

                @else

                    @include('partials.contact-chip',  [
                      'role' => $transfer->createdbY->role->description,
                      'userName' => $transfer->createdbY->fullName,
                      'shortName' => $transfer->createdbY->nameProfile,
                      'time' => $transfer->created_at,
                      'message' => ' creo una transferencia.'
                      ])
                @endif


                <h1>
                    <small>{!! trans('app.attributes.transfer') !!}</small>
                    </br>
                    {!! $transfer->transfer_num !!}
                </h1>

                <small>{!! $transfer->created_at !!}</small>
            </div>


            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="transfer">


                    <div class="panel-body">

                        <div class="form-horizontal form-condensed">
                            <div class="row-sm">
                                <div class="col-sm-6">


                                    <div class="form-group mdb">
                                        <label for=""
                                               class="control-label col-md-3">{!! trans('app.attributes.warehouse_origen') !!}</label>
                                        <div class="col-md-9">
                                            <div class="form-control">{!! $transfer->warehouseOrigen !!}</div>

                                        </div>
                                    </div>


                                    <div class="form-group mdb">
                                        <label for=""
                                               class="control-label col-md-3">{!! trans('app.attributes.warehouse_destination') !!}</label>
                                        <div class="col-md-9">
                                            <div class="form-control">{!! $transfer->warehouseDestination !!}</div>
                                        </div>
                                    </div>


                                    <div class="form-group mdb">
                                        <label for="" class="control-label col-md-3">De</label>
                                        <div class="col-md-9">
                                            <div class="form-control">{!! isset($transfer->transferredBy )? $transfer->transferredBy->fullName  : '' !!}</div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-6">


                                    <div class="form-group mdb">
                                        <label for="" class="control-label col-md-3"># de Pedido</label>
                                        <div class="col-md-9">
                                            <div class="form-control">{!! $transfer->orderNum !!}</div>
                                        </div>

                                    </div>


                                    <div class="form-group mdb">
                                        <label for="" class="control-label col-md-3">Referencia de transporte</label>
                                        <div class="col-md-9">
                                            <div class="form-control">{!! $transfer->transfer_reference !!}</div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--.panel-body-->


                    <div class="panel-body">

                        <legend>{!! trans('app.attributes.articles') !!}</legend>

                        <div class="row-sm">
                            <table id="table-transfer-details" class="table table-bordered small">
                                <thead>
                                <th>{!! trans('app.attributes.description') !!}</th>
                                <th class="hidden-xs">{!! trans('app.attributes.internal_reference') !!}</th>
                                <th class="hidden-xs">{!! trans('app.attributes.measurement_unit') !!}</th>
                                <th>{!! trans('app.attributes.pending') !!}</th>
                                <th class="hidden-xs">{!! trans('app.attributes.in_stock') !!}</th>
                                <th>{!! trans('app.attributes.available') !!}</th>
                                <th>A transferir.</th>
                                <th>{!! trans('app.attributes.received') !!}</th>

                                </thead>
                                <tbody class="text-uppercase">


                                @if(isset($transfer->details))
                                    @foreach($transfer->details as $key => $detail)
                                        <tr>
                                            <td class="cell-article">{!! $detail->article->description !!}
                                            </td>

                                            <td class="hidden-xs cell-internal_reference">{!! $detail->article->internal_reference !!}
                                            </td>

                                            <td class="hidden-xs cell-measurement-unit">{!! $detail->article->measurementUnit !!}</td>

                                            <td class="cell-pending">
                                                {!! $detail->pending() !!}
                                                {!! Form::hidden('input-pending',  old('input-pending') | $detail->pending(), ['class' => 'input-pending']) !!}
                                            </td>

                                            <td class="cell-stock hidden-xs">{!! $detail->stock($transfer->bodega_id) !!}
                                            </td>

                                            <td class="cell-available">
                                                {!! $detail->article->availableByWarehouse($transfer->bodega_id) !!}
                                            </td>

                                            <td class="cell-quantity">
                                                {!! $detail->quantity !!}
                                                {!! Form::hidden('input-quantity',  old('input-quantity') | $detail->quantity, ['class' => 'input-quantity']) !!}

                                                @if($transfer->isDraft() )
                                                    <span class="text-capitalize">(Reservado)</span>
                                                @endif
                                            </td>

                                            <td>{!! $detail->received_quantity !!}
                                            </td>
                                        </tr>


                                    @endforeach
                                @endif
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <!--.panel-body-->

                    <div class="panel-body">
                        @include('partials.observation_field', ['textarea' => isset($transfer->observation)? $transfer->observation : ''])
                    </div>
                    <!--.panel-body-->

                </div>
                <!--.tabpanel-->

                <div role="tabpanel" class="tab-pane" id="tracking">

                    <div class="panel-body">

                        <legend class="pull-left">{!! trans('app.attributes.tracking') !!}</legend>

                        <div class="row-sm">
                            @include('transfers.partials.tracking')
                        </div>
                    </div>
                </div>
                <!--.tabpanel-->
            </div>
            <!-- .tab-content -->

        </div>
        <!-- .panel -->

    </section>



    @if(count($errors->transfer_reference))


        <script>

            var title = '', text = '';
            switch ('{!! $errors->transfer_reference->first('transfer_reference', ':message')  !!}') {
                case 'unique':
                    text = 'La ' + '{!! trans('app.attributes.transfer_reference')!!}' + ' ya ha sido registrada.';
                    break;

                case 'required':
                    text = 'La ' + '{!! trans('app.attributes.transfer_reference')!!}' + ' es requerida.';
                    break;
            };

            swal({
                    title: '',
                    text: text,
                    type: "input",
                    showCancelButton: true,
                    closeOnConfirm: false
                },
                function (inputValue) {
                    if (inputValue === false) return false;

                    if (inputValue === "") {
                        swal.showInputError("El campo " + '{!! trans('app.attributes.transfer_reference')!!}' + " es obligatorio.");
                        return false
                    }

                    var form = $('#form-transfer');
                    $('#form-transfer').find('input[name="transfer_reference"]').val(inputValue);
                    $('#form-transfer').find('#transfer_confirm').click();
                });
        </script>
    @endif

@endsection


@section('extra_scriptBody')
    <script>
        $('#tab a').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).tab('show');
        });

        // store the currently selected tab in the hash value
        $("ul#tab > li > a").on("shown.bs.tab", function (e) {
            e.preventDefault();
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });


        $(document).ready(function () {
            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#tab a[href="' + hash + '"]').tab('show');
        })

    </script>


@endsection


@section('header')
    <div class="col-md-12">
        <div class="row">
            <div class="navbar">
                @include('transfers.partials.step_state', ['status' => $transfer->status])

                <div class="clearfix"></div>

                <div class="container-fluid">

                    <div class="navbar-btn">
                        @include('transfers.partials.action_controls')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! url('/') !!}">Inicio</a></li>
        <li><a href="{!! route('transfer.all') !!}">Transferencias</a></li>
        <li class="active">{!! $transfer->transfer_num !!}</li>
    </ol>
@endsection



