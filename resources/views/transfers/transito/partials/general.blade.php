<div class="col-sm-6">


    <div class="form-group mdb @if($errors->has('bodega_id')) has-error @endif">
        {!! Form::label('bodega_id', trans('app.attributes.warehouse_origen'), ['class' => 'control-label col-md-3']) !!}
        <div class="col-md-9">
            {!! Form::select('bodega_id', \TradeMarketing\Models\Warehouse::listing(), old('bodega_id'), ['class' => 'form-control']) !!}

            {!! $errors->first('bodega_id', '<p class="text-danger">:message</p>')  !!}
        </div>
    </div>


    <div class="form-group mdb @if($errors->has('bodega_id_end')) has-error @endif">
        {!! Form::label('bodega_id_end', trans('app.attributes.warehouse_destination'), ['class' => 'control-label col-md-3']) !!}
        <div class="col-md-9">
            {!! Form::select('bodega_id_end', \TradeMarketing\Models\Warehouse::listing(), old('bodega_id_end'), ['class' => 'form-control']) !!}

            {!! $errors->first('bodega_id_end', '<p class="text-danger">:message</p>')  !!}
        </div>
    </div>


    {{--<div class="form-group mdb @if($errors->has('transferred_by')) has-error @endif">--}}
        {{--{!! Form::label('transferred_by', 'De', ['class' => 'control-label col-md-3' ]) !!}--}}

        {{--<div class="col-md-9">--}}
            {{--{!! Form::select('transferred_by', \TradeMarketing\User::listing() , old('transferred_by'), ['class' => 'form-control']) !!}--}}

            {{--{!! $errors->first('transferred_by', '<p class="text-danger">:message</p>')  !!}--}}
        {{--</div>--}}
    {{--</div>--}}


    {{--<div class="form-group mdb @if($errors->has('transferred_for')) has-error @endif">--}}

        {{--{!! Form::label('transferred_for', 'Para', ['class' => 'control-label col-md-3' ]) !!}--}}
        {{--<div class="col-md-9">--}}
            {{--{!! Form::select('transferred_for', \TradeMarketing\User::listing() , old('transferred_for'), ['class' => 'form-control']) !!}--}}

            {{--{!! $errors->first('transferred_for', '<p class="text-danger">:message</p>')  !!}--}}
        {{--</div>--}}
    {{--</div>--}}


</div>


<div class="col-sm-6">


    <div class="form-group mdb @if($errors->has('order')) has-error @endif">
        {!! Form::label('order', trans('app.attributes.order_num'), ['class' => 'control-label col-md-3']) !!}

        <div class="col-md-9">
            {!! Form::text('order',
            old('order') | isset($transfer->orderNum)? $transfer->orderNum : '',
            [ 'class' => 'form-control' ]) !!}

            {!! $errors->first('order', '<p class="text-danger">:message</p>')  !!}
        </div>
    </div>


    <div class="form-group mdb @if($errors->has('transfer_reference')) has-error @endif">
        {!! Form::label('transfer_reference', trans('app.attributes.transfer_reference'), ['class' => 'control-label col-md-3']) !!}
        <div class="col-md-9">
            {!! Form::text('transfer_reference', old('transfer_reference'), ['class' => 'form-control', 'placeholder' => 'Referencia de Transporte']) !!}

            {!! $errors->first('transfer_reference', '<p class="text-danger">:message</p>')  !!}
        </div>
    </div>

</div>

