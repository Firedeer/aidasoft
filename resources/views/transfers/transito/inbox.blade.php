@extends('layout_principal')

@section('container')

    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                <table class="table table-bordered table-condensed text-middle small" id="table-transfers">
                    <thead class="hidden-xs hidden-sm">
                    <tr>
                        <th>{!! trans('app.attributes.date') !!}</th>
                        <th>{!! trans('app.attributes.transfer_num') !!}</th>
                        <th>{!! trans('app.attributes.transfer_reference') !!}</th>
                        <th>{!! trans('app.attributes.from_user') !!}</th>
                        <th>{!! trans('app.attributes.for_user') !!}</th>
                        <th>{!! trans('app.attributes.status') !!}</th>
                        <th>{!! trans('app.attributes.created_by') !!}</th>
                    </tr>
                    </thead>

                    @include('transfers.transito.partials.table_lists')

                </table>

            </div>
        </div>
    </div>

@endsection



@section('extra_scriptBody')
    @include('partials.dataTables_script', ['buttons' => true])

    <script>
        $(document).ready(function () {
            $('#table-transfers').dataTableConfig({
                "dom": 'Bfrtip',
                "buttons": [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection


@section('header')
    <div class="col-md-12">
        <div class="row">
            <div class="navbar" style="background-color: transparent">
                <div class="container-fluid">

                    <div class="navbar-btn">

                        @include('partials.dataTables_filter', ['table' => 'table-transfers'])

                        @include('partials.dataTables_exports')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('transfer.all') !!}">Transferencias</a></li>
        <li class="active">Entrantes</li>
    </ol>
@endsection