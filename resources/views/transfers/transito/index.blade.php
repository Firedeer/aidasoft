@extends('layout_principal')

@section('container')


    <div class="col-md-12">
        <div class="page-header">
            <h1>{!! trans('app.attributes.transfers') !!}</h1>
        </div>
    </div>

    <div class="col-md-12">

        <div class="row-sm">
            <div class="panel">

                <div class="panel-heading">
                    <div class="pull-right">
                        @include('partials.dataTables_exports')
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row-sm">
                        <table class="table table-bordered table-condensed text-middle small" id="table-transfers">
                            <thead class="hidden-xs">
                            <tr>
                                <th>{!! trans('app.attributes.date') !!}</th>
                                <th>{!! trans('app.attributes.transfer_num') !!}</th>
                                <th class="hidden-xs">{!! trans('app.attributes.transfer_reference') !!}</th>
                                <th>{!! trans('app.attributes.warehouse') !!}</th>
                                <th>{!! trans('app.attributes.status') !!}</th>
                                <th class="hidden-xs" width="40">{!! trans('app.attributes.user') !!}</th>
                            </tr>

                            </thead>

                            @include('transfers.transito.partials.table_lists')

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@section('extra_scriptBody')

    <!-- Modal -->
    <div class="modal fade" id="cancel-transfer-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Cancelar Transferencia</h4>
                </div>
                <div class="modal-body">
                    Estas seguro que deseas cancelar la transferencia?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary mdb" id="cancel-transfer">Confirmar</button>
                </div>
            </div>
        </div>
    </div>

    <script>

        $('#cancel-transfer-modal').on('show.bs.modal', function (event) {

            event.stopPropagation();
            var button = $(event.relatedTarget);
            var $recipient = button.data('transfer');
            var route = button.data('route');


            var modal = $(this);

            $('#cancel-transfer').click(function () {


                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: route,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response, status) {
                        console.log(response);
                    }
                });
            });
        });
    </script>


    <script>

        $('a[name="confirm-reception"]').click(function (event) {
            event.preventDefault();
            var route = event.target.href;
            $.ajax({
                url: route,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'GET',
                dataType: 'json',
                succcess: function (data) {
                    console.log(data);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        });
    </script>

    <!-- Moment.js 2.14.1 -->
    <script src="{!! asset('moment_js/moment.js') !!}"></script>

    <script src="{!! asset('moment_js/locale/es.js') !!}"></script>


    <!-- DatePiker -->
    <!-- Bootstrap DatePiker Pluging CSS -->
    <link rel="stylesheet"
          href="{!! asset('bootstrap/bootstrap-datetimepicker-4/css/bootstrap-datetimepicker.css') !!}">

    <!-- Bootstrap DatePiker Pluging JS -->
    <script src="{!! asset('bootstrap/bootstrap-datetimepicker-4/js/bootstrap-datetimepicker.js') !!}"></script>


    @include('partials.dataTables_script', ['buttons' => true])

    <script>
        var dpOptions = {
            useCurrent: false,
            format: 'DD-MM-YYYY',
            locale: 'es',
            tooltips: {
                today: 'Ir a hoy',
                clear: 'Selección clara',
                close: 'Cierre el selector',
                selectMonth: 'Seleccione mes',
                prevMonth: 'Mes anterior',
                nextMonth: 'Próximo mes',
                selectYear: 'Seleccione Año',
                prevYear: 'Año anterior',
                nextYear: 'El próximo año',
                selectDecade: 'Seleccione Decade',
                prevDecade: 'Década anterior',
                nextDecade: 'Próxima Década',
                prevCentury: 'Siglo anterior',
                nextCentury: 'Próximo siglo'
            }
        };

        function formatISODate(date) {
            var array = date.split("-");
            return array[1] + "/" + array[0] + "/" + array[2];
        }

        $(document).ready(function () {
            var oTable = $('#table-transfers').dataTableConfig({
                "dom": 'Bfrtip',
                "buttons": [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });


            $("#datepicker_from").datetimepicker(dpOptions)
                .on('dp.change', function (e) {
                    minDateFilter = new Date(formatISODate(e.target.value)).getTime();
                    oTable.draw();
                });

            $("#datepicker_to").datetimepicker(dpOptions)
                .on('dp.change', function (e) {
                    maxDateFilter = new Date(formatISODate(e.target.value)).getTime();
                    oTable.draw();
                });
        });

        // Date range filter
        minDateFilter = "";
        maxDateFilter = "";

        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {

                if (typeof aData._date == 'undefined') {
                    aData._date = new Date(formatISODate(aData[0])).getTime();
                }

                if (minDateFilter && !isNaN(minDateFilter)) {
                    if (aData._date < minDateFilter) {
                        return false;
                    }
                }

                if (maxDateFilter && !isNaN(maxDateFilter)) {
                    if (aData._date > maxDateFilter) {
                        return false;
                    }
                }

                return true;
            }
        );
    </script>
@endsection



@section('header')


    <div class="col-md-12">
        <div class="row">
            <div class="navbar">
                <div class="container-fluid">

                    <div class="col-md-12">
                        <div class="row">
                            <div class="navbar-btn col-md-6">
                                <div class="row-sm">

                                    <div> @include('partials.dataTables_filter', ['table' => 'table-transfers'])
                                    @include('transfers.partials.menu_transfer')
                                    </div>
                                    <div class="clearfix"></div>

                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="row-sm">
                                    <div id="date_filter" class="form-horizontal">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label id="date-label-from"
                                                       class="date-label control-label col-xs-2 hidden-xs">
                                                    {!! trans('app.attributes.from') !!}:
                                                </label>

                                                <div class="col-xs-10">
                                                    <div class="input-group">
                                                        <input id="datepicker_from"
                                                               class="date_range_filter date form-control"
                                                               type="text"
                                                               placeholder="{!! trans('app.attributes.initial_date') !!}"/>
                                                        <span class="input-group-addon">
                                                        <i class="material-icons md-18">&#xE916;</i>
                                                    </span>
                                                    </div>
                                                    <!-- .input-group -->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .col -->

                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label id="date-label-to"
                                                       class="date-label control-label col-xs-2 hidden-xs">
                                                    {!! trans('app.attributes.to') !!}:
                                                </label>

                                                <div class="col-xs-10">
                                                    <div class="input-group">
                                                        <input id="datepicker_to"
                                                               class="date_range_filter date form-control"
                                                               type="text"
                                                               placeholder="{!! trans('app.attributes.final_date') !!}"/>

                                                        <span class="input-group-addon">
                                                        <i class="material-icons md-18">&#xE916;</i>
                                                    </span>
                                                    </div>
                                                    <!-- .input-group -->
                                                </div>
                                            </div>

                                        </div>
                                        <!-- .col -->
                                    </div>
                                </div>
                            </div>
                            <!-- .col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('breadcrumb')
    <ol class="breadcrumb">
    <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('transfer.all') !!}">{!! trans('app.attributes.transfers') !!}</a></li>
        <li class="active">En transito</li>
    </ol>
@endsection