<div class="col">
    <div class="row">


        <div class="navbar">


            <div class="container-fluid">

                <div class="navbar-btn">



                    <ul id="tab" class="nav nav-pills tabs-flex" role="tablist">
   <!--INVENTARIO-->
                        <li role="presentation">
                            <a class="btn  btn-light" href="{!! route('transfer.transito') !!}"><i class="material-icons md-18">local_shipping</i> En transito</a>
                        </li>

    <!--MOVIMIENTOS-->
                        
                        <li role="presentation" >
                        
                            <a class="btn  btn-light" href="{!! route('transfer.transferir') !!}"><i class="material-icons md-18">open_in_browser</i> Pendientes </a>
                        </li>
    <!--TRANSFERENCIAS-->
                        <li role="presentation" >
                            <a class="btn  btn-light" href="{!! route('transfer.confirmados') !!}"><i class="material-icons md-18">event_available</i> Confirmados</a>
                        </li>



                        <!-- La ruta de external movement esta en Routes -->
                        <li class="pull-right " >
                            <a class="btn  btn-light" href="{!! route('transfer.parciales') !!}">
                            <i class="material-icons md-18">event_note</i> Confirmados parcial
                            </a>
                        </li>



                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>






