@if($transfer->isDraft())

    {!! Form::open(['route' => ['transfer.destroy', $transfer->id], 'method' => 'DELETE', 'id' => 'form-transfer-delete']) !!}
    {!! Form::close() !!}

    <button type="button"
            class="btn pull-right"
            onclick="deleteTransfer()">
        Eliminar
        <i class="material-icons md-18">&#xE92B;</i>
    </button>

    <script>
        function deleteTransfer() {
            swal({
                    title: "¿Estas seguro que quieres eliminar esta transferencia?",
                    text: "¡No podrás recuperar esta transferencia!",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancelar",
                    confirmButtonText: "Si, Quiero Eliminarlo!",
                    closeOnConfirm: true
                },
                function () {
                    $('#form-transfer-delete').submit();
                });
        }
    </script>
@endif

<a href="{!! route('order.show', $transfer->order_id) !!}" class="btn">
    <!-- reply -->
    <i class="material-icons md-18">&#xE15E;</i>Ir al Pedido
</a>

@can('update', $transfer)

    @if( $transfer->status == 'draft' )
        <a href="{!! route("transfer.edit", $transfer->id) !!}" class="btn">
            Editar<i class="material-icons md-18">&#xE254;</i>
        </a>
    @endif


    @if( $transfer->status == 'in_transit' || $transfer->status == 'for_transfer' )
        <button type="button" class="btn btn-warning  pull-right"
                onclick="document.getElementById('transfer-cancel').submit()">
            Cancelar<i class="material-icons md-18">&#xE14C;</i>
        </button>

        {!! Form::open(['route' => ['transfer.cancel', $transfer->id], 'method' => 'GET', 'id' => 'transfer-cancel',  'class' => 'hidden']) !!}
        {!! Form::close() !!}
    @endif
@endcan





 <!-- AREA DE BOTONES DE SOLICITAR TRANSFERENCIA O TRANSFERIR -->


<!-- TRANSFERIR -->
               @if($transfer->status_id==1)
                     
                               <button type="button" class="btn"
                               onclick="document.getElementById('transfer_confirm').click()">
                               Transferir <i class="material-icons md-18">&#xE8D4;</i>
                               </button>
                    

<!-- SOLICITAR TRANSFERENCIA -->

                           @can('requestTransfer', $transfer)
                            <button type="button" class="btn"
                          onclick="document.getElementById('form-transfer-nose').submit()">
                                   Solicitar transferencia
                                 </button>

                             {!! Form::open(['route' => ['transfer.nose', $transfer->id], 'method' => 'GET', 'id' =>            'form-transfer-nose', 'class' => 'hidden']) !!}
                                {!! Form::close() !!}
                             @endcan
               @endif



    {!! Form::model($transfer, ['route' => ['transfer.transfer', $transfer->id], 'method' => 'PUT', 'id' => 'form-transfer',  'class' => 'hidden']) !!}
    <input type="checkbox"
           name="transfer_confirm"
           id="transfer_confirm"
           class="hidden"
           onclick="document.getElementById('form-transfer').submit()">

    {!! Form::text('transfer_reference', old('transfer_reference')) !!}
    {!! Form::close() !!}





 <!-- AREA DE BOTONES DE SOLICITAR TRANSFERENCIA O TRANSFERIR -->









 <!-- BOTON DE CONFIRMACION SE AGREGO UN IF PARA ADMIN SABER SI EL ADMIN ES QUIEN RECIBE LA MERCANCIA 09/12/2017  NCT02167 -->


 <!-- BOTON DE CONFIRMAR BODEGA CENTRAL-->
 
 @if($transfer->warehouseDestination ==currentUser()->warehouses[0]->description)
<button type="button" id="btn-confirm" class="btn"> Confirmar </button>
@endif
 <!-- BOTON DE CONFIRMAR BODEGA CENTRAL-->
 @if(currentUser()->authRole('admin') && $transfer->status_id!=1)
    <button type="button" id="btn-confirm" class="btn"> Confirmar </button>
@endif
    {!! Form::open(['route' => ['transfer.receipt.confirm', $transfer->id], 'method' => 'GET', 'id' => 'form-transfer-receipt', 'class' => 'hidden']) !!}
    <input type="checkbox" id="confirm_partial" name="confirm_partial">
    {!! Form::close() !!}



    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">
                        Las cantidades recibidas no coinciden con las solicidadas en el pedido.
                    </h4>
                </div>
                <div class="modal-body">


                    <h3>Se ha detectado que su mercancia esta incompleta.</h3>

                    <p class="text-muted"> Puedes procesar tu pedido parcialmente seleccionando <strong>confirmar
                            parcial</strong> si recibiras los artículos restantes en otro momento;
                        de lo contrario indica <strong>Confirmar completo</strong> para procesar la compra actual como
                        completo.
                    </p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary mdb" onclick="confirmReception(true)">Confirmar
                        completo
                    </button>
                    <button type="button" class="btn btn-primary mdb" onclick="confirmReception()">Confirmar Parcial
                    </button>
                    <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            $('#btn-confirm').on('click', function (event) {


                var tbody = $('#table-transfer-details tbody tr');
                var i = 0;

                tbody.each(function (key, item) {
                    var pending = parseInt($(item).find('.cell-pending .input-pending').val());
                    var quantity = parseInt($(item).find('.cell-quantity .input-quantity').val());

                    if (quantity < pending && quantity >= 0) {
                        i++;
                    }
                });

                if (i > 0) {

                    $('#myModal').modal('show');

                } else if (i == 0) {
                    confirmReception(true);
                }
            });
        });


        function confirmReception(confirm_all) {

            confirm_all = (confirm_all) ? confirm_all : false;

            var form = $('#form-transfer-receipt');

            $('#myModal').modal('hide');

            if (confirm_all) {

                console.log('todo');
//
                form.submit();
            } else {
                console.log('parcial')

                $('#confirm_partial').click();

                form.submit();
            }
        }
    </script>
