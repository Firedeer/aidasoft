<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Administracion y control de inventario </title>

    <!-- Bootstrap core CSS -->
{!!Html::style('/bootstrap/3.3.7/css/bootstrap.css')!!}

<!-- Bootstrap Material Design CSS -->
{!!Html::style('/bootstrap/material-design/css/bootstrap-material-design.css')!!}
{!!Html::style('/bootstrap/material-design/css/ripples.min.css')!!}

<!--  Style CSS -->
    {!! Html::style('/assets/css/app.css') !!}

    <link rel="stylesheet" href="{{ asset('/material-design-table/material_design_table.css')}}">


    @section('extra_scriptHead')
    @show
</head>

<body class="color">
{{-- <ul class="nav navbar-nav navbar-right">
    @if (Auth::guest())
        <li><a href="{{ url('/auth/login') }}">Login</a></li>
        <li><a href="{{ url('/auth/register') }}">Register</a></li>
    @else
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
            </ul>
        </li>
    @endif
</ul> --}}

@yield('container')

<!-- JQuery Pluging -->
{!!Html::script('/jquery/2.2.2/jquery.min.js')!!}
<!-- Bootstrap JS -->
{!! Html::script('/bootstrap/3.3.7/js/bootstrap.min.js') !!}

<!-- Bootstrap Meterial Design JS -->
{!! Html::script('/bootstrap/material-design/js/material.js') !!}
{!! Html::script('/bootstrap/material-design/js/ripples.min.js') !!}
<script>
    // init bootstrap material design
    $.material.init()
</script>
</body>
</html>
