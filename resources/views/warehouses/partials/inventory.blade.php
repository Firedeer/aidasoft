<table id="table-inventory" class="table table-bordered table-condensed text-middle small">

    <thead>
    <tr>
    <th>.</th>
        <th>{!! trans('app.attributes.description') !!}</th>
        <th>{!! trans('app.attributes.internal_reference') !!}</th>
        <th>{!! trans('app.attributes.category') !!}</th>
        <th>{!! trans('app.attributes.brand') !!}</th>
        <th>{!! trans('app.attributes.stock') !!}</th>
        <th>{!! trans('app.attributes.last_movement') !!}</th>
    </tr>
    </thead>
    <tbody>

    @foreach($inventory as $key => $article)

        <tr>
     <td>
                                
                                    <img @if(isset($article->image))
                                         src="{!! asset('assets/img/'.$article->image) !!}"
                                         @else
                                         src="{!! asset('assets/img/trade.jpg') !!}"
                                         @endif class="img-rounded" width="60px" onmouseover="javascript:this.height=300;this.width=300"
                                          onmouseout="javascript:this.width=60;this.height=60"/>
                                         

                            
                            
                            </td>
            <td>
                {!! $article->description !!}

                @if( $article->num_articles > 1 )

                    <button type="button"
                            class="btn btn-xs btn-link mdb"
                            data-toggle="modal"
                            data-target="#showVariantModal"
                            data-title="{!! $article->article_master !!}"
                            data-article="{!! $article->article_master_id !!}"
                            data-route="{!! route('article_master_variants', [$article->article_master_id, $warehouse_id ]) !!}">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>

                @endif

                <span class="visible-xs visible-sm">{!! $article->barcode !!}</span>
                <span class="visible-xs visible-sm">{!! $article->category !!}</span>
                <span class="visible-xs visible-sm">{!! $article->brand !!}</span>
            </td>

            <td class="hidden-xs hidden-sm">{!! $article->internal_reference !!}</td>

            <td class="hidden-xs hidden-sm">{!! $article->category !!}</td>

            <td class="hidden-xs hidden-sm">{!! $article->brand !!}</td>

            <td width="100">

                <a href="javascript:void(0)"
                   data-toggle="popover"
                   data-placement="top"
                   data-container="body"
                   data-content="<div style='with: 100px'>
                        <h6>Stock actual</h6>
                        {!! $article->min_stock !!} <span>minimo</span><br>
                        {!! $article->max_stock !!} <span>maximo</span><br>
                        {!! $article->stock !!} <span>actual</span><br>
                        <small>Ult. movimiento {!! $article->last_movement !!}</small>
                        </div>">

                    @include('partials.stock_state', [
                    'stock' => $article->stock,
                    'min_stock' => $article->min_stock,
                    'max_stock' => $article->max_stock
                    ])
                </a>

            </td>
            <td>{!! $article->last_movement !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>


<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover({
            trigger: 'hover',
            html: true
        });
    });
</script>


<!-- Modal -->
<div class="modal fade" id="showVariantModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary mdb" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#showVariantModal').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget) // Button that triggered the modal
        var article = button.data('article') // Extract info from data-* attributes
        var title = button.data('title');
        var route = button.data('route');
        var modal = $(this);
        var body = modal.find('.modal-body');
        body.html('');
        modal.find('.modal-title').text(title)

        $.ajax({
            url: route,
            type: 'GET',
            dataType: 'json',
            beforeSend: function () {
                body.addClass('ajax loader');
            },
            success: function (res) {
                body.html(res);
            },
            complete: function () {
                body.removeClass('ajax loader');
            }
        });
    })
</script>
