<div class="col-md-12">
    <div class="row">


        <div class="navbar">
            <div class="col-md-12">

            

                <div class="col-md-6">

                    <h3>
                        <i class="material-icons">store_mall_directory</i>{!! $warehouse->description !!}
                    </h3>
                    <small>
                        <p><strong class="">Descripción: </strong>{!! $warehouse->description !!}<br>
                            <strong class="">Region: </strong>{!! $warehouse->sucursal->region->DESCR_REGION !!}<br>
                            <strong class="">Scursal: </strong>{!! $warehouse->sucursal->DESCR_SUCURSAL !!}
                        </p>
                    </small>

                </div>
            </div>

            <div class="container-fluid">

                <div class="navbar-btn">

                    <!-- .navbar-btn -->

                    <!-- Nav tabs -->
                    <!-- style="display:flex; justify-content:center; border-bottom:0" -->


                    <ul id="tab" class="nav nav-pills tabs-flex" role="tablist">
   <!--INVENTARIO-->
                        <li role="presentation">
                            <a href="{!! route('warehouse.inventory', $warehouse->id) !!}">{!! trans('app.attributes.inventory') !!}</a>
                        </li>

    <!--MOVIMIENTOS-->
                        <li role="presentation">
                            <a href="{!! route('warehouse.movements', $warehouse->id) !!}">{!! trans('app.attributes.movements') !!}</a>
                        </li>
    <!--TRANSFERENCIAS-->
                        <li role="presentation">
                            <a href="{!! route('warehouse.transfers', $warehouse->id) !!}">{!! trans('app.attributes.transfers') !!}</a>
                        </li>



                        <!-- La ruta de external movement esta en Routes -->
                        <li class="pull-right">
                            <a class="btn" href="{!! route('external.movement.warehouse.out', $warehouse->id) !!}">
                            Salida de articulos
                            </a>
                        </li>

                        <li class="pull-right"> 
                            <a class="btn" href="{!! route('external.movement.warehouse.in', $warehouse->id) !!}">
                            Entrada de articulos
                            </a>
                        </li>


                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


{{--<div class="col-md-9 pull-right">--}}
{{--<ul class="stats-overview">--}}
{{--<li>--}}
{{--<span class="name">Articulos</span>--}}
{{--<span class="value text-success"> 2300 </span>--}}
{{--</li>--}}
{{--<li>--}}
{{--<span class="name"> Total amount spent </span>--}}
{{--<span class="value text-success"> 2000 </span>--}}
{{--</li>--}}

{{--<li>--}}
{{--<span class="name"> Total amount spent </span>--}}
{{--<span class="value text-success"> 2000 </span>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</div>--}}

{{--<div class="col-md-9">--}}

{{--// content--}}

{{--</div>--}}




