<table class="table table-striped">
    <thead class="text-uppercase">
    <tr>
        <td>Articulo</td>
        <td></td>
        <td>Stock</td>
    </tr>
    </thead>
    <tbody>
    @foreach($articles as $article)
        <tr>
            <td>{!! $article->article->serial_code !!}</td>
            <td>{!! $article->article->description !!}</td>
            <td width="40">{!! $article->stock !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>