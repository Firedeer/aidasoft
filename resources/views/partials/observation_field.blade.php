@if(isset($textarea))


    <div class="form-group mdb">
        {!! Form::label('observation', 'Observación', ['class' => 'control-label ']) !!}
        <div class="form-control" style="height: auto">{!! $textarea !!}</div>
    </div>
@else


    <div class="form-group @if($errors->has('observation')) has-error @endif">
        {!! Form::label('observation', 'Observación', ['class' => 'control-label ']) !!}
        <br><br>
        {!! Form::textarea('observation', old('observation'), ['class' => 'form-control', 'rows' => '3']) !!}

        {!! $errors->first('observation', '<p class="text-danger">:message</p>')  !!}
    </div>


    <script src="{!! asset('assets/js/demo_textarea_handler.js') !!}"></script>

    <script>
        $(document).ready(function () {
            $('#observation').textAreaHandler({
                max: 250
            });
        });
    </script>

@endif