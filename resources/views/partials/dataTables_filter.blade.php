<style>
    #custom-search-input {
        padding: 2px;
        border: solid 1px #E4E4E4;
        background-color: #fff;
    }

    #custom-search-input input {
        border: 0;
        box-shadow: none;
    }


    #custom-search-input button {
        margin: 2px 0 0 0;
        background: none;
        box-shadow: none;
        border: 0;
        color: #666666;
        padding: 0 8px 0 10px;
        border-left: solid 1px #ccc;
    }

    #custom-search-input button:hover {
        border: 0;
        box-shadow: none;
        border-left: solid 1px #ccc;
    }

    #custom-search-input i {
        font-size: 23px;
    }
</style>


<div id="custom-search-input">
    <div class="input-group col-md-12">
        <input type="text" id="table-filter" class="form-control" placeholder="Buscar"/>
        <span class="input-group-btn">
            <button class="btn btn-info" type="button">
             <i class="material-icons">search</i>
            </button>
        </span>
    </div>
</div>


{{--<div id="" role="search">--}}
{{--<div class="input-group">--}}
{{--<input type="text" class="  search-query form-control" placeholder="Search" />--}}
{{--<input type="text" id="table-filter" class="form-control"  placeholder="Buscar..." style="color:#000">--}}
{{--<span class="input-group-btn">--}}
{{--<button class="btn" type="button">--}}
{{--<i class="material-icons md-18">search</i>--}}
{{--<i class="material-icons control-back">arrow_back</i>--}}

{{--<i class="material-icons control-cancel">close</i>--}}
{{--</button>--}}
{{--</span>--}}

{{--</div>--}}
{{--</div>--}}


<script>
    $(document).ready(function () {
        $('#table-filter').keyup(function (event) {
            $('#' + '{!! isset($table)? $table : '' !!}' + '_filter input[type="search"]').val(this.value).keyup();
        });
    });
</script>

