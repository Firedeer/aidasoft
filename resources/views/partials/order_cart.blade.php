<div class="dropdown">

    <a href="#" class="icon" data-toggle="dropdown" title="Ver pedido" style="position: relative;">
        <i class="material-icons md-24">&#xE8CC;</i>

        @if(isset(currentUser()->hasOrder()->details))
            <span class="badge" style="position: absolute; top: 10px; right: 10px;">
            {!!  count(currentUser()->hasOrder()->details) !!}</span>
        @endif
    </a>

    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-shopping-cart"></span> 7 - Items<span class="caret"></span></a>--}}
    <ul class="dropdown-menu dropdown-menu-right dropdown-cart" role="menu">

        @if(isset(currentUser()->hasOrder()->details))

            @foreach(currentUser()->hasOrder()->details as $key => $detail)

                @if($key < 4)

                    <li class="item">
                        <div class="item-left">
                            <img src="{!! asset('assets/img/'.$detail->article->image) !!}"
                                 onerror="this.src='{!! asset('assets/img/_not_available.png') !!}'"
                                 alt=""
                                 style="width: 50px; height: 50px;"/>


                            <div class="item-info">
                                <a href="{!! route('article.show', $detail->article->id) !!}">
                                    {!! $detail->article->description !!}
                                </a>

                                <span class="item-info-quantity btn-link text-lowercase">
                                    {!! $detail->quantity !!}
                                    {!! ($detail->quantity > 1) ? trans('app.attributes.articles') : trans('app.attributes.article') !!}</span>
                            </div>
                        </div>
                    </li>
                @endif
            @endforeach


        @else
            <li>
                <div class="item text-center">
                    <span>El carrito está vacío</span>
                </div>
            </li>
        @endif


        <li class="item">

            <div class="item-left">
                @if(isset(currentUser()->hasOrder()->details))
                    @if(count(currentUser()->hasOrder()->details) > 4)
                        <span class="more-items">
                    {!! count(currentUser()->hasOrder()->details) - 4 !!} +
                    </span>
                    @endif
                @endif
            </div>

            <div class="item-right">
                <a class="btn btn-xs text-middle" href="{!! route('order.create') !!}">Ver el pedido</a>
            </div>
        </li>
    </ul>
</div>


<script>
    function refreshCart() {

        var $get = $.get('{!! route('order.cart.refresh') !!}');
        $get.complete(function (response) {

            if (response.responseJSON.cart)
                $('#shopping-cart-icon').html(response.responseJSON.cart);

        });
    }
</script>
