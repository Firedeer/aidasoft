@if( $order->order_status_id == 1 ) {{--Draft--}}

<span class="label label-default">{!! isset($order->order_status)? trans('app.status.'.$order->order_status) :trans('app.status.'.$order->status->description )  !!}</span>
@elseif(  $order->order_status_id == 5)

    <span class="label label-info">{!! isset($order->order_status)? trans('app.status.'.$order->order_status) : trans('app.status.'.$order->status->description ) !!}</span>

@elseif(  $order->order_status_id == 6)
    <span class="label label-success">{!! isset($order->order_status)? trans('app.status.'.$order->order_status) : trans('app.status.'.$order->status->description )  !!}</span>

@elseif($order->order_status_id == 7)

    <span class="label label-danger">{!! isset($order->order_status)? trans('app.status.'.$order->order_status) : trans('app.status.'.$order->status->description ) !!}</span>
@endif