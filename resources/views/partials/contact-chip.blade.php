<div class="contact-chip @if(isset($mini) === true ) contact-chip-sm @endif">
    <div class="contact-chip-img contact-chip-role-{!! $role !!}">
        {!! $shortName !!}
    </div>

    @if(isset($userName) || isset($message) || isset($time))
        <div class="contact-chip-body">
        <span>
            <span class="contact-title">{!! isset($userName)? $userName : '' !!}</span>
            {!! isset($message)? $message : '' !!}

        </span>

            @if(isset($time))
                <div class="contact-time">
                    <i class="material-icons">&#xE192;</i>
                    {!! $time !!}
                </div>
            @endif
        </div>
    @endif
</div>