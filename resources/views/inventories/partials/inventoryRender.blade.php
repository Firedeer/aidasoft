@foreach($inventory as $inv)
    <tr>
        <td>
            <div class="media">
                <div class="media-left">
                    @if( $inv->articles->image == null )
                        <img src="{{ asset('assets/img/trade.jpg') }}" width="45px"
                             class="media-object img-rounded">
                    @else
                        <img src="{{ asset('assets/img/'.$inv->articles->image ) }}" width="45px"
                             class="media-object img-rounded">
                    @endif
                </div>
                <div class="media-body">
                    {!! $inv->articles->description !!}
                </div>
            </div>
        </td>
        <td>{!! $inv->min_stock !!}</td>
        <td>{!! $inv->max_stock !!}</td>
        <td>{!! $inv->stock !!}</td>
        <td>{!! $inv->date !!}</td>
    </tr>
@endforeach
