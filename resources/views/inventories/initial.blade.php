@extends('layout_principal')

@section('container')




    {!! Form::open(['id' => 'import-data', 'class' => 'form-horizontal' ]) !!}

    <div class="col-md-12">
        <div class="col-md-6">

            <legend>{!! trans('app.attributes.warehouse') !!}</legend>


            <div class="form-group">
                {!! Form::label('warehouse_id', trans('app.attributes.warehouse'), ['class' => 'control-label col-md-2 hidden']) !!}
                <div class="col-md-10">
                    {!! Form::select('warehouse_id', \TradeMarketing\Models\Warehouse::listing(), \TradeMarketing\Models\Warehouse::main()->first()->id, ['class' => 'form-control']) !!}
                </div>
            </div>

        </div>
    </div>



    @include('partials.loader')

    <div class="col-md-12">
        <div class="panel" id="excel-container">

            <div class="panel-body">

                <legend>{!! trans('app.attributes.articles') !!}</legend>
                <table class="table table-condensed table-striped table-control small" id="table-excel">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{!! trans('app.attributes.description') !!}</th>
                        <th>{!! trans('app.attributes.internal_reference') !!}</th>
                        <th>{!! trans('app.attributes.unit_cost') !!}</th>
                        <th>{!! trans('app.attributes.stock') !!}</th>
                    </tr>
                    </thead>
                    <tbody>

                    @include('inventories.table')

                    </tbody>
                </table>

            </div>
        </div>
    </div>

    {!! Form::submit('Send', ['id'=> 'btn-send', 'class' => 'hidden']) !!}
    {!! Form::close() !!}



    <script src="{!! asset('assets/js/send_to_ajax.js') !!}"></script>
    <script>
        $(document).ready(function () {

            var form = $('#import-data');

            $(form).sendToAjax({
                method: 'POST',
                token: '{!! csrf_token() !!}',
                route: '{!! route('import.initial.excel') !!}'
            });

            function nombre(fic) {
                fic = fic.split('\\');
                return fic[fic.length - 1];
            }

            $('#excel_file').on('change', function () {
                var filename = $('#file_name')[0];

                if (this.value != '') {
                    filename.innerHTML = nombre(this.value);
                    processExcel();
                } else {
                    filename.innerHTML = 'No hay archivo';
                }
            });


            function processExcel() {
                var formData = new FormData(document.getElementById('form-upload-excel'));

                $.ajax({
                    headers: {'X-CSRF-TOKEN': '{!! csrf_token() !!}'},
                    type: "POST",
                    url: '{!!  route('process.excel.initial') !!}',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#excel-container').addClass('hidden');
                        $('#sk-loading').toggleClass('hidden');
                        $('#btn-loading-file').button('loading');
                        $('.btn-loading-file').toggleClass('hidden');

                    },
                    success: function (response) {
                        $('#table-excel').find('tbody').html('');
                        $('#table-excel').find('tbody').html(response.html);
                        $('#excel-container').toggleClass('hidden');
                        $('#btn-cancel-process').toggleClass('hidden');
                        $('#btn-process-data').toggleClass('hidden');
                    },
                    error: function (errors) {
                        console.log(errors);
                        $('.btn-loading-file').toggleClass('hidden');
                    },
                    complete: function () {
                        $('#btn-loading-file').button('reset');
                        $('#sk-loading').toggleClass('hidden');
                    }
                });
            }


            $('#form-upload-excel').on('submit', function (event) {
                event.preventDefault();

                var formData = new FormData(document.getElementById('form-upload-excel'));

                $.ajax({
                    headers: {'X-CSRF-TOKEN': '{!! csrf_token() !!}'},
                    type: "POST",
                    url: '{!!  route('process.excel.initial') !!}',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#excel-container').addClass('hidden');
                        $('#sk-loading').toggleClass('hidden');
                        $('#btn-loading-file').button('loading');
                        $('.btn-loading-file').toggleClass('hidden');
                    },
                    success: function (response) {
                        $('#table-excel').find('tbody').html('');
//                        $('#table-excel').find('tbody').html(response.html);
                        $('#excel-container').toggleClass('hidden');
                        $('#btn-cancel-process').toggleClass('hidden');
                        $('#btn-process-data').toggleClass('hidden');
                    },
                    error: function (errors) {
                        console.log(errors);
                        $('.btn-loading-file').toggleClass('hidden');
                    },
                    complete: function () {
                        $('#btn-loading-file').button('reset');
                        $('#sk-loading').toggleClass('hidden');
                    }
                });
            });
        });
    </script>


    <script>
        $('#table-excel').on('click', 'tbody td', function () {
            displayForm($(this));
        });


        function displayForm(cell) {

            var column = cell.attr('class'),
                id = cell.closest('tr').attr('id'),
                cellWidth = cell.css('width'),//obtiene el ancho de la celda para el estilo de ancho del campo de entrada
                cellHeight = cell.css('height'),//obtiene el alto de la celda para el estilo de ancho del campo de entrada
                cellText = $(cell.find(column.replace('cell', '.text'))),//obtiene el texto actual de la celda para el campo de entrada
                prevContent = cellText.text();//almacena el valor anteror


            //borra el texto actual de la celda, mantiene la celda con el ancho actual
            cellText.html('');
            cell.css('width', cellWidth);

            var controls = cell.find('.form-control');
            var control;
            var select2;

            if (controls.length > 1) {
                control = $(controls[0]);
                select2 = $(controls[1]);
                select2.css({'width': cellWidth, 'height': cellHeight, 'display': 'block'});
                control.select();

            } else {
                control = $(controls[0]);
                control.css({'width': cellWidth, 'height': cellHeight, 'display': 'block'}).select();
            }

            //desactiva el listener en la celda individual una vez hecho clic
            cell.on('click', function () {
                return false
            });

            //on keypress within td
            cell.on('keydown', function (event) {
                if (event.keyCode == 13) {//13 == enter

                    cellText.text(control.val());
                    control.css('display', 'none');

                    if (select2 != null)
                        select2.css('display', 'none');


                } else if (event.keyCode == 27) {//27 == escape

                    cellText.text(prevContent);//vuelve al valor original
                    control.val(prevContent);//vuelve al valor original
                    cell.off('click'); //reactivar edición
                    control.css('display', 'none');

                    if (select2 != null)
                        select2.css('display', 'none');
                }
            });


            control.blur(function () {

                cell.off('click'); //reactivar edición
                control.css('display', 'none');

                if (select2 != null) {

                    if (control[0].options[control.val()])
                        cellText.text(control[0].options[control.val()].text);

                    select2.css('display', 'none');

                } else {
                    cellText.text(control.val());
                }
            });


            control.on('change', function (event) {
                event.preventDefault();
                cell.off('click'); //reactivar edición

                var text;

                if (select2 != null) {

                    if (control[0].options[control.val()]) {
                        text = control[0].options[control.val()].text;//revert to original value
                    }
                    select2.css('display', 'none');

                } else {
                    text = control.val();
                }

                cellText.text(text);
            });
        }


        $('#table-excel').on('mouseenter', 'tbody td', function () {

            $(this).css({
                'box-shadow': '0 0 1px 0 rgba(0, 0, 0, 0.5), 0 0 1px 0 rgba(0, 0, 0, 0.5)',
                'cursor': 'pointer',
                'z-index': '10'
            });
        }).on('mouseleave', 'tbody td', function () {
            $(this).css({
                'box-shadow': 'none'
            });
        });
    </script>
@endsection


@section('header')
    <button type="button" onclick="document.getElementById('excel_file').click();"
            class="btn"><i class="material-icons md-18">&#xE2C6;</i>Subir un archivo
    </button>
    <label id="file_name" class="control-label"></label>

    <button type="button" onclick="document.getElementById('btn-import').click();"
            class="btn">Importar
    </button>

    <button type="button" id="btn-process-data" class="btn"
            onclick="document.getElementById('btn-send').click();"><i class="material-icons md-18">
            &#xE161;</i>Guardar
    </button>

    {!! Form::open(['route' => [ 'process.excel'], 'method' => 'POST', 'id' => 'form-upload-excel', 'files' => true]) !!}


    <input type="file" id="excel_file" name="excel_file" class="hidden">
    {!! Form::submit('import', ['id' => 'btn-import', 'class' => 'hidden']) !!}
    {!! Form::close() !!}
@endsection