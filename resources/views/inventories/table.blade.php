@foreach($articles as $key => $article)
    <tr>
        <td>{!! $key+1 !!}</td>
        <td>
            <span class="text-description">{!! $article->description !!}</span>
            <div class="form-group">
                {!! Form::hidden('items['.$key.'][description]', $article->description, ['id' => 'items.'.$key.'.internal_reference']) !!}
            </div>
        </td>
        <td>
            <span class="text-internal-reference">{!! $article->internal_reference !!}</span>

            <div class="form-group">
                {!! Form::hidden('items['.$key.'][internal_reference]', $article->internal_reference, ['id' => 'items.'.$key.'.internal_reference']) !!}
            </div>
        </td>
        <td class="cell-unit-cost-initial">
            <span class="text-unit-cost-initial">{!! $article->unit_cost_initial !!}</span>
            <div class="form-group">
                {!! Form::text(
                'items['.$key.'][unit_cost]',
                $article->unit_cost_initial,
                [
                    'id' => 'items.'.$key.'.unit_cost',
                    'class' => 'form-control text-right'
                ]
                ) !!}
            </div>
        </td>
        <td class="cell-stock">
            <span class="text-stock">{!! $article->stock !!}</span>
            <div class="form-group">
                {!! Form::text('items['.$key.'][stock]', $article->stock, ['id' => 'items.'.$key.'.stock', 'class' => 'form-control']) !!}
            </div>
        </td>
    </tr>
@endforeach