@extends('layout_principal')

@section('container')


    <section class="col-md-10">


        @foreach($alarms as $alarm)
            <table class="table">
                <thead>
                <tr class="text-uppercase">
                    <th>Articulo</th>
                    <th>Min stock</th>
                    <th>Max stock</th>
                    <th>Stock</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{!! $alarm->articles->description !!}</td>
                    <td>{!! $alarm->min_stock !!}</td>
                    <td>{!! $alarm->max_stock !!}</td>
                    <td>
                        @if($alarm->stock == 0)
                            <label class="text-danger" style="width: 80px;">
                                <span class="glyphicon glyphicon-arrow-down"></span> {!! $alarm->stock !!}
                            </label>
                        @elseif($alarm->stock <= $alarm->min_stock)
                            <label class="text-warning" style="width: 80px;">
                                <span class="glyphicon glyphicon-arrow-down"></span> {!! $alarm->stock !!}
                            </label>
                        @elseif($alarm->stock >= $alarm->max_stock )
                            <label class="text-success" style="width: 80px;">
                                <span class="glyphicon glyphicon-arrow-up"></span> {!! $alarm->stock !!}
                            </label>
                        @endif
                    </td>
                </tr>
                </tbody>

            </table>
        @endforeach
    </section>


@endsection