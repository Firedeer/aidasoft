@extends('layout_principal')

@section('extra_scriptHead')
    <!-- Croppie JS -->
    <link rel="stylesheet" href="{{ asset('croppie/croppie.css')}}"/>
    <script src="{{ asset('croppie/croppie.js') }}"></script>

    <!-- Select2  Pluging -->
    <link href="{{asset('select2/css/select2.css')}}" rel="stylesheet"/>
    <script src="{{asset('select2/js/select2.js')}}"></script>
    <script src="{{asset('select2/js/i18n/es.js')}}" type="text/javascript"></script>
@endsection

@section('container')

    <div class="col-md-10 col-md-offset-1">

        {!! Form::model( $master, [ 'route' => [ 'admin.article.update', $master->id ],  'method' => 'PUT', 'role' => 'update', 'id' => 'form-article-update']) !!}

        <section class="panel">

            <div class="panel-heading">
                <h1>Editar Articulo</h1>
            </div>

            <div class="panel-body">

                <div class="btn-group btn-group-sm pull-right">
                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#categoryModal">
                        <i class="material-icons md-18">add_circle_outline</i> Categoría
                    </button>
                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#brandModal">
                        <i class="material-icons md-18">add_circle_outline</i> Marca
                    </button>
                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#measurementModal">
                        <i class="material-icons md-18">add_circle_outline</i> Unidad de medida
                    </button>
                </div>


                <legend style="width: auto">Datos Generales</legend>
            </div>


            <div class="panel-body form-horizontal">
                @include('admin.forms.article_general')
            </div>
            <!-- .panel-body -->


            <div class="panel-body hidden">

                <button type="button" class="btn btn-sm btn-link pull-right" data-toggle="modal"
                        data-target="#attributeModal">
                    <i class="material-icons md-18">add_circle_outline</i> Nuevo Atributo
                </button>

                <legend style="width: auto;">Atributos y Variantes</legend>
                @include('admin.forms.article_attributes')
            </div>
            <!-- .panel-body -->


            <div class="panel-body form-horizontal">
                <legend>Inventario</legend>
                @include('admin.forms.article_stock')
            </div>
            <!-- .panel-body -->


            <div class="panel-body">

                <legend>Carga una imagen</legend>
                {{--<p class="text-muted">Distinge a tus articulos....</p>--}}

                @include('admin.forms.upload_image')
            </div>
            <!-- .panel-body -->
        </section>
        <!-- .panel -->

        {!! Form::submit('Update', ['id' => 'btn-article-update', 'class' => 'hidden']) !!}
        {!! Form::close() !!}

    </div>
@endsection



@section('extra_scriptBody')

    @include('admin.forms.new_caracteristic_modal')

    <script src="{{ asset('assets/js/demo_image.js')}}"></script>

    <script src="{!! asset('assets/js/send_to_ajax.js') !!}"></script>
    <script>

        $(document).ready(function () {
            var form = $('form[role="update"]');

            $(form).sendToAjax({
                method: $(form).find('input[name="_method"]').val(),
                token: $(form).find('input[name="_token"]').val(),
                route: $(form)[0].action,
                success: function (responce) {
//                    swal({
//                        title: "",
//                        text: "Actualizado",
//                        type: "success",
//                        confirmButtonText: "Aceptar"
//                    });

                    location.href = '{!! url('article') !!}/' + responce.redirectTo;
                }
            });
        });
    </script>

    {{--<script src="{!! asset('assets/js/variants.js') !!}"></script>--}}
    <script src="{!! asset('assets/js/warehouse.js') !!}"></script>
    {{--<script src="{!! asset('assets/js/attributes.js') !!}"></script>--}}


    <script>
        $('select.select2').select2();
    </script>
@endsection



@section('header')

    <div class="col-md-12">
        <div class="row">
            <div class="navbar" style="background-color: transparent">
                <div class="container-fluid">

                    <div class="navbar-btn">

                        <a href="{!! URL::previous() !!}" class="btn">
                            <i class="material-icons md-18">&#xE5C4;</i><span class="hidden-xs">Atras</span>
                        </a>

                        <button type="button" class="btn btn-default btn-sm mdb" onclick="document.location.reload()">
                            Cancelar
                        </button>

                        <button type="button" id="btnSubmit" class="btn btn-primary btn-sm mdb" onclick="document.getElementById('btn-article-update').click();">
                            <i class="material-icons md-18">save</i> Guardar
                        </button>

                        <a href="{!! route('admin.article.index') !!}" class="btn btn-default btn-xs mdb pull-right">
                            <i class="material-icons md-18">list</i> <span class="hidden-xs">Ir a la lista</span>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('breadcrumb')

    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('article.index') !!}">{!! trans('app.attributes.articles') !!}</a></li>
        <li class="active">Editar Artículo</li>
    </ol>
@endsection