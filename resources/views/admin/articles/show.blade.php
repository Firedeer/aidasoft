@extends('layout_principal')

@section('extra_scriptHead')
    <script src="{{ asset('Chart.js/2.0-dev/Chart.min.js') }}"></script>

    <style>

        .editing a[role=edit-variant] {
            display: none;
        }

    </style>
@endsection

@section('container')


    @include('alerts.flash_response')

 {!! dd("hklsjkljal") !!}


    <div class="col-md-12">

        <div class="panel-heading">
            <ul class="stats-overview">
                <li>
                    <span class="name">Articulos</span>
                    <span class="value
                    @if($master->articleView->sum('stock') == 0)
                            text-danger
                        @elseif($master->articleView->sum('stock') <= $master->min_stock)
                            text-warning
                        @elseif($master->articleView->sum('stock') >= $master->max_stock )
                            text-success
                        @endif"> {!! $master->articleView->sum('stock') !!}
                </span>
                </li>
                <li>
                    <span class="name"> Pedido Pendiente </span>
                    <span class="value text-success"> {!! $master->articleView->sum('pendeing') !!} </span>
                </li>

                <li>
                    <span class="name"> Ultima Compra </span>
                    <span class="value text-success">

                    <?php $lastPurchase = $master->lastPurchase();?>

                        @if(isset($lastPurchase->quantity))
                            {!! $lastPurchase->quantity !!}
                        @else No hay compras
                        @endif

                        <br>

                        @if(isset($lastPurchase->order))
                            <a href="{!! url('/order/purchases/'. $lastPurchase->order_id) !!}">
                        {!! $lastPurchase->order !!}<br>
                        <small>{!! $lastPurchase->created_at !!}</small>
                    </a>
                        @else
                            <a href="{!! url('/order/create/purchases/') !!}" class="btn btn-link btn-xs mdb">
                        <span class="glyphicon glyphicon-plus"></span> Crear una compra
                    </a>
                        @endif

                    </span>
                </li>
            </ul>
        </div>

        <div class="panel">

            <div class="panel-body">
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img @if(isset($master->image))
                                 src="{!! asset('assets/img/'.$master->image) !!}"
                                 @else
                                 src="{!! asset('assets/img/trade.jpg') !!}"
                                 @endif class="media-object img-rounded" width="125px">
                        </a>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">{!! $master->name !!}
                            <small>{!! $master->description !!}</small>
                        </h3>


                        @if($master->articleView->sum('stock') == 0)
                            <label class="label label-danger" style="width: 80px;">
                                <span class="glyphicon glyphicon-arrow-down"></span> Agotado
                            </label>
                        @elseif($master->articleView->sum('stock') <= $master->min_stock)
                            <label class="label label-warning" style="width: 80px;">
                                <span class="glyphicon glyphicon-arrow-down"></span> Stock Bajo
                            </label>
                        @elseif($master->articleView->sum('stock') >= $master->max_stock )
                            <label class="label label-success" style="width: 80px;">
                                <span class="glyphicon glyphicon-arrow-up"></span> Stock Alto
                            </label>
                        @endif

                        <br>
                        <br>


                        <p>Código:
                            <span class="serial-code">{!! $master->barcode !!}</span>
                        </p>

                        <p>Categoría:
                            @if(isset($master->category->description))
                                <strong>{!! $master->category->description !!}</strong>
                            @endif
                        </p>

                        <p>Marca:
                            @if(isset($master->brand->description))
                                <strong>{!! $master->brand->description !!}</strong>
                            @endif
                        </p>

                        <p>Caracteristicas:
                        @if($attributes)
                            @foreach($attributes as $key => $attribute)
                                <li>{!! $attribute->description !!}</li>
                                @endforeach
                                @endif
                                </p>
                    </div>

                    @if(currentUser()->isAdmin())
                        <div class="media-right">
                            <a href="{!! url('article/master/admin/'.$master->id.'/edit') !!}"
                               class="btn btn-fab btn-fab-sm mdb">
                                <i class="material-icons">mode_edit</i>
                            </a>
                        </div>
                    @endif
                </div>
            </div>


            <!-- Nav tabs -->
            <ul class="nav nav-pills tabs-flex" role="tablist">
                <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab"
                                                          data-toggle="tab">Información</a>
                </li>
                <li role="presentation"><a href="#variants" aria-controls="variants" role="tab"
                                           data-toggle="tab">Variantes</a>
                </li>
                <li role="presentation"><a href="#inventory" aria-controls="inventory" role="tab"
                                           data-toggle="tab">Invetario</a>
                </li>
                <li role="presentation"><a href="#movements" aria-controls="movements" role="tab"
                                           data-toggle="tab">Movimientos</a>
                </li>
            </ul>

            <div class="panel-body">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="info">

                    </div>


                    <div role="tabpanel" class="tab-pane" id="variants">


                        <div class="page-header">

                            <button class="btn btn-link btn-xs mdb pull-right" id="btn-handle-attributes">
                                Crear
                                automaticamente
                            </button>

                            <h3>Variantes</h3>
                        </div>

                        <div class="row-sm">
                            {!! Form::model($master,['url' => ['article/master/admin/update/variants', $master->id], 'method' => 'PUT', 'role' => 'edit']) !!}

                            {!! Form::hidden('name') !!}
                            {!! Form::hidden('barcode') !!}


                            <table class="table table-condensed table-striped text-middle">

                                <tbody id="show-variants">

                                @foreach($master->articles as $key =>  $article )
                                    <tr>
                                        <td width="40">
                                            @if( $article->image == null )
                                                <img src="{{ asset('assets/img/trade.jpg') }}" width="45px"
                                                     class="media-object img-rounded">
                                            @else
                                                <img src="{{ asset('assets/img/'.$article->image ) }}" width="45px"
                                                     class="media-object img-rounded">
                                            @endif
                                        </td>

                                        <td class="hidden-xs">
                                            <strong>{!! $article->description !!}</strong>
                                            <input type="hidden" name="variant[{!! $key !!}][description]" class="attr"
                                                   value="{!! $article->description !!}">

                                            <input type="hidden" name="variant[{!! $key !!}][id]"
                                                   value="{!! $article->id !!}">
                                        </td>

                                        <td class="hidden-xs">
                                            <span class="serial-code">{!! $article->internal_reference !!}</span>
                                        </td>

                                        <td>
                                            @if(count($article->variants))

                                                @foreach($article->variants as $variant)

                                                    <span class="chip chip-sm">{!! $variant->attributeValue->value !!}
                                    </span>
                                                    <input type="hidden" name="variant[{!! $key !!}][value][]"
                                                           value="{!! $variant->attributeValue->id !!}"
                                                           class="attr-value">
                                                @endforeach

                                            @else
                                                <input type="hidden" name="variant[{!! $key !!}][value][]" value=""
                                                       class="attr-value">
                                            @endif
                                        </td>


                                        <td width="40">
                                            <div class="togglebutton">
                                                <label>
                                                    <input type="checkbox"
                                                           name="variant[{!! $key !!}][active]"
                                                           @if( $article->active == 1 || old('active') == 1) checked="checked"
                                                           @endif
                                                           class="attr"
                                                    >
                                                </label>
                                            </div>
                                        </td>

                                        <td width="40" hidden>
                                            <a href="{!! url('article/'.$article->id.'/edit') !!}"
                                               class="btn btn-info btn-raised btn-xs"
                                               role="edit-variant">
                                                <span class="glyphicon glyphicon-pencil"></span>
                                                <span class="hidden-xs">Editar</span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>


                            {!! Form::open() !!}

                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="inventory">

                        <table class="table table-condensed table-bordered table-hover text-middle">
                            <thead>
                            <tr class="text-uppercase">
                                <th colspan="2">Descripción</th>
                                <th>Codigo</th>
                                <th>Bodega</th>
                                <th>Stock</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\TradeMarketing\Models\Views\ArticleMasterWarehousesView::where('article_master_id', $article->article_master_id)->get() as $article)
                                <tr>
                                    <td width="40">
                                        <img @if(isset($article->image))
                                             src="{!! asset('assets/img/'.$article->image) !!}"
                                             @else
                                             src="{!! asset('assets/img/trade.jpg') !!}"
                                             @endif class="media-object img-rounded" width="40px">
                                    </td>
                                    <td>{!! $article->article !!}</td>
                                    <td class="serial-code">{!! $article->internal_reference !!}</td>
                                    <td>{!! $article->warehouse !!}</td>
                                    <td>{!! $article->stock !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="movements">

                        <table id="movements" class="table table-hover table-bordered table-sm">
                            <thead>
                            <tr class="text-uppercase">
                                <th>Fecha</th>
                                <th>Movimiento</th>
                                <th></th>
                                <th>Referencia</th>
                                <th>Bodega</th>
                                <th>Articulo</th>
                                <th>Unidad</th>
                                <th>Entrada</th>
                                <th>Salida</th>

                                <th>Costo u.</th>
                                <th>De</th>
                                <th>Para</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\TradeMarketing\Models\Views\MovementsView::where('article_master_id', $article->article_master_id)->get() as $movement)
                                <tr>

                                    <td>
                            <span class="hidden-xs hidden-sm">{!! Carbon\Carbon::parse( $movement->created_at )->formatLocalized('%d/%m/%y')  !!}
                            </span>
                                        <span class="hidden-md hidden-lg">{!! Carbon\Carbon::parse( $movement->created_at )->formatLocalized('%b/%y')  !!}
                            </span>
                                    </td>


                                    <td>
                                        {!! $movement->type_movement !!}
                                        <br>
                                        {!! $movement->movement_tag !!}
                                    </td>

                                    <td>{!! $movement->movement_code !!} / {{ $movement->movement }}</td>

                                    <td>
                                        @if($movement->movement_tag)
                                            <a href="{!! route($movement->movement_tag, $movement->reference_id) !!}">
                                                {!! $movement->reference !!}
                                            </a>
                                        @endif
                                    </td>

                                    <td>{!! $movement->bodega !!}</td>


                                    <td>
                                        <strong class="hidden-xs hidden-sm text-uppercase">{!! $movement->internal_reference !!}</strong>
                                        /
                                        {{ $movement->article }}
                                        <br>
                                        <small>
                                            <strong class="hidden-xs hidden-sm text-uppercase">{{ $movement->barcode }}</strong>
                                            /
                                            <a href="{!! route('article.show', $movement->article_master_id) !!}">{{ $movement->article_master }} </a>
                                        </small>
                                    </td>

                                    <td class="hidden-xs hidden-sm">{{ $movement->unity }}</td>

                                    <td class="text-right">{{ $movement->quantity }}</td>

                                    <td class="text-right">{{ $movement->quantity_exit }}</td>

                                    <td class="hidden-xs hidden-sm text-right">{{ $movement->unit_cost }}</td>

                                    <td class="hidden-xs hidden-sm text-right">{{ $movement->from }}</td>

                                    <td class="hidden-xs hidden-sm text-right">{{ $movement->for }}</td>
                                </tr>

                            @endforeach
                            </tbody>

                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection







@section('extra_scriptBody')




    <div class="modal fade" tabindex="-1" role="dialog" id="handle-attributes">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Crear variantes</h4>
                </div>
                <div class="modal-body">

                    <div id="variants">

                        @foreach($attributes as $key => $attribute)
                            <div class="form-group mdb">
                                <label for="" class="control-label">{!! $attribute->description !!}</label>

                                <div class="checkbox">
                                    @foreach($attribute->values as  $value)
                                        <label>
                                            <input type="checkbox"
                                                   name="attributeValue[]"
                                                   class="attributeValue"
                                                   value="{!! $value->id !!}"
                                                   @if( \DB::table('tmk_articles')
                                                   ->join('tmk_variants', 'tmk_variants.article_id', '=', 'tmk_articles.id')
                                                   ->where('article_master_id', $master->id)
                                                   ->where('attribute_value_id', $value->id)
                                                   ->where('tmk_articles.deleted_at', null)
                                                   ->get()
                                                   )

                                                   checked="checked"
                                                    @endif
                                            > {!! $value->value !!}
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach


                        <hr>
                        <div class="checkbox">
                            <label for="replace">
                                <input type="checkbox" id="replace" name="replace"> Remplazar actuales
                            </label>
                            <div class="help-block">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit,
                                suscipit.
                            </div>
                        </div>
                        <hr>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                    <button type="button" id="btnAutoVariants" class="btn btn-primary mdb">Crear</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

    </div><!-- /.modal -->


    <script>

        (function ($) {

            var modal = $('#modalVariantEdit');

            $('a[role="edit-variant"]').click(function (event) {
                event.preventDefault();

                $.get(this.href, function (response) {

                    modal.find('.modal-body').html(response);

                    modal.modal({
                        backdrop: 'static',
                        show: 'true'
                    });

                    $.material.init();
                });
            });
        })(jQuery);
    </script>

    <script src="{!! asset('assets/js/variants.js') !!}"></script>



@endsection




