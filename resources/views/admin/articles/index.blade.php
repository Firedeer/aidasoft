@extends('layout_principal')

@section('extra_scriptHead')
    @include('partials.dataTables_script')

    <style>
        td.details-control:before {
            content: "+";
            font-size: 18px;
            cursor: pointer;
        }

        tr.shown td.details-control:before {
            content: "-";
            font-size: 18px;
        }
    </style>
@endsection


@section('container')

    <div class="col-md-12">
        <div class="page-header">
            <h1>{!! trans('app.attributes.articles') !!}</h1>
        </div>
    </div>


    <div class="col-md-12">
        <div class="row-sm">
            <div class="panel">
                <div class="panel-body">

                    <table id="tbl-admin-articles"
                           class="table table-condensed table-responsive table-bordered small">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{!! trans('app.attributes.description') !!}</th>
                            <th>{!! trans('app.attributes.internal_reference') !!}</th>
                            <th class="hidden-xs hidden-sm">{!! trans('app.attributes.category') !!}</th>
                            <th class="hidden-xs hidden-sm">{!! trans('app.attributes.brand') !!}</th>
                            <th class="hidden-xs hidden-sm">{!! trans('app.attributes.measurement_unit') !!}</th>
                            <th>{!!  trans('app.attributes.min_stock') !!}</th>
                            <th>{!! trans('app.attributes.inventory') !!}</th>
                            <th class="hidden-xs hidden-sm">{!! trans('app.attributes.last_update') !!}</th>
                            <th width="20"></th>
                        </tr>
                        </thead>
                        <tbody class="text-uppercase">
                        @foreach($masters as $key => $master)
                            <tr data-article="{!! $master->id !!}">
                                <td class="details-control" width="20"></td>
                                <td>
                                    <a href="{!! route('article.show', $master->id) !!}">

                                        <img src="{!! asset('assets/img/'.$master->image ) !!}"
                                             onerror="this.src='{!! asset('assets/img/_not_available.png')  !!}'"
                                             class="img-rounded" width="50px" height="50px">
                                    </a>

                                    <a href="{!! route('article.show', $master->id) !!}">{!! $master->name !!}
                                    </a>
                                </td>

                                <td class="serial-code">{!! $master->internal_reference !!}</td>

                                <td class="hidden-xs hidden-sm">{!! isset($master->category)? $master->category->description : '' !!}</td>

                                <td class="hidden-xs hidden-sm">{!! isset($master->brand)? $master->brand->description : '' !!}</td>

                                <td class="hidden-xs hidden-sm">{!! $master->measurementUnity->description !!}</td>

                                <td>{!! $master->min_stock !!}</td>

                                <td>{!! \TradeMarketing\ClientesContactos::buscar_inventario( $master->id); !!}</td>

                                <td class="hidden-xs hidden-sm">
                                    <a href="javascript:void(0)" class="user-profile" data-toggle="tooltip"
                                       data-placement="top" title="{!! $master->updatedBy->fullName !!}">
                                        <div class="user-profile-img">
                                            {!! $master->updatedBy->nameProfile !!}
                                        </div>
                                    </a>


                                    <small>{!! $master->updated_at !!}</small>
                                </td>

                                <td width="20">

                                    <a href="{!! route( 'admin.article.edit', $master->id) !!}"
                                       class="btn btn-xs">
                                        <i class="material-icons md-18">mode_edit</i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

@endsection


@section('extra_scriptBody')
    <script>


        /* Formatting function for row details - modify as you need */
        function format(d) {
            // `d` is the original data object for the row

            var row = '<row><th class="hidden-xs">Descripción</th><th class="hidden-xs">Código</th><th>Inventario</th><th>Bodega</th></row>';

            var route = '{!! route('warehouse.show', ':WAREHOUSE_ID') !!}';

            d.forEach(function (item) {

                row += '<tr>' +
                    '<td class="hidden-xs">' + item.article + '</td>' +
                    '<td class="hidden-xs">' + item.internal_reference + ' </td>' +
                    '<td>' + item.stock + '</td>' +
                    '<td><a href="' + route.replace(':WAREHOUSE_ID', item.warehouse_id) + '">' + item.warehouse + '</a></td>' +
                    '</tr>';
            });

            return '<table class="table table-condensed table-hover table-striped text-middle" ' +
                'cellpadding="5" ' +
                'cellspacing="0" ' +
                'border="0" ' +
                'style="padding-left:50px;">' + row + '</table>';
        }


        $(document).ready(function () {

            var tableId = '#tbl-admin-articles';

            var table = $(tableId).dataTableConfig({
                "columns": [
                    {"orderable": false},
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"orderable": false}
                ]
            });

            // Add event listener for opening and closing details
            $(tableId).on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var id = tr.data('article');

                var row = table.row(tr);

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {

                    var route = "{!! route('article.warehouses', ':ARTICLE_ID') !!}";

                    $.getJSON(route.replace(':ARTICLE_ID', id))
                        .done(function (json) {

                            // Open this row
                            row.child(format(json)).show();
                            tr.addClass('shown');

                        });
                }
            });
        });
    </script>
@endsection





@section('header')



    <div class="col-md-12">
        <div class="row">
            <div class="navbar">
                <div class="container-fluid">


                    <div class="navbar-btn">

                        <a href="{!! route('admin.article.create') !!}" class="btn">
                            <i class="material-icons">&#xE145;</i>Nuevo Artículo
                        </a>

                        <a href="{!! route('import.excel') !!}" class="btn">
                            <i class="material-icons md-18">&#xE2C6;</i>Agregar desde excel
                        </a>

                        <a href="{!! route('inventory.initial') !!}" class="btn pull-right">Cargar inicial</a>


                        @include('partials.dataTables_filter', ['table' => 'tbl-admin-articles'])
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li class="active">{!! trans('app.attributes.articles') !!}</li>
    </ol>
@endsection