

<div class="col-sm-6">

    <div class="form-group">

        {!! Form::label('initial_stock', 'Inventario inicial', ['class'  => 'control-label col-md-2']) !!}

        <div class="col-md-9">
            {!! Form::text('initial_stock', old('initial_stock'), ['class'=>'form-control']) !!}

            <p class="help-block">El inventario inicial se aplicara a todas las variantes relacionadas.</p>
        </div>
    </div>


    <div class="form-group">

        {!! Form::label('warehouse_id', 'Bodega', ['class'  => 'control-label col-md-2']) !!}

        <div class="col-md-9">
            {!! Form::select('warehouse_id', \TradeMarketing\Models\Warehouse::listing(), old('warehouse_id'), ['class'=>'form-control', 'placeholder'=>'----']) !!}

            <br>
        </div>
    </div>
</div>





<div class="col-sm-6">

    <div class="form-group @if($errors->has('min_stock')) has-error @endif">
        {!! Form::label('min_stock', 'Inventario mínimo', ['class' => 'control-label col-md-2']) !!}

        <div class="col-md-9">
            {!! Form::text('min_stock', old('min_stock'), ['class' => 'form-control',  'min' => '1'])!!}

            @if ($errors->has('min_stock'))
                <p class="text-danger">{!!$errors->first('min_stock')!!}</p>
            @endif

            <p class="help-block">Por defecto se tomará como valor minimo uno "1"</p>

        </div>

    </div>
    <!--.form-group -->


    <div class="form-group @if($errors->has('max_stock')) has-error @endif">

        {!! Form::label('max_stock', 'Inventario máximo', ['class' => 'control-label col-md-2']) !!}

        <div class="col-md-9">

            {!! Form::text('max_stock', old('max_stock'), ['class' => 'form-control', 'min' => '0']) !!}

            @if ($errors->has('max_stock'))
                <p class="text-danger">{!!$errors->first('max_stock')!!}</p>
            @endif
        </div>
    </div>
    <!--.form-group -->
</div>
<!--.col -->
