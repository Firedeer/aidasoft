<div class="form-group mdb">
    {!! Form::label('description', 'Descripción', ['class' => 'col-md-2 control-label']) !!}

    <div class="col-md-10">
        {!! Form::text('description', old('description'), ['class' => 'form-control']) !!}

        {!! $errors->first('description', '<p class="text-danger">:message</p>') !!}
    </div>
</div>


<div class="form-group mdb">
    {!! Form::label('sucursal', 'Sucursal', ['class' => 'col-md-2 control-label']) !!}

    <div class="col-md-10">
        {!! Form::select('sucursal', $sucursalList,  isset($warehouse->sucursal_id)?  $warehouse->sucursal_id : old('sucursal'), ['class' => 'form-control']) !!}

        {!! $errors->first('sucursal', '<p class="text-danger">:message</p>') !!}
    </div>
</div>



<div class="form-group mdb">
    {!! Form::label('users[]', 'Usuarios', ['class' => 'col-md-2 control-label']) !!}

    <div class="col-md-10">
        {!! Form::select('users[]', $userList, isset($warehouse->users)?  $warehouse->users : old('users'), ['class' => 'form-control select2',  'multiple', 'id' => 'users']) !!}

        {!! $errors->first('users', '<p class="text-danger">:message</p>') !!}
    </div>
</div>
