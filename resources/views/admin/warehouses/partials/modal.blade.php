<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Nueva Bodega</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => ['admin.warehouse.update', ':WAREHOUSE_ID'], 'method' => 'PUT', 'id' => 'form-warehouse', 'class' => 'form-horizontal form-condensed']) !!}

{{--                @include('admin.warehouses.partials.save')--}}
{{----}}
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb"
                        onclick="updateWarehouse('form-warehouse')">Guardar
                </button>

            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Nueva Bodega</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => ['admin.warehouse.store'], 'method' => 'POST', 'id' => 'form-warehouse-store', 'class' => 'form-horizontal form-condensed']) !!}

{{--                @include('admin.warehouses.partials.save')--}}

                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb"
                        onclick="storeWarehouse('#form-warehouse-store')">Guardar
                </button>
            </div>
        </div>
    </div>
</div>


<script>

    function storeWarehouse(form) {

        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: $(form).attr('action'),
            type: $(form).attr('method'),
            data: $(form).serialize(),
            dataType: 'json',
            beforeSend: function () {
//                var alerts = Array.prototype.slice.call(document.querySelectorAll('.alert.flash'));
//
//                alerts.forEach(function (item) {
//                    item.remove();
//                });
//
//                $(modal).modal('hide');
            },
            error: function (xhr, status) {
                console.log(xhr)
            },
            success: function (response) {

                console.log(response);

                $('#exampleModal').modal('hide');
//                $(row).fadeOut();
//
//                var body = document.getElementsByClassName('container-wrapper')[0];
//                body.insertAdjacentHTML('afterbegin', response.alert);
            }
        });

    }
    var cloneRoute = '';
    function updateWarehouse(form) {
        var form = $('#form-warehouse');

        console.log(form);

        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: cloneRoute,
            type: $(form).attr('method'),
            data: form.serialize(),
            dataType: 'json',
            beforeSend: function () {
//                var alerts = Array.prototype.slice.call(document.querySelectorAll('.alert.flash'));
//
//                alerts.forEach(function (item) {
//                    item.remove();
//                });
//
//                $(modal).modal('hide');
            },
            error: function (xhr, status) {
                console.log(xhr)
            },
            success: function (response) {

                console.log(response);

                $('#exampleModal2').modal('hide');
//                $(row).fadeOut();
//
//                var body = document.getElementsByClassName('container-wrapper')[0];
//                body.insertAdjacentHTML('afterbegin', response.alert);
            }
        });

    }

    var routeBase = '{!! route('admin.warehouse.edit', ':WAREHOUSE_ID') !!}';

    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var warehouse_id = button.data('warehouse');
        var modal = $(this);
        var form = modal.find('form');

        var route = routeBase.replace(':WAREHOUSE_ID', warehouse_id);

        $.get(route, function(responce){
            console.log(responce);
        });



        if(warehouse_id){


        var row = $(button.closest('tr'));

        var user = row.find('.user').attr('id');
        var sucursal = row.find('.sucursal').attr('id');
        var warehouse = row.find('.warehouse')[0].textContent.trim();


        var userField = modal.find('#user');
        var sucursalField = modal.find('#sucursal');
        var warehouseField = modal.find('#description');


        $(sucursalField).val(sucursal).change();
        $(userField).val(user).change();
        $(warehouseField).val(warehouse);


            cloneRoute =  form.attr('action').replace(':WAREHOUSE_ID', warehouse_id);
        }

    });

</script>