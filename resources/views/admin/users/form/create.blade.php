{{-- FIRST NAME --}}
<div class="form-group mdb @if($errors->has('firstname')) has-error @endif">
    {!! Form::label('firstname', 'Nombre (*)', ['class' => 'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('firstname', old('firstname'), ['class' => 'form-control', 'autofocus']) !!}

        {!! $errors->first('firstname', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>

{{-- LAST NAME --}}
<div class="form-group mdb @if($errors->has('lastname')) has-error @endif">
    {!! Form::label('lastname', 'Apellido (*)', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::text('lastname', old('lastname'), ['class' => 'form-control']) !!}

        {!! $errors->first('lastname', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>



{{-- PHONE --}}
<div class="form-group mdb @if($errors->has('phone')) has-error @endif">
    {!! Form::label('phone', 'Teléfono', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::text('phone', old('phone'), ['class' => 'form-control']) !!}

        {!! $errors->first('phone', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>




{{-- ADDRESS --}}
<div class="form-group mdb @if($errors->has('address')) has-error @endif">
    {!! Form::label('address', 'Dirección', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::text('address', old('address'), ['class' => 'form-control']) !!}

        {!! $errors->first('address', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>

{{-- EMAIL	 --}}
<div class="form-group mdb @if($errors->has('email')) has-error @endif">
    {!! Form::label('email', 'Correo Electrónico (*)', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}

        {!! $errors->first('email', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>
<div class="form-group mdb @if($errors->has('tipo')) has-error @endif" style="display: none;">
    {!! Form::label('tipo', 'tipo (*)', ['class' => 'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('tipo', 'edit', ['class' => 'form-control', 'autofocus']) !!}

        {!! $errors->first('tipo', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>

{{--<div class="form-group mdb @if ($errors->first('warehouse') != '') has-error @endif">--}}
{{--{!! Form::label('warehouse', 'Bodega', array('class' => 'col-md-3 control-label')) !!}--}}

{{--<div class="col-md-9">--}}
{{--{!! Form::text('warehouse', old('warehouse'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}--}}

{{--{!! $errors->first('warehouse', '<p class="text-danger">:message</p>')  !!}--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group mdb @if ($errors->first('agent_id') != '') has-error @endif">--}}
    {{--{!! Form::label('agent_id', 'Codigo de agente interno(*)', array('class' => 'col-md-3 control-label')) !!}--}}

    {{--<div class="col-md-9">--}}
        {{--{!! Form::text('agent_id', old('agent_id'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}--}}

        {{--{!! $errors->first('agent_id', '<p class="text-danger">:message</p>')  !!}--}}
    {{--</div>--}}
{{--</div>--}}


{{--<div class="form-group mdb @if ($errors->first('username') != '') has-error @endif">--}}
    {{--{!! Form::label('username', 'Usuario (*)', array('class' => 'col-md-3 control-label')) !!}--}}

    {{--<div class="col-md-9">--}}
        {{--{!! Form::text('username', old('username'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}--}}

        {{--{!! $errors->first('username', '<p class="text-danger">:message</p>')  !!}--}}
    {{--</div>--}}
{{--</div>--}}
