

{{-- PASSWORD --}}
<div class="form-group mdb @if ($errors->first('password') != '') has-error @endif">
    {!! Form::label('password', 'Contraseña (*)', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::password('password', ['class' => 'form-control', 'autocomplete' => 'off']) !!}

        {!! $errors->first('password', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>

{{-- PASSWORD CONFIRM --}}
<div class="form-group mdb @if ($errors->first('password_confirmation') != '') has-error @endif">
    {!! Form::label('password_confirmation', 'Confirmar Contraseña', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::password('password_confirmation', ['class' => 'form-control', 'autocomplete' => 'off']) !!}

        {!! $errors->first('password_confirmation', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>