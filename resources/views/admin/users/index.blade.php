@extends('layout_principal')



@section('container')



    <div class="col-md-10 col-md-offset-1">


        @include('partials.dataTables_filter', ['table' => 'table-users'])


        <div class="panel">
            <div class="panel-body">

                <table id="table-users" class="table table-bordered table-condensed text-middle small">
                    <thead>
                    <tr class="text-uppercase">
                        <td>Nombre</td>
                        <td>Usuario</td>
                        <td>Email</td>
                        <td>Teléfono</td>
                        <td>Dirección</td>
                        <td>Bodega</td>
                        <td>Rol</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                <a href="{!! route('user.show', $user->id) !!}">
                                    {{--<img @if(isset($user->image))--}}
                                    {{--src="{!! asset('assets/img/'.$user->image) !!}"--}}
                                    {{--@else--}}
                                    {{--src="http://lorempixel.com/56/56/people/"--}}
                                    {{--@endif class="img-circle" width="40px">--}}

                                    <span class="text-capitalize">{!! $user->fullName !!}</span>
                                </a>
                            </td>

                            <td>{!! $user->username !!}</td>
                            <td>{!! $user->email !!}</td>
                            <td>{!! $user->phone !!}</td>
                            <td>{!! $user->address !!}</td>
                            <td>
                                @foreach($user->warehouses as $warehouse)
                                    {!! $warehouse->description !!}<br>
                                @endforeach


                            </td>
                            {{--<td>{!! isset($user->sucursal) ?  '' : 'No tiene sucursal' !!}</td>--}}
                            <td>{!! trans('auth.roles.'. $user->role->description) !!}</td>
                            <td>
                                <a href="{!! route('admin.user.edit', $user->id) !!}" class="btn btn-xs btn-default"
                                   title="Editar">
                                    <i class="material-icons md-18">mode_edit</i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection




@section('extra_scriptBody')
    @include('partials.dataTables_script')
    <script>
        $(document).ready(function () {
            $('#table-users').dataTableConfig({
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"orderable": false}
                ]
            });
        });
    </script>
@endsection

@section('header')


    <div class="col-md-12">
        <div class="row">
            <div class="navbar">
                <div class="container-fluid">

                    <div class="navbar-btn">

                        <div class="row-sm">

                            <a class="btn" href="{!! route('admin.user.create') !!}">
                                <!-- add -->
                                <i class="material-icons">&#xE145;</i>Nueva usuario
                            </a>


                            <div class="col-sm-6 col-md-3 pull-right">
                                <div class="row">
                                    @include('partials.dataTables_filter', ['table' => 'table-purchases'])
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! url('/') !!}">Inicio</a></li>
        <li class="active">Usuarios</li>
    </ol>
@endsection

<!--<li><a href="{!! route('admin.user.create') !!}">Agregar Usuario</a></li>-->