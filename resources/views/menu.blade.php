<ul id="menu" class="menu">

    <li>
        <div id="toggle-menu" class="toggle-menu">
            {{--menu--}}
            <i class="icon icon-menu material-icons">&#xE5D2;</i>
        </div>

        <div class="title">
            <a href="{!! route('home') !!}"><img src="{{ asset('/assets/logo/apple-icon-57x57.png')}}" style="heigth:10%"></a>
        </div>
    </li>


    <!--HOME-->
    <li>
        <a href="{!! route('home') !!}">
            <i class="icon material-icons md-18">&#xE88A;</i>Inicio
        </a>
    </li>
@if(!currentUser()->authRole('admin'))

         <li>
            <a href="{!! route('article.index')  !!}" style="{font:bold}">
                <i class="icon material-icons md-18" >&#xE8F6;</i>Solicitar Articulos
            </a>
        </li> 
        <li>
            <a href="{!! route('order.Pendientes.index')  !!}" style="{font:bold}">
                <i class="icon material-icons md-18" >slow_motion_video</i>Mis pedidos
            </a>
        </li>
@endif



       
        <li>
            <a href="{!! url('admin/my/warehouse') !!}">
                <i class="icon material-icons md-18">store</i>Bodega
            </a>
        </li> 
       


        <!--PEDIDOS-->
        <li>
            <a href="{!! url('admin/my/warehouse_io') !!}" >
                <i class="icon material-icons md-18">compare_arrows</i>Entrada/Salida
            </a>
            <ul class="sub-menu">
                <!---->

            </ul>
        </li> 
        <!--
    <li>
        <a href="javascript:void(0)" >
            <i class="icon material-icons md-18">&#xE8F6;</i>Pedidos
            <i class="icon menu-down material-icons">keyboard_arrow_down</i>
        </a>
        <ul class="sub-menu">
         
             <li><a href="{!! route('order.Pendientes.index') !!}" >Pendientes</a></li>
            <li><a href="{!! route('order.Transito.index') !!}">Por confirmar</a></li>
          <li><a href="{!! route('order.Finalizado.index') !!}">Finalizados</a></li>
          <li><a href="{!! route('order.Cancelados.index') !!}">Cancelados</a></li>
         
        </ul>
    </li>-->


@if(currentUser()->authRole('admin'))
        <!--PURCHASE-->

        <li>
            <a href="{!! route('purchase.index') !!}">
                <i class="icon material-icons md-18">&#xE227;</i>Compras
          
            </a>


            <!--.submenu-->
        </li>
        <li>
            <a href="javascript:void(0)">
            <i class="icon material-icons md-18">&#xE54E;</i>Movimientos
            <i class="icon menu-down material-icons">keyboard_arrow_down</i>
            </a>
       <ul class="sub-menu">
                <li class="{!! URL::current() == route('movement.all') ? 'active' : ''  !!}">
                    <a href="{!! route('movement.all') !!}">Movimientos de Articulos</a>
                    
                </li>
                <li><a href="{!! route('transfer.all') !!}">Transferencia</a></li>
                <li><a href="{!! route('order.Pendientes.index')  !!}">Pedidos</a></li>
        </ul>
        </li>

        <li>
        <a href="javascript:void(0)" >
            <i class="icon material-icons md-18">settings_applications</i>Administrar
            <i class="icon menu-down material-icons">keyboard_arrow_down</i>
        </a>
            <ul class="sub-menu">
                <li><a href="{!! route('admin.article.index') !!}">Articulos</a></li>
                <li><a href="{{ route('admin.user.index') }}" >Usuarios</a></li>
                <li><a href="{!! route('admin.warehouse.index') !!}">Bodegas</a></li>
                <li><a href="{!! route('clientes.index') !!}">Clientes y proveedores</a></li>

     
            </ul>
        </li>

        <li>
            <a href="{!! url('order.Cancelados.index') !!}">
                <i class="icon material-icons md-18">show_chart</i>Reportes
            </a>
        </li> 


@endif



</ul>
<!--.menu-->

