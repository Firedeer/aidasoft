<div class="form-group mdb">

    {!! Form::label('description', 'Nombre (*)', ['class'   => 'control-label col-md-2']) !!}
    <div class="col-md-10">
        {!! Form::text('description', old('description'), ['class' => 'form-control', 'autofocus', 'autocomplete' => 'off']) !!}

        <p class="help-block">Nombre de la marca</p>
    </div>

</div>


{{--<div class="form-group mdb">--}}

    {{--{!! Form::label('description', 'Proveedor', ['class'   => 'control-label col-md-2']) !!}--}}
    {{--<div class="col-md-10">--}}
        {{--{!! Form::text('description', old('description'), ['class' => 'form-control']) !!}--}}
    {{--</div>--}}

{{--</div>--}}