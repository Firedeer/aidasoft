<?php

namespace TradeMarketing\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
    }


    public function show($user, $order)
    {
        if($user->isAdmin()){
            return true;
        }

         if($user->isInCharge($order->warehouse_origen)){
            return true;
        }

        return $user->isInCharge($order->warehouse_destination) and $order->notDraft();
    }


    public function edit($user, $order)
    {
        if ($user->isInCharge($order->warehouse_origen)) {
            return true;
        }

        return false;
    }


    public function send($user, $order)
    {
        if ($user->isInCharge($order->warehouse_origen)) {
            return true;
        }

        return false;
    }


    public function delete($user, $order){

        if($order->isDraft() and $user->isAdmin()){
            return true;
        }

        if($order->isDraft() and $user->isInCharge($order->warehouse_origen)){
            return true;
        }

        return false;
    }


    public function cancel($user, $order){

        if($order->isDraft()){
            return false;
        }

        if($order->orderState('canceled') and $user->isAuthor($order)){
            return true;
        }

        return false;
    }



    public function process($user, $order){

        if($order->isDraft()){
            return false;
        }

        return $user->isAdmin() and $user->isInCharge($order->warehouse_destination);
    }


    public function reject($user, $order){
        return $user->isAdmin() and $user->isInCharge($order->warehouse_destination);
    }
}
