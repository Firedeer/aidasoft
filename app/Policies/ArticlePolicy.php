<?php

namespace TradeMarketing\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

//    public function before(){
//        return auth()->user()->roleAuth('admin');
//    }


    public function updateOrDelete($user,  $article)
    {
        return $user->isAuthor($article);
    }


    public function edit($user, $article){
        return $user->isAdmin();
    }
}
