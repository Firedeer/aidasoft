<?php

namespace TradeMarketing\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class PurchasePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function edit($user, $purchase)
    {
        return $purchase->isDraft() and $user->isAdmin();
    }


    public function delete($user, $purchase)
    {
        return $purchase->isDraft() and $user->isAdmin();
    }


    public function receipt($user, $purchase){
        return $purchase->orderState('confirmed');
    }

}
