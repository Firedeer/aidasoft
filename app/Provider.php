<?php namespace TradeMarketing;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use TradeMarketing\Models\PurchaseOrder;
use TradeMarketing\Traits\FormatDates;
use DB;


class Provider extends Model
{

    use  SoftDeletes, FormatDates;

    protected $table = 'tmk_providers';
    protected $fillable = ['firstname', 'lastname', 'address', 'email', 'phone', 'company', 'type'];
    protected $guarded = ['id'];
    protected $date = ['deleted_at'];


    public static function listing()
    {
        $providers = [];

        foreach (static::all() as $key => $provider) {
            $providers[$provider->id] = $provider->fullName;
        }

        return $providers;
    }


    public function isValid($data)
    {
        $rules = [
            'firstname' => 'required',
            'lastname' => 'min:3',
            'address' => 'required',
            'email' => 'unique:tmk_providers',
            'phone' => 'required',
            'company' => 'required',
            'type' => 'required|in:' . implode(',', config('enums.provider_types')) . ''
        ];

        // Si el articulo existe:
        if ($this->exists) {
            //Evitamos que la regla “unique” tome en cuenta el nombre y el codigo de barras
            $rules['email'] .= ',email,' . $this->id;
        }

        $validator = Validator::make($data, $rules);

        if ($validator->passes()) {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
    }


    public static function getListProviders()
    {
        return DB::table('tmk_providers')
            ->select(DB::raw("CONCAT( firstname, ' ', lastname ) as label"), 'id as value')
            ->get();
    }


    public function type()
    {
        return $this->belongsTo('TradeMarketing\TypeProvider', 'local_key');
    }


    public function scopeName($query, $name)
    {
        if (trim($name) != "")
            $query->where('firstname', 'like', '%' . $name . '%')->orWhere('lastname', 'like', '%' . $name . '%')->whereNull('deleted_at');
    }


    public function scopeType($query, $type)
    {
        // $types = config('enums.provider_types');
        // if( $type != "" && isset($types[$type]) )
        // 	$query->orWhere('type', $types[$type] );
        if ($type != "")
            $query->orWhere('type', 'like', '%' . $type . '%')->whereNull('deleted_at');
    }

    public function scopeCompany($query, $company)
    {

        if (trim($company) != "")
            $query->orWhere('company', 'like', '%' . $company . '%')->whereNull('deleted_at');
    }


    public function purchases()
    {
        return $this->hasMany(PurchaseOrder::class);
    }


    public function getFullNameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }


    public function getCountPurchasesAttribute()
    {
        return $this->join('tmk_purchase_orders', 'tmk_purchase_orders.provider_id', '=', 'tmk_providers.id')
            ->where('provider_id', $this->id)
            ->whereIn('order_status_id', [5, 6])
            ->count();
    }


    public function getLastPurchaseAttribute()
    {
        return $this->join('tmk_purchase_orders', 'tmk_purchase_orders.provider_id', '=', 'tmk_providers.id')
            ->where('provider_id', $this->id)
            ->whereIn('order_status_id', [5, 6])
            ->select('tmk_purchase_orders.*')
            ->orderBy('tmk_purchase_orders.updated_at', 'desc')
            ->first();
    }
}
