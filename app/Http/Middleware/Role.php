<?php

namespace TradeMarketing\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {

        $user = Auth::user();

        if(!$user->authRole($role)){
            abort(404);
        }

        return $next($request);
    }
}
