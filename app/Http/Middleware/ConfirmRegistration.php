<?php

namespace TradeMarketing\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ConfirmRegistration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();

        if($user->confirmation_token != null){
            return  redirect()->to('/')->with('alert-message', 'Aun no has verificado tu cuenta... ');
        }

        return $next($request);
    }
}
