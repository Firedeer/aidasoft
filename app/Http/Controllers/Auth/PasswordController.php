<?php namespace TradeMarketing\Http\Controllers\Auth;

use TradeMarketing\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @return void
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;

		$this->middleware('guest');
	}


	public function update(Request $request)
    {
        $data = $request->only('firstname', 'lastname', 'phone', 'address', 'email', 'role_id');

        if($this->user->isValid($data)){

            $this->user->fill($data);
            $this->user->save();

            ResponseHandler::alertView(1, '', 'El usuario ha sido actualizado correctamente.', false);

            return redirect()->route('user.show', $this->user->id);
        }

        ResponseHandler::alertView(2, 'Soluciona los errores en el formulario.', '', false);

        return back()
            ->withInput($request->all())
            ->withErrors($this->user->errors);
    }

}
