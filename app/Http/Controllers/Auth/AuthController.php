<?php namespace TradeMarketing\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use League\Flysystem\Exception;
use TradeMarketing\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


use Illuminate\Http\Request;
use DB;

use Auth;
use Session;
use Redirect;
use TradeMarketing\User;

class AuthController extends Controller
{

    protected $username = 'username';

    protected $newUserCredentials = [];


    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard $auth
     * @param  \Illuminate\Contracts\Auth\Registrar $registrar
     * @return void
     */


    public function __construct(Guard $auth, Registrar $registrar)
    {
        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }




    public function postLogin(Request $request)
    {
    	   
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }




            User::isConfirmated($request->only('username', 'password', 'confirmation_token'));
        
            $credentials = $this->getCredentials($request);
  
            if (Auth::attempt($credentials, $request->has('remember'))) {
                return $this->handleUserWasAuthenticated($request, $throttles);
            }



            if ($throttles) {
                $this->incrementLoginAttempts($request);
            }





            return redirect($this->loginPath())
                ->withInput($request->only($this->loginUsername(), 'remember'))
                ->withErrors([
                    $this->loginUsername() => $this->getFailedLoginMessage(),
                ]);





    }


    public function isValid($credentials)
    {
        // validar que el usuario tiene permisos
        // agregar un campo boleano en la tabla usuarios trade
        $userValid = DB::table('tbl_usuarios')
            ->where('username', '=', $credentials['username'])
            ->where('password', '=', hash('md5', $credentials['password']))
            ->first();

        if ($userValid) {

            $user = User::newUser($userValid);

            $user->sendMailConfirmation();

            $this->newUserCredentials = $user;

            return true;
        }

        return false;
    }


    public function getConfirmation($token)
    {

        try {
            $user = User::where('confirmation_token', $token)->firstOrFail();

        } catch (\Exception $e) {

            abort(400);
        }

        return redirect()->route('login')

            ->with(['username_confirm' => $user->username,  'confirmation_token' => $token ]);
    }


    public function getCredentials($request)
    {
        return [
            'username' => $request->get('username'),
            'password' => $request->get('password'),
            'active' => true,
            'confirmation_token' => null
        ];
    }


    protected function getFailedLoginMessage()
    {
        return Lang::has('auth.failed')
            ? Lang::get('auth.failed')
            : 'Las credenciales no coinciden con ningún registro.';
    }


    public function loginPath()
    {
        return property_exists($this, 'loginPath') ? $this->loginPath : route('login');
    }


    protected function getLockoutErrorMessage($seconds)
    {

        return Lang::has('auth.throttle')
            ? Lang::get('auth.throttle', ['seconds' => $seconds])
            : 'Too many login attempts. Please try again in ' . $seconds . ' seconds.';
    }


}
