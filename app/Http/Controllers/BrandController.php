<?php namespace TradeMarketing\Http\Controllers;

use Illuminate\Routing\Route;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;

use Illuminate\Http\Request;

use \TradeMarketing\Models\Brand;
use TradeMarketing\Traits\ArrayHandler;
use Validator;


class BrandController extends Controller
{


    public function __construct(){
        $this->beforeFilter('@isAjax');
    }



    public function isAjax(Route $route, Request $request){

        if(!$request->ajax()){
            abort(404);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $data = $request->only('description', 'provider');

        $data = array_map('trim', $data);

        $messages = [
            'required' => 'El nombre de la marca es obligatorio',
            'unique' => 'El nombre de la marca ya ha sido registrado'
        ];

        $validator = Validator::make($data, [
            'description' => 'required|unique:tmk_brands,description',
            'provider' => 'exists:tmk_providers,id'
        ], $messages);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = ArrayHandler::array_upper($data);

        $brand = new Brand;
        $brand->fill($data);
        $brand->save();


        return response()->json($brand);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    public function show_brands_provider(Request $request, $id)
    {
        if ($request->ajax()) {
            $brand = Brand::Get_Brands_Provider($id);
            return response()->json($brand);
        }
    }

    public function show_brands_actives()
    {
        $brand = Brand::Show_Brands_Actives();
        return response()->json($brand);
    }

    public function show_brand_to_provider(Request $request)
    {
        if ($request->ajax()) {
            return Brand::list_brand_to_provider();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        // $data = ArrayHandler::array_upper($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
