<?php

namespace TradeMarketing\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;

// Models
use TradeMarketing\Clients;
use TradeMarketing\Models\PurchaseReceipt;
use TradeMarketing\Models\Views\ArticleView;
use TradeMarketing\Models\Warehouse;
use TradeMarketing\Provider;
use TradeMarketing\Purchase;
use TradeMarketing\Models\PurchaseOrder;
use TradeMarketing\Models\PurchaseOrderDetail;
use TradeMarketing\Models\Views\PurchaseOrderView;
use TradeMarketing\Models\ArticleBodega;
use TradeMarketing\PurchaseArticleBodega;
use TradeMarketing\Traits\ArrayHandler;
use TradeMarketing\Traits\ResponseHandler;
use Storage;
use Log;
class PurchaseController extends Controller
{


    public function __construct()
    {
        $this->beforeFilter('@find', ['only' => ['show', 'edit', 'update', 'receipt', 'confirm', 'cancel', 'destroy']]);
        $this->beforeFilter('@editing', ['only' => ['update']]);
        $this->beforeFilter('@isConfirmed', ['only' => ['receipt', 'confirm']]);
    }


    public function find(Route $route, Request $request)
    {
        if (!$this->purchase = PurchaseOrder::find($route->getParameter('purchase'))) {
            if ($request->ajax()) {
                abort(404);
            }
            return back();
        }
    }


    public function editing(Route $route, Request $request)
    {
        if ($this->purchase->order_status_id != 1) {
            if ($request->ajax()) {
                abort(404);
            }
            return back();
        }
    }


    public function isConfirmed(Route $route)
    {
        if ($this->purchase->order_status_id == 5) {
            abort(404);
        }
    }


    public function index()
    {
        $purchases = PurchaseOrderView::orderBy('created_at', 'desc')->get();
		
		
        return view('purchases.index', compact('purchases'));
    }


    public function create()
    {
		
        $order = PurchaseController::getNewOrderPurchase();
        $providers = Provider::All();
        $warehouses = Warehouse::where('main', 1)->lists('description', 'id');
        $bo = Warehouse::where('main', 1)->select('id')->get();
        $bodega=$bo[0];
        $num_doc=array("num_doc_temp"=>str_replace(' ','',str_replace(':','',str_replace('-','', Carbon::now()->toDateTimeString().Carbon::now()->micro))));
        $articleList = ArticleView::listing();
        
        return view('purchases.create', compact('order', 'articleList', 'providers', 'warehouses','num_doc','bodega'));
    } // end create


    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        
        $data = $request->only('require_date', 'warehouse_id', 'provider_id', 'observation', 'items','num_doc_temp');
        Storage::disk('local')->put('data.txt',json_encode($data['num_doc_temp']));
        $data['order'] = PurchaseController::getNewOrderPurchase();
        $data['require_date'] = $data['require_date'] ? Carbon::parse($data['require_date'])->format('Y-m-d') : null;
        $data['created_by'] = auth()->user()->id;
        $data['updated_by'] = auth()->user()->id;

        $data = ArrayHandler::array_upper($data);
      
     
        foreach($data ['items'] as $key )
        {
            $id_article=DB::table('tmk_articles')
                ->select('id')
                ->where('internal_reference',$key['internal_reference'])
                ->get();
     
            $addid[]=array(
                'article'=>$id_article[0]->id
                ,'internal_reference'=>$key['internal_reference']
                ,'quantity'=>$key['quantity']
                ,'unit_cost'=>$key['unit_cost']
                ,'amount'=>$key['amount']
            );
          
        }
        $data['items']=$addid;
       
        $purchase = new PurchaseOrder;
		
        if (!$purchase->isValid($data)|| !isset($data['items'])) {
			
            ResponseHandler::alertView(2, '', 'Soluciona los errores en el formulario.', false);
			return redirect('purchase/create');
            //return back()->withInput($request->all())->withErrors($purchase->errors);
        }
		

        try {

            DB::beginTransaction();

            $purchase->fill($data);
            $purchase->save();

            $items = [];

            foreach ($data['items'] as $item) {
                array_push($items, new PurchaseOrderDetail([
                    'order_id' => $purchase->id,
                    'article_id' => $item['article'],
                    'quantity' => $item['quantity'],
                    'unit_cost' => $item['unit_cost'],
                    'amount' => $item['quantity'] * $item['unit_cost']
                ]));
            }

            $purchase->details()->saveMany($items);

            if (1==1) {


                $articles = ArticleBodega::movementsInWarehouse($purchase->details, $purchase->warehouse_id, 1);

                $purchase->articleWarehouseMovements()->saveMany($articles);
                $purchase->order_status_id = 5; // Finalizado
                $purchase->confirmed_at = Carbon::now()->toDateTimeString();
                $purchase->save();
            }
//SE ACTUALIZA EL ESTADO DE LAS SERIES INGRESADAS
                DB::table('tmk_purchase_series')
                ->where('num_doc_temp', $data['num_doc_temp'])
                ->update(['status_doc' => 1]);

               // $data['items'] 
        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();

        } catch (Exception $e) {
            DB::rollback();
        }

        DB::commit();

        ResponseHandler::alertView(1, '', 'La compra se ha registrado correctamente.', false);

         return redirect('purchases/'.$purchase->id);

    } 






    public function show()
    {
        $proveedores=Clients::buscar_cliente($this->purchase->provider_id);
        if(isset($proveedores[0]))
        {
            $proveedor=$proveedores[0];
        }
        else
        {
            $proveedor=$proveedores;
        }
        
        $purchase=$this->purchase;
        return view('purchases.show', compact('purchase','proveedor'));
    } // function


    public function edit()
    {
        $this->purchase->editing = true;
        $providers = Provider::All();
        $warehouses = Warehouse::listing();
        $articleList = ArticleView::listing();

        return view('purchases.edit', [
            'purchase' => $this->purchase,
            'articleList' => $articleList,
            'providers' => $providers,
            'warehouses' => $warehouses
        ]);
    } // function


    public function update(Request $request)
    {
        try {
            $data = $request->only('require_date', 'warehouse_id', 'provider_id', 'observation', 'items');
            $data['updated_by'] = currentUser()->id;
            $data['require_date'] = $data['require_date'] ? Carbon::parse($data['require_date'])->format('Y-m-d') : null;

            if (!$this->purchase->isValid($data)) {
                ResponseHandler::alertView(2, '', 'Soluciona los errores en el formulario.', false);
                return back()
                    ->withInput($request->all())
                    ->withErrors($this->purchase->errors);
            }

            $data = ArrayHandler::array_upper($data);

            DB::beginTransaction();

            $this->purchase->fill($data);
            $this->purchase->save();

            $items = [];

            foreach ($data['items'] as $item) {
                array_push($items, new PurchaseOrderDetail([
                    'order_id' => $this->purchase->id,
                    'article_id' => $item['article'],
                    'quantity' => $item['quantity'],
                    'unit_cost' => $item['unit_cost'],
                    'amount' => $item['quantity'] * $item['unit_cost']
                ]));
            }

            $this->purchase->details()->delete();
            $this->purchase->details()->saveMany($items);


            if ($request->get('confirm_reception')) {

                $articles = ArticleBodega::movementsInWarehouse($this->purchase->details, $this->purchase->warehouse_id, 1);

                $this->purchase->articleWarehouseMovements()->saveMany($articles);
                $this->purchase->order_status_id = 6; // Finalizado
                $this->purchase->save();
            }


        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();

            dd($e);
        } catch (\Exception $e) {
            DB::rollback();

            dd($e);
        }

        DB::commit();

        ResponseHandler::alertView(1, '', 'Guardado.', false);

        return redirect()->route('purchase.show', [$this->purchase->id]);

    }


    public function receipt()
    {
        $proveedores=Clients::buscar_cliente($this->purchase->provider_id);
        $proveedor=$proveedores[0];
        $purchase=$this->purchase;
        return view('purchases.receipt', compact('purchase','proveedor'));
    }


    public function confirm(Request $request)
    {
        try {

            $data = $request->get('items') ? $request->only('items') : [];
            $rules = [];
            $messages = [];
            $dataFilter = [];
            Storage::disk('local')->put('data.txt',json_encode($data));

            if(count($data['items'])){

                $dataFilter = array_filter($data['items'], function ($item) {
                    return $item['quantity'];
                });
    
                $data['items'] = $dataFilter;
            }

            
            foreach ($data['items'] as $key => $value) {
                $rules['items.' . $key . '.article'] = 'required|id|exists:tmk_articles,id,deleted_at,NULL';
                $rules['items.' . $key . '.quantity'] = 'required|numeric|integer|min:1|max:' . PurchaseOrderDetail::pendingArticlesByPurchase($this->purchase->id, $value['article']);


                $messages['items.' . $key . '.quantity.required'] = 'La cantidad es requerida';
                $messages['items.' . $key . '.quantity.numeric'] = 'La cantidad es inválida';
                $messages['items.' . $key . '.quantity.integer'] = 'La cantidad es inválida';
                $messages['items.' . $key . '.quantity.max'] = 'La cantidad es inválida';
            }


            $validator = Validator::make($data, $rules, $messages);

            if ($validator->fails()) {
                ResponseHandler::alertView(2, 'Error', '', false);

                return back()
                    ->withInput($request->all())
                    ->withErrors($validator->errors());
            }



            $details = [];

            foreach ($data['items'] as $detail) {
                array_push($details, new PurchaseReceipt([
                    'article_id' => $detail['article'],
                    'quantity' => $detail['quantity'],
                ]));
            }


            DB::beginTransaction();

            $this->purchase->purchaseReceipts()->saveMany($details);


            // Crea un objeto del modelo PurchaseOrderDetail con los datos introducidos 
            // por el usuario para almacenarlos en la bodega
            $purchaseDetails = [];
             foreach ($data['items'] as $detail) {
                array_push($purchaseDetails, new PurchaseOrderDetail([
                    'article_id' => $detail['article'],
                    'quantity' => $detail['quantity'],
                    'order_id' => $this->purchase->id
                ]));
            }


            // Movement => 1 = Compra entrada proveedor
            $articles = ArticleBodega::movementsInWarehouse($purchaseDetails, $this->purchase->warehouse_id, 1);
			
            $this->purchase->articleWarehouseMovements()->saveMany($articles);
        
            $this->purchase->order_status_id = $request->get('confirm_reception') ? 5 : 9; // Confirmed : Partially Confirmed
            $this->purchase->confirmed_at = Carbon::now()->toDateTimeString();
            $this->purchase->save();

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(2, 'Ocurrió un error mientras se confirmaba la compra.', 'Error al confirmar la compra!', false);
        
            return back()->withInput($request->all());
        } catch (Exception $e) {
            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(2, 'Ocurrió un error mientras se confirmaba la compra.', 'Error al confirmar la compra!', false);
            return back()->withInput($request->all());
        }

        DB::commit();

        ResponseHandler::alertView(1, 'La compra ha sido confirmada con exito.', 'Compra Confirmada! .', false);
		return redirect('purchases/'.$this->purchase->id);
       // return redirect()->route('purchase.show', [$this->purchase->id]);
    }




    public function cancel()
    {
        try {

            DB::beginTransaction();

            $this->purchase->order_status_id = 7;
            $this->purchase->canceled_at = Carbon::now()->toDateTimeString();
            $this->purchase->save();

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            dd($e);

        } catch (Exception $e) {
            DB::rollback();
            dd($e);
        }

        DB::commit();

        ResponseHandler::alertView(1, '', 'La Compra fue cancelada.', false);

        return back();
    }


    public function destroy()
    {
        try {
            if (Gate::denies('delete', $this->purchase)) {

                $view = ResponseHandler::alertView(2, 'No se puede elimnar la compra ' . $this->purchase->order, 'Error!', true);
                return response()->json(['alert' => $view], 500);
            }

            DB::beginTransaction();

            if ($this->purchase->isDraft()) {

                $this->purchase->forceDelete();
            } else {

                $this->purchase->delete();
            }

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            return response()->json('Ocurrió un error!', 500);

        } catch (Exception $e) {

            DB::rollback();
            return response()->json('Ocurrió un error!', 500);
        }

        DB::commit();

        $view = ResponseHandler::alertView(1,
            'La compra ' . $this->purchase->order . ' ha sido eliminada con exito.',
            currentUser()->fullName . ' eliminó una compra!',
            true);

        return response()->json(['alert' => $view], 200);
    }

    public function getNewOrderPurchase()
    {

        $indice = 0;
        $order = '';

        do {

            $order = trans('app.folio.purchase') . str_pad($indice, 2, "0", STR_PAD_LEFT) . date("my");

            if ($indice < 100 and $indice >= 0)
                $indice++;

        } while (count(PurchaseOrder::getOrderPurchaseByOrder($order)) > 0);

        return $order;

    } // function


    public function approveOrderPurchase(Request $request)
    {
        DB::table('users')
            ->where('id', 1)
            ->update(['votes' => 1]);

        return $request;
    }


    /**
     * ORDER STATUS
     *
     * ID  DESCRIPTION
     * 1   Borrador
     * 2   En revisión
     * 3   Rechazado
     * 4   Aprobado
     * 5   Confirmado
     * 6   Finalizado
     * 7   Cancelado
     */
    public function confirmReceivedPurchase(Request $request)
    {
        if (PurchaseOrder::updatePurchaseOrderState($request->get('order_id'), 5)) {
            return redirect('received/purchase/' . $request->get('order_id') . '/create');
        } else {
            return back();
        }
    }


    public function cancelReceivedPurchase(Request $request)
    {
        if (PurchaseOrder::updatePurchaseOrderState($request->get('order_id'), 7)) {
            return back();
        } else {
            return back();
        }
    }


    public function storeInBodega(Request $request)
    {

        $articleBodega = ArticleBodega::create([
            'article_id' => (int)$request->get('article_id'),
            'bodega_id' => (int)$request->get('bodega'),
            'quantity' => $request->get('quantity'),
            'movement_id' => 1,
        ]);

        PurchaseArticleBodega::create([
            'purchase_detail_id' => (int)$request->get('purchase_detail_id'),
            'article_bodega_id' => $articleBodega->id,
        ]);

        return response()->json(['message' => 'Exito']);
    }



    public function create_series($num_doc,$id,$internal_reference,Request $request)
    {
      
        $data=json_decode($request->getContent(), true);
        
        foreach($data as $key => $value)
        {
            DB::table('tmk_purchase_series')->insert(
                ['item_id' => $value['id']
                , 'internal_reference' => $internal_reference
                , 'num_doc_temp' => $num_doc
                ,'serie'=> $value['serie']
                ,'article_id'=>$id
                ,'status_doc'=>0
                ,'status_in'=>1
                ,'creado_el'=>Carbon::now()->toDateTimeString()
                ]
            );
           // Storage::disk('local')->put($key.'.txt',$value['serie']);
        }
        $r=Purchase::buscar_series($num_doc);
        return $data;
    }


    public function edit_series($num_doc,$id,$internal_reference,Request $request)
    {
      
        $data=json_decode($request->getContent(), true);

   

        DB::table('tmk_purchase_series')
            ->where('internal_reference', $internal_reference)
            ->where('num_doc_temp',$num_doc)
            ->where('article_id',$id)
            ->delete();
            Storage::disk('local')->put('delete.txt',$request);
        foreach($data as $key => $value)
        {
            DB::table('tmk_purchase_series')->insert(
                ['item_id' => $value['id']
                , 'internal_reference' => $internal_reference
                , 'num_doc_temp' => $num_doc
                ,'serie'=> $value['serie']
                ,'article_id'=>$id
                ,'status_doc'=>0
                ,'status_in'=>1
                ,'creado_el'=>Carbon::now()->toDateTimeString()
                ,'actualizado_el'=>Carbon::now()->toDateTimeString()
                ]
            );
           // Storage::disk('local')->put($key.'.txt',$value['serie']);
        }
        $r=Purchase::buscar_series($num_doc);
        return $data;
    }

    public function cancelar($num_doc,$warehouse_id)
    {
        
        DB::table('tmk_purchase_series')->where('num_doc_temp',$num_doc)->delete();

        DB::table('tmk_lotes')->where('num_doc_temp',$num_doc)->delete();
        return redirect()->route('purchase.index');

    } 



}
