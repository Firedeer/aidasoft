<?php

namespace TradeMarketing\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Routing\Route;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;

// User Strore Request 
use TradeMarketing\Http\Requests\UserStoreRequest;

// Model User
use TradeMarketing\Models\Roles;
use TradeMarketing\Traits\ResponseHandler;
use TradeMarketing\User;
// Model Roles

// Session and Redirect
use Session;
use Redirect;

use DB;
use Auth;

class UserController extends Controller
{

    public function __construct()
    {
        $this->beforeFilter('@find', ['only' => ['show', 'edit', 'update','actualizar']]);
    }



    public function find(Route $route, Request $request){
        $this->user = User::find($route->getParameter('user'));

        if(!$this->user){

            if($request->ajax()){
                abort(404);
            }
            return back();
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $res = User::Create_User($request);
        $res = json_decode( $res); // codifica el Json en formato Array 
        
        if( $res->{'state'} == true ){

            $role = DB::table('tmk_roles')
                        ->where('description', '=', 'Usuario')
                        ->select('id')
                        ->get();

            $role = json_encode($role); // convierte el resultado de la consulta en formato JSON
            $role = json_decode($role, true);
            $role = $role[0]['id'];

            User::create([
                'agent_id' => $res->{'id'},
                'role_id'  => $role,
                'password' => bcrypt($request->password)
            ]);
            
            return response()->json(['res' => $res]);
            // return Redirect::to('user');
        }



        // Session::flash('title', 'Registrado Correctamente');
        // Session::flash('message', 'Usuario registrado Correctamente');
        // Session::flash('status', 'success');
        // Session::flash('icon', 'thumbs-up');

        // return view('users')->compact('message','Registrago Correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = $this->user;
        return view('users.show', compact('user'));
    }



    public function show_users()
    {
        // $ids = base64_decode($id);
        $users = User::list_users();
        // return $user;
        return response()->json($users);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = $this->user;
   


        if($user['role_id']=='2' or $user['role_id']=='1' )
        {
        $roleList = Roles::listing();
        return view('admin.users.edit', compact('user', 'roleList'));
        }
        else
        {
         return view('users.show', compact('user'));
        }

    }





   public function password($userd)
    {

         $roleList = Roles::listing();
         $user= User::find($userd);
    
        return view('users.pass', compact('user','roleList'));
       
    }

  public function actualizar(Request $request)
    {

        $data = $request->only('firstname', 'lastname', 'phone');

        ResponseHandler::alertView(1, '', 'El usuario ha sido actualizado correctamente.', false);

    }





    public function update(Request $request)
    {

             

                $request['password']=bcrypt($request['password']);
   


     if($request['tipo']=='change_password'){
        $data = $request->only('firstname', 'lastname', 'phone', 'address', 'email', 'role_id','password');
        }
      else
      {
          $data = $request->only('firstname', 'lastname', 'phone', 'address', 'email', 'role_id'); 
       }

        if($this->user->isValid($data)){

            $this->user->fill($data);
            $this->user->save();

            ResponseHandler::alertView(1, '', 'El usuario ha sido actualizado correctamente.', false);

            return redirect()->route('user.show', $this->user->id);
        }

        ResponseHandler::alertView(2, 'Soluciona los errores en el formulario.', '', false);

        return back()
            ->withInput($request->all())
            ->withErrors($this->user->errors);
    }




 public function getCredentials($request)
    {
        return [
            'username' => $request->get('username'),
            'password' => $request->get('password'),
            'active' => true,
            'confirmation_token' => null
        ];
    }

    public function destroy($id)
    {
        //
    }


    public function updateUserRole(Request $request){
        $user =  Auth::user()->agent_id;
        $role = $request->role;
        
        $response = User::Update_User_Role($user, $role);

        return $response;
    }
	 public function init(Request $request)
    {
		$users = User::all();
		
		$rol;
		if($request->txt_rol=='Choose...')
		{
			ResponseHandler::alertView(1, '', 'El usuario ha sido actualizado correctamente.', false);
		}
		elseif($request->txt_rol=="superadmin")
		{
			$rol=1;
		}
		elseif($request->txt_rol=="admin")
		{
			$rol=2;
		}
		elseif($request->txt_rol=="user")
		{
			$rol=3;
		}
		elseif($request->txt_rol=="editor")
		{
			$rol=4;
		}
		
		$act=0;
		if(isset($request->chk_active))
		{
		$act=1;	
		}
		$result=DB::table('tmk_users')->insert(
				['firstname' => strtoupper($request->txt_nombre)
				,'lastname' => strtoupper($request->txt_apellido)
				,'address' => strtoupper($request->txt_direccion)
				,'phone' => $request->txt_telefono
				,'email' => $request->txt_email
				,'username' => $request->txt_usuario
				,'password' => '$2y$12$GLwWxixAlY2YF8.wQz5NaeazdeHUm5GQH1Y3u.bNBAnhHDcApY9Re'
				,'active' =>$act
				,'agent_id' =>time()
				,'sucursal_id' => NULL
				,'confirmation_token' => NULL
				,'remember_token' => NULL
				,'created_at' => date("Y-m-d H:i:s")
				,'updated_at'=> date("Y-m-d H:i:s")
				,'role_id' => $rol
				]
								);
		
		ResponseHandler::alertView(1, '', 'El usuario ha sido creado correctamente.', false);
        return redirect()->route('admin.user.index', compact('users'));
        
    }

}
