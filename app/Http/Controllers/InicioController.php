<?php

namespace TradeMarketing\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Routing\Route;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;

// User Strore Request 
use TradeMarketing\Http\Requests\UserStoreRequest;

// Model User
use TradeMarketing\Models\Roles;
use TradeMarketing\Traits\ResponseHandler;
use TradeMarketing\User;
// Model Roles

// Session and Redirect
use Session;
use Redirect;

use DB;
use Auth;
class InicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inicio()
    {
		$in = DB::table('tmk_entradas_salidas_inicio')
                     ->where('fecha', '=',date("Y-m-d"))
					 ->where('movement_type','=','entry')
                     ->get();
		$salidas = DB::table('tmk_entradas_salidas_inicio')
                     ->where('fecha', '=',date("Y-m-d"))
					 ->where('movement_type','=','exit')
                     ->get();
		$stock = DB::table('tmk_inventario_inicio')
                     ->select(DB::raw('sum(stock) as stock'))
                     ->where('fecha', '=',date("Y-m-d"))
                     ->get();

        $entradas='';
       
        if(isset($in) && count($in)>0)
        {
           
            foreach($in as $entrada)
            {
                $entradas=$entrada;
            }
            
        }
		
        
        return view('index',compact('entradas'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
