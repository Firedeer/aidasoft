<?php

namespace TradeMarketing\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Validator;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;
use \TradeMarketing\Models\Attribute;
use \TradeMarketing\Models\AttributeValue;
use TradeMarketing\Traits\ArrayHandler;

class AttributeController extends Controller
{


    public function __construct(){
        $this->beforeFilter('@isAjax');
    }



    public function isAjax(Route $route, Request $request){

        if(!$request->ajax()){
            abort(404);
        }
    }




    public function getValues(Request $request, $id){
		return Attribute::findOrFail($id)->values;
	}

	public function attributeLists(){
		return  Attribute::all()->lists('description', 'id'); 
	}


	public function lists(){
		return Attribute::select('id', 'description')->get();
	}

	public function listValue(){
		return AttributeValue::select('id', 'description', 'attribute_id')->get();
	}


	public function findAttributeValue(Request $request, $id){
		return AttributeValue::find($id);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // return "hola";

        // if($request->ajax()){
    	return Attribute::get(['id', 'description'])->toJson();
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	return view('attributes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->only('description');

        $data = array_map('trim', $data);

        $data['created_by'] = auth()->user()->id;
        $data['updated_by'] = auth()->user()->id;

        $messages = [
            'required' => 'El nombre del atributo es obligatorio',
            'unique' => 'El nombre del attributo ya ha sido registrado'
        ];

        $validator = Validator::make($data, [
            'description' => 'required|unique:tmk_attributes,description'
        ], $messages);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }


        $data = ArrayHandler::array_upper($data);

        $attribute = new Attribute;
        $attribute->fill($data);
        $attribute->save();


        return response()->json($attribute);

//    	$attribute =  Attribute::where('description', $request->only('description'))->first();
//        $values  = $request->get('attrValue');
//
//        if( !$attribute ){
//            $attribute =  Attribute::create(['description' => $request->get('attribute'), 'created_by' => \Auth::user()->id, 'updated_by'  => \Auth::user()->id]);
//        }
//
//    	AttributeValue::addNewValues($attribute->id, $values);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
