<?php

namespace TradeMarketing\Http\Controllers;

use Illuminate\Http\Request;

use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;


// Models
use \TradeMarketing\Movement;
use \TradeMarketing\Devolution;
use \TradeMarketing\ArticleBodega;
use \TradeMarketing\Invoice;

class DevolutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoice = Invoice::list_invoices(); 
        return view( 'movements.devolutions.create', compact('invoice') ) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDevolutionPurchase(Request $request)
    {   

        // dd( $request->request );

        $devolution = Devolution::create([
            'date'        => $request->get('date'),
            'description' => $request->get('description'),
            'amount'      => 0.00
            ]);

        for ( $i=0; $i<count( $request->get('check') ); $i++ ){

            if( $request->get('check')[$i] == 1 ){

                $article = ArticleBodega::create([
                    'date'              => $request->get('date'),
                    'article_id'        => $request->get('article')[$i],
                    'bodega_id'         => $request->get('bodega'),
                    'quantity'          => $request->get('devolution')[$i],
                    'type_movement_id'  => $request->get('movement')
                    ]);

                Devolution::insert_devolution_purchase( $devolution->id, $request->get('invoice'), $article->id, $request->get('reference')[$i], 1, '' );
            } 
        }

        return "Exito";

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
