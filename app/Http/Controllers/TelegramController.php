<?php

namespace TradeMarketing\Http\Controllers;

use App\Telegram;
use Carbon\Carbon;
use coinmarketcap\api\CoinMarketCap;
use Exception;
use Illuminate\Http\Request;
use Telegram\Bot\Api;
use Storage;
use GuzzleHttp;
use DB;

class TelegramController extends Controller
{
    

    protected $telegram;


    protected $update_id;
    protected $message_id;
    protected $first_name;
    protected $last_name;
    protected $language_code;
    protected $type;
    protected $date;
    protected $chat_id;
    protected $text;
	protected $texto_numero;
    protected $tipoarchivo;
	protected $aplicacion;
	
	
	
    protected $com=array();
    protected $com1=array();
    protected $com2=array();
    protected $com3=array();
    protected $com4=array();
    protected $com5=array();
	protected $fotos=array();

    protected $mtexto=array();
    protected $MBotones=array();
    protected $MImagenes=array();
    protected $MDocumentos=array();
    protected $MAudio=array();
    protected $MVideos=array();
    protected $ArregloMulti=array();



    public function __construct()
    {
        $this->telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
    }


    public function getMe()
    {
        $response = $this->telegram->getMe();
        return $response;
    }



    public function setWebHook()
        {
    $url = 'https://systrade.movistar.com.pa/' . env('TELEGRAM_BOT_TOKEN') . '/webhook';
    $response = $this->telegram->setWebhook(['url' => $url]);

    return $response == true ? redirect()->back() : dd($response);
        }
public function prueba(Request $request)
{
print_r($request);
}
public function handleRequest(Request $request)
{

    try{

	

mb_internal_encoding('UTF-8');
//Storage::put('error.txt',$request);
// Tell PHP that we'll be outputting UTF-8 to the browser
mb_http_output('UTF-8');
if(isset($request['message']['aplicacion']))
{
$this->aplicacion = $request['message']['aplicacion'];
}
else
{
	$this->aplicacion ='t3l3gr4m';
}

	if(isset($request['message']['from']['first_name']))
		{
			$this->first_name = $request['message']['from']['first_name'].' ';
			
		}
		else
		{
			$this->first_name='Nombre Fict';
		}
		if(isset($request['message']['from']['last_name']))
		{
			$this->last_name = $request['message']['from']['last_name'].' ';
		}
		else
		{
			$this->last_name = 'Apellido fict';
		}






        if(isset($request['message']['text']))
         {

        $this->update_id = $request['update_id'];
        $this->message_id = $request['message']['message_id'];

		
		
		
		if(isset($request['message']['from']['first_name']))
		{
			$this->first_name = $request['message']['from']['first_name'].' ';
			
		}
		else
		{
			$this->first_name='Nombre Fict';
		}
		if(isset($request['message']['from']['last_name']))
		{
			$this->last_name = $request['message']['from']['last_name'].' ';
		}
		else
		{
			$this->last_name = 'Apellido fict';
		}
        
		
        $this->type = 'app';
        $this->date = $request['message']['date'];
        $this->chat_id = $request['message']['chat']['id'];
        $this->text = $request['message']['text'];
        $this->tipoarchivo="TEXTO";
        
        }
        else if(isset($request['message']['photo']))
        {
			if(isset($request['message']['from']['first_name']))
		{
			$this->first_name = $request['message']['from']['first_name'].' ';
			
		}
		else
		{
			$this->first_name='Nombre Fict';
		}
		if(isset($request['message']['from']['last_name']))
		{
			$this->last_name = $request['message']['from']['last_name'].' ';
		}
		else
		{
			$this->last_name = 'Apellido fict';
		}
			
			
			
			
        $this->update_id = $request['update_id'];
        $this->message_id = $request['message']['message_id'];

        $this->type = $request['message']['chat']['type'];
        $this->date = $request['message']['date'];
        $this->chat_id = $request['message']['chat']['id'];
        $this->fotos[] = $request['message']['photo'];
        $this->tipoarchivo = 'photo'; 
		$this->enviarData('Imagen',"nada");
        }
          elseif(isset($request['message']['document']))
        {
           $this->update_id = $request['update_id'];
        $this->message_id = $request['message']['message_id'];
        $this->first_name = $request['message']['from']['first_name'];
        $this->last_name = $request['message']['from']['last_name'];
        $this->type = $request['message']['chat']['type'];
        $this->date = $request['message']['date'];
        $this->chat_id = $request['message']['chat']['id'];
        $this->text = $request['message']['document']; 
        $this->tipoarchivo = 'documento';
		$this->enviarData('Documento',"nada");
        }
          elseif(isset($request['message']['contact']['phone_number']))
        {
					if(isset($request['message']['from']['first_name']))
		{
			$this->first_name = $request['message']['from']['first_name'].' ';
			
		}
		else
		{
			$this->first_name='Nombre Fict';
		}
		if(isset($request['message']['from']['last_name']))
		{
			$this->last_name = $request['message']['from']['last_name'].' ';
		}
		else
		{
			$this->last_name = 'Apellido fict';
		}
			
			
			
        $this->update_id = $request['update_id'];
        $this->message_id = $request['message']['message_id'];
        $this->first_name = $request['message']['from']['first_name'];
        $this->last_name = $request['message']['from']['last_name'];
        $this->type = 'PEDNU';
        $this->date = $request['message']['date'];
        $this->chat_id = $request['message']['chat']['id'];
        $this->text = substr($request['message']['contact']['phone_number'],3); 
        $this->tipoarchivo = 'PHONE';
        }

	
        if($this->tipoarchivo == 'photo')
        {

         	$this->Imagenes();
        }
        else
        {
        	$this->masterOp();
        }
     }
     catch(Exception $e)
     {
     	dd("error ".$e);
     }



    }
 
protected function masterOpPrueba()
{
    try{
     $prueba= array();
    $client = new GuzzleHttp\Client();
    $res= $client->request('POST','http://micrositio.telefonica.cam/master.php',[
       'json' => ['message_id'=>3000
      ,'id'=>249997265
      ,'FirstName'=>'Johnny'
      ,'LastName'=>'Gonzalez'
      ,'type'=>'app'
      ,'date'=>1534772253
    ,'text'=>'cancelar'
    ,'id_bot'=>'5' 
    ,'tipo'=>'TEXTO'
	,'aplicacion'=>'tel'
    ]
    ]);
    $response=$res->getBody();
    $rsponm=json_decode($response,true);
	dd($rsponm);
    $contador=0;
        foreach ($rsponm as $value) 
        {
        	$this->com[] .=$value['TIPO'] .chr(10);
       		if($value['TIPO']== "PREGUNTA")
           	 {
            	
              		 if($value['VALOR']=="TEXT")
               		{

        			$this->mtexto[] .= $value['VALOR2']."\n" . chr(10);

             	  }
                elseif($value['VALOR']=="BOTON")
                {
                  $this->MBotones[]=$value['VALOR2'] . chr(10);
                }
                elseif($value['VALOR']=="BOTON_MULTI")
                {
                	$this->ArregloMulti[]=$this->MBotones;
                	unset($this->MBotones);
                }

             }
                   elseif($value['TIPO']=="FLUJO")
             {
             	
             		$this->com1[] .=$value['VALOR'] .chr(10);

             }

}

print_r($this->com);
}
catch(Exception $e)
{
  dd($e);
}
    
}


protected function Imagenes()
{  
//print_r(count($this->fotos[0]));   
//print_r("        ");  
//print_r($this->fotos[0][(count($this->fotos[0])-1)]["file_id"]);
//Storage::disk('uploads')->put('file.txt', 'Contents');
$dk=($this->fotos[0][(count($this->fotos[0])-1)]["file_id"]);
foreach ($this->fotos as $value) 
{
//$this->enviarData($value[count($this->fotos)]['file_id'],"nada");
//$this->enviarData($value[0]['file_id'],"nada");

//EXIT;

  $client = new GuzzleHttp\Client();
    $res= $client->request('GET','https://api.telegram.org/bot' . env('TELEGRAM_BOT_TOKEN') . '/getFile?file_id='.$dk);
  $reponse=json_decode($res->getBody(),true);

$this->enviarData('Espere un momento analizando documento',"nada");

$this->update_id = 0;
        $this->message_id = 9999999;
        $this->first_name = 'BOTCOMERCIAL';
        $this->last_name = 'BOTCOMERCIAL';
        $this->type = 'PROCESO';
       	$this->date = 1494449903;

       	$this->text =  'https://api.telegram.org/file/bot256599318:AAG4RpZPRfAw791dBUHJ2sBJtUI1bfI3uTw/'.$reponse['result']['file_path'];
       	$this->tipoarchivo="IMAGEN";

$this->masterOp();


}

}


protected function masterOp()
{

try{


    $client = new GuzzleHttp\Client();
    $res= $client->request('POST','http://micrositio.telefonica.cam/master.php',[
    	 'json' => ['message_id'=>$this->message_id
    	,'id'=>$this->chat_id
    	,'FirstName'=>$this->first_name
    	,'LastName'=>$this->last_name
    	,'type'=>$this->type
    	,'date'=>$this->date
		,'text'=>$this->text
		,'id_bot'=>'5' 
		,'tipo'=>$this->tipoarchivo 
		,'aplicacion'=>$this->aplicacion
    ]
    ]);
    $response=$res->getBody();
   
        $rsponm=json_decode($response,true);

	
		$contador=0;
        foreach ($rsponm as $value) 
        {
        
        $this->com[] =ltrim(rtrim($value['TIPO'])) .chr(10);		
 		if(ltrim(rtrim($value['TIPO']))== "PREGUNTA")
            {
					
            	
               if(ltrim(rtrim($value['VALOR']))=="TEXT")
               	{
					
        			$this->mtexto[] = $value['VALOR2'] . chr(10);

             	}
                elseif($value['VALOR']=="BOTON")
                {
                  //  echo $contador.'  '.$value['VALOR2'] . chr(10);
                   $this->MBotones[]=$value['VALOR2'] . chr(10);
                   
                }
                elseif($value['VALOR']=="BOTON_MULTI")
                {
                   
                    // echo $contador.'  '.$value['VALOR2'] . chr(10);
                	$this->ArregloMulti[]=$this->MBotones;
                	unset($this->MBotones);
                }
               	elseif($value['VALOR']=="IMAGE")
               	{

        			$this->MImagenes[]= $value['VALOR2'] . chr(10);

             	}
             	elseif($value['VALOR']=="DOCUMENT")
               	{

        			$this->MDocumentos[]= $value['VALOR2'] . chr(10);

             	}
             	elseif($value['VALOR']=="AUDIO")
               	{

        			$this->MAudio[]= $value['VALOR2'] . chr(10);

             	}
             	elseif($value['VALOR']=="VIDEO")
               	{

        			$this->MVideos[]= $value['VALOR2'] . chr(10);

             	}



             }
             elseif(ltrim(rtrim($value['TIPO']))=="FLUJO")
             {
             		//$this->com .=ltrim(rtrim($value['TIPO'])) .chr(10);
             		    $this->com1[]=ltrim(rtrim($value['VALOR'])) .chr(10);
                    $this->com2[]=ltrim(rtrim($value['VALOR2'])) .chr(10);
                    $this->com3[]=ltrim(rtrim($value['VALOR3'])) .chr(10);
                    $this->com4[]=ltrim(rtrim($value['IDKEY'])) .chr(10);
                    $this->com5[]=ltrim(rtrim($value['NUMERO'])) .chr(10);


             }


     
		 $contador=$contador+1;

         }


$this->enviar();
 $this->verificarFlujo();
}
catch(Exception $e)
{
  dd($e);
}
    
}



protected function enviar($info = null)
{
	


$texto_='';
if(count($this->mtexto)>0)
{
	foreach ($this->mtexto as $key) 
	{
		
		$texto_ .=$key."\n" .chr(10);
	}
}

if(count($this->MBotones)>0)
{
$this->ArregloMulti[]=$this->MBotones;
unset($this->MBotones);
}


if(count($this->ArregloMulti)>0)
{
            $message = '';
            $replyMarkup = 
            array(
            'keyboard' =>
            $this->ArregloMulti,
            'resize_keyboard' => true,
            'one_time_keyboard' => true,
            'selective' => true
                );

        $encodedKeyboard = json_encode($replyMarkup);
        $this->enviarData($texto_ ,$encodedKeyboard);
}
else
{
	if(count($this->mtexto)>0)

	{


     
        $this->enviarData($texto_,"nada");

	}

}      
}

protected function verificarFlujo($info = null)
{
	
    for($i = 0, $size = count($this->com1); $i < $size; ++$i) 
    {


	if(ltrim(rtrim($this->com1[$i]))=="WS" )
    {
    	$wsr=ltrim(rtrim($this->com2[$i]));
	if($wsr=="getnum")
	{
		$this->texto_numero=$this->text;
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/getNum.php',[
    	 'json' => ['numero'=>$this->text ,'id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
	elseif($wsr=="getinfo")
	{
		
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/getInfo.php',[
    	 'json' => ['id'=>$this->chat_id]]);
		
        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		//$this->enviarData($va->response,"nada");
		$this->proc($this->com3[$i]);
	}
		elseif($wsr=="getinfoI")
	{
		
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/getInfoICC.php',[
    	 'json' => ['id'=>$this->chat_id]]);
		
        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		//$this->enviarData($va->response,"nada");
		$this->proc($this->com3[$i]);
	}
	elseif($wsr=="getresidencial")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/getResidencialbyParameter.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
	elseif($wsr=="getresid")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/getResidencial.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}


	elseif($wsr=="CreateBilling")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/createBilling.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
	elseif($wsr=="Createsale")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/createsales.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}

		elseif($wsr=="submitOrder" && $this->chat_id!='249997265')
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/submitOrden.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
		elseif($wsr=="updateuser")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/update_user.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
			elseif($wsr=="modifyCr")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/modify.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
	
				elseif($wsr=="virtual")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/getNumber250.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
					elseif($wsr=="virtuallock")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/locknumber.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
				elseif($wsr=="modifyChange")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/modifyChance.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
	elseif($wsr=="getproduct")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/GetProduct.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
	elseif($wsr=="DownloadIm")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/DownImage.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
	elseif($wsr=="SendVale")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/enviarVale.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
	elseif($wsr=="Wsalta")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/Wsalta.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
	elseif($wsr=="getimei")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/GetProduct_v1.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
	elseif($wsr=="desactivarlinea")
	{
				  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/desactivar_linea.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
		elseif($wsr=="getnumPre")
	{
				  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/getNumPreactivo.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
		elseif($wsr=="ordenVenta" && $this->chat_id!='249997265')
	{
		
				  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/orden_desactivar_linea.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	
	}
			elseif($wsr=="getinfom")
	{
				  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/getInfoRepocision.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
			elseif($wsr=="getProdR")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/GetProductReposicion.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
	elseif($wsr=="mody")
	{
		  $client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/modifyReposicion.php',[
    	 'json' => ['id'=>$this->chat_id]]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	$va=$res->getBody();
		$this->proc($this->com3[$i]);
	}
    }

	 elseif(ltrim(rtrim($this->com1[$i]))=="PR")
       {

	

        $this->update_id = 0;
        $this->message_id = 9999999;
        $this->first_name = 'BOTCOMERCIAL';
        $this->last_name = 'BOTCOMERCIAL';
        $this->type = 'PROCESO';
       	$this->date = 1494449903;

       	$this->text =  $this->com3[$i];
       	$this->tipoarchivo="TEXTO";
       // $this->enviarData("enviando a master op","nada");
	unset($this->com);
   unset($this->com1);
   unset($this->com2);
    unset($this->com3);
    unset($this->com4);
    unset($this->com5);
	unset($this->fotos);

    unset($this->mtexto);
    unset($this->MBotones);
    unset($this->MImagenes);
    unset($this->MDocumentos);
    unset($this->MAudio);
    unset($this->MVideos);
    unset($this->ArregloMulti);
     	$this->masterOp();

       }
    elseif (ltrim(rtrim($this->com1[$i])) == "ORAQUERY")
    {


    }
    elseif (ltrim(rtrim($this->com1[$i])) == "ORANONQUERY")
    {


    }
    elseif(ltrim(rtrim($this->com1[$i]))=="PEDIRNUM" )
    {

            $message = '';
            $keyboard = [
            'keyboard' => [
                    [
                    ['text'=>'Menu principal','request_contact'=>true]
                 ]],
            'resize_keyboard' => true,
             'one_time_keyboard' => true,
            'selective' => true
     ];

        $encodedKeyboard = json_encode($keyboard);
        $message .= 'Bienvenido al bot de  Gestiones Movistar' . chr(10);
        $this->enviarData($message,$encodedKeyboard);
     }





	}
  

}


protected function proc($dato)
{   
        $this->update_id = 0;
        $this->message_id = 9999999;
        $this->first_name = 'BOTCOMERCIAL';
        $this->last_name = 'BOTCOMERCIAL';
        $this->type = 'PROCESO';
       	$this->date = 1494449903;

       	$this->text = $dato;
       	$this->tipoarchivo="TEXTO";
        //$this->enviarData("enviando","nada");
	unset($this->com);
   unset($this->com1);
   unset($this->com2);
    unset($this->com3);
    unset($this->com4);
    unset($this->com5);
	unset($this->fotos);

    unset($this->mtexto);
    unset($this->MBotones);
    unset($this->MImagenes);
    unset($this->MDocumentos);
    unset($this->MAudio);
    unset($this->MVideos);
    unset($this->ArregloMulti);

		$this->masterOp();
       // $this->enviarData("enviando a master op".$dato,"nada");
     	
}
protected function getprueba()
{
	$client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/getInfo.php',[
    	 'json' => ['numero'=>'64390284' ,'id'=>'26669555']]);

        $res->getStatusCode();
       	$res->getHeader('content-type');
       	echo $res->getBody();
		
}


protected function enviarData($message,$encodedKeyboard)
{

          
            if($encodedKeyboard=="nada")
            {

            $data = [
            'chat_id' => $this->chat_id,
            'text' => $message,
              ];
            }
            else
            {
              $data = [
            'chat_id' => $this->chat_id,
            'text' => $message,
            'reply_markup' => $encodedKeyboard,
              ];  
            }
            

			if($this->aplicacion=="03jdpfinrn3455mskown249ns")
		{
		dd(json_encode($data));
		}

else if($this->aplicacion=="t3l3gr4m")
{	
			
            $this->sendMessage($data);
}
			
}

protected function formatArray($data)
{
        $formatted_data = "";
        foreach ($data as $item => $value) {
            $item = str_replace("_", " ", $item);
            if ($item == 'last updated') {
                $value = Carbon::createFromTimestampUTC($value)->diffForHumans();
            }
            $formatted_data .= "<b>{$item}</b>\n";
            $formatted_data .= "\t{$value}\n";
        }
        return $formatted_data;
    }
 
protected function sendMessage($data, $parse_html = false)
{

 
        if ($parse_html) $data['parse_mode'] = 'HTML';
 
        $this->telegram->sendMessage($data);
    }

protected function crontab()
{
	$client = new GuzzleHttp\Client();
		 $res = $client->request('POST','http://micrositio.telefonica.cam/Crontab_Proceso.php');

		
}

}

