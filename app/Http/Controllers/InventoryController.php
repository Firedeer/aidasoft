<?php namespace TradeMarketing\Http\Controllers;

use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;

use Illuminate\Http\Request;

use \TradeMarketing\ArticleBodega;
use \TradeMarketing\Inventory;
use \TradeMarketing\InventoryDetail;
use \TradeMarketing\Models\views\InventoryView;


class InventoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
//        $inventories = Inventory::All();
//        $inventory   = Inventory::list_inventory_all();

        if($request->ajax()){


            $filterBy = $request->get('filterBy');
            $filter = $request->get('filter');
            $inventory = [];

            if(isset($filter) && isset($filterBy)){

                switch ($filterBy){
                    case 'category':
                        $inventory = InventoryView::category($filter);
                        break;

                    case 'warehouse':
                        $inventory = InventoryView::warehouse($filter);

                        break;

                    case 'movement':
                        $inventory = InventoryView::movement($filter);
                        break;

                    case 'article':
                        $inventory = InventoryView::article($filter);
                        break;

                    case 'all':
                        $inventory = InventoryView::all();
                        break;

                    default:
                        break;
                }

                return response()->json( view('inventories.partials.inventoryRender', compact('inventory'))->render());

            }
        }


        $inventory = InventoryView::all();

        return view('inventories.index', compact('inventory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('inventories/create.bodega');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $array = json_decode($request['data']); // convierte un string codificado en JSON a una variable de PHP

        $quantity = $request['articulos'];
        $total = explode("$", $request['total']); // separa la cantidad del signo de dollar, retorna un array
        $amount = $total[1];

        // obtener la fecha de hoy 
        $date = date("Y-m-d");

        // registrar el nuevo inventario
        $inventory = Inventory::create([
            'description' => '',
            'date' => $date,
            'quantity_items' => $quantity,
            'amount' => $amount
        ]);


        $id = $inventory['id']; // obtengo el id del inventario insertado 

        // bucle para registrar los articulos del inventario
        foreach ($array as $obj) {

            $res = InventoryDetail::create([
                'article_id' => $obj->{'article'}, // articulo
                'bodega_id' => $obj->{'bodega'},  // bodega
                'inventory_id' => $id,                 // codigo del inventario
                'quantity' => $obj->{'compro'},  // unidades comprometidas
                'unit_cost' => $obj->{'costou'}   // costo por unidad
            ]);
        }
        return response()->json(['message' => 'Inventario registrado correctamente', 'inventario' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $inventory = InventoryDetail::List_Inventory_Details($id);
        return view('inventories/show.bodega', ['inventory' => $inventory]);
    }

    public function show_inventory_bodega($id)
    {
        $inventory_bodega = Inventory::List_Inventory_Bodega($id);
        return response()->json($inventory_bodega);

    }

    public function show_annual_inventory()
    {
        $inventories = Inventory::Annual_Inventory();
        return $inventories;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {

    }

    public function dest()
    {

        return Inventory::destruir();
    }

}