<?php

namespace TradeMarketing\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use TradeMarketing\Purchase;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;
use TradeMarketing\Models\ArticleBodega;
use TradeMarketing\Models\ExternalMovement;
use TradeMarketing\Models\ExternalMovementDetail;
use TradeMarketing\Models\Warehouse;
use TradeMarketing\Traits\ArrayHandler;
use TradeMarketing\Traits\ResponseHandler;
use TradeMarketing\User;
use TradeMarketing\Aprobar;
use Storage;
use Response;

class LotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function findlotes($referencia,$num_doc,$num_item)
    {
        
        
        $series = \TradeMarketing\Purchase::buscar_lotes_num_doc($referencia,$num_doc,$num_item);
        return response()->json($series);

    } 


    public function findlotesall($referencia,$num_doc,$num_item)
    {
        
      
        $lotes = \TradeMarketing\Purchase::buscar_lotes_edit($referencia,$num_doc,$num_item);
        
        $key = 0;
  
        $view = view('purchases.tables.Lotes',compact('lotes', 'key'));
        
        $sections = $view->render();
    
        return Response::json($sections);
 

    }
    
    
    public function create_lotes($num_doc,$id,$internal_reference,Request $request)
    {
      
        $data=json_decode($request->getContent(), true);
        
        foreach($data as $key => $value)
        {
            DB::table('tmk_lotes')->insert(
                ['item_id' => $value['id']
                , 'internal_reference' => $internal_reference
                , 'num_doc_temp' => $num_doc
                ,'lote'=> $value['lote']
                ,'article_id'=>$id
                ,'status_doc'=>0
                ,'status_in'=>1
                ,'cantidad_in'=>$value['cantidad']
                ,'caducidad'=>$value['caduca']
                ,'creado_el'=>Carbon::now()->toDateTimeString()
                ]
            );
           // Storage::disk('local')->put($key.'.txt',$value['serie']);
        }
 
        return $data;
    }


    public function edit_lotes($num_doc,$id,$internal_reference,Request $request)
    {
      
        $data=json_decode($request->getContent(), true);


        DB::table('tmk_lotes')
        ->where('internal_reference', $internal_reference)
        ->where('num_doc_temp',$num_doc)
        ->where('article_id',$id)
        ->delete();
        
        foreach($data as $key => $value)
        {
            DB::table('tmk_lotes')->insert(
                ['item_id' => $value['id']
                , 'internal_reference' => $internal_reference
                , 'num_doc_temp' => $num_doc
                ,'lote'=> $value['lote']
                ,'article_id'=>$id
                ,'status_doc'=>0
                ,'status_in'=>1
                ,'cantidad_in'=>$value['cantidad']
                ,'caducidad'=>$value['caduca']
                ,'creado_el'=>Carbon::now()->toDateTimeString()
                ]
            );
           // Storage::disk('local')->put($key.'.txt',$value['serie']);
        }
 
        return $data;
    }



    public function findlotesout($referencia,$num_doc,$num_item,$tipo)
    {
        
        if($tipo=="create"){
            $lotes = \TradeMarketing\Purchase::buscarlotes_create($referencia,$num_doc,$num_item);
        }
        else
        {
            $lotes = \TradeMarketing\Purchase::buscarlotes_edit($referencia,$num_doc,$num_item);

            
        }
    
        $series=[];
        $row=2;
        $salida;
        foreach($lotes as $lote) 
        {
            $salida=null;
            if(isset($lote->cantidad_out) && $lote->cantidad_out!=null)
            {
               $salida= (object) array(
                    'row'=>$row,
                    'existe'=>0,
                    'id'=>$lote->id,
                    'item_id'=>$lote->item_id,
                    'internal_reference'=>$lote->internal_reference,
                    'num_doc_temp'=>$lote->num_doc_temp,
                    'lote'=>$lote->lote,
                    'cantidad'=>$lote->cantidad_disponible,
                    'cantidad_out'=>$lote->cantidad_out,
                    'caducidad'=>$lote->caducidad,
                    'status_doc'=>$lote->status_doc,
                    'status_in'=>$lote->status_in,
                    'num_doc_temp_out'=>$lote->num_doc_temp_out,
                    'article_id'=>$lote->article_id,
                );
            }
            else
            {
                $salida= (object) array(
                    'row'=>$row,
                    'existe'=>0,
                    'id'=>$lote->id,
                    'item_id'=>$lote->item_id,
                    'internal_reference'=>$lote->internal_reference,
                    'num_doc_temp'=>$lote->num_doc_temp,
                    'lote'=>$lote->lote,
                    'cantidad'=>$lote->cantidad_disponible,
                    'caducidad'=>$lote->caducidad,
                    'status_doc'=>$lote->status_doc,
                    'status_in'=>$lote->status_in,
                    'num_doc_temp_out'=>$lote->num_doc_temp_out,
                    'article_id'=>$lote->article_id,
                );
            }


            $series[]=$salida;
            $row++;   
        }

        $key = 0;
        $view = view('movements.external.partials.list_out_lotes',compact('series', 'key'));
        
        $sections = $view->render();
    
        return Response::json($sections);
 

    } 


    public function create_lotes_out($num_doc,$id,$internal_reference,Request $request)
    {
      
        $data=json_decode($request->getContent(), true);
        Storage::disk('local')->put($num_doc.'.txt',$request);
        foreach($data as $key => $value)
        {
            DB::table('tmk_lotes_out')->insert(
                ['item_id' => $value['id']
                , 'internal_reference' => $internal_reference
                , 'num_doc_temp' => $num_doc
                ,'lote'=> $value['lote']
                ,'cantidad'=> $value['cantidad_out']
                ,'article_id'=>$id
                ,'status_doc'=>0
                ,'status'=>1
                ,'creado_el'=>Carbon::now()->toDateTimeString()
                ]
            );
           // Storage::disk('local')->put($key.'.txt',$value['serie']);
        }
        
        return $data;
    }

    public function edit_lotes_out($num_doc,$id,$internal_reference,Request $request)
    {
      
        $data=json_decode($request->getContent(), true);
        Storage::disk('local')->put($num_doc.'_out.txt',$request);
        DB::table('tmk_lotes_out')
        ->where('internal_reference', $internal_reference)
        ->where('num_doc_temp',$num_doc)
        ->where('article_id',$id)
        ->delete();


        foreach($data as $key => $value)
        {
            DB::table('tmk_lotes_out')->insert(
                ['item_id' => $value['id']
                , 'internal_reference' => $internal_reference
                , 'num_doc_temp' => $num_doc
                ,'lote'=> $value['lote']
                ,'cantidad'=> $value['cantidad_out']
                ,'article_id'=>$id
                ,'status_doc'=>0
                ,'status'=>1
                ,'creado_el'=>Carbon::now()->toDateTimeString()
                ]
            );
           //
        }
        
        return $data;
    }
}
