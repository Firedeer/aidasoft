<?php

namespace TradeMarketing\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Http\Request;

use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use TradeMarketing\Events\OrderWasSended;
use TradeMarketing\Events\OrderWasUpdated;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;
use TradeMarketing\Models\Order;
use TradeMarketing\Models\OrderDetail;
use TradeMarketing\Models\Views\ArticleView;
use TradeMarketing\Models\Warehouse;
use TradeMarketing\Traits\ArrayHandler;
use TradeMarketing\Traits\ResponseHandler;


class OrderController extends Controller
{
    use ResponseHandler, ArrayHandler;


    public function __construct()
    {
        $this->beforeFilter('@find', ['only' => ['show', 'edit', 'update', 'send', 'cancel', 'transfers', 'destroy']]);
        $this->beforeFilter('@editable', ['only' => ['update']]);
        $this->beforeFilter('@sending', ['only' => ['send']]);
        $this->beforeFilter('@isAuthor', ['only' => ['cancel']]);
        $this->beforeFilter('@ifExist', ['only' => ['create']]);
    }


    public function find(Route $route, Request $request)
    {
        if (!$this->order = Order::find($route->getParameter('order'))) {
            if ($request->ajax()) {
                abort(404);
            }

            ResponseHandler::alertView(2, 'No se encontraron registros coincidentes.', 'Pedido no encontrado!', false);

            return redirect()->route('order.create');
        }
    }


    public function editable(Route $route)
    {
        if (!$this->order->isDraft()) {
            return back();
        }
    }


    public function sending()
    {
        if (Gate::denies('send', $this->order)) {

            ResponseHandler::alertView(3, 'No tienes permiso de realizar esta acción.', 'Sin permisos!', false);

            return back();
        }
    }


    public function isAuthor(Route $route)
    {
        if (currentUser()->isAuthor($this->order) || currentUser()->authRole('admin')) {
        } else {
            abort(404);
        }
    }


    public function ifExist(Route $route)
    {
        if ($order = currentUser()->orders()->draft()->get()->last()) {

            return redirect()->route('order.show', $order->id);
        }

//        ResponseHandler::alertView(3, 'Debes agregar artículos al carrito para crear un pedido.', 'No has creado ningún pedido!', false);
//
//        return back();
    }


    public function index()
    {
        $orders = Warehouse::orders()->sortByDesc('sortOrderedAt()');

        $orders = $orders->filter(function ($item) {
            return $item['ordered_at'] != null;
        });

        return view('orders.index', compact('orders'));
    }

  public function pendientes()
    {
        $orders = Warehouse::orders()->sortByDesc('sortOrderedAt()');

        $orders = $orders->filter(function ($item) {
            return $item['ordered_at'] != null;
        });

        return view('orders.Pendientes.index', compact('orders'));
    }

 public function cancelados()
    {
        $orders = Warehouse::orders()->sortByDesc('sortOrderedAt()');

        $orders = $orders->filter(function ($item) {
            return $item['ordered_at'] != null;
        });

        return view('orders.Cancelados.index', compact('orders'));
    }

 public function finalizado()
    {
        $orders = Warehouse::orders()->sortByDesc('sortOrderedAt()');

        $orders = $orders->filter(function ($item) {
            return $item['ordered_at'] != null;
        });

        return view('orders.Finalizado.index', compact('orders'));
    }
 public function transito()
    {
        $orders = Warehouse::orders()->sortByDesc('sortOrderedAt()');

        $orders = $orders->filter(function ($item) {
            return $item['ordered_at'] != null;
        });

        return view('orders.Transito.index', compact('orders'));
    }











    public function create()
    {
        $articleList = ArticleView::listing();

        $order = new Order();
        $order->status_id = 1;

        return view('orders.create', compact('order', 'articleList'));
    }


    public function store(Request $request)
    {
        try {

            $data = $request->only(['require_date', 'original_reference', 'observation', 'items', 'warehouse_origen', 'warehouse_destination']);

            // Additional data
            $data['require_date'] = $data['require_date'] ? Carbon::parse($data['require_date'])->format('Y-m-d') : null;
            $data['warehouse_destination'] = Warehouse::where('main', 1)->first()->id;
            $data['order_num'] = config('enums.doc_references.order') . date('misy');
            $data['from_user_id'] = currentUser()->id;
            $data['created_by'] = currentUser()->id;
            $data['for_user_id'] = Warehouse::where('main', 1)->first()->user->id;

            $order = new Order;

            if (!$order->isValid($data)) {

                ResponseHandler::alertView(2, '', 'Soluciona los errores en el formulario.', false);

                return back()->withErrors($order->errors)
                    ->withInput($request->all());
            }


            $data['items'] = isset($data['items']) ? ArrayHandler::array_condensed($data['items'], 'article', 'quantity') : [];
//
//            // Filters amounts equal to zero
//            $data['items'] = array_filter($data['items'], function ($item) {
//                return $item['quantity'];
//            });

            $data = ArrayHandler::array_upper($data);

            $details = [];

            foreach ($data['items'] as $key => $value) {
                array_push($details, new OrderDetail([
                    'article_id' => $value['article'],
                    'quantity' => $value['quantity'],
                    'pending' => isset($data['send_order']) ? $value['quantity'] : null,
                ]));
            }


            DB::beginTransaction();

            $order->fill($data);
            $order->save();
            $order->details()->saveMany($details);

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            return response()->json($e, 500);

        } catch (Exception $e) {

            DB::rollback();
            return response()->json($e, 500);
        } catch (ValidationException $e) {

        }

        DB::commit();

        ResponseHandler::alertView(1, 'Registrado', 'Registrado Correctamente.', false);
        return redirect()->route('order.show', [$order->id]);
    }


    public function show()
    {
        if (Gate::denies('show', $this->order)) {

            ResponseHandler::alertView(3, 'No tienes permiso para ver este pedido.', 'Sin permisos!', false);

            return back();
        }

        $articleList = [];

        return view('orders.show', ['order' => $this->order, 'articleList' => $articleList]);
    }


    public function edit()
    {
        $articleList = [];

        if ($this->order->isDraft()) {

            if (Gate::allows('edit', $this->order)) {

                $this->order->editing = true;
                $articleList = ArticleView::listing();
            }

            return view('orders.show', ['order' => $this->order, 'articleList' => $articleList]);
        }
    }


    public function update(Request $request)
    {
       
        try {

            $data = $request->only(['for_user_id', 'require_date', 'observation', 'items', 'warehouse_origen', 'warehouse_destination']);

            // Additional data
            $data['require_date'] = $data['require_date'] ? Carbon::parse($data['require_date'])->format('Y-m-d') : null;
            $data['warehouse_destination'] = isset($data['warehouse_destination'])
                                                ? $data['warehouse_destination'] 
                                                : Warehouse::where('main', 1)->first()->id;
            $data['from_user_id'] = currentUser()->id;
            $data['updated_by'] = currentUser()->id;
 

            if (!$this->order->isValid($data)) {

                ResponseHandler::alertView(2, '', 'Soluciona los errores en el formulario.', false);

                return back()->withErrors($this->order->errors)
                    ->withInput($request->all());
            }


            $data['items'] = ArrayHandler::array_condensed($data['items'], 'article', 'quantity');


            // Filters amounts equal to zero
            $data['items'] = array_filter($data['items'], function ($item) {
                return $item['quantity'];
            });

            $data = ArrayHandler::array_upper($data);

            $details = [];

            foreach ($data['items'] as $key => $value) {
                array_push($details, new OrderDetail([
                    'article_id' => $value['article'],
                    'quantity' => $value['quantity'],
                    'pending' => isset($data['send_order']) ? $value['quantity'] : null,
                ]));
            }

            DB::beginTransaction();

            $this->order->fill($data);
            $this->order->save();
            $this->order->details()->delete();
            $this->order->details()->saveMany($details);


            // Se dispara el evento para manejar las notificaciones
            event(new OrderWasUpdated($this->order));


        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            return response()->json($e, 500);

        } catch (Exception $e) {

            DB::rollback();
            return response()->json($e, 500);
        }

        DB::commit();

        ResponseHandler::alertView(1, $this->order->order_num . ' se ha actualizado.', 'Actualizado', false);

        return redirect()->route('order.show', $this->order);
    }


    public function destroy()
    {
        try {
            DB::beginTransaction();

            if ($this->order->isDraft()) {

                $this->order->forceDelete();
            } else {

                $this->order->delete();
            }

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            Log::error($e->getMessage());

            $view = ResponseHandler::alertView(2, 'Ocurrió un error mientras se eliminaba el pedido', 'Error!', true);
            return response()->json(['alert' => $view], 500);

        } catch (\Exception $e) {

            DB::rollback();
            Log::error($e->getMessage());

            $view = ResponseHandler::alertView(2, 'Ocurrió un error mientras se eliminaba el pedido', 'Error!', true);
            return response()->json(['alert' => $view], 500);

        }

        DB::commit();

        $message = view('orders.partials.deleted_order', ['order' => $this->order])->render();
        $alert = ResponseHandler::alertView(1,
            'El pedido ' . $this->order->order_num . ' ha sido eliminado.',
            'Pedido eliminado!', true);

        return response()->json(['alert' => $alert, 'message_html' => $message], 200);
    }


    public function cancel()
    {

        if (Gate::denies('cancel', $this->order)) {

            ResponseHandler::alertView(3, 'El ' . trans('app.attributes.order') . ' no puede ser ' . trans('app.status.canceled'), 'No se puede realizar esta acción!', false);

            return back();
        }

        try {
            DB::beginTransaction();

            $this->order->status_id = 7;
            $this->order->canceled_by = currentUser()->id;
            $this->order->canceled_at = Carbon::now()->toDateTimeString();
            $this->order->save();

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            return $e;
        } catch (Exception $e) {
            DB::rollback();
            return $e;
        }

        DB::commit();

        ResponseHandler::alertView(1, 'El pedido ' . $this->order->order_num . ' ha sido cancelado.', 'Pedido Cancelado', false);

        return back();
    }


    public function send()
    {
        try {
            DB::beginTransaction();

            $this->order->status_id = 2; // 2 => in_revision
            $this->order->ordered_at = Carbon::now()->toDateTimeString();
            $this->order->save();

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            return $e;
        } catch (Exception $e) {
            DB::rollback();
            return $e;
        }

        DB::commit();

        ResponseHandler::alertView(1, 'El pedido ' . $this->order->order_num . ' se ha enviado.', 'Pedido Enviado!', false);

        return back();
    }


    public function transfers()
    {
        // 10 => in_transit
        $transfers = $this->order->transfers()->transferred()->get();
        return view('receipts.transfers', compact('transfers'));
    }

}