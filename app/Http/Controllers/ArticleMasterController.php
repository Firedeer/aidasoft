<?php

namespace TradeMarketing\Http\Controllers;

use Illuminate\Http\Request;


use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Requests\ArticleStoreRequest; //Validator
use TradeMarketing\Http\Controllers\Controller;

use TradeMarketing\ClientesContactos;
use \TradeMarketing\Models\Article;
use \TradeMarketing\InventarioGeneral;
use \TradeMarketing\ArticleMaster;
use \TradeMarketing\Provider;
use \TradeMarketing\Models\ArticleMasterAttribute;
use \TradeMarketing\Models\AttributeValue;
use \TradeMarketing\Models\Variant;
use \TradeMarketing\Models\Category;
use \TradeMarketing\Models\Sucursal;
use \TradeMarketing\Models\Brand;
use \TradeMarketing\Models\Attribute;
use \TradeMarketing\Models\MeasurementUnits;
use DB;
use Session;
use Auth;



class ArticleMasterController extends Controller
{

    public function index()
    {
        $masters = ArticleMaster::all();
       // $invent =ClientesContactos::buscar_inventario(9);
     /*
        foreach($master as $item)
        {
            $stock=DB::table('tmk_inventario_general')
            ->where('article_master_id',$item->id)
            ->sum('stock');
            if(!isset($stock))
            {
                $stock=0;
            }
            $masters[]=(object) array(

                "id" =>$item->id
                ,"name" => $item->name
                ,"barcode" => $item->barcode
                ,"internal_reference" => $item->internal_reference
                ,"min_stock" => $item->min_stock
                ,"max_stock" =>$item->max_stock
                ,"stock"=>$stock
                ,"image" => $item->image
                ,"category_id" =>$item->category_id
                ,"brand_id" => $item->brand_id
                ,"measurementUnity" => $item->measurement_unit_id
                ,"long_description" => $item->long_description
                ,"created_by" => $item->created_by
                ,"updated_by" =>$item->updated_by
                ,"deleted_by" => $item->deleted_by
                ,"created_at" =>$item->created_at
                ,"updated_at" => $item->updated_at
                ,"deleted_at" => $item->deleted_at
            );
        }
     */

        return view('admin.articles.index', compact('masters'));
    }


    public function updateVariants(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $variants = $request->get('variant');

            if (isset($variants)) {
                $articleMaster = ArticleMaster::find($id);

                if ($articleMaster) {


                    $articles = array_filter(array_map(function ($item) {

                        if (isset($item['id']))
                            return $item['id'];
                    }, $variants));


                    // trae los las variantes de articulos existentes
                    $articles_exists = \DB::table('tmk_articles')
                        ->where('tmk_articles.article_master_id', $articleMaster->id)
                        ->where('tmk_articles.deleted_at', null)
                        ->get(['tmk_articles.id']);


                    $articles_exists = array_map(function ($item) {
                        return $item->id;
                    }, $articles_exists);


                    // retorna la diferencia entre los articulos que existen y los articulos que se estan insertando
                    // la diferencia se tomara como sobrante y se eliminara
                    $articles_to_deletes = array_diff($articles_exists, $articles);

//                    dd($articles_to_deletes);

                    if ($articles_to_deletes) {
                        Article::whereIn('id', $articles_to_deletes)->delete();
                    }

                    // end
                    foreach ($variants as $key => $variant) {

                        // verificasi la variante trae un id
                        if (isset($variant['id'])) {

                            // verifica si la variable ya ha sido registrada
                            // si ha sido registrada la actualizara
                            $article = Article::find($variant['id']);


                            if ($article) {

                                $article->description = $variant['description'];
//                            $article->image = $variant['image'];
                                $article->active = (isset($variant['active'])) ? 1 : 0;
                                $article->updated_by = \Auth::user()->id;
                                $article->save();


                                // verifica si se le ha asociado algun attributo
                                if (isset($variant['value'])) {

                                    // Validate los attributos asociado a la variante
                                    //
                                    $associated_attributes = Variant::where('article_id', $article->id)->get(['attribute_value_id'])->toArray();


                                    if ($associated_attributes) {

                                        $associated_attributes = array_map(function ($item) {

                                            return $item['attribute_value_id'];
                                        }, $associated_attributes);


                                        $difference = array_diff($variant['value'], $associated_attributes);
                                        $diff_excluded = array_diff($associated_attributes, $variant['value']);

                                        if ($difference) {
                                            foreach ($difference as $diff) {

                                                $verify = AttributeValue::find($diff);

                                                if ($verify) {
                                                    Variant::create([
                                                        'article_id' => $article->id,
                                                        'attribute_value_id' => $diff
                                                    ]);
                                                }
                                            }
                                        } else if ($diff_excluded) {

                                            AttributeValue::whereIn('attribute_id', $diff_excluded)
                                                ->where('article_id', $article->id)
                                                ->delete();
                                        }
                                    }
                                }
                            } // end if

                            else {

                                $article = Article::create([
                                    'description' => $variant['description'],
                                    'serial_code' => $articleMaster->barcode,
//                       'image' => $variant['image'],
                                    'active' => (isset($variant['active'])) ? 1 : 0,
                                    'article_master_id' => $articleMaster->id,
                                    'created_by' => \Auth::user()->id,
                                    'updated_by' => \Auth::user()->id
                                ]);


                                foreach ($variant['value'] as $value) {

                                    $attribute = AttributeValue::find($value);
                                    if ($attribute) {

                                        Variant::create([
                                            'article_id' => $article->id,
                                            'attribute_value_id' => $attribute->id
                                        ]);

                                    }

                                }

                            } // end else

                        } // end if

                    }

                }

                // el Articulo no existe
            }


            DB::commit();
            return back();

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            Session::flash('status', 2);
            Session::flash('message', $e->getMessage());

            return back()->withInput();

        } catch (Exception $e) {

            DB::rollback();
            Session::flash('status', 2);
            Session::flash('message', $e->getMessage());

            return back()->withInput();
        }
    }


    public function getVariantsByBodega(Request $request, $article, $warehouse)
    {
        if (!$request->ajax()) {
            abort(404);
        }

        $article = ArticleMaster::find($article);
        $articles = $article->articleByWarehouse($warehouse);

        return response()->json( view('partials.article_variants', compact('articles'))->render());
    }
}




//    public function show($id)
//    {
//
//        if (!ArticleMaster::find($id)) {
//            return back();
//        }
//
//        $master = ArticleMaster::find($id);
//
//
//        $attributes = [];
//
//        foreach ($master->masterAttributes as $masterAttribute) {
//            $attributes[] = $masterAttribute->attribute;
//        }
//
//        $variants = Variant::variantsByArticle($master->id);
//
//
//        return view('admin.articles.show', compact('master', 'variants', 'attributes'));
//    }

//    public function store(ArticleStoreRequest $request)
//    {
//        try {
//
//            DB::beginTransaction();
//
//
//            $array = $request->only(['name', 'barcode', 'brand_id', 'min_stock', 'max_stock', 'category_id', 'measurement_unit_id']);
//
//            if ($request->get('image') != "") {
//                $image = \TradeMarketing\Image::save($request->get('image'));
//
//                // Agregar la variable image dentro del array
//                $array['image'] = $image;
//            }
//
//            $array['created_by'] = \Auth::user()->id;
//            $array['updated_by'] = \Auth::user()->id;
//
//
//            $master = ArticleMaster::create($array);
//
//            if ($request->get('variant')) {
//
//                // Asigna los attributos para el articulo
//                $attributes = [];
//
//                if ($request->get('attributes')) {
//                    $attributes = AttributeValue::filterArray($request->get('attributes'));
//
//                } else {
//
//                    abort(500);
//                }
//
//
//                foreach ($attributes as $attribute) {
//                    ArticleMasterAttribute::create([
//                        'article_master_id' => $master->id,
//                        'attribute_id' => $attribute
//                    ]);
//                }
//
//
//                // Si la check-variants existe se crearan las variantes automaticamente
//                if ($request->get('check-variants')) {
//
//                    for ($i = 0; $i < count($request->get('variant')); $i++) {
//
//                        $article = Article::create([
//                            'serial_code' => $master->barcode . "-" . strval($i + 1),
//                            'description' => $request->get('variant')[$i]['description'],
//                            'article_master_id' => $master->id,
//                            'created_by' => \Auth::user()->id,
//                            'updated_by' => \Auth::user()->id
//                        ]);
//
//                        for ($j = 0; $j < count($request->get('variant')[$i]['value']); $j++) {
//
//                            Variant::create([
//                                'article_id' => $article->id,
//                                'attribute_value_id' => $request->get('variant')[$i]['value'][$j]
//                            ]);
//                        }
//                    }
//
//                } else {
//                    $article = Article::create([
//                        'serial_code' => $master->barcode . '-0',
//                        'description' => $master->name,
//                        'article_master_id' => $master->id,
//                        'created_by' => \Auth::user()->id,
//                        'updated_by' => \Auth::user()->id
//                    ]);
//                }
//
//
//            } else {
//
//                $article = Article::create([
//                    'serial_code' => $master->barcode . '-0',
//                    'description' => $master->name,
//                    'article_master_id' => $master->id,
//                    'created_by' => \Auth::user()->id,
//                    'updated_by' => \Auth::user()->id
//                ]);
//            }
//
//
//            DB::commit();
//            Session::flash('status', 1);
//            Session::flash('message', 'Tu información ha sido registrada con éxito.');
//
//
//            return response()->json(['id' => $master->id]);
//
//        } catch (\Illuminate\Database\QueryException $e) {
//
//            DB::rollback();
//            Session::flash('status', 2);
//            Session::flash('message', $e->getMessage());
//
//            return back()->withInput();
//
//        } catch (Exception $e) {
//
//            DB::rollback();
//            Session::flash('status', 2);
//            Session::flash('message', $e->getMessage());
//
//            return back()->withInput();
//        }
//    }


//    public function edit($id)
//    {
//
//        if (!ArticleMaster::find($id)) {
//            return back();
//        }
//
//        $master = ArticleMaster::find($id);
//
//        $brandList = Brand::listing();
//        $sucursalList = Sucursal::listing();
//        $categoryList = Category::listing();
//        $providerList = Provider::listing();
//        $attributeList = Attribute::listing();
//        $measurementUnitList = MeasurementUnits::listing();
//
//        return view('admin.articles.edit',
//            compact(
//                'providerList',
//                'categoryList',
//                'sucursalList',
//                'brandList',
//                'measurementUnitList',
//                'attributeList',
//                'master',
//                'attributeValues'
//            ));
//    }


//    public function update(Request $request, $id)
//    {
//        if (!$request->ajax()) {
//            abort(500);
//        }
//
//        $article = ArticleMaster::find($id);
//
//        if (is_null($article)) {
//            abort(404);
//        }
//
//        $data = $request->only('name', 'barcode', 'brand_id', 'category_id', 'measurement_unit_id', 'min_stock', 'min_stock');
//
//
//        if ($request->get('image') != "") {
//            $image = \TradeMarketing\Image::save($request->get('image'));
//            $data['image'] = $image;
//        }
//
//        $data['updated_by'] = \Auth::user()->id;
//
//
//        // If data is valid
//        if ($article->isValid($data)) {
//
//            DB::beginTransaction();
//
//
//            // If the data is valid, we assign the article
//            $article->fill($data);
//
//            // save
//            $article->save();
//
//
//            // Attributes
//            $newAttributes = AttributeValue::filterArray($request->get('attribute_id'));
//
//            $exists = ArticleMasterAttribute::where('article_master_id', $id)->lists('attribute_id')->toArray();
//
//            $diff = array_diff($newAttributes, $exists);
//
//            $remove = array_diff($exists, $newAttributes);
//
//            if ($remove) {
//                ArticleMasterAttribute::whereIn('attribute_id', $remove)->where('article_master_id', $id)->delete();
//            }
//
//            $reals = Attribute::whereIn('id', $diff)->lists('id')->toArray();
//
//            $noExist = array_diff($diff, $reals);
//
//            foreach ($noExist as $key => $value) {
//                unset($diff[$key]);
//            }
//
//            foreach ($diff as $attribute_id) {
//
//                ArticleMasterAttribute::create([
//                    'article_master_id' => $id,
//                    'attribute_id' => $attribute_id,
//                ]);
//            }
//
//            DB::commit();
//            Session::flash('status', 1);
//            Session::flash('message', 'El articulo ha sido modificado.');
//            return response()->json(['message' => 'Actualizado', 'id' => $article->id]);
//        }
//
//        DB::rollback();
//        return response()->json($article->errors, 422);
//    }
