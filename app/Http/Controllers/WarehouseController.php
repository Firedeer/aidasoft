<?php namespace TradeMarketing\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;
use Illuminate\Http\Request;
use TradeMarketing\Models\Sucursal;
use TradeMarketing\Models\UserWarehouse;
use TradeMarketing\Models\Warehouse;
use TradeMarketing\Traits\ArrayHandler;
use TradeMarketing\Traits\ResponseHandler;
use TradeMarketing\User;
use TradeMarketing\aprobar;
class WarehouseController extends Controller
{
    public function __construct()
    {
        $this->beforeFilter('@find', ['only' => ['show', 'edit', 'update', 'active', 'inventory', 'movements','movements_io', 'transfers','aprobaciones']]);
//        $this->beforeFilter('@isAjax', ['only' => '']);
        $this->beforeFilter('@accessible', ['only' => ['show', 'inventory', 'movements','movements_io', 'transfers','aprobaciones']]);
    }
    public function find(Route $route)
    {
        if (!$this->warehouse = Warehouse::find($route->getParameter('warehouse'))) {
            abort(404);
        }
    }
    public function isAjax(Route $route, Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }
    }
    public function accessible(Route $route){
        if (Gate::denies('show', $this->warehouse)) {
            ResponseHandler::alertView(3, 'No tienes permisos para ver esta bodega.', 'Sin permisos!', false);
            return back();
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $warehouses = Warehouse::orderBy('active', 'desc')->get();
        return view('admin.warehouses.index', compact('warehouses'));
    }
    public function create()
    {
        $this->userList = User::listing();
        $this->sucursalList = Sucursal::listing();
        return view('admin.warehouses.create', ['userList' => $this->userList, 'sucursalList' => $this->sucursalList]);
    }
    public function show()
    {
        return view('warehouses.show', ['warehouse' => $this->warehouse]);
    }
    public function store(Request $request)
    {
        try {
            $data = [
                'description' => $request->get('description'),
                'sucursal_id' => $request->get('sucursal'),
                'users' => $request->get('users') ? $request->get('users') : []
            ];
            $data['created_by'] = auth()->user()->id;
            $warehouse = new Warehouse;
            if (!$warehouse->isValid($data)) {
                $view = ResponseHandler::alertView(2, ' Soluciona los errores.', 'Error', false);
                return back()->withInput($request->all())
                    ->withErrors($this->warehouse->errors);
            }
            $data = ArrayHandler::array_upper($data);
            DB::beginTransaction();
            $warehouse->fill($data);
            $warehouse->save();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return response()->json([], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            dd($e);
        }
        DB::commit();
        ResponseHandler::alertView(1, 'La bodega ' . $warehouse->description . ' se creo correctamente.', 'Nueva Bodega!', false);
        return response()->json(200);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit()
    {
        $this->userList = User::listing();
        $this->sucursalList = Sucursal::listing();
        $this->warehouse->users = $this->warehouse->usersActive->lists('id')->toArray();
        return view('admin.warehouses.edit', [
            'warehouse' => $this->warehouse,
            'userList' => $this->userList,
            'sucursalList' => $this->sucursalList
        ]);
    }

    
    public function update(Request $request)
    {
        try {
            $data = [
                'description' => $request->get('description'),
                'sucursal_id' => $request->get('sucursal'),
                'users' => $request->get('users') ? $request->get('users') : []
            ];
            $data['updated_by'] = auth()->user()->id;
            if (!$this->warehouse->isValid($data)) {
                $view = ResponseHandler::alertView(2, ' Soluciona los errores.', 'Error', true);
                return response()->json(['alert' => $view, 'errors' => $this->warehouse->errors], 422);
            }
            $data = ArrayHandler::array_upper($data);
            DB::beginTransaction();
            $this->warehouse->fill($data);
            $this->warehouse->save();
            $this->warehouse->userWarehouses()->saveMany($this->warehouse->attachUsers($data['users']));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json($e, 500);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            return response()->json($e, 500);
        }
        DB::commit();
        $view = ResponseHandler::alertView(1, $this->warehouse->description . ' se ha actualizado.', 'Actualizado', false);
        return response()->json(200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function active()
    {
        try {
            DB::beginTransaction();
            $this->warehouse->active = $this->warehouse->active ? 0 : 1;
            $this->warehouse->save();
        } catch (\Exception $e) {
            DB::rollback();
            abort($e, 500);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            abort($e, 500);
        }
        DB::commit();
        $view = ResponseHandler::alertView(4, $this->warehouse->description . ' esta ' . (($this->warehouse->active) ? 'activo' : 'inactivo'), '', true);
        return response()->json(['alert' => $view], 200);
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function inventory()
    {
        return view('warehouses.inventory', [
            'warehouse' => $this->warehouse,
            'inventory' => $this->warehouse->inventory
        ]);
    }
    public function movements()
    {
        return view('warehouses.movements', [
            'warehouse' => $this->warehouse,
            'movements' => $this->warehouse->movements
        ]);
    }
    public function movements_io($warehouse)
    {
        return view('warehouses.movements_io', [
            'warehouse' => $this->warehouse,
            'movements' => $this->warehouse->movements
        ]);
    }
    public function transfers()
    {
        $transfers = collect($this->warehouse->transfers());
        $transfers = $transfers->filter(function ($item) {
            return $item->status != 'draft';
        });
        return view('warehouses.transfers', [
            'warehouse' => $this->warehouse,
            'transfers' => $transfers
        ]);
    }
  public function aprobaciones()
    {
        $transfers = collect($this->warehouse->Aproba());
          $transfers = $transfers->filter(function ($item) {
            return $item->status != 'draft';
         });
      
        return view('warehouses.aprobaciones', [
            'warehouse' => $this->warehouse,
            'transfers' => $transfers
        ]);
    }
//    public function show_bodega_articles($id)
//    {
//        $bodega = Warehouse::list_bodega_articles($id);
//        return response()->json($bodega);
//    }
//
//    public function show_bodegas_actives()
//    {
//        // $bodega = Warehouse::Show_Warehouses_Actives();
//        $bodega = Warehouse::list_bodegas();
//        return response()->json($bodega);
//    }
//
//    public function show_bodegas_user(Request $request, $id)
//    {
//        if ($request->ajax()) {
//            $user = 0;
//
//            if ($id == '_') {
//                $user = Auth::user()->id;
//            } else {
//                $user = $id;
//            }
//            $bodegas = Warehouse::list_bodegas_user($user);
//            return response()->json($bodegas);
//        }
//    }
}