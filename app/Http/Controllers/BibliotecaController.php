<?php


namespace TradeMarketing\Http\Controllers;

use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;

use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use TradeMarketing\Models\Article;
use TradeMarketing\Models\ArticleBodega;
use TradeMarketing\ArticleMaster;
use TradeMarketing\Http\Requests;
use TradeMarketing\Biblioteca;
use TradeMarketing\prestamos;
use TradeMarketing\Http\Controllers\Controller;
use TradeMarketing\Models\Attribute;
use TradeMarketing\Models\AttributeValue;
use TradeMarketing\Models\Brand;
use TradeMarketing\Models\Category;
use TradeMarketing\Models\MeasurementUnits;
use TradeMarketing\Models\Sucursal;
use TradeMarketing\Models\Variant;
use TradeMarketing\Models\Views\ArticleMasterWarehousesView;
use TradeMarketing\Models\Warehouse;
use TradeMarketing\Traits\ArrayHandler;
use TradeMarketing\Traits\ResponseHandler;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;
use Session;
use Validator;

use Yajra\Datatables\Facades\Datatables;

class BibliotecaController extends Controller
{

    use ResponseHandler;
public function __construct()
    {

           $this->beforeFilter('@isAjax', ['only' => ['filter']]);
           $this->beforeFilter('@find', ['only' => ['show', 'update', 'destroy']]);
    }

   public function isAjax(Route $route, Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }
    }
    public function index()
    {
		


		 $masters = Biblioteca::all();
        $attributes = [];

        foreach ($masters as $masterAttribute) {

            if($masterAttribute->PRESTADO=="1")
            {

              $id_libros=DB::table('Tmk_libros_pedidos')->where('ID_LIBRO',$masterAttribute->ID)->value('FEC_MAX_DEVOLUCION');
              $attributes[] =$id_libros;
            }
            else
            {
              $attributes[]="0";
            }
            
       



        }


        return view('Biblioteca.index', compact('masters','attributes'));

    }


  public function hist_index($id)
    {
        

        $masters=prestamos::where('ID_LIBRO',$id)->get();

        return view('Biblioteca.hist_index', compact('masters'));

    }




	    public function edit($id)
    {

        if (!Biblioteca::find($id)) 
		{
            return back();
        }

        $master = Biblioteca::find($id);

        $brandList = Brand::listing();
        $sucursalList = Sucursal::listing();
        $categoryList = Category::listing();

        $attributeList = Attribute::listing();
        $measurementUnitList = MeasurementUnits::listing();
         
        return view('Biblioteca.edit',
            compact(
//                'providerList',
                'categoryList',
                'sucursalList',
                'brandList',
                'measurementUnitList',
                'attributeList',
                'master',
                'attributeValues'
            ));
    }






        public function prestar1($id)
        {

        if (!Biblioteca::find($id)) 
        {
            return back();
        }

        $master = Biblioteca::find($id);

        $brandList = Brand::listing();
          $id_libros=DB::table('Tmk_libros_pedidos')->where('ID_LIBRO',$id)->where('LIBERADO','0')->value('id');
     if (!prestamos::find($id_libros)) 
        {
          return back();
        }
        $pres=prestamos::find($id_libros);


        

        return view('Biblioteca.prestado_index',
            compact(
//                'providerList',
                'categoryList',
                'sucursalList',
                'brandList',
                'measurementUnitList',
                'attributeList',
                'master',
                'pres'
            ));



    }
 public function prestar3($id)
        {


        $id_libros=DB::table('Tmk_libros_pedidos')->where('ID',$id)->value('id_libro');
        if (!Biblioteca::find($id_libros)) 
        {
            return back();
        }

        $master = Biblioteca::find($id_libros);

        $brandList = Brand::listing();
       
      
      if (!prestamos::find($id)) 
        {
          return back();
        }
        $pres=prestamos::find($id);


        

        return view('Biblioteca.prestado_hist_index',
            compact(
//                'providerList',
                'categoryList',
                'sucursalList',
                'brandList',
                'measurementUnitList',
                'attributeList',
                'master',
                'pres'
            ));
    }







        public function prestar2($id)
    {
     if (!Biblioteca::find($id)) 
        {
            return back();
        }
        $master = Biblioteca::find($id);
        
     return view('Biblioteca.prestamos_index',compact('master','pres'));
    }


public function find(Route $route, Request $request)
    {
        $this->article = Biblioteca::find($route->getParameter('article'));

        if (!$this->article) {
            if ($request->ajax()) {
                abort(404);
            }

            return back();
        }
    }





	  public function create()
    {
  
        return view('Biblioteca.create');
     }






   public function actualizar(Request $request,$id)
    {
     
  try{

             $imageBase64 = $request->get('image');
             $master = new ArticleMaster;

            $user= Biblioteca::find($id);
             if ($imageBase64) {
              $imagenesname = $master->getImageName($request->get('image_name')).'';
                 DB::table('tmk_libros')
                ->where('ID', $id)
                ->update(['NOMBRE' =>$request['NOMBRE'],
                'GENERO'=>$request['GENERO'],
                'ANIO_EDICION'=>$request['ANIO_EDICION'],
                'PAIS_AUTOR'=>$request['PAIS_AUTOR'],
                'EDITORIAL'=>$request['EDITORIAL'],
                'AUTOR'=>$request['AUTOR'],
                'CODIGO_BARRAS'=>$request['CODIGO_BARRAS'],
                'COD_LIBRO'=>$request['COD_LIBRO'],
                'IMAGE'=>$imagenesname 
                ]);
               }
               else
               {
                    DB::table('tmk_libros')
                ->where('ID', $id)
                ->update(['NOMBRE' =>$request['NOMBRE'],
                'GENERO'=>$request['GENERO'],
                'ANIO_EDICION'=>$request['ANIO_EDICION'],
                'PAIS_AUTOR'=>$request['PAIS_AUTOR'],
                'EDITORIAL'=>$request['EDITORIAL'],
                'AUTOR'=>$request['AUTOR'],
                'CODIGO_BARRAS'=>$request['CODIGO_BARRAS'],
                'COD_LIBRO'=>$request['COD_LIBRO']
                  ]);
               }
         


        if ($imageBase64) {
                $master->saveImage($imageBase64, $imagenesname );

                
            }




        //RETORNO SI HA GUARDADO  
        $masters = Biblioteca::all();
        return view('Biblioteca.index', compact('masters'));
            //redirect to somewhere
        }
              catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            Log::info('Error creating article: ' . $e);

            return response()->json([
                    'alert' => $this->alertView(2, "Ocurrió un error mientras se creaba el Libro. ")]
                , 500);

        }
     

        DB::commit();
        return response()->json([
            'alert' => $this->alertView(2, "El artículo se ha registrado correctamente."), 'redirectTo' =>$maestro->id.'/edit'
        ], 200);
       






    }



public function prueba(Request $equest)
{
      dd($request);

}

public function store(Request $request)
    {

              // 'name' => 'required|unique:tmk_article_masters|max:100',
              //   'brand_id' => 'required|exists:tmk_brands,id',
              //   'category_id' => 'required|exists:tmk_categories,id',
              //   'measurement_unit_id' => 'required|exists:tmk_measurement_units,id',
              //   'barcode' => 'unique:tmk_article_masters|min:6|max:100',
              //   'internal_reference' => 'required|unique:tmk_article_masters|min:6|max:100',
              //   'initial_stock' => 'numeric|min:0',
              //   'min_stock' => 'numeric|min:1',
              //   'max_stock' => 'numeric|min:1',
              //   'warehouse_id' => 'required_if:initial_stock,not_null|exists:tmk_bodegas,id',
              //   'long_description' => 'min:5|max:500',
              //   'image' => 'image_base64',

        try {
         $master = new ArticleMaster;

            $validator = Validator::make($request->all(), [
                'NOMBRE'=> 'required|min:5|max:500',
                'AUTOR'=>'required|min:5|max:500',
                'GENERO'=>'required|min:5|max:500',
                'COD_LIBRO'=>'required|min:5|max:500',
                'IMAGE' => 'image_base64',
            ]);
        if ($validator->fails()) {
                return response()->json([
                    'alert' => $this->alertView(2, "Soluciona los errores en el formulario."),
                    'errors' => $validator->errors()
                ], 422);
            }



               $imageBase64 = $request->get('image');
                 if($imageBase64)
            {
            $imagenesname = $master->getImageName($request->get('image_name')).'';
            }
            $maestro = new Biblioteca;
       
     
           
            $data = $request->only([
               'COD_LIBRO', 'NOMBRE', 'EDITORIAL', 'AUTOR', 'GENERO', 'PAIS_AUTOR',  'ANIO_EDICION', 'CODIGO_BARRAS'
            ]);

            
     

            $data = ArrayHandler::array_upper($data);
         if($imageBase64)
            {
              $data['IMAGE'] = $imagenesname;
             }
           $data['FEC_MAX'] = '0';
            DB::beginTransaction();

            $maestro->fill($data);

            $maestro->save();
  

          if ($imageBase64) {
                $master->saveImage($imageBase64,$imagenesname);
            }

       

            }
         catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            Log::info('Error creating article: ' . $e);

            return response()->json([
                    'alert' => $this->alertView(2, "Ocurrió un error mientras se creaba el Libro. ")]
                , 500);

        }
     

        DB::commit();
        return response()->json([
            'alert' => $this->alertView(1, "El artículo se ha registrado correctamente."), 'redirectTo' =>$maestro->id.'/edit'
        ], 200);
    }




    public function liberar(Request $request,$id)
    {  


        try{
   

           $id_libros=DB::table('Tmk_libros_pedidos')->where('ID_LIBRO',$id)->where('LIBERADO','0')->value('id');
            $liberacion= Biblioteca::find($id);
            $libera2= prestamos::find($id_libros);


            DB::table('tmk_libros')
                ->where('ID', $id)
                ->update(['PRESTADO' =>0]);
              
               $liberacion->save();


            DB::table('Tmk_libros_pedidos')
                ->where('ID', $id_libros)
                ->update(['LIBERADO' =>1]);

               $libera2->save();



           $masters = Biblioteca::all();

        return view('Biblioteca.index', compact('masters'));

          }
              catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            Log::info('Error creating article: ' . $e);

            return response()->json([
                    'alert' => $this->alertView(2, "Ocurrió un error mientras se creaba el Libro. ")]
                , 500);

        }
     

        DB::commit();
        return response()->json([
            'alert' => $this->alertView(2, "El artículo se ha registrado correctamente."), 'redirectTo' =>$maestro->id.'/edit'
        ], 200);
       

      }





    public function prestar_libro(Request $request,$id)
    {

     try {
 $liberacion= Biblioteca::find($id);
           
               DB::table('Tmk_libros_pedidos')->insert(
              ['ID_LIBRO' => $id
              , 'NOMBRE_APELLIDO' =>$request['NOMBRE_APELLIDO']
              ,'DEPARTAMENTO'=>$request['DEPARTAMENTO']
              ,'NCT'=>$request['NCT']
              ,'FEC_ENTREGA'=>date("Y-m-d")
              ,'FEC_DEVOLUCION'=>date("Y-m-d")
              ,'FEC_MAX_DEVOLUCION'=>date("Y-m-d")
              ,'LIBERADO'=>0
              ]
);
             DB::table('tmk_libros')
                ->where('ID', $id)
                ->update(['PRESTADO' =>1,
                  'HIST' =>1]);

               $liberacion->save();
    

    $masters = Biblioteca::all();

        return view('Biblioteca.index', compact('masters'));
            }
         catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            Log::info('Error creating article: ' . $e);

            return response()->json([
                    'alert' => $this->alertView(2, "Ocurrió un error mientras se creaba el Libro. ")]
                , 500);

        }
     

    }










}
