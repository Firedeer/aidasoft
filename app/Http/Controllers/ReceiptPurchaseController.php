<?php

namespace TradeMarketing\Http\Controllers;

use Illuminate\Http\Request;

use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Requests\PurchaseReceivedRequest;
use TradeMarketing\Http\Controllers\Controller;

use TradeMarketing\Models\Warehouse;
use \TradeMarketing\PurchaseOrder;
use \TradeMarketing\PurchaseReceipt;
use \TradeMarketing\PurchaseOrderDetail;
use \TradeMarketing\Models\views\PurchaseOrderDetailView;
use \TradeMarketing\Utility;
use \TradeMarketing\Receipt;



use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


use DB;
use Auth;
use Session;

class ReceiptPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {

    	$order = PurchaseOrder::find($id);

    	if ( $order ){

    		$details = PurchaseOrderDetailView::order($id)->missing()->get();
            $unassigned = PurchaseOrderDetailView::order($id)->received()->get();
    		$bodegas = Warehouse::listing();

    		return view('purchases.received')
    		->with('order', $order)
    		->with('details', $details)
    		->with('today', Utility::todayFormat())
    		->with('bodegas', $bodegas)
    		->with('unassigned', $unassigned);

    	} else {

    		return redirect('/order/purchases');
    	}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

    	try {

    		DB::beginTransaction();

    		if($request->get('item')){


    			$receipt = Receipt::store($request->only(['received_in', 'observation', 'purchase_order_id']));
    			$items = $request->get('item');

    			foreach ($items as $key => $item) {

    				if($item['quantity'] > 0 ){

    					$purchase = PurchaseOrderDetailView::findOrFail($item['id']);


    					if( $purchase ){

    						PurchaseReceipt::create([ 
    							'receipt_id' =>  $receipt->id,
    							'purchase_detail_id' => $purchase->id,
    							'quantity' =>  $item['quantity']
    							]);


    						$missing = $purchase ? $purchase->missing : 0;

    						if( $item['quantity'] >= $missing ){

    							$detail = PurchaseOrderDetail::find( $purchase->id );

    							$detail->complete = 1;

    							$detail->save();
    						} else if($item['quantity'] < $missing && $request->get('backorder') == 0) {
    							$detail = PurchaseOrderDetail::find( $purchase->id );

    							$detail->complete = 1;

    							$detail->save();
    						}


    						if( $request->get('bodega') !== '' and $request->get('bodega_confirm') ){

    							\TradeMarketing\PurchaseArticleBodega::store([
    								'article_id'         => $purchase->article_id,
    								'bodega_id'          => $request->get('bodega'),
    								'quantity'           => $item['quantity'],
    								'movement_id'        => 1,
    								'purchase_detail_id' => $purchase->id,
    								]);
    						}
    					}
    				}
    			}


    			$missing = DB::table('tmk_purchase_orders_view')
    			->where('id', $request->get('order') )
    			->first(['missing']);

    			$missing = Utility::object_to_array( $missing );

    			if( $missing && $missing == 0 ){
    				dd( $missing );
    				$res  = PurchaseOrder::updatePurchaseOrderState( $request->get('order'), 6);
    				DB::rollback();
    				return $res;
    			}
    		}   

    		DB::commit();

    		Session::flash('status', 1);
    		Session::flash('message', 'Tu información ha sido registrada con éxito.' );

    		// return  redirect( '/order/purchases/'.$order );
    		return  back();


    	} catch (ModelNotFoundException $e) {
    		DB::rollback();
    		Session::flash('status', 2);
    		Session::flash('message', $e->getMessage() );

    		return  back()->withInput();
    	} catch (QueryException $e) {

    		DB::rollback();
    		Session::flash('status', 2);
    		Session::flash('message', $e->getMessage() );

    		return  back()->withInput();

    	} catch (Exception $e) {

    		DB::rollback();
    		Session::flash('status', 2);
    		Session::flash('message', $e->getMessage() );

    		return  back()->withInput();
    	} 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}