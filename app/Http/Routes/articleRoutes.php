<?php

Route::pattern('article', '\d+');


Route::group(['middleware' => 'role:admin'], function () {

    /*
     *
     * ARTICULOS => ArticleController
     */

    Route::get('admin/article/create', [
        'as' => 'admin.article.create',
        'uses' => 'ArticleController@create'
    ]);


    Route::get('admin/article/{article}/edit', [
        'as' => 'admin.article.edit',
        'uses' => 'ArticleController@edit'
    ]);

    Route::post('admin/article', [
        'as' => 'admin.article.store',
        'uses' => 'ArticleController@store'
    ]);


    Route::put('admin/article/{article}', [
        'as' => 'admin.article.update',
        'uses' => 'ArticleController@update'
    ]);

});

Route::get('article', [
    'as' => 'article.index',
    'uses' => 'ArticleController@index'
]);

Route::get('article/{article}', [
    'as' => 'article.show',
    'uses' => 'ArticleController@show'
]);


Route::get('admin/article/{article}', [
    'as' => 'admin.article.destroy',
    'uses' => 'ArticleController@destroy'
]);

Route::get('admin/article', [
    'as' => 'admin.article.index',
    'uses' => 'ArticleMasterController@index'
]);

Route::post('article/filter', [
    'as' => 'articles.filter',
    'uses' => 'ArticleController@filter'
]);




Route::get('admin/article/{article}/variants', [
    'as' => 'admin.article.variants',
    'uses' => function ($article) {

        $article = \TradeMarketing\ArticleMaster::find($article);

        $variants = $article->articles;

        foreach ($variants as $variant) {

            $values = [];

            foreach ($variant->attributeValues as $value) {
                $values[$variant->id][] = $value->id;
            }

            $variant->attributes = $values;
        }

        return view('admin.articles.variants', compact('variants'));
    }
]);


Route::get('article/{article}/warehouses', [
    'as' => 'article.warehouses',
    'uses' => function ($article) {

        $warehouses = TradeMarketing\Models\Views\ArticleMasterWarehousesView::where('article_master_id', $article)
            ->whereNotNull('warehouse_id')
            ->get(['article', 'internal_reference', 'warehouse_id', 'warehouse', 'stock']);

        return response()->json($warehouses);
    }
]);

Route::get('articles', 'ArticleController@getArticles');
// Route::get('article', 'ArticleController@show');

// almacenar los articulos por bodegas del nuevo articulo
Route::post('article/bodega', 'ArticleController@store_article_bodega');
Route::post('article/validate', 'ArticleController@validates');

Route::get('article/bodegas/{id}', 'ArticleController@show_article_bodegas');


/*
* Obtener la cantidad de articulos disponibles para agregar detalles
* return view ( articles.create_detail )
*/
Route::get('article/create/detail/{id}/{bodega}', function ($id = null, $bodega = null) {
    $res = ArticleBodega::get_quantity_article((int)$id, (int)$bodega);

    if ($res == null) {
        $quantity = 0;
        $quantity2 = 0;

        return view('articles.create_detail', compact('id', 'quantity', 'quantity2'));
    } else {
        $quantity1 = (int)$res[0]->quantity;  // cantidad disponible
        $quantity2 = (int)$res[0]->quantity2; // cantidad detallada
        $quantity = $quantity1 - $quantity2; // cantidad sin detallar
        $name = $res[0]->name;

        return view('articles.create_detail', compact('id', 'quantity', 'quantity2', 'name'));
    }
});


/**
 *  VERIFY EXIST ARTICLE BY NAME
 */
Route::get('article/exists/{article}', function ($article) {

    $article = trim($article);

    $article = TradeMarketing\ArticleMaster::where('name', $article)->toJson();

    return $article->name;
});


Route::get('article/find/{id}', [
    'as' => 'article.find',
    'uses' => function ($id) {
        return TradeMarketing\Models\Views\ArticleView::find($id);
    }
]);


Route::put('article_master/update/variants/{id}', 'ArticleMasterController@updateVariants');

Route::get('admin/article/master/{id}', [
    'as' => 'admin.article.master.show',
    'uses' => 'ArticleMasterController@show'
]);
//Route::get('article/{id}/master/edit', 'ArticleMasterController@edit');

//Route::get('article/master/all', 'ArticleMasterController@index');


Route::get('article/master/{id}/variants/warehouse/{bodega}', [
    'as' => 'article_master_variants',
    'uses' => 'ArticleMasterController@getVariantsByBodega'
]);

Route::get('article/findById/{article}', [
    'as' => 'article.findById',
    'uses' => 'ArticleController@findById'
]);
Route::get('article/referencia/{referencia}', [
    'as' => 'article.referencia',
    'uses' => 'ArticleController@referencia'
]);
Route::get('article/findseries/{referencia}/{num_doc}/{num_item}', [
    'as' => 'article.findseries',
    'uses' => 'ArticleController@findseries'
]);


Route::get('article/findseries_edit/{referencia}/{num_doc}/{num_item}', [
    'as' => 'article.findseries_edit',
    'uses' => 'ArticleController@findseries_edit'
]);


Route::get('article/findseriesall/{referencia}/{num_doc}/{num_item}', [
    'as' => 'article.findseriesall',
    'uses' => 'ArticleController@findseriesall'
]);



Route::get('get/select', [
    'as' => 'get.select',
    'uses' => function () {

        $key = 0;
       
 
        $articleList = \TradeMarketing\Models\Views\ArticleView::listing();

        $view = view('purchases.tables.details_editing', compact('articleList', 'key'));
        
        $sections = $view->render();
    
        return Response::json($sections);

    }
]);

Route::get('get/row', [
    'as' => 'get.row',
    'uses' => function () {

        $key = 0;

        $view = view('purchases.tables.detail_series');
        
        $sections = $view->render();
    
        return Response::json($sections);

    }
]);


Route::get('get/edit', [
    'as' => 'get.edit',
    'uses' => function () {

        $key = 0;
        $series=\TradeMarketing\Purchase::buscar_series('20191007114353680979');
        $view = view('purchases.tables.series',compact('series', 'key'));
        
        $sections = $view->render();
    
        return Response::json($sections);

    }
]);






Route::get('warehouse/{warehouse}/article/{article}', [
    'as' => 'warehouse.article',
    'uses' => 'ArticleController@articuloWarehouse'
]);
Route::get('warehouse/{warehouse}/article/{article}', [
    'as' => 'warehouse.article',
    'uses' => 'ArticleController@articuloWarehouse'
]);



//'movement.warehouse.add.article.render'