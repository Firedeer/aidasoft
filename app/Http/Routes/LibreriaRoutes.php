
<?php

Route::pattern('article', '\d+');


Route::group(['middleware' => 'role:admin'], function () {

Route::get('Biblioteca', [
    'as' => 'Biblioteca',
    'uses' => 'BibliotecaController@index'
]);

Route::get('Biblioteca/{id}/hist_index', [
    'as' => 'Biblioteca.hist_index',
    'uses' => 'BibliotecaController@hist_index'
]);
Route::get('biblioteca', [
    'as' => 'Biblioteca',
    'uses' => 'BibliotecaController@index'
]);

    Route::post('Biblioteca/{id}/edit', [
        'as' => 'Biblioteca.update',
        'uses' => 'BibliotecaController@actualizar'
    ]);

    Route::get('Biblioteca/{id}/edit', [
        'as' => 'Biblioteca.edit',
        'uses' => 'BibliotecaController@edit'
    ]);
	    Route::get('Biblioteca/create', [
        'as' => 'Biblioteca.create',
        'uses' => 'BibliotecaController@create'
    ]);
            Route::post('Biblioteca', [
        'as' => 'Biblioteca.store',
        'uses' => 'BibliotecaController@store'
    ]);

            
    Route::get('Biblioteca/{id}/prestado', [
        'as' => 'Biblioteca.prestar1',
        'uses' => 'BibliotecaController@prestar1'
    ]);
      Route::get('Biblioteca/{id}/prestado_hist', [
        'as' => 'Biblioteca.prestar3',
        'uses' => 'BibliotecaController@prestar3'
    ]);
        Route::get('Biblioteca/{id}/prestar', [
        'as' => 'Biblioteca.prestar2',
        'uses' => 'BibliotecaController@prestar2'
    ]);

    Route::post('Biblioteca/{id}/prestado', [
        'as' => 'Biblioteca.liberar',
        'uses' => 'BibliotecaController@liberar'
    ]);

         Route::post('Biblioteca/{id}/prestar', [
        'as' => 'Biblioteca.prestar_libro',
        'uses' => 'BibliotecaController@prestar_libro'
    ]);


});


