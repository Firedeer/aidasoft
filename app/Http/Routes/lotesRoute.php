<?php

Route::get('lotes/findlotes/{referencia}/{num_doc}/{num_item}', [
    'as' => 'lotes.findlotes',
    'uses' => 'LotesController@findlotes'
]);

Route::get('lotes/findlotesall/{referencia}/{num_doc}/{num_item}', [
    'as' => 'lotes.findlotesall',
    'uses' => 'LotesController@findlotesall'
]);

Route::get('get/rowlotes', [
    'as' => 'get.rowlotes',
    'uses' => function () {

        $key = 0;

        $view = view('purchases.tables.detail_lotes');
        
        $sections = $view->render();
    
        return Response::json($sections);

    }
]);

Route::post('lotes/create_lotes/{num_doc}/{id}/{internal_reference}', [
    'as' => 'lotes.create_lotes',
    'uses' => 'LotesController@create_lotes'
]);


Route::post('lotes/edit_lotes/{num_doc}/{id}/{internal_reference}', [
    'as' => 'lotes.edit_lotes',
    'uses' => 'LotesController@edit_lotes'
]);


Route::get('lotes/findlotesout/{referencia}/{num_doc}/{num_item}/{tipo}', [
    'as' => 'lotes.findlotesout',
    'uses' => 'LotesController@findlotesout'
]);

Route::post('lotes/create_lotes_out/{num_doc}/{id}/{internal_reference}', [
    'as' => 'lotes.create_lotes_out',
    'uses' => 'LotesController@create_lotes_out'
]);


Route::post('lotes/edit_lotes_out/{num_doc}/{id}/{internal_reference}', [
    'as' => 'lotes.edit_lotes_out',
    'uses' => 'LotesController@edit_lotes_out'
]);
?>