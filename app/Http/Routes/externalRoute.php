<?php

Route::get('external/movement/warehouse/{purchase}', [
         'as' => 'ExternalMovement.show',
        'uses' => 'ExternalMovementController@show'
    ]);





    Route::get('movement/warehouse/{warehouse}/add/article', [
        'as' => 'movement.warehouse.add.article.render',
         'uses' => function($warehouse, \Illuminate\Http\Request $request){
     
             if(!$request->ajax()){
                 abort(404);
             }
     
             $key = 0;
             $articleList = \TradeMarketing\Models\Warehouse::find($warehouse)->articleListing();
             $view = view('movements.external.partials.table_row', compact('articleList', 'key'));
             $sections = $view->render();
     
             return Response::json($sections);
         }
     ]);



     Route::get('article/movement/purchase', 'PurchaseController@createPurchase');

Route::post('article/movement/purchase', 'PurchaseController@storePurchase');




    /*
     *
     * MOVEMENT => MovementController
     */
    Route::resource('movement', 'MovementController');


    // movimientos de bodegas
    Route::get('movements/all', [
        'as' => 'movement.all',
        'uses' => 'MovementController@showMovementList'
    ]);


    Route::get('movement/article/show/{id}', 'MovementController@show_movements_articles');

    Route::post('movement/article', [
        'as' => '/movement/article/store',
        'uses' => 'MovementController@store_movement_article'
    ]);



    
    Route::get('external/movement/{external}', [
        'as' => 'externalmovement.show',
        'uses' => 'ExternalMovementController@show'
    ]);

    Route::get('external/movement/warehouse/{warehouse}/in', [
        'as' => 'external.movement.warehouse.in',
        'uses' => 'ExternalMovementController@createEntry'
    ]);


    Route::get('external/movement/warehouse/{warehouse}/out', [
        'as' => 'external.movement.warehouse.out',
        'uses' => 'ExternalMovementController@createExit'
    ]);


    Route::get('external/movement/warehouse/{warehouse}', [
        'as' => 'external.movement.warehouse',
        'uses' => function ($warehouse) {

            $movements = \TradeMarketing\Models\ExternalMovement::where('warehouse_id', $warehouse)->get();
            return view('movements.external.index', compact('movements'));
        }
    ]);


    Route::post('external/movement/warehouse/in', [
        'as' => 'external.movement.in',
        'uses' => 'ExternalMovementController@storeEntry'
    ]);

    Route::post('external/movement/warehouse/out', [
        'as' => 'external.movement.out',
        'uses' => 'ExternalMovementController@storeExit'
    ]);


    Route::post('external/movement/create_series_out/{num_doc}/{id}/{internal_reference}', [
        'as' => 'movement.create_series_out',
        'uses' => 'ExternalMovementController@create_series_out'
    ]);
    
    Route::post('external/movement/edit_series_out/{num_doc}/{id}/{internal_reference}', [
        'as' => 'movement.edit_series_out',
        'uses' => 'ExternalMovementController@edit_series_out'
    ]);


    Route::get('external/findseries/{referencia}/{num_doc}/{num_item}', [
        'as' => 'external.findseries',
        'uses' => 'ExternalMovementController@findseries'
    ]);
    
    
    Route::get('external/findseries_edit/{referencia}/{num_doc}/{num_item}', [
        'as' => 'external.findseries_edit',
        'uses' => 'ExternalMovementController@findseries_edit'
    ]);
    
    
    Route::get('external/findseriesall/{referencia}/{num_doc}/{num_item}', [
        'as' => 'external.findseriesall',
        'uses' => 'ExternalMovementController@findseriesall'
    ]);

    Route::get('external/cancelar_out/{num}/{warehouse_id}', [
        'as' => 'external.cancelar_out',
        'uses' => 'ExternalMovementController@cancelar_out'
    ]);
    Route::get('external/cancelar_in/{num}/{warehouse_id}', [
        'as' => 'external.cancelar_in',
        'uses' => 'ExternalMovementController@cancelar_in'
    ]);

?>