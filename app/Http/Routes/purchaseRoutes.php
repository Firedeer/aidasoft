<?php

/*
 *
 * DEVOLUTION => DevolutionController
 */
Route::get('devolution/purchase', 'DevolutionController@index');
Route::post('devolution/purchase', 'DevolutionController@storeDevolutionPurchase');


/*
 *
 * PURCHASES =>PurchaseController
 */
Route::pattern('purchase', '\d+');


Route::group(['middleware' => 'role:admin'], function () {

    Route::get('purchases', [
        'as' => 'purchase.index',
        'uses' => 'PurchaseController@index'
    ]);

    Route::get('purchase/create', [
        'as' => 'purchase.create',
        'uses' => 'PurchaseController@create'
    ]);

    Route::get('purchases/{purchase}', [
        'as' => 'purchase.show',
        'uses' => 'PurchaseController@show'
    ]);

    Route::get('purchases/order/{purchase}', [
        'as' => 'purchaseorder.show',
        'uses' => 'PurchaseController@show'
    ]);


    Route::get('purchase/{purchase}/edit', [
        'as' => 'purchase.edit',
        'uses' => 'PurchaseController@edit'
    ]);


    Route::post('purchase', [
        'as' => 'purchase.store',
        'uses' => 'PurchaseController@store'
    ]);

    Route::put('purchase/{purchase}', [
        'as' => 'purchase.update',
        'uses' => 'PurchaseController@update'
    ]);


    Route::post('purchase/{purchase}/confirm', [
        'as' => 'purchase.confirm',
        'uses' => 'PurchaseController@confirm'
    ]);


    Route::get('purchase/{purchase}/confirm/partial', [
        'as' => 'purchase.confirm.partial',
        'uses' => function ($purchase) {
            return $purchase;
        }
    ]);


    Route::post('purchase/{purchase}/confirm/partial', [
        'as' => 'purchase.confirm.partial',
        'uses' => 'PurchaseController@confirm'
    ]);


    Route::get('purchase/{purchase}/receipt', [
        'as' => 'purchase.receipt',
        'uses' => 'PurchaseController@receipt'
    ]);


    Route::post('purchase/{purchase}/cancel', [
        'as' => 'purchase.cancel',
        'uses' => 'PurchaseController@cancel'
    ]);


    Route::get('purchase/{purchase}/delete', [
        'as' => 'purchase.delete',
        'uses' => 'PurchaseController@destroy'
    ]);


});
Route::post('purchase/create_series/{num_doc}/{id}/{internal_reference}', [
    'as' => 'purchase.create_series',
    'uses' => 'PurchaseController@create_series'
]);

Route::post('purchase/edit_series/{num_doc}/{id}/{internal_reference}', [
    'as' => 'purchase.edit_series',
    'uses' => 'PurchaseController@edit_series'
]);



//Route::get('order/create/purchases', 'PurchaseController@create');


Route::put('order/purchase/approve', 'PurchaseController@approveOrderPurchase');

Route::post('order/purchase/received', 'PurchaseController@storeInBodega');


Route::resource('received/purchase', 'ReceiptPurchaseController');

Route::resource('received/purchase/{id}/create', 'ReceiptPurchaseController@create');

Route::post('received/purchase/confirm', 'PurchaseController@confirmReceivedPurchase');

Route::post('received/purchase/cancel', 'PurchaseController@cancelReceivedPurchase');

Route::get('purchase/cancelar/{num}/{warehouse_id}', [
    'as' => 'purchase.cancelar',
    'uses' => 'PurchaseController@cancelar'
]);
