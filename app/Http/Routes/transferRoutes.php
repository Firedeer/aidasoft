<?php


Route::pattern('id', '\d+');

Route::pattern('warehouse', '\d+');

Route::pattern('transfer', '\d+');


/*
 *
 * TRANSFERS => TransferController
 */
Route::get('transfer', [
    'as' => 'transfer.index',
    'uses' => function () {
        $transfers = \TradeMarketing\Models\Transfer::notDraft()->get();
        return view('transfers.index', compact('transfers'));
    }
]);


Route::get('transfer/transito', [
    'as' => 'transfer.transito',
    'uses' => function () {
        $transfers = \TradeMarketing\Models\Transfer::notDraft()->get();
        return view('transfers.transito.index', compact('transfers'));
    }
]);

Route::get('transfer/portransferir', [
    'as' => 'transfer.transferir',
    'uses' => function () {
        $transfers = \TradeMarketing\Models\Transfer::notDraft()->get();
        return view('transfers.transferir.index', compact('transfers'));
    }
]);



Route::get('transfer/confirmados', [
    'as' => 'transfer.confirmados',
    'uses' => function () {
        $transfers = \TradeMarketing\Models\Transfer::notDraft()->get();
        return view('transfers.confirmados.index', compact('transfers'));
    }
]);


Route::get('transfer/parciales', [
    'as' => 'transfer.parciales',
    'uses' => function () {
        $transfers = \TradeMarketing\Models\Transfer::notDraft()->get();
        return view('transfers.parciales.index', compact('transfers'));
    }
]);













Route::get('transfer/all', [
    'as' => 'transfer.all',
    'uses' => function () {

        $user = auth()->user();
        $transfers = [];

        foreach ($user->warehouses as $warehouse) {

            foreach ($warehouse->transfersViewAll() as $transfer) {
                array_push($transfers, $transfer);
            }
        }

        $transfers = collect($transfers)->unique()->sortByDesc('transferred_at');

        return view('transfers.index', compact('transfers'));
    }
]);







Route::get('transfer/inbox', [
    'as' => 'transfer.inbox',
    'uses' => function () {
        $user = auth()->user();
        $transfers = [];

        foreach ($user->warehouses as $warehouse) {

            foreach ($warehouse->transfersViewEntry as $transfer) {
                array_push($transfers, $transfer);
            }
        }

        $transfers = collect($transfers)->unique()->sortByDesc('transferred_at');

//        $transfers = $transfers->filter(function ($item) {
//            return $item->status != 'draft';
//        });

        return view('transfers.inbox', compact('transfers'));
    }
]);


/**
 * Middleware Role : editor
 */
Route::group(['middleware' => 'role:editor'], function () {


    Route::post('transfer', [
        'as' => 'transfer.store',
        'uses' => 'TransferController@store'
    ]);


    Route::get('order/{order}/transfer/create', [
        'as' => 'order.transfer.create',
        'uses' => 'TransferController@create'
    ]);


    Route::get('transfer/{transfer}/edit', [
        'as' => 'transfer.edit',
        'uses' => 'TransferController@edit'
    ]);


    Route::put('transfer/{transfer}', [
        'as' => 'transfer.update',
        'uses' => 'TransferController@update'
    ]);


    Route::delete('transfer/{transfer}/delete', [
        'as' => 'transfer.destroy',
        'uses' => 'TransferController@destroy'
    ]);


    Route::get('transfer/{transfer}/cancel', [
        'as' => 'transfer.cancel',
        'uses' => 'TransferController@cancel'
    ]);


    Route::put('transfer/{transfer}/confirm', [
        'as' => 'transfer.transfer',
        'uses' => 'TransferController@transfer'
    ]);


});


/**
 * Middleware Role : Admin
 */
Route::group(['middleware' => 'role:admin'], function () {

    Route::get('transfer/{transfer}/nose', [
        'as' => 'transfer.nose',
        'uses' => 'TransferController@nose'
    ]);
});


Route::get('transfer/{transfer}', [
    'as' => 'transfer.show',
    'uses' => 'TransferController@show'
]);


Route::get('transfer/{transfer}/receive', [
    'as' => 'transfer.receipt.confirm',
    'uses' => 'TransferController@confirmReception'
]);


Route::get('transfer/{transfer}/warehouse/stock', [
    'as' => 'transfer.warehouse.stock',
    'uses' => function (\Illuminate\Http\Request $request, $transfer) {

        if (!$request->ajax()) {
            abort(404);
        }

        if ($transfer = \TradeMarketing\Models\Transfer::find($transfer)) {


            $warehouse = \TradeMarketing\Models\Warehouse::find($transfer->bodega_id);

            if ($warehouse) {

                $articles = $warehouse->articleMasterWarehousesView;

                foreach ($articles as $article) {

                    if ($transfer->details()->where('article_id', $article->article_id)->first()) {

                        $article->available_quantity = $article->available_quantity + $transfer->details()->where('article_id', $article->article_id)->first()->quantity;
                    }
                }

                return response()->json($articles);
            }
        }

        return [];
    }
]);


/**
 *  VALIDATE
 *  Validate articles params
 */
Route::get('transfer/article/validate/', function (\Illuminate\Http\Request $request) {

    if ($request->ajax()) {
        $rules = [
            'article_id' => 'required|exists:tmk_articles,id',
            'quantity' => 'required|numeric|min:1|max:' . $request->get('stock') . ''
        ];

        $messages = [
            'min' => 'La cantidad minima debe ser 1',
        ];


        $validator = Validator::make($request->only('article_id', 'quantity'), $rules, $messages);

        if ($validator->passes()) {
            return response()->json(['valid' => true, 'article' => $request->only('article_id', 'quantity', 'stock')]);
        }

        $errors = [];

        foreach ($validator->errors()->toArray() as $key => $validatorErrors) {
            foreach ($validatorErrors as $error) {
                $errors[$key] = $error;
            }
        }

        return response()->json($errors, 422);
    }

    return back();
});


Route::get('transfer/{id}/articles', function (\Illuminate\Http\Request $request, $id) {

    if (!$request->ajax()) {
        return back();
    }

    $transfer = \TradeMarketing\Models\Transfer::find($id);
    $bodega = \TradeMarketing\Models\Warehouse::find($transfer->bodega_id);
    $articles = [];
    $details = Array();


    foreach ($transfer->details as $detail) {

        $stock = \TradeMarketing\Models\ArticleWarehouse::where('article_id', $detail->article_id)->where('bodega_id', $bodega->id)->first();

        $detail['stock'] = $stock->stock;

        array_push($details, $detail);
    }

    return response()->json(['details' => $details]);
});