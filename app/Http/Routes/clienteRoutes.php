
<?php



Route::get('clientes', [
    'as' => 'clientes.index',
    'uses' => 'ClientesController@index'
]);


Route::post('clientes/add', [
    'as' => 'clientes.add',
    'uses' => 'ClientesController@store'
]);

Route::get('clientes/create', [
    'as' => 'clientes.create',
    'uses' => 'ClientesController@create'
]);
Route::get('clientes/list', [
    'as' => 'clientes.list',
    'uses' => 'ClientesController@list'
]);

Route::get('clientes/edit/{id}', [
    'as' => 'clientes.edit',
    'uses' => 'ClientesController@edit'
]);

Route::put('clientes/update/{id}', [
    'as' => 'clientes.update',
    'uses' => 'ClientesController@update'
]);

Route::post('clientes/addsucursal', [
    'as' => 'clientes.addsucursal',
    'uses' => 'ClientesController@addsucursal'
]);

Route::post('clientes/editsucursal', [
    'as' => 'clientes.editsucursal',
    'uses' => 'ClientesController@editsucursal'
]);
Route::post('clientes/image/{id}', [
    'as' => 'clientes.image',
    'uses' => 'ClientesController@image'
]);

Route::get('clientes/insert_pru', [
    'as' => 'clientes.insert_pru',
    'uses' => 'ClientesController@insert_pru'
]);

Route::post('clientes/addcontactos/{id}', [
    'as' => 'clientes.addcontactos',
    'uses' => 'ClientesController@addcontactos'
]);
Route::post('clientes/editcontactos/{id}', [
    'as' => 'clientes.editcontactos',
    'uses' => 'ClientesController@editcontactos'
]);
Route::post('clientes/destroy/{id}', [
    'as' => 'clientes.destroy',
    'uses' => 'ClientesController@destroy'
]);