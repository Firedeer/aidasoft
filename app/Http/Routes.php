<?php

/*

|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the Routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Mail;

use \TradeMarketing\Inventory;
// use \TradeMarketing\Utility;
use TradeMarketing\Models\Attribute;


Route::pattern('id', '\d+');


//
//Route::get('/', 'WelcomeController@index');
//

Route::get('home', function () {
    return Redirect::to('/');
});


Route::get('home', [
    'as' => 'home',
    'uses', function () {
        return Redirect::to('/');
    }
]);

// //
// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);


// Route::post('auth/log', 'Auth\AuthController@user_login');

// Route::resource('auth/login', 'Auth\AuthController@login');

// Authentication Routes...


//Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::get('login', [
    'as' => 'login',
    'uses' => 'Auth\AuthController@getLogin'
]);

Route::post('login', 'Auth\AuthController@postLogin');


Route::post('auth/login/confirm', 'Auth\AuthController@postLogin');


Route::get('confirmation/{token}', [
    'as' => 'confirmation',
    'uses' => 'Auth\AuthController@getConfirmation'
]);


// Route::post('auth/login', 'Auth\AuthController@postLogin');

// // Registration Routes...
// Route::get('auth/register', 'Auth\AuthController@getRegister');
// Route::post('auth/register', 'Auth\AuthController@postRegister');


// Password reset link request Routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset Routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('logout', [
    'as' => 'logout',
    'uses' => 'Auth\AuthController@getLogout'
]);


/**
 * Middleware Auth
 *
 */
Route::group(['middleware' => 'auth'], function () {


    Route::group(['middleware' => 'confirm'], function () {

        // Aqui vas las rutas

    });


    Route::group(['middleware' => 'role:admin'], function () {

        // Aqui vas las rutas

    });


    /*
	 *	Authentication 
	 *  
	 */
    // Route::get('auth/logout', 'Auth\AuthController@getLogout');
//    Route::get('logout', [
//        'as' => 'logout',
//        'uses' => 'Auth\AuthController@getLogout'
//    ]);


    Route::get('/', [ 'as' => 'inicio.app','uses' => 'InicioController@inicio']);

    /*
     *
     * USERS => UserController
     */

    Route::pattern('user', '\d+');

	//EN ESTE AREA VAN LAS RUTAS DE USER ADMIN
	//admin/user/
	
    Route::resource('admin/user', 'UserController');
	//
	Route::post('admin/user/create/store',[ 'as' => 'user.creacion','uses' => 'UserController@init']);
	
    Route::get('user/{user}', [
        'as' => 'user.show',
        'uses' => 'UserController@show'
    ]);


    Route::get('user/{user}/edit', [
        'as' => 'user.edit',
        'uses' => 'UserController@edit'
    ]);
	

    Route::get('user/s/{user}', [
        'as' => 'admin.user.nose',
        'uses' => function ($user) {

            if ($newUser = \TradeMarketing\User::newUser(\TradeMarketing\User::isTrade($user))) {

                $newUser->sendMailConfirmation();

                return response()->json([
                    'alert' => \TradeMarketing\Traits\ResponseHandler::alertView(1, 'Ahora solo nos falta confirmar la cuenta. Para ello hemos enviado un email ha ' . $newUser->email . '', 'Hemos verificado tus datos con exito', 200)
                ]);
            }

            return response()->json([
                    'alert' => \TradeMarketing\Traits\ResponseHandler::alertView(2, "Ocurrio un error inesperado al intentar validar el usuario.", 'Error!')]
                , 500);
        }
    ]);


    // return list users
    Route::get('users', 'UserController@show_users');
	

    /*
     *
     * PROVIDERS => ProviderController
     */
    Route::group(['middleware' => 'role:admin'], function () {

        Route::resource('admin/provider', 'ProviderController');

    });
    include __DIR__ . '/Routes/externalRoute.php';
    include __DIR__ . '/Routes/LibreriaRoutes.php';
    // Include Articles Routes file
    include __DIR__ . '/Routes/articleRoutes.php';

	include __DIR__ . '/Routes/clienteRoutes.php';
    /**
     *
     * BODEGAS => BodegaController
     */
    include __DIR__ . '/Routes/warehouseRoutes.php';
	include __DIR__ . '/Routes/PasswordRoutes.php';
    include __DIR__ . '/Routes/lotesRoute.php';
    /*
     *
     * MARCAS => BrandController
     */
    Route::resource('brand', 'BrandController');
    Route::get('brands', 'BrandController@show_brands_actives');

    Route::get('brands/provider/{id}', 'BrandController@show_brands_provider');
    Route::resource('brand/provider', 'BrandController@show_brand_to_provider');


    /*
     *
     * CLASES => ClassController
     */
    Route::get('classes', 'ClassController@show_classes_actives');
    Route::get('classes/category/{id}', 'ClassController@show_classes_category');


    /**
     *
     * CATEGORY =>  CategoryController
     */
    Route::resource('category', 'CategoryController');


    /**
     *
     *  Measurement UNITS => MeasurementController
     */

    Route::resource('measurement', 'MeasurementController');


    /*
     *
     * CONFIG =>
     */
    Route::resource('setting', 'SettingController');
// Route::get('setting', function(){
// 	return view('c.index');
// });


    Route::get('role', 'RoleController@index');

    Route::get('save/user/role/', 'UserController@updateUserRole');


    // Include Order Routes file
    include __DIR__ . '/Routes/orderRoutes.php';


    // Include Purchase Routes file
    include __DIR__ . '/Routes/purchaseRoutes.php';


    Route::get('articles/autoco
        plete', function () {
        $term = Request::input('term');

        $data = DB::table('tmk_articles_view')
            ->select('article_id as value', 'article as label', 'brand as desc')
            ->where('article', 'LIKE', "%" . $term . "%")
            ->orWhere('brand', 'LIKE', "%" . $term . "%")
            ->orderBy('article', 'asc')
            ->get();

        return Response::json($data);
    });


    Route::get('attribute/{id}/values', [
        'as'  => 'attribute.values',
        'uses' => 'AttributeController@getValues'
    ]);

    Route::get('attribute/all/lists', [
        'as' => 'attribute.all.lists',
        'uses' => 'AttributeController@lists'
    ]);

    Route::get('attribute/value/{id}', [
        'as' => 'attribute.value',
        'uses' => 'AttributeController@findAttributeValue'
    ]);


    Route::resource('attribute', 'AttributeController');


    Route::get('attribute/values/lists', [
        'as' => 'attribute.values.lists',
        'uses' => 'AttributeController@listValue'
    ]);


    Route::get('article/attribute/add', function () {

        $attributeList = Attribute::listing();

        return response()->json(view('articles.partials.variants', compact('attributeList'))->render());
    });


    Route::get('autocomplete/attribute', function () {

        $term = Request::get('term');

        return Attribute::select('id', 'description')
            ->where('description', 'LIKE', "%$term%")->get();
    });


    include __DIR__ . '/Routes/receiptRoutes.php';


    Route::get('inventory/alarms/list', function () {
        $alarms = \TradeMarketing\Models\views\InventoryView::alarmsByArticle();

        return view('inventories.alarms.list', compact('alarms'));

    });


    /**
     * INVENTORY
     */

    Route::get('inventory', 'InventoryController@index');

    Route::get('inventory/filter/{filterBy}', function ($filterBy) {

        $list = [];
        if (isset($filterBy)) {

            switch ($filterBy) {
                case 'category':
                    $list = \TradeMarketing\Models\Category::listing();
                    break;

                case 'warehouse':
                    $list = \TradeMarketing\Models\Warehouse::listing();
                    break;

                case 'movement':
                    $list = \TradeMarketing\Models\Movement::listing();
                    break;

                case 'article':
                    $list = \TradeMarketing\ArticleMaster::listing();
                    break;

                default:
                    break;
            }
            return $list;
        }

        return [];

    });


    /**
     * Include file
     * TRANSFERS ROUTES
     */
    include __DIR__ . '/Routes/transferRoutes.php';


    /*
     *
     * INVENTORY => InventoryController
     */

    Route::pattern('inventory', '\d+');


    Route::resource('inventory', 'InventoryController');
    Route::get('inventory/bodega/{}', 'InventoryController@show_inventory_bodega');
    Route::get('annual_inventory', 'InventoryController@show_annual_inventory');

    Route::get('annual', 'InventoryController@dest');


//    Route::get('movements', [
//        'as' => 'admin.movement.index',
//        'uses' => function(){
//
//            return view('movements.index');
//        }
//    ]);
//



    /***************/

//    Route::get('external/movement/{external}', [
//        'as' => 'external.movement.show',
//        'uses' => 'ExternalMovementController@show'
//    ]);




});
// end Middleware Auth


Route::get('export/excel', [
    'as' => 'export.to.excel',
    'uses' => 'MovementController@export'
]);

Route::get('/importExcel', [
    'as' => 'import.excel',
    'uses' => function () {
        return view('importExcel');
    }
]);


Route::post('/processExcel', [
    'as' => 'process.excel',
    'uses' => function (\Illuminate\Http\Request $request) {

        $dataExcel = \Maatwebsite\Excel\Facades\Excel::load($request->file('excel_file')->getRealPath());
        $articles = $dataExcel->all();

        return response()->json(['html' => view('table_import', compact('articles'))->render()]);
    }
]);


Route::post('/importExcel', [
    'as' => 'import.excel',
    'uses' => function (\Illuminate\Http\Request $request) {


        $rules = [];
        $messages = [];

        foreach ($request->get('items') as $key => $item) {
            $rules['items.' . $key . '.name'] = 'required|max:100|unique:tmk_article_masters,name';
            $rules['items.' . $key . '.brand'] = 'required';
            $rules['items.' . $key . '.category'] = 'required';
//            $rules['items.' . $key . '.measurement_unit'] = 'required';
            $rules['items.' . $key . '.barcode'] = 'min:6|max:100';
            $rules['items.' . $key . '.internal_reference'] = 'required|min:6|max:100|unique:tmk_article_masters,internal_reference';
            $rules['items.' . $key . '.initial_stock'] = 'numeric|min:0';
            $rules['items.' . $key . '.min_stock'] = 'numeric|min:1';
            $rules['items.' . $key . '.max_stock'] = 'numeric|min:1';
            $rules['items.' . $key . '.warehouse_id'] = 'required_if:initial_stock,not_null|exists:tmk_bodegas,description';
            $rules['items.' . $key . '.long_description'] = 'min:5|max:500';
            $rules['items.' . $key . '.image'] = 'image_base64';

            $messages['items.' . $key . '.name.unique'] = 'Ya ha sido registrado';
            $messages['items.' . $key . '.internal_reference.unique'] = 'Ya ha sido registrado';
        }

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'alert' => \TradeMarketing\Traits\ResponseHandler::alertView(2, '', 'Soluciona los errores.'),
                'errors' => $validator->errors()
            ], 422);
        }

        $items = \TradeMarketing\Traits\ArrayHandler::array_upper($request->get('items'));

        $articles = [];
        foreach ($items as $key => $item) {

            $article = new \TradeMarketing\ArticleMaster([
                'name' => $item['name'],
                'internal_reference' => $item['internal_reference'],
                'image' => '',
                'brand_id' => \TradeMarketing\Models\Brand::firstOrCreate(['description' => $item['brand']])->id,
                'category_id' => \TradeMarketing\Models\Category::firstOrCreate(['description' => $item['category']])->id,
                'measurement_unit_id' => 1,
                'long_description' => $item['long_description']
            ]);

            array_push($articles, $article);

            $article->save();

            $article->articles()->saveMany([
                new \TradeMarketing\Models\Article ([
                    'internal_reference' => $article->internal_reference,
                    'reference_code' => 0,
                    'unit_cost_initial' => (float)$item['unit_cost'],
                    'description' => $article->name
                ]),

            ]);


        }


//        \Illuminate\Support\Facades\DB::beginTransaction();

//        \Illuminate\Support\Facades\DB::rollback();
        return response()->json([
            'alert' => \TradeMarketing\Traits\ResponseHandler::alertView(1, 'Carga completada!', 'Se han creado ' . count($articles) . ' artículos con exito')
        ], 200);
    }
]);

Route::get('hola_excel', function () {
    return view('excel');
});

Route::get('mail', function () {
    Mail::send('emails.confirm', [], function ($m) {
        $m->from('noreply@mail.com', 'Your Application');

        $m->to('jhonny8522@gmail.com', 'Ariel')->subject('Your Reminder!');
    });
});


//$rules['items.' . $key . '.name'] = 'required|max:100|unique:tmk_article_masters,name';
//$rules['items.' . $key . '.brand_id'] = 'required|exists:tmk_brands,description';
//$rules['items.' . $key . '.category_id'] = 'required|exists:tmk_categories,description';
//$rules['items.' . $key . '.measurement_unit_id'] = 'required|exists:tmk_measurement_units,description';
//$rules['items.' . $key . '.barcode'] = 'min:6|max:100|unique:tmk_article_masters,barcode';
//$rules['items.' . $key . '.internal_reference'] = 'required|min:6|max:100|unique:tmk_article_masters,internal_reference';
//$rules['items.' . $key . '.initial_stock'] = 'numeric|min:0';
//$rules['items.' . $key . '.min_stock'] = 'numeric|min:1';
//$rules['items.' . $key . '.max_stock'] = 'numeric|min:1';
//$rules['items.' . $key . '.warehouse_id'] = 'required_if:initial_stock,not_null|exists:tmk_bodegas,description';
//$rules['items.' . $key . '.long_description'] = 'min:5|max:500';
//$rules['items.' . $key . '.image'] = 'image_base64';


Route::get('Carga_incial', [
    'as' => 'inventory.initial',
    'uses' => function () {

        $articles = \TradeMarketing\Models\Article::all();

        foreach ($articles as $article) {
            $article->stock = \TradeMarketing\Models\Views\ArticleMasterWarehousesView::where('article_id', $article->id)
                ->where('warehouse_id', TradeMarketing\Models\Warehouse::main()->first()->id)->first()
                ? \TradeMarketing\Models\Views\ArticleMasterWarehousesView::where('article_id', $article->id)
                    ->where('warehouse_id', TradeMarketing\Models\Warehouse::main()->first()->id)->first()->stock
                : 0;
        }

        return view('inventories.initial', compact('articles'));
    }
]);


Route::post('/process/Excel', [
    'as' => 'process.excel.initial',
    'uses' => function (\Illuminate\Http\Request $request) {

        $dataExcel = \Maatwebsite\Excel\Facades\Excel::load($request->file('excel_file')->getRealPath());
        $articles = $dataExcel->all();

        return response()->json(['html' => view('inventories.table', compact('articles'))->render()]);
    }
]);


Route::post('/import/initial/excel', [
    'as' => 'import.initial.excel',
    'uses' => function (\Illuminate\Http\Request $request) {
        $rules = [];
        $messages = [];

        foreach ($request->get('items') as $key => $item) {
            $rules['items.' . $key . '.internal_reference'] = 'required|max:100|exists:tmk_articles,internal_reference';
            $rules['items.' . $key . '.initial_stock'] = 'numeric|min:0';
            $rules['items.' . $key . '.stock'] = 'required|numeric|min:0';

            $messages['items.' . $key . '.name.unique'] = 'Ya ha sido registrado';
        }

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules, $messages);

//        dd($validator);

        if ($validator->fails()) {
            return response()->json([
                'alert' => \TradeMarketing\Traits\ResponseHandler::alertView(2, '', 'Soluciona los errores en el formulario.'),
                'errors' => $validator->errors()
            ], 422);
        }

        $items = \TradeMarketing\Traits\ArrayHandler::array_upper($request->get('items'));

        $articles = [];

        foreach ($items as $key => $item) {

            // Movement =>  14 = Inventario Inicial
            $article = new \TradeMarketing\Models\ArticleBodega([
                'article_id' => (int)\TradeMarketing\Models\Article::where('internal_reference', $item['internal_reference'])->first()->id,
                'bodega_id' => (int)$request->get('warehouse_id'),
                'quantity' => (int)$item['stock'],
                'movement_id' => 14
            ]);

            if ($item['stock']) {
                array_push($articles, $article);

                $article->save();
            }

        }


//
//        $articles2 = \TradeMarketing\Models\ArticleBodega::movementsInWarehouse(collect($articles), $request->get('warehouse_id'), 14);
//        $articles2->save();


//        \Illuminate\Support\Facades\DB::beginTransaction();

//        \Illuminate\Support\Facades\DB::rollback();
        return response()->json([
            'alert' => \TradeMarketing\Traits\ResponseHandler::alertView(1, 'Carga completada!', 'Se han creado un inventario inicial con ' . count($articles) . ' artículos.')
        ], 200);
    }
]);


Route::get('download/format/articles', [
    'as' => 'download.format.articles',
    'uses' => function () {

        \Maatwebsite\Excel\Facades\Excel::create('Filename', function ($excel) {

            $excel->setTitle('Our new awesome title');

            $excel->setCreator('Maatwebsite')
                ->setCompany('Maatwebsite');

            $excel->setDescription('A demonstration to change the file properties');

            $excel->sheet('Sheetname', function ($sheet) {

                $sheet->row(1, array(
                    'description', 'long_description', 'stock', 'brand', 'unit_cost', 'attributes', 'colour', 'code', 'category'
                ));

                $sheet->row(1, function ($row) {
                    $row->setBackground('#00a9e0')
                        ->setFont(array(
                            'size' => '12',
                            'bold' => true
                        ));
                });
            });


        })->download('xls');;
    }
]);
