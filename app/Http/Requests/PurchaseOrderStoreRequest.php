<?php

namespace TradeMarketing\Http\Requests;

use TradeMarketing\Http\Requests\Request;

class PurchaseOrderStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
    	return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	return  [
        	'provider'       => 'required|exists:tmk_providers,id',
        	'require_date'   => 'required|date_format:Y-m-d',
        	'sucursal'       => 'required|exists:tbl_sucursales,id_sucursal',
            'item'           => 'required|array|min:1',
        	'item.article.id'=> 'array|min:1|exists:tmk_articles,id',
        	'item.tax'       => 'array|min:1',
        	'item.quantity'  => 'array|min:1',
        	'item.amount'    => 'array|min:1',
        	'subtotal'       => 'required_if:article, confirmed',
        	'total'          => 'required',
        	'discount'       => 'required',
            'observation'    => 'max:255',
        ];

    }
}
