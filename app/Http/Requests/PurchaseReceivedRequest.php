<?php

namespace TradeMarketing\Http\Requests;

use TradeMarketing\Http\Requests\Request;

class PurchaseReceivedRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order'           => 'required|exists:tmk_purchase_orders,id',
            'provider'        => 'required|exists:tmk_purchase_orders,provider_id',
            'bodega'          => 'required_if:bodega_confirm,on|exists:tmk_bodegas,id',
            'sucursal'        => 'required|exists:tmk_purchase_orders,require_place_id',
            'item.id'         => 'required|array|min:1',
            'item.quantity'   => 'required|array|min:1',
        ];
    }
}
