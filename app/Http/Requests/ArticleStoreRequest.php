<?php namespace TradeMarketing\Http\Requests;

use TradeMarketing\Http\Requests\Request;

class ArticleStoreRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|unique:tmk_article_masters|max:100',
			'brand_id' => 'exists:tmk_brands,id',
            'category_id' => 'required|exists:tmk_categories,id',
            'measurement_unit_id' => 'required|exists:tmk_measurement_units,id',
            'barcode' => 'unique:tmk_article_masters|min:6|max:100',
            'internal_reference' => 'unique:tmk_article_masters|min:6|max:100',
            'stock' => 'numeric|min:0',
            'warehouse_id'  => 'required_if:stock,min:1',
            'min_stock' => 'numeric|min:1',
            'max_stock' => 'numeric|min:1',
            'description' => 'min:5|max:255',
            'image' => 'image_base64',
        ];
	}

}
