<?php namespace TradeMarketing\Providers;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\ServiceProvider;
use TradeMarketing\Models\UserWarehouse;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	
	public function boot(UrlGenerator $url)
	{

//        UserWarehouse::updated(function ($user) {
//            dd($this);
//        });
		//$url->forceSchema('http');
		
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'TradeMarketing\Services\Registrar'
		);
	}

}
