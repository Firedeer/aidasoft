<?php namespace TradeMarketing\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use TradeMarketing\ArticleMaster;
use TradeMarketing\Models\Article;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
//        'auth.login' => [
//            'UpdateLastLoggedAt',
//        ],

        'TradeMarketing\Events\OrderWasUpdated' => [
         'TradeMarketing\Listeners\SendMail',
        ],
        'TradeMarketing\Events\OrderWasSended' => [
            'TradeMarketing\Listeners\OrderEventListener@notify',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        // Fired on successful logins
        $events->listen('auth.login', function ($user, $remember) {
            $user->last_logged_at = Carbon::now()->toDateTimeString();
            $user->save();
        });

        $events->listen('eloquent.creating:*', function ($data) {

            $attributes = $data->getFillable();

            if(in_array('created_by', $attributes)){
                $data->created_by = currentUser()->id;
            }

            if(in_array('updated_by', $attributes)){
                $data->updated_by = currentUser()->id;
            }
        });


        $events->listen('eloquent.updating:*', function ($data) {

            $attributes = $data->getFillable();

            if(in_array('updated_by', $attributes)){
                $data->updated_by = currentUser()->id;
            }
        });


        $events->listen('eloquent.deleting:*', function ($data) {

            $attributes = $data->getFillable();

            if(in_array('deleted_by', $attributes)){
                $data->deleted_by = currentUser()->id;
            }
        });
    }

}
