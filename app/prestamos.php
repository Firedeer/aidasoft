<?php

namespace TradeMarketing;

use DB;
use Illuminate\Database\Eloquent\Model;

class prestamos extends Model
{
  protected $table = "Tmk_libros_pedidos";

  protected $fillable = [
      'ID_LIBRO'
      ,'NOMBRE_APELLIDO'
      ,'DEPARTAMENTO'
      ,'FEC_ENTREGA'
      ,'FEC_DEVOLUCION'
 
    ];


 public function bibli()
    {
        return $this->belongsTo(Article::class);
    }

        public static function listing($id)
        {
       
        return DB::table('Tmk_libros_pedidos')->where('ID_LIBRO',$id);
    }  //
}


