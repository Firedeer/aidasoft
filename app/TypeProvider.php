<?php namespace TradeMarketing;

use Illuminate\Database\Eloquent\Model;

class TypeProvider extends Model {

	protected $table = 'tmk_type_providers';

	protected $fillable = ['id', 'description'];


	  public function providers()
    {
        return $this->hasMany('TradeMarketing\Provider');
    }

}
