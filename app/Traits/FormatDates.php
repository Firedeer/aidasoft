<?php

namespace TradeMarketing\Traits;


use Carbon\Carbon;

trait FormatDates
{


    protected $format = 'Y-m-d g:i:s a'; // d/m/Y
    protected $sortFormat = 'YmdHis';
    protected $shortFormat = 'd-m-Y';


    public function getUpdatedAtAttribute($value)
    {
        if ($value) {
            setlocale(LC_TIME, "es");
            return Carbon::parse($value)->format($this->format);
        }

        return '';
    }

    public function getCreatedAtAttribute($value)
    {
        if ($value) {
            setlocale(LC_TIME, "es");
            return Carbon::parse($value)->format($this->format);
        }

        return '';
    }

    public function getReceivedAtAttribute($value)
    {
        if ($value) {
            setlocale(LC_TIME, "es");
            return Carbon::parse($value)->format($this->format);
        }

        return '';
    }

    public function getOrderedAtAttribute($value)
    {
        if ($value) {
            setlocale(LC_TIME, "es");
            return Carbon::parse($value)->format($this->format);
        }

        return '';
    }

    public function getRequireDateAttribute($value)
    {
        if ($value) {
            setlocale(LC_TIME, "es");
            return Carbon::parse($value)->format($this->format);
        }

        return '';
    }


    public function orderedAt()
    {
        if ($this->ordered_at) {
            setlocale(LC_TIME, "es");
            return Carbon::parse($this->ordered_at)->format($this->shortFormat);
        }

        return '';
    }


    public function createdAt()
    {
        if ($this->created_at) {
            setlocale(LC_TIME, "es");
            return Carbon::parse($this->created_at)->format($this->shortFormat);
        }

        return '';
    }

    public function requiredAt()
    {
        if ($this->require_date) {
            setlocale(LC_TIME, "es");
            return Carbon::parse($this->require_date)->format($this->shortFormat);
        }

        return '';
    }


    public function transferredAt()
    {
        if ($this->transferred_at) {
            setlocale(LC_TIME, "es");
            return Carbon::parse($this->transferred_at)->format($this->shortFormat);
        }

        return '';
    }


    public function sortOrderedAt()
    {
        if ($this->ordered_at) {
            setlocale(LC_TIME, "es");
            return Carbon::parse($this->ordered_at)->format($this->sortFormat);
        }

        return '';
    }

    public function sortRequiredAt()
    {
        if ($this->require_date) {
            setlocale(LC_TIME, "es");
            return Carbon::parse($this->require_date)->format($this->sortFormat);
        }

        return '';
    }

    public function sortCreatedAt()
    {
        if ($this->created_at) {
            setlocale(LC_TIME, "es");
            return Carbon::parse($this->created_at)->format($this->sortFormat);
        }

        return '';
    }
}