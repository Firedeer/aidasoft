<?php

namespace TradeMarketing\Traits;

use Illuminate\Support\Str;

/**
 * Class ArrayHandler
 * @package TradeMarketing\Traits
 */
trait ArrayHandler
{


    /**
     * Comprime los valores repetidos de un array por su clave
     *
     *
     * @param $array
     * @param $key
     * @param $value
     * @return array
     */
    public static function array_condensed($array, $key, $value, $remove = true)
    {

        for ($i = 0; $i < count($array); $i++) {

            for ($j = $i + 1; $j < count($array); $j++) {

                if ($array[$i][$key] == $array[$j][$key]) {
                    $array[$i][$value] = $array[$i][$value] + $array[$j][$value];
                    $array[$j][$value] = 0;
                }
            }
        }

        if (is_null($array)) {
            return [];
        }


        // Filters amounts equal to zero
        if($remove == true){

            $array = array_filter($array, function ($item) {
                return $item['quantity'];
            });
        }

        return $array;
    }


    /**
     * Convert array values to Uppercase
     *
     * @param $array
     * @return array
     */
    public static function array_upper($array)
    {
        $values = [];

        foreach ($array as $key => $value) {

            if (is_array($value)) {
                $values[$key] = self::array_upper($value);

            } else {

                $values[$key] = self::strUpperText($value);

            }
        }

        return $values;
    }


    public static function strUpperText($value)
    {
        if (is_string($value)) {
            return Str::upper($value);
        }
        return $value;
    }

}