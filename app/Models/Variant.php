<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{

    protected $table = "tmk_variants";
    protected $fillable = ['article_id', 'attribute_value_id'];

    public function attributeValue(){
    	return $this->belongsTo(AttributeValue::class, 'attribute_value_id');
    }


    public static function variantsByArticle(){
        return \DB::table('tmk_articles')
            ->join('tmk_variants', 'tmk_variants.article_id', '=', 'tmk_articles.id')
            ->join('tmk_attribute_values', 'tmk_attribute_values.id', '=', 'tmk_variants.attribute_value_id')
            ->join('tmk_attributes', 'tmk_attributes.id', '=', 'tmk_attribute_values.attribute_id')
            ->select(
                'tmk_articles.article_master_id',
                'tmk_articles.id AS article_id',
                'tmk_articles.internal_reference',
                'tmk_attribute_values.id AS value_id',
                'tmk_attribute_values.value',
                'tmk_attributes.id AS attribute_id',
                'tmk_attributes.description AS attribute')
            ->get();
    }

}
