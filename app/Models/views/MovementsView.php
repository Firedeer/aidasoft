<?php

namespace TradeMarketing\Models\Views;

use Illuminate\Database\Eloquent\Model;
use TradeMarketing\Traits\FormatDates;

class MovementsView extends Model
{
    use FormatDates;

    protected $table = 'tmk_movements_view';

    protected $date = ['created_at'];


}