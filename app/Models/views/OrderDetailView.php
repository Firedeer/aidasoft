<?php

namespace TradeMarketing\Models\Views;

use Illuminate\Database\Eloquent\Model;

class OrderDetailView extends Model
{
    protected $table = "tmk_order_details_view";
    protected $primaryKey = 'id';
}