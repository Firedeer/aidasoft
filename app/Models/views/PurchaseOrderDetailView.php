<?php
namespace TradeMarketing\Models\views;

use Illuminate\Database\Eloquent\Model;
use TradeMarketing\Models\Article;

class PurchaseOrderDetailView extends Model
{
	protected $table = 'tmk_purchase_order_details_view';

	protected $date = ['created_at', 'updated_at', 'deleted_at'];


	public function scopeOrder($query, $id){
		return $query->where('order_id', $id);
	}


	public function scopeReceived($query){
		return $query->where('received', '<>', 0);
	}


	public function scopeMissing($query){
		return $query->where('missing', '<>', 0 );
	}


	public function article(){
		return $this->belongsTo(Article::class, 'article_id', 'id');
	}






}