<?php

namespace TradeMarketing\Models\Views;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TradeMarketing\Models\Warehouse;


class ArticleMasterWarehousesView extends Model
{
    use SoftDeletes;

    protected $table = "tmk_article_master_warehouses_view";
    protected $dates = ['deleted_at'];

    public function availableForOrders()
    {
        $quantity = $this->stock - $this->reserved;

        if ($quantity < 0) {
            return 0;
        }

        return $quantity;
    }


    public function scopeWarehouse($query, $warehouse)
    {
        return $query->where('warehouse_id', $warehouse);
    }

    public function scopeWarehouseMain($query)
    {
        return $query->where('warehouse_id', Warehouse::main()->first()->id);
    }


    public function getDescriptionAttribute(){
        return ArticleView::findOrFail($this->article_id)->description;
    }

}


