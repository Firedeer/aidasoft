<?php

namespace TradeMarketing\Models\Views;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use PhpParser\Node\Expr\List_;
use \TradeMarketing\Models\Views;

use \TradeMarketing;
use \TradeMarketing\Models\AttributeValue;


class ArticleView extends Model
{

    use SoftDeletes;

    protected $table = "tmk_articles_view";

    protected $primaryKey = 'article_id';

    protected $date = "deleted_at";


    public static function listing()
    {
        $Lists = static::select('article_id', 'internal_reference', 'article_master', 'description')->get();
        $articleList = [];

        foreach ($Lists as $key => $list) {

            $articleList[$list->article_id] = $list->description;
        }

        return $articleList;
    }


    public function attributeValues()
    {
        return $this->belongsToMany(AttributeValue::class, 'tmk_variants', 'article_id');
    }


    /**
     * @return string
     */
    public function getValuesByArticleAttribute()
    {
        $values = [];

        foreach ($this->attributeValues as $key => $value) {
            array_push($values, $value->description);
        }

        return implode(", ", $values);
    }


    public function getDescriptionAttribute()
    {
        return $this->article_master . ' ' . $this->valuesByArticle;
    }


    public function pendeing()
    {
        return $this->hasMany(views\PurchaseOrderDetailView::class, 'article_id');
    }


    /**
     * Filter Scope
     */
    public function scopeName($query, $name)
    {
        return $query->Where('name', 'like', "%" . $name . "%")
            ->orWhere('description', 'like', "%" . $name . "%")
            ->where('deleted_at', null);
    }


    public function scopeBrand($query, $brand)
    {
        return $query->orWhere('brand', $brand);
    }


    public function scopeCategory($query, $category)
    {
        return $query->orWhere('category', $category);
    }


    public function scopeFilterByCategory($query, $category)
    {
        return $query->select(
            'id', 'article_master_id', 'name', 'description', 'image', 'serial_code', 'brand', 'category', 'min_stock', 'max_stock', 'quantity'
        )->groupBy(
            'id', 'article_master_id', 'name', 'description', 'image', 'serial_code', 'brand', 'category', 'min_stock', 'max_stock', 'quantity'
        )->having('category', '=', $category);
    }

}