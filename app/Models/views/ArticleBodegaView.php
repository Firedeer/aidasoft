<?php

namespace TradeMarketing\Models\Views;

use Illuminate\Database\Eloquent\Model;
use TradeMarketing\Bodega;
use TradeMarketing\Models\Warehouse;

class ArticleBodegaView extends Model{

    protected $table = "tmk_article_bodegas_view";


    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'bodega_id');
    }


}