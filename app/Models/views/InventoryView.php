<?php

namespace TradeMarketing\Models\Views;

use Illuminate\Database\Eloquent\Model;
use \TradeMarketing\Article;

class InventoryView extends Model{

    protected $table = 'tmk_inventory_view';



    public static function alarmsByArticle(){
        return static::join('tmk_articles', 'tmk_articles.id', '=', 'tmk_inventory_view.article_id')
            ->select('article_id', 'min_stock', 'max_stock', \DB::raw('SUM(stock) as stock'))
            ->groupBy('article_id', 'min_stock', 'max_stock')
            ->havingRaw('min_stock >= SUM(stock)')
            ->orHavingRaw('max_stock <= SUM(stock)')
            ->havingRaw('max_stock <> 0')
            ->get();
    }



    public function articles(){
        return $this->belongsTo(Article::class, 'article_id');
    }



    public function scopeCategory($query, $category){
        return $query->join('tmk_categories', 'tmk_categories.id', '=', 'tmk_inventory_view.category_id')
            ->where('tmk_categories.id', $category)
            ->get();
    }


    public function scopeArticle($query, $article){
        return $query->join('tmk_article_masters', 'tmk_article_masters.id', '=', 'tmk_inventory_view.article_master_id')
            ->where('tmk_article_masters.id', $article)
            ->get();
    }


    public function scopeWarehouse($query, $warehouse){
        return $query->join('tmk_bodegas', 'tmk_bodegas.id', '=', 'tmk_inventory_view.bodega_id')
            ->where('tmk_bodegas.id', $warehouse)
            ->get();
    }


    public function scopeMovement($query, $movement){
        return $query->join('tmk_movements', 'tmk_movements.id', '=', 'tmk_inventory_view.movement_id')
            ->where('tmk_movements.id', $movement)
            ->get();
    }

}
