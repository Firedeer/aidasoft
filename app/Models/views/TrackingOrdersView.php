<?php
namespace TradeMarketing\Models\Views;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TrackingOrdersView extends Model
{

    protected $table = 'tmk_tracking_orders_view';


    public function getMadeAtAttribute($value){
        setlocale(LC_TIME, "es");
        return Carbon::parse($value)->format('d/m/Y  g:i:s');
    }

}