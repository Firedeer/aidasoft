<?php

namespace TradeMarketing\Models\Views;

use Illuminate\Database\Eloquent\Model;
use TradeMarketing\Traits\FormatDates;
use TradeMarketing\User;

class TransferView extends Model
{

    use FormatDates;

    protected $table = 'tmk_transfers_view';


    public function scopeNotDraft($query){
        return $query->where('status', '<>', 'draft');
    }


    public function getWarehouseOrigenAttribute(){
        return $this->transfer_warehouse_origen;
    }


    public function getWarehouseDestinationAttribute(){
        return $this->transfer_warehouse_origen;
    }


    public function transferredBy()
    {
        return $this->hasOne(User::class, 'id', 'transferred_by');
    }


    public function transferredFor()
    {
        return $this->hasOne(User::class, 'id', 'transferred_for');
    }

}
