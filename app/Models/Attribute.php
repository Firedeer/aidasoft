<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;
use TradeMarketing\ArticleMaster;

class Attribute extends Model
{
    
    protected $table = "tmk_attributes";
    protected $fillable = ['description', 'created_by', 'updated_by'];



    public function values(){
    	return $this->hasMany(AttributeValue::class);
    }

    public function articleMasters(){
        return $this->belongsToMany(ArticleMaster::class, 'tmk_article_master_attributes');
    }


    public static function listing(){
    	return static::all()->lists('description', 'id');
    } 



    
   
}
