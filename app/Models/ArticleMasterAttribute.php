<?php

namespace TradeMarketing\Models;


use Illuminate\Database\Eloquent\Model;

class ArticleMasterAttribute extends Model{


    protected $table = 'tmk_article_master_attributes';


    protected $fillable = ['article_master_id', 'attribute_id'];


    public function attributeValues(){
        return $this->hasMany(AttributeValue::class, 'attribute_id', 'id');
    }

    public function attribute(){
        return $this->belongsTo(Attribute::class);
    }
}