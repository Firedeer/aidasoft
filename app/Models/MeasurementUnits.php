<?php
namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class MeasurementUnits extends Model {

	protected $table = "tmk_measurement_units";

	protected $fillable = ['description', 'unity'];

	public static function insert_measured_articles($unity, $article, $value){
		DB::table('tmk_measured_articles')->insert( array (
			'unity_id'   => $unity,
			'article_id' => $article,
			'value' 	 => $value,
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s"),
		));
	}


	public static function listing(){
		return static::orderBy('description')->lists('description', 'id');
	}

}
