<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
	protected $table = 'tbl_sucursales';
	protected $guarded = ['ID_SUCURSAL'];
	protected $primaryKey = "id_sucursal";



	public function region(){
		return $this->belongsTo(Region::class, 'ID_REGION');
	}


	public static function listing(){
		return static::orderBy('descr_sucursal')->lists('descr_sucursal', 'id_sucursal');
	}

}
