<?php namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class ArticleBodega extends Model {

    protected $table = 'tmk_article_bodegas';
    protected $fillable = ['article_id', 'bodega_id', 'quantity', 'quantity_exit', 'initial', 'movement_id'];


    public function transfers(){
        return $this->belongsToMany(Transfer::class, 'tmk_transfer_article_bodegas');
    }


    public function transferReceipts(){
        return $this->belongsToMany(TransferReceipts::class, 'tmk_transfer_receipt_article_bodegas');
    }


    /**
     * Get all of the owning movementable models.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function movementable(){
        return $this->morphTo();
    }


    /**
     * @param $details
     * @param $warehouse
     * @param $movement
     * @return array
     */
    public static function movementsInWarehouse($details, $warehouse, $movement){

        $articles = [];
        $type = '';

        if(Movement::find($movement)->type_movement_id == 2){
            $type = '_exit';
        }

        foreach ($details as $detail) 
        {

		
            array_push($articles, new ArticleBodega([
                'article_id' => (int)$detail->article_id,
                'bodega_id' => (int)$warehouse,
                'quantity'.$type => (int)$detail->quantity,
                'movement_id' => (int)$movement
            ]));
        }
      
        return $articles;
    }







    /*
     SELECT tmk_articles.description as 'article', tmk_articles.image, tmk_article_bodegas.bodega_id as 'idbodega', tmk_article_bodegas.quantity as 'cant' ,tmk_article_bodegas.active, bodegas.description as 'bodega', bodegas.colour
     FROM tmk_article_bodegas
     INNER JOIN tmk_articles ON tmk_articles.id = tmk_article_bodegas.article_id
     INNER JOIN bodegas ON bodegas.id = tmk_article_bodegas.bodega_id
     WHERE tmk_article_bodegas.article_id = $id
     */
	public static function list_article_bodega2($id){
        return DB::table('tmk_article_bodegas')
            ->join('tmk_articles','tmk_articles.id','=','tmk_article_bodegas.article_id')
            ->join('tmk_bodegas','tmk_bodegas.id','=','tmk_article_bodegas.bodega_id')
            ->select('tmk_articles.name as article', 
                    'tmk_articles.image',
                    'tmk_article_bodegas.bodega_id as idbodega',
                    'tmk_article_bodegas.quantity as cant', 
                    'tmk_article_bodegas.active',   
                    'tmk_bodegas.description as bodega',
                    'tmk_bodegas.colour')
            ->where('tmk_article_bodegas.article_id','=',$id)
            ->get();
    }

    public static function list_article_bodega($id = 1){
            // return DB::select('Exec list_article_bodegas ?', array($id));
        return DB::table('tmk_inventory_view')
        ->join('tmk_bodegas', 'tmk_bodegas.id', '=', 'tmk_inventory_view.bodega_id')
        ->select( 'bodega_id', 'bodega', 'colour', 'article_id', 'barcode', 'article', 'max_stock', 'min_stock', 'initial', 'stock'  )
        ->where('article_id', '=', $id)
        ->get();
    }
    
    
    /* 
     SELECT tmk_articles.id, tmk_articles.description as 'article', tmk_articles.barcode, bodegas.id as 'idbodega',      bodegas.description as 'bodega', TBL_SUCURSALES.DESCR_SUCURSAL as 'center', brands.description as 'brand',     tmk_classes.description  'clase', tmk_categories.description as 'category', tmk_article_bodegas.quantity as 'cant', tmk_articles.unit_cost
     FROM tmk_article_bodegas
     INNER JOIN tmk_articles ON tmk_articles.id = tmk_article_bodegas.article_id
     INNER JOIN bodegas ON bodegas.id = tmk_article_bodegas.bodega_id 
     INNER JOIN TBL_SUCURSALES ON TBL_SUCURSALES.ID_SUCURSAL = bodegas.center_id
     INNER JOIN brands ON brands.id = tmk_articles.brand_id
     INNER JOIN tmk_classes ON tmk_classes.id = tmk_articles.class_id
     INNER JOIN tmk_categories ON tmk_categories.id = tmk_classes.category_id
     GROUP BY tmk_articles.id, tmk_articles.description, 
     tmk_articles.barcode, bodegas.id, bodegas.description, TBL_SUCURSALES.DESCR_SUCURSAL, brands.description,   tmk_classes.description, tmk_categories.description, tmk_article_bodegas.quantity
     ORDER BY tmk_articles.id ASC
     */
    public static function List_Stock_All(){
        return DB::table('tmk_article_bodegas')
            ->join('tmk_articles','tmk_articles.id','=','tmk_article_bodegas.article_id')
            ->join('tmk_bodegas','tmk_bodegas.id','=','tmk_article_bodegas.bodega_id')  
            ->join('tmk_brands','tmk_brands.id','=','tmk_articles.brand_id')
            ->join('TBL_SUCURSALES','TBL_SUCURSALES.ID_SUCURSAL','=','tmk_bodegas.center_id')
            ->join('tmk_classes','tmk_classes.id','=','tmk_articles.class_id')
            ->join('tmk_categories','tmk_categories.id','=','tmk_classes.category_id')
            ->select('tmk_articles.id',
                     'tmk_articles.description as article', 
                     'tmk_articles.barcode', 
                     'tmk_brands.description as brand',
                     'tmk_bodegas.id as idbodega',
                     'tmk_bodegas.description as bodega',
                     'TBL_SUCURSALES.DESCR_SUCURSAL as center', // centro  = sucursal
                     'tmk_classes.description as class',
                     'tmk_categories.description as category',
                     'tmk_articles.unit_cost as cost',
                     'tmk_article_bodegas.quantity as cant'
                    )
            
            ->groupBy('tmk_articles.id',
                      'tmk_articles.description', 
                      'tmk_articles.barcode',
                      'tmk_brands.description',
                      'tmk_bodegas.id',
                      'tmk_bodegas.description',
                      'TBL_SUCURSALES.DESCR_SUCURSAL', 
                      'tmk_classes.description',
                      'tmk_categories.description',
                      'tmk_articles.unit_cost',
                     'tmk_article_bodegas.quantity'
                     )
            ->orderBy('tmk_articles.id', 'asc')
            ->get();
    }



    public static function insert_article_bodega_detail($id, $colour, $sex, $size, $quantity){
        DB::table('tmk_article_details')->insert( array(
            'article_bodega_id' => $id,
            'colour'            => $colour,
            'sex'               => $sex,
            'size'              => $size,
            'quantity'          => $quantity,
            'created_at'        => date("Y-m-d H:i:s"),
            'updated_at'        => date("Y-m-d H:i:s"),
        ));
    }


    /*
     SELECT tmk_article_bodegas.quantity, SUM( tmk_article_details.quantity ) AS 'quantity2'
     FROM tmk_article_bodegas LEFT JOIN tmk_article_details
     ON tmk_article_bodegas.id = tmk_article_details.article_bodega_id
     WHERE tmk_article_bodegas.id = {id del articulo en la bodega} 
     AND tmk_article_bodegas.bodega_id = {id de la bodega}
     GROUP BY tmk_article_bodegas.quantity
     */
    public static function get_quantity_article($id, $bodega){
        return DB::table('tmk_article_bodegas')
            ->leftJoin('tmk_article_details', 'tmk_article_details.article_bodega_id', '=', 'tmk_article_bodegas.id')
            ->join('tmk_articles', 'tmk_articles.id', '=', 'tmk_article_bodegas.article_id')
            ->where('tmk_article_bodegas.id', '=', $id)
            ->where('tmk_article_bodegas.bodega_id', '=', $bodega)
            ->select( 'tmk_article_bodegas.quantity', DB::raw('SUM(tmk_article_details.quantity) as quantity2'), 'tmk_articles.name' )
            ->groupby('tmk_article_bodegas.quantity', 'tmk_articles.name')
            ->get();
    }

}
