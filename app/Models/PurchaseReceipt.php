<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class PurchaseReceipt extends Model{

	use SoftDeletes;

	protected $table = "tmk_purchase_receipts";
	protected $fillable = ['purchase_order_id', 'article_id', 'quantity'];
	protected  $dates = ['deleted_at'];



	public function purchaseOrder(){
	    return $this->belongsTo(PurchaseOrder::class);
    }







	/*************************************
	**************************************/

	public static function store($data = []){
		if ($data){
			return PurchaseReceipt::create($data);            
		}
	}

	public static function purchaseReceipts($id){
		return DB::table('tmk_purchase_receipts')
		->join('tmk_users', 'tmk_users.id', '=', 'tmk_purchase_receipts.received_by')
		->join('tbl_usuarios', 'tbl_usuarios.cod_agente', '=', 'tmk_users.agent_id')
		->join('tbl_sucursales', 'tbl_sucursales.id_sucursal', '=', 'tmk_purchase_receipts.received_in')
		->where('purchase_detail_id', $id)
		->select('purchase_detail_id', 'nombre as received_by', 'quantity', 'tmk_purchase_receipts.created_at', 'tbl_sucursales.descr_sucursal as received_in')
		->get();
	}
}
