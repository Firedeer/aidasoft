<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

class TransferReceiptDetail extends Model{

    protected $table = "tmk_transfer_receipt_details";

    protected $fillable = ['transfer_receipt_id', 'article_id', 'quantity'];
}