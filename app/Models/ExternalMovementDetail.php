<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ExternalMovementDetail extends Model
{
    protected $table = 'tmk_external_movement_details';

    protected $fillable = ['article_id', 'quantity', 'external_agent_id'];

    public function externalMovement()
    {
        return $this->belongsTo(ExternalMovement::class);
    }

    public function article(){
     return $this->belongsTo(Article::class);
    }

    public function getAgentNameAttribute(){

        return config('enums.external_agents.'.$this->external_agent_id);

//        if(array_key_exists($this->external_agent_id, config('enums.external_agents')));
//        return $this->external_agent_id;
//        return DB::table('tbl_usuarios')
//            ->where('COD_AGENTE', $this->external_agent_id)
//            ->first()->nombre;
    }

}
