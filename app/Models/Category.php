<?php
namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table = 'tmk_categories';
	protected $fillable = ['description', 'created_by', 'updated_by'];


	public static function listing(){
		return static::where('description', '<>', 'Desconocido')->orderBy('description')->lists('description', 'id');
	}

}
