<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use TradeMarketing\Models\Views\OrderDetailView;

class OrderDetail extends Model
{

    protected $table = 'tmk_order_details';
    protected $fillable = ['order_id', 'article_id', 'quantity', 'pending'];


    public function order()
    {
        return $this->belongsTo(Order::class);
    }


    public function article()
    {
        return $this->belongsTo(Article::class);
    }


    public function getPendingAttribute()
    {
        return $this->quantity - $this->received;
    }

    public function getReceivedAttribute()
    {
        return DB::table('tmk_transfers')
            ->join('tmk_transfer_details', 'tmk_transfer_details.transfer_id', '=', 'tmk_transfers.id')
            ->select(DB::raw('SUM(tmk_transfer_details.received_quantity) as received'))
            ->where('tmk_transfer_details.article_id', $this->article_id)
            ->where('tmk_transfers.order_id', $this->order_id)
            ->first()->received;
    }

    public function scopeArticle($query, $article)
    {
        return $query->where('article_id', $article);
    }


    public function getStatusAttribute()
    {
        return Round($this->received / $this->quantity * 100, 2);
    }

}
