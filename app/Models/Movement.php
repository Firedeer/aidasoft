<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

// Database
use DB;

class Movement extends Model
{
	protected $table = 'tmk_movements';
    // protected $fillable = ['id'];




    public static function listing(){
        return static::orderBy('description')->lists('description', 'id');
    }




	/*
	SELECT 
		tmk_movements.id, 
		tmk_movements.description AS 'movement', 
		tmk_type_movements.id AS 'type', 
		tmk_type_movements.code, 
		tmk_type_movements.description 
	FROM tmk_movements 
	INNER JOIN tmk_type_movements ON  tmk_type_movements.movement_id =  tmk_movements.id
	*/
	public static function list_movements(){
		return DB::table('tmk_movements')
		->join('tmk_type_movements', 'tmk_type_movements.movement_id', '=', 'tmk_movements.id')
		->select(
			'tmk_movements.id',
			'tmk_movements.description as movement',
			'tmk_type_movements.id as type',
			'tmk_type_movements.code',
			'tmk_type_movements.description'
			)
		->get();
	}
/*
    public static function list_movements_articles($id){
    	return DB::table('tmk_article_bodegas')
    	->join('tmk_articles', 'tmk_articles.id', '=', 'tmk_article_bodegas.article_id') 
    	->join('tmk_bodegas', 'tmk_bodegas.id', '=', 'tmk_article_bodegas.bodega_id') 
    	->join('tmk_type_movements', 'tmk_type_movements.id', '=', 'tmk_article_bodegas.type_movement_id') 
    	->join('tmk_movements', 'tmk_movements.id', '=', 'tmk_type_movements.movement_id')
    	->leftjoin('tmk_invoice_details', 'tmk_invoice_details.article_id', '=', 'tmk_article_bodegas.id')
    	->where('tmk_articles.id', '=', $id)
    	->select(
    		'tmk_article_bodegas.id',
			'tmk_article_bodegas.date',
			'tmk_article_bodegas.quantity',
			'tmk_invoice_details.unit_cost',
			'tmk_invoice_details.tax',
			// 'tmk_invoice_details.discount',
			'tmk_article_bodegas.article_id',
			'tmk_articles.barcode',
			'tmk_articles.name as article',
			'tmk_bodegas.description as bodega',
			'tmk_article_bodegas.bodega_id',
			'tmk_article_bodegas.type_movement_id',
			'tmk_type_movements.description as type_movement',
			'tmk_movements.description as movement'
    		)
    	->orderBy('tmk_article_bodegas.date', 'asc')
    	->get();
    }*/

    public static function list_movements_articles($id){
    	return DB::table('tmk_movement_list_view')
    	->where('article_id', '=', $id)
    	->orderBy('created_at', 'asc')
    	->orderBy('id', 'asc')
    	->get();
    }

}
