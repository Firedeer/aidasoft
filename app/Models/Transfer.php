<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use TradeMarketing\Models\Views\ArticleMasterWarehousesView;
use TradeMarketing\Models\Views\TrackingOrdersView;
use TradeMarketing\Traits\FormatDates;
use TradeMarketing\User;
use DB;

class Transfer extends Model
{
    use FormatDates;

    use SoftDeletes;

    protected $table = 'tmk_transfers';

    protected $fillable = [
        'order_id',
        'transfer_num',
        'transfer_reference',
        'subject',
        'bodega_id',
        'bodega_id_end',
        'transferred_by',
        'transferred_for',
        'transferred_from',
        'transferred_to',
        'observation',
        'status_id',
        'created_by',
        'updated_by',
        'receipt_num',
        'received_by',
        'received_at',
        'transferred_at',
        'receipt_observation',
        'receipt_file',
        'updated_by',
        'canceled_by',
        'canceled_at',
        'requested_at',
        'requested_by',
    ];

    protected $date = ['received_at', 'transferred_at', 'canceled_at', 'requested_at'];


    public function isValid($data)
    {

        $rules = [
            'order_id' => 'required|exists:tmk_orders,id',
            'transfer_num' => 'required_unless:status_id,1|unique:tmk_transfers',
            'transfer_reference' => 'unique:tmk_transfers',
            'transferred_for' => 'exists:tmk_users,id',
            'transferred_to' => 'required|exists:tbl_sucursales,ID_SUCURSAL',
            'bodega_id' => 'required|exists:tmk_bodegas,id',
            'bodega_id_end' => 'required|different:bodega_id|exists:tmk_bodegas,id',
            'observation' => 'min:5|max:255',
            'items' => 'required',
        ];

        if ($this->exists) {
            $rules['transfer_num'] .= ',transfer_num,' . $this->id;
            $rules['transfer_reference'] .= ',transfer_reference,'. $this->id;
        }

        $items = isset($data['items']) ? $data['items'] : [];

        foreach ($items as $key => $item) {
            $rules['items.' . $key . '.article'] = 'required|id|exists:tmk_articles,id,deleted_at,NULL';
            $rules['items.' . $key . '.quantity'] = 'required|numeric|min:1|max: ' . $this->availableQuantity($item['article'], $data['bodega_id']) . '';
        }


        $messages = [
            'bodega_id_end.different' => 'La bodega destino y la bodega origen deben ser diferentes.',
            'items.required' => 'Se requiere de al menos un artículo en la lista.',
            'items.required_unless' => 'Se requiere de al menos un artículo en la lista.',
        ];

        foreach ($items as $key => $value) {
            $messages['items.' . $key . '.article.required'] = 'El artículo es obligatorio.';
            $messages['items.' . $key . '.article.exists'] = 'El artículo es inválido.';
            $messages['items.' . $key . '.article.id'] = 'El artículo es inválido.';
            $messages['items.' . $key . '.quantity.required'] = 'La cantidad es obligatoria.';
            $messages['items.' . $key . '.quantity.numeric'] = 'La cantidad debe ser numérico.';
            $messages['items.' . $key . '.quantity.min'] = 'El tamaño de la cantidad debe ser de al menos :min';
            $messages['items.' . $key . '.quantity.max'] = 'La bodega no cuenta con stock suficiente.';
        }


        $validator = Validator::make($data, $rules, $messages);

        if ($validator->passes()) {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
    }


    public function availableQuantity($article, $warehouse)
    {
        if ($quantity = ArticleMasterWarehousesView::where('warehouse_id', $warehouse)->where('article_id', $article)->first()) {
            $old = ($this->details()->where('article_id', $article)->first())
                ? $this->details()->where('article_id', $article)->first()->quantity
                : 0;

            return $quantity->available_quantity + $old;
        }

        return 0;
    }


    public function isDraft()
    {
        return $this->status == 'draft';
    }


    public function orderState($stateRequired)
    {
        $status = OrderStatus::statusHierarchy();

        return $status[$this->status] < $status[$stateRequired];
    }


    /**
     * Eloquent: Relationships
     *
     *
     * Las tablas de base de datos suelen estar relacionadas entre sí.
     * Por ejemplo, una entrada de blog puede tener muchos comentarios o un pedido podría estar relacionado
     * con el usuario que lo colocó.
     * Eloquent facilita la gestión y el trabajo con estas relaciones y apoya varios tipos diferentes de relaciones:
     *
     * https://laravel.com/docs/5.1/eloquent-relationships
     */

    public function articleBodegaMovements()
    {
        return $this->belongsToMany(ArticleBodega::class, 'tmk_transfer_article_bodegas');
    }


    public function articleWarehouseMovements()
    {
        return $this->morphMany(ArticleBodega::class, 'movementable');
    }


    public function createdBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function details()
    {
        return $this->hasMany(TransferDetails::class);
    }


    public function order()
    {
        return $this->belongsTo(Order::class);
    }


    public function tracking()
    {
        return $this->hasMany(TrackingOrdersView::class)->orderBy('made_at', 'asc');
    }


    public function transferredBy()
    {
        return $this->hasOne(User::class, 'id', 'transferred_by');
    }


    public function transferredFor()
    {
        return $this->hasOne(User::class, 'id', 'transferred_for');
    }


    public function transferredFrom()
    {
        return $this->hasOne(Sucursal::class, 'id_sucursal', 'transferred_from');
    }


    public function transferredTo()
    {
        return $this->hasOne(Sucursal::class, 'id_sucursal', 'transferred_to');
    }


    /**
     * Eloquent: Mutators
     *
     *
     * Los accesores y mutadores le permiten dar formato a valores de atributos de Eloquent cuando los recupera
     * o los configura en instancias de modelo.
     * Por ejemplo, puede utilizar el cifrador de Laravel para cifrar un valor mientras se almacena en la base de datos y,
     * a continuación, descifrar automáticamente el atributo cuando se accede a él en un modelo Eloquent.
     *
     * https://laravel.com/docs/5.1/eloquent-mutators
     */

    public function getOrderNumAttribute()
    {
        return Order::where('id', $this->order_id)->first()->order_num;
    }


    public function getStatusAttribute()
    {
        return Status::where('id', $this->status_id)->first()['description'];
    }


    public function getWarehouseDestinationAttribute()
    {
        return Warehouse::find($this->bodega_id_end)['description'];
    }


    public function getWarehouseOrigenAttribute()
    {
        return Warehouse::find($this->bodega_id)['description'];
    }



    /**
     * Eloquent: Local Scopes Queries
     *
     *
     * Los ámbitos locales le permiten definir conjuntos comunes de restricciones
     * que puede reutilizar fácilmente en toda la aplicación.
     *
     * https://laravel.com/docs/5.1/eloquent#query-scopes
     */

    public function scopeDraft($query)
    {
        return $query->where('status_id', 1);
    }

    public function scopeNotDraft($query)
    {
        return $query->where('status_id', '<>', 1);
    }

    public function scopeTransferred($query)
    {
        return $query->where('status_id', 10);
    }


}