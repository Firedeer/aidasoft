<?php namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model {

	protected $table = 'tbl_regiones';
	protected $fillable = ['ID_REGION', 'DESCR_REGION'];
	protected $primaryKey = 'ID_REGION';

}
