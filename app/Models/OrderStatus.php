<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{

    protected $table = 'tmk_order_status';
    protected $fillable = ['acronym', 'description', 'value', 'observation', 'colour'];



    public static function statusHierarchy(){
        return static::orderBy('value')->lists('value', 'description')->toArray();
    }

}
