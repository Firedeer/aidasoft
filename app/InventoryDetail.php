<?php namespace TradeMarketing;

use Illuminate\Database\Eloquent\Model;

use DB;

class InventoryDetail extends Model {
    
    protected $table = 'tmk_inventory_details';
    protected $fillable = ['id', 'article_id', 'bodega_id', 'inventory_id', 'quantity', 'unit_cost'];
    
    /**
     * SELECT tmk_inventory_details.bodega_id, 
     * tmk_inventory_details.quantity, 
     * tmk_inventory_details.unit_cost, 
     * tmk_inventory_details.quantity * tmk_inventory_details.unit_cost AS 'subtotal', 
     * tmk_articles.description, 
     * tmk_bodegas.description
     * FROM tmk_inventory_details 
     * IN1NER JOIN tmk_articles ON tmk_articles.id =  tmk_inventory_details.article_id 
     * INNER JOIN tmk_bodegas ON tmk_bodegas.id = tmk_inventory_details.bodega_id
     * WHERE  tmk_inventory_details.inventory_id = 19 
     */
     public static function List_Inventory_Details($id){
        return DB::table('tmk_inventory_details')
            ->join('tmk_articles','tmk_articles.id','=','tmk_inventory_details.article_id')
            ->join('tmk_bodegas','tmk_bodegas.id','=','tmk_inventory_details.bodega_id')  
            ->select('tmk_inventory_details.article_id as article',
                     'tmk_inventory_details.bodega_id as bodega', 
                     'tmk_inventory_details.quantity as cantidad',
                     'tmk_inventory_details.unit_cost as costo',
                     'tmk_articles.description as articlename',
                     'tmk_bodegas.description as bodeganame',
                     'tmk_articles.barcode', 
                     DB::raw(' tmk_inventory_details.quantity * tmk_inventory_details.unit_cost as subtotal')
                    )
            
            ->where('tmk_inventory_details.inventory_id','=', $id)
            ->groupBy('tmk_inventory_details.article_id',
                      'tmk_inventory_details.bodega_id',
                      'tmk_inventory_details.quantity',
                      'tmk_inventory_details.unit_cost',
                      'tmk_articles.description',
                      'tmk_bodegas.description',        
                      'tmk_articles.barcode'
                     )
            ->get();
    }
}