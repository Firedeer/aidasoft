<?php
namespace TradeMarketing;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use TradeMarketing\Models\Order;
use TradeMarketing\Models\Roles;
use TradeMarketing\Models\Sucursal;
use TradeMarketing\Models\Transfer;
use TradeMarketing\Models\Warehouse;

use DB;


class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{

    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'tmk_users';

    protected $fillable = [
        'agent_id', 'active', 'role_id', 'firstname', 'lastname', 'phone', 'address', 'email', 'username', 'password',
        'confirmation_token', 'sucursal_id', 'warehouse_id'
    ];

    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['deleted_at'];

    protected $roles = [
        'superadmin' => 100,
        'admin' => 75,
        'editor' => 50,
        'user' => 10
    ];


    public function firstChar($word)
    {
        return substr($word, 0, 1);
    }


    public function getNameProfileAttribute()
    {
        $first = $this->firstChar($this->firstname);
        $last = $this->firstChar($this->lastname);

        return $first . '' . $last;
    }

    public static function listing()
    {
        $users = static::all()->map(function ($name) {
            return ['id' => $name->id, 'fullName' => $name->fullName];
        });

        return $users->lists('fullName', 'id');
    }


    public function isValid($data)
    {
        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'username' => 'unique:tmk_users,username',
            'agent_id' => 'unique:tmk_users,agent_id',
            'phone' => '',
            'address' => '',
            'email' => 'required|email|unique:tmk_users,email',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required'
        ];


        if ($this->exists) {
            $rules['username'] .= ',' . $this->id;
            $rules['email'] .= ',' . $this->id;
            $rules['agent_id'] .= ',' . $this->id;
            $rules['password'] = '';
            $rules['password_confirmation'] = '';
        }


        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $this->errors = $validator->errors();
            return false;
        }

        return true;
    }


    public function warehouses()
    {
        return $this->belongsToMany(Warehouse::class, 'tmk_user_warehouses')->whereNull('tmk_user_warehouses.deleted_at');
    }


    public function warehousesId()
    {
        return $this->warehouses->pluck('id')->toArray();
    }


    /**
     * Verifica si la bodega pasada por parámetro es administrada por el usuario relacionado
     *
     * @param $warehouse
     * @return bool
     */
    public function isInCharge($warehouse)
    {

        if (in_array($warehouse, $this->warehousesId())) {
            return true;
        }

        return false;
    }


    public function sucursal()
    {
        if ($sucursal = Sucursal::join('tmk_bodegas', 'tmk_bodegas.sucursal_id', '=', 'tbl_sucursales.id_sucursal')
            ->where('tmk_bodegas.user_id', $this->id)
            ->first()
        ) {

            return $sucursal;
        }

        return $this;
    }


    /**
     *
     */
    public function role()
    {
        return $this->belongsTo(Roles::class);
    }


    public function orders()
    {
        return $this->hasMany(Order::class, 'created_by');
    }


    public function ordersIn()
    {
        return $this->hasMany(Order::class, 'for_user_id');
    }


    public function transferInbox()
    {
        return $this->hasMany(Transfer::class, 'transferred_for')->where('status', '<>', 'draft');
    }


    public function getFullNameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }


    public function authRole($role)
    {
        $user_role = $this->role->description;

        if ($this->roles[$user_role] < $this->roles[$role]) {
            return false;
        }

        return true;
    }


    public function isAuthor($model)
    {
        return $this->id == $model->created_by;
    }



    public function isAdmin()
    {
        return $this->authRole('admin');
    }



    public function hasOrder()
    {
        return $this->orders()->draft()->get()->last();
    }


    public static function supervisorListing()
    {
        return DB::table('tmk_clientes_empresas')->where('cliente', '1')->lists('Nombre', 'id');
    }


    public static function externalAgentListing()
    {
        return DB::table('tmk_clientes_empresas')->where('cliente', '1')->lists('Nombre', 'id');
    }


    public static function comercialAgents()
    {
        return DB::table('tmk_clientes_empresas')
            ->where('cliente', 1)
            ->get();
    }


    public static function nameFormat($name, $lastname = false)
    {
        $array = explode(' ', $name, 2);

        if (count($array) > 1 and $lastname == true) {
            return $array[1];
        }

        return $array[0];
    }

    public static function newUser($data)
    {
        $user = new User([
            'firstname' => static::nameFormat($data->nombre),
            'lastname' => static::nameFormat($data->nombre, true),
            'email' => $data->EMAIL,
            'username' => $data->USERNAME,
            'agent_id' => $data->COD_AGENTE,
            'password' => bcrypt(str_random(30)),
            'confirmation_token' => str_random(60),
            'role_id' => 4,
        ]);

        $user->save();

        return $user;
    }


    public static function isTrade($user)
    {
        return DB::table('tmk_clientes_empresas')
            ->where('Cliente', '1')
            ->where('id', $user)
            ->first();
    }


    public function sendMailConfirmation()
    {
        $user = $this;

        $url_confirmation = route('confirmation', ['token' => $user->confirmation_token]);

        Mail::send('emails.confirm', compact('user', 'url_confirmation'), function ($m) use ($user) {
            $m->to($user->email, $user->firstname)->subject('Confirma tu cuenta!');
        });

    }


    public static function isConfirmated($data)
    {
/*
        if ($user = User::where('username', $data['username'])->whereNotNull('confirmation_token')->first()) {
            if ($user->confirmation_token = $data['confirmation_token']) {

                if (DB::table('tbl_usuarios')
                    ->where('username', '=', $data['username'])
                    ->where('password', '=', hash('md5', $data['password']))
                    ->first()
                ) {

                    $user->password = bcrypt($data['password']);
                    $user->confirmation_token = null;
                    $user->save();
                }
            }
        }*/
    }


    /*************************
     **************************
     **************************/

    public static function Show_All()
    {
        /*
        return DB::table('tmk_users')
            ->join('tbl_usuarios', 'tbl_usuarios.COD_AGENTE', '=', 'tmk_users.agent_id')
            ->select('tmk_users.*',
                'tbl_usuarios.*'
            )
            ->get();
            */
    }

    public static function Show_User($id)
    {
        /*
        return DB::table('tmk_users')
            ->join('tbl_usuarios', 'tbl_usuarios.COD_AGENTE', '=', 'tmk_users.agent_id')
            ->join('tmk_roles', 'tmk_roles.id', '=', 'tmk_users.role_id')
            ->where('tmk_users.id', '=', $id)
            ->select('tmk_users.*',
                'tbl_usuarios.nombre as name',
                'tbl_usuarios.EMAIL as email',
                'tmk_roles.description as role'
            )
            ->get();*/
    }

    public static function list_users()
    {
        /*
        return DB::table('tmk_users')
            ->join('tbl_usuarios', 'tbl_usuarios.COD_AGENTE', '=', 'tmk_users.agent_id')
            ->select('tmk_users.id',
                'tbl_usuarios.nombre as name',
                'tbl_usuarios.EMAIL as email'
            )
            ->get();
            */
    }

    public static function Update_User_Role($user, $role)
    {
        return DB::table('tmk_users')
            ->where('agent_id', $user)
            ->update(['role_id' => $role]);

    }

}
