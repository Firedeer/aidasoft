<?php

namespace TradeMarketing;

use Illuminate\Database\Eloquent\Model;

// database 
use DB;

class Devolution extends Model
{
    //

    protected $table = 'tmk_devolutions';

    protected $fillable = ['id', 'date', 'amount', 'description'];


    public static function insert_devolution_purchase($devolution, $invoice, $article, $reference, $quantity, $observation ){
    	return DB::table('tmk_devolution_purchases')->insert( array(
    		'devolution_id' =>  $devolution,
    		'invoice_id' 	=>  $invoice,
    		'article_id' 	=>  $article,
    		'reference_id' 	=>  $reference,
    		'quantity' 		=>  $quantity,
    		'observation'	=> 	$observation,
    		'created_at'    => date("Y-m-d H:i:s"),
            'updated_at'    => date("Y-m-d H:i:s"),
    	));
    } 
}
