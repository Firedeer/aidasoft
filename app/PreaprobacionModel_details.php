<?php


namespace TradeMarketing;



use Illuminate\Auth\Access\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


use TradeMarketing\Models\ArticleMasterAttribute;
use TradeMarketing\Models\ArticleMasterWarehouse;
use TradeMarketing\Models\Views\ArticleView;
use TradeMarketing\Models\ArticleWarehouse;
use TradeMarketing\Traits\ResponseHandler;
use TradeMarketing\Traits\ImageHandler;
use TradeMarketing\Models\Attribute;
use TradeMarketing\Models\Brand;
use TradeMarketing\Models\Category;
use TradeMarketing\Models\MeasurementUnits;
use Validator;
use DB; 

use Illuminate\Database\Eloquent\Model;

class PreaprobacionModel_details extends Model
{
    protected $table = 'tmk_preaprobacionSalida_details';

    protected $fillable = ['article_id', 'quantity', 'external_agent_id'];

    public function externalMovement()
    {
        return $this->belongsTo(ExternalMovement::class);
    }

    public function article(){
     return $this->belongsTo(Article::class);
    }

    public function getAgentNameAttribute(){

        return config('enums.external_agents.'.$this->external_agent_id);

//        if(array_key_exists($this->external_agent_id, config('enums.external_agents')));
//        return $this->external_agent_id;
//        return DB::table('tbl_usuarios')
//            ->where('COD_AGENTE', $this->external_agent_id)
//            ->first()->nombre;
    }
}
