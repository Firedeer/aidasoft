<?php

function currentUser()
{
    return auth()->user();
}


function userIP()
{
    // IP Share
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];

    // IP Proxy
    if (!empty($_SERVER['TTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];

    // IP Access
    return $_SERVER["REMOTE_ADDR"];
}