<?php

namespace TradeMarketing\Listeners;

use Illuminate\Support\Facades\Log;
use TradeMarketing\Events\OrderWasSended;
use TradeMarketing\Events\OrderWasUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderWasUpdated  $event
     * @return void
     */
    public function handle(OrderWasUpdated $event)
    {
        Log::info('Se actualizo la orden ' .$event->order->order_num );
    }


}
