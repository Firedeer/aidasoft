<?php
namespace TradeMarketing;
use Illuminate\Auth\Access\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TradeMarketing\Traits\ImageHandler;
use Validator;
use DB;
class Sucursalclientes extends Model
{
     protected $table = "tmk_clientes_sucursales";


    protected $fillable = [
		'id','nombre','telefono','movil','correo','image'
    ];
	
	 public function isValid($data)
    {
		 $rules = 
		 [
		 'nombre' => 'min:5|max:255',
		 ];
		    // Si el articulo existe:
        if ($this->exists) {

            $rules['nombre'] .= ',nombre,' . $this->nombre;
        }
		 $validator = \Illuminate\Support\Facades\Validator::make($data, $rules);
		  if ($validator->passes()) {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
	}
	 public static function listing($id)
    {
        return static::orderBy('nombre')->where('id_empresa',$id)->lists('nombre', 'id');
    }
	 public static function buscar_sucursalcliente($id)
    {
         return DB::table('tmk_clientes_sucursales')
            ->where('id_empresa',$id)
            ->get();
    }
		 public static function buscar_sucursales($id)
    {
         return DB::table('tmk_clientes_sucursales')
            ->where('id',$id)
            ->get();
    }
}
