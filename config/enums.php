<?php
return [

	'provider_types' => [ 
		'Bienes' => 'Bienes', 
		'Servicios' => 'Servicios', 
		'Recursos' => 'Recursos' 
	],

	'taxes' => [
		'.00' => '0%', 
		'.05' => '5%', 
		'.07' => '7%', 
		'.10' => '10%' 
	],

	'attributes' => [
		'1' => 'color', 
		'2' => 'talla',
		'3' => 'dimenciones',
		'4' => 'peso', 
		'5' => 'volumen',
		'6' => 'temperatura', 
		'7' => 'material' 
	],

    'searchFilter' => [
        'all'      => 'Todo',
        'category' => 'Categoria',
        'warehouse'=> 'Bodega',
        'article'  => 'Articulo'
    ],


    'transfer_status' => [
        'draft'    => 'Borrador',
        'transit' => 'En tránsito',
        'complete' => 'Completado',
        'cancel'=> 'Cancelado',
    ],


//    'transfer_status' => ['draft', 'transit', 'complete', 'cancel']



    'doc_references'  => [
        'order' => 'PE-',
        'purchase_order' => 'CO-',
        'transfer'  => 'TF-',
        'warehouse' => 'WH',
        'devolution' => 'DV',
        'external_movement_exit' => 'SE-',
        'external_movement_entry' => 'EE-',
    ],

    'external_agents' => [
        '1' => 'Mayoristas',
        '2' => 'Tiendas',
        '3' => 'Kioscos',
        '4' => 'Busitos',
        '5' => 'Mixtos',
        '6' => 'BTL',
        '7' => 'Cadenas',
        '8' => 'Minisuper',
		'9' => 'Solo para uso de bodega negocio'

    ],


    'role_colours' => [
        'superadmin' => '#00a9e0', // celeste
        'admin'      => '#954B97', // morado
        'editor'     => '#5bc500', // verde
        'user'       => '#e9426d', // fucsia
    ],


//    'external_agents' => [
//        'mayorista' => 'Mayoristas',
//        'tienda' => 'Tiendas',
//        'kiosco' => 'Kioscos',
//        'panel' => 'Paneles'
//    ]



];