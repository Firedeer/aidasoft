
// var btnEnd = $('#btn-end');

$('#check_weight').click( function (){
    var element = $(this);

    if( element.is(':checked') ) {
        element.val(1)
    } else {
        element.val(0)
    }
})

$('#check_volume').click( function (){
    var element = $(this);

    if( element.is(':checked') ) {
        element.val(1)
    } else {
        element.val(0)
    }
})

$('#check_length').click( function (){
    var element = $(this);

    if( element.is(':checked') ) {
        element.val(1)
    } else {
        element.val(0)
    }
})

$('#check_temperature').click( function (){
    var element = $(this);

    if( element.is(':checked') ) {
        element.val(1)
    } else {
        element.val(0)
    }
})

    /*ALMACENAR EL ARTICULO*/
   /* function new_article(){ 
        var token = $('#token').val();
        var route = '/article';
        var data = new FormData(document.getElementById("form-article"));
        var dataS;

        new Promise ( function(resolve, reject ) {
            $.ajax({
                url: route,
                headers: { 'X-CSRF-TOKEN': token },
                type: 'POST',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    $("#loaderDiv").show()
                },
                success: function(res){ 
                    return resolve(res)
                },
                error: function (err){
                    $("#loaderDiv").hide();
                    return reject(err)
                }
        }) // end first ajax
            .then( function (value) {
                $('#article').val(value)
                var data = $('#formArticleDetailList').serializeArray();
            // dataS.push({ name:'article', value: value});
            // console.log(data)
            $.ajax({
              url: route+'/bodega',
              headers: { 'X-CSRF-TOKEN': token },
              type: 'POST',
              dataType: 'json',
              data:{ data: data} ,
              async: false,
              success: function (res) {
                  console.log(res.message);
              },
              error: function(err) {
                return Promise.reject(err)
            },
            complete: function(){
                // setTimeout( function(){
                    $("#loaderDiv").hide();
                // HTTP redirect
                window.location = "complete";
                // , 3000 )
            }
            }); // end ajax
        })
        // .catch( function(error){
        //     console.log(error)
        // })

    }) // end promise


} // end function 
*/


/*Limpiar los mensajes y quitar la clase <has-error> */
function clean_help_block_create() {
    document.getElementById('barcodeHelpBlock').innerHTML = "";
    $('#barcodeHelpBlock').parent().removeClass('has-error');

    document.getElementById('nameHelpBlock').innerHTML = "";
    $('#nameHelpBlock').parent().removeClass('has-error');

    document.getElementById('classesHelpBlock').innerHTML = "";
    $('#classesHelpBlock').parent().removeClass('has-error');

    document.getElementById('brandHelpBlock').innerHTML = "";
    $('#brandHelpBlock').parent().removeClass('has-error');
}



function validate_form (element){
 var data = new FormData(document.getElementById("form-article"));
 $.ajax({
    url: route +'/validate',
    headers: { 'X-CSRF-TOKEN': token },
    type: 'POST',
    dataType: 'json',
    data: data,
    cache: false,
    contentType: false,
    processData: false,
    success: function (res) {
       clean_help_block_create();
   }, error: function(res){
       clean_help_block_create();
       $.each(res.responseJSON, function (item, message) {
        if (item == element.name){
            document.getElementById(item + 'HelpBlock').innerHTML = message;
            var padre = $('#' + item + 'HelpBlock').parent();
            padre.parent().addClass('has-error');
        }
    });
   }
})
}



                // document.getElementById("form-article").reset();
                // var snackbar  = $("#snackbar");
                // snackbar.addClass("snackbar-opened success");
                // snackbar.children('span.snackbar-content').html(res.message);
                // // Limpiar el formulario

                // // Poner el foco en el  primer campo
                // $('#barcode').focus()

                // // Tiempo que tarda el alert mostrandose
                //  setTimeout(function () {
                //     $("#snackbar").snackbar("hide")
                // }, 3000);




 // clean_help_block_create();

                // activar el evento click del boton btn-previous en el formulario
                // se mostrara el paso anterior con los campos sin completar
                // document.getElementById('btn-previous').click();

                // $.each(res.responseJSON, function (item, message) {
                //     document.getElementById(item + 'HelpBlock').innerHTML = message;
                //     var padre = $('#' + item + 'HelpBlock').parent();
                //     padre.parent().addClass('has-error');
                // });


                    // document.getElementById("msj").innerHTML = res.message;
            // $("#msj-alert > strong").append("Éxito");
            // $("#msj-alert").addClass("alert-success");
            // $("#msj-alert > icon").addClass("fa-thumbs-up");

            // clean_help_block_create();

            // // $btn.button('reset');

            // // Resetear el formulario
            // $('#form-article').each (function(){
            //     this.reset();
            // });
            // $('#form-articulos').each (function(){
            //     this.reset();
            // });

            // $('#msj-alert').fadeIn();
            // setTimeout(function () {
            //     $('#msj-alert').fadeOut(100);
            // }, 3000);