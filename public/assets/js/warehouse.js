'use strict';

$(function(d){

	var pluss = $("button[role='pluss-value']");
	var minus = $("button[role='minus-value']");



	pluss.click(function(event){
		event.preventDefault();
		var parent = event.target.closest('div');
		var control = $(parent).siblings('input');
		var value = 1;
		if( control.val() == '' ){
			
			control.val( value );

		} else {
			if( $.isNumeric( parseInt(control.val()) )){

				control.val( parseInt(control.val()) + 1);

			} 
		}
	});



	minus.click(function(event){
		event.preventDefault();
		var parent = event.target.closest('div');
		var control = $(parent).siblings('input');
		var value = 1;
		if( control.val() == '' ){
			control.val( value );
			
		} else {

			if( $.isNumeric( parseInt(control.val()) )){
				if( parseInt(control.val()) > 1 )
					control.val( parseInt(control.val()) - 1);

			} 
		}
	});


});