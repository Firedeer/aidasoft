/**
 * Required
 * demo_insert_after_node.js
 */

'use strict';

(function ($) {

    $.fn.sendToAjax = function (options) {

        var defaults = {
            method: 'GET',
            route: $(this).attr('action'),
            token: '',
            form: 'form',
            data: '',
            resetForm: true,
            success: function (response, status) {
            },
            error: function (err, status) {
            },
            complete: function (xhr) {
            }
        };

        options = $.extend(defaults, options);

        var methods = {

            send: function (data) {

                this.removeAlerts();

                $.ajax({
                    url: options.route,
                    headers: {'X-CSRF-TOKEN': options.token},
                    type: options.method,
                    data: data,
                    dataType: 'json',

                    /**
                     * BeforeSend callback
                     */
                    beforeSend: function () {


                        methods.removeAlerts();

                        document.getElementsByTagName("body")[0].classList.add("loader-ajax");
                    },

                    /**
                     * Success callback
                     */
                    success: function (response, status) {

                        methods.publishAlerts(response.alert);

                        (options.resetForm) ? options.form[0].reset() : '';

                        return options.success(response, status);
                    },

                    /**
                     * Error callback
                     */
                    error: function (xhr, status) {

                        if (xhr.status == 422 && xhr.responseJSON.errors) {

                            var clearErr = $(options.form).find('.text-danger');
                            clearErr.remove();

                            $.each(xhr.responseJSON.errors, function (item, message) {

                                var p = document.createElement('P');

                                p.appendChild(document.createTextNode(message));
                                p.setAttribute('class', 'text-danger');

                                // $(options.form).find('#' + item + '')[0]
                                var elem = document.getElementById(item);

                                if (elem) {
                                    var parent = elem.closest('.form-group');
                                    parent.classList.add('has-error');

                                    $(parent).insertAfterNode(p, elem);
                                }
                            });
                        }

                        if ( xhr.responseJSON.alert != undefined) {

                            methods.publishAlerts(xhr.responseJSON.alert);
                        }

                        return options.error(xhr, status);
                    },

                    /**
                     * Complete callback
                     */
                    complete: function (xhr) {
                        setTimeout(function () {
                            document.getElementsByTagName("body")[0].classList.remove("loader-ajax");
                        }, 100);

                        return options.complete(xhr);
                    }
                });
            },

            publishAlerts: function (alert) {

                var container = $(options.form).find('.panel .panel-heading')[0];

                if (container) {

                    container.insertAdjacentHTML('afterend', alert);

                } else {

                    var body = document.getElementsByClassName('container-wrapper')[0];
                    body.insertAdjacentHTML('afterbegin', alert);
                }
            },

            removeAlerts: function () {
                var alerts = Array.prototype.slice.call(document.querySelectorAll('.alert.flash'));

                alerts.forEach(function (item) {
                    item.remove();
                });
            }

        };

        $.fn.sendToAjax.destroy = function () {

            console.log('destruido');
            // $.fn.sendToAjax={};
            console.log($.fn.sendToAjax);
        }

        return this.each(function () {

            options.form = $(this);

            $(this).submit(function (event) {
                event.preventDefault();
                event.stopPropagation();

                options.data = $(this).serialize();

                methods.send(options.data);
            });
        });
    }

})(jQuery);






