'use stric';

(function($){

    $.fn.insertAfterNode = function (newElement, element) {

        return this.each(function () {

            if (element.nextSibling) {
                element.parentNode.insertBefore(newElement, element.nextSibling);
            } else {
                element.parentNode.appendChild(newElement);
            }
        });
    }
})(jQuery);