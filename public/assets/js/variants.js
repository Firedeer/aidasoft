/**
 * Required
 * demo_array_handle.js
 */

'use strict';

(function (d) {

    var el = "#tbl-atrributes";
    var variantsLog = [];
    var attributeValueList = [];
    var attributeList = [];
    var attributes = "";
    var selecAttribute = $('select[name="attribute[]"]');
    var selecAttributeValue = $('select[name="attribute_value[]"]');
    var combinar = $('#combinar');
    var numbreOfVariants = $('#number-variants');
    var _delete = $('#tbl-atrributes a[role="_delete"]');


    /**
     *
     *
     */
    var createVariants = function (attrArray) {

        var x = 0;
        if (variantsLog.length == 0) {

            for (var i = 0; i < attrArray.length; i++) {

                var array = [];

                array[x] = attrArray[i];

                variantsLog[i] = array;
            }

            x++;

        } else {

            var arrayTemp = variantsLog;
            variantsLog = [];

            for (var i = 0; i < arrayTemp.length; i++) {

                var newAttr = arrayTemp[i].length;

                for (var j = 0; j < attrArray.length; j++) {

                    arrayTemp[i][newAttr] = attrArray[j];

                    var str = arrayTemp[i].toString();
                    var newArray = str.split(",");
                    variantsLog.push(newArray);
                }
            }
        }

        return variantsLog;
    }


    /**
     *
     *
     */
    var filterValue = function (object, value) {

        return Object.keys(object).filter(function (key) {

            if (key == value)

                return object[key];

        }).map(function (key) {

            return object[key];
        });
    }


    /**
     *
     *
     */
    var createArray = function (attrValues) {

        variantsLog = []; // vaciar la variantes
        var array = [];
        var variants = [];

        for (var i = 0; i < attrValues.length; i++) {

            array = [];

            if (attrValues[i] != null) {

                for (var j = 0; j < attrValues[i].length; j++) {

                    array[j] = attrValues[i][j];
                }
            }

            variants = createVariants(array);
        }

        return variants;
    }


    /**
     * Shows automatically generated variants
     *
     */
    var showVariants = function (array, element, n = 0) {

        var desc = $('form[role="edit"] input[name="name"]').val();
        var barcode = $('form[role="edit"] input[name="barcode"]').val();

        for (var i = 0; i < array.length; i++) {

            var tr = document.createElement('TR');

            for (var j = 0; j < 7; j++) {

                var td = document.createElement('TD');

                switch (j) {
                    case 0:
                        td.width = 40;
                        td.innerHTML = '<img src="assets/img/trade.jpg" width="45px" class="media-object img-rounded">';
                        break;

                    case 1:
                        var attr = '';
                        for (var k = 0; k < array[i].length; k++) {

                            var value = filterValue(attributeValueList, array[i][k]);

                            if (value.length) {
                                attr += ' ' + value;
                            }
                        }

                        td.classList.add('hidden-xs');
                        td.innerHTML = '<strong>' + (desc + attr) + ' <small>(Nuevo)</small></strong><input type="hidden" name="variant[' + ( i + n ) + '][description]" value="' + (desc + attr) + '"><input type="hidden" name="variant[' + (i + n) + '][id]" value="">';
                        break;

                    case 2:
                        td.classList.add('hidden-xs');
                        td.innerHTML = '<span class="serial-code">' + barcode + '</span>';
                        break;

                    case 3:
                        var html = '';
                        for (var k = 0; k < array[i].length; k++) {

                            var value = filterValue(attributeValueList, array[i][k]);

                            if (value.length) {
                                html += '<span class="chip chip-sm">' + value + '</span><input type="hidden" name="variant[' + ( i + n ) + '][value][]" value="' + array[i][k] + '" class="attr-value">';
                            }
                            else {
                                html += '<input type="hidden" name="variant[' + ( i + n ) + '][value][]" value="' + array[i][k] + '" class="attr-value">';
                            }

                        }
                        td.innerHTML = html;
                        break;

                    case 4:
                        td.width = 40;
                        td.innerHTML = '<div class="togglebutton"><label><input type="checkbox" name="variant[' + ( i + n ) + '][active]" checked="checked"><span class="toggle"></span></label></div>';
                        break;

                    case 5:
                        td.innerHTML = '<a href="article/69/edit" class="btn btn-info btn-raised btn-xs" role="edit-variant"><i class="material-icons">mode_edit</i><span class="hidden-xs">Editar</span></a>';
                        break;
                }

                tr.appendChild(td);
            }

            element.append(tr);
        }
    }


    /**
     * Returns an array with existing variants
     *
     */
    function existingVariants(array, filterElement) {

        var array2 = [];

        array.forEach(function (item) {

            var elements = $(item).find(filterElement);
            var array = [];

            elements.each(function (key, item) {
                array.push(this.value);
            });

            array2.push(array);
        });

        return array2;
    }


    /**
     *
     *
     */
    var loadAttributes = function (attributesArray = []) {

        var newVariants = createArray(attributesArray);
        newVariants.sort(sortAsc);

        var tblVariants = $('#show-variants');

        var initIn = 0;

        var form = $('form[role="edit"] table tbody tr');
        form = Array.prototype.slice.apply(form);

        if (form.length != 0) {

            var nose = [];

            form.forEach(function (item) {

                var str = $(item).find('input.attr-value').attr('name');

                if (str) {
                    var res = str.split("[");
                    var res2 = res[1].split("]");
                    nose.push(res2[0]);
                }
            });

            if (nose.length) {
                nose.sort(sortDesc);
                initIn = parseInt(nose[0]) + 1;
            }
        }


        var variants = [];

        if ($('#replace').is(':checked')) {

            initIn = 0;
            tblVariants.empty();
            variants = newVariants;
        } else {

            var exist = existingVariants(form, 'input.attr-value');
            exist.sort(sortAsc);
            variants = multipleArrayFilter(exist, newVariants);
        }

        showVariants(variants, tblVariants, initIn);
        numbreOfVariants.text(variants.length);


        btnGroupHandleAttributes();

    }


    /**
     *
     *
     */
    var getValues = function () {

        $.get('/attribute/values/lists', function (response) {
            attributeValueList = response;
        });
    }


    /**
     *
     *
     */
    var getAttributes = function () {

        $.get('/attribute/all/lists', function (response) {

            Object.keys(response).forEach(function (key) {
                attributes += '<option value="' + key + '">' + response[key] + '</option>';
            });
        });
    }


    /**
     *
     *
     */
    $(d).delegate('select[name="attributes[]"]', 'change', function (event) {

        var row = event.target.closest('tr');
        var select = $(row).find('select[name="attribute_value[]"]')[0];

        $(select).empty();

        if (event.target.value) {
            $.get('/attribute/' + event.target.value + '/value', function (response, status) {

                response.forEach(function (element, index) {

                    $(select).append('<option value="' + element.id + '">' + element.description + '</option>');
                });
            });
        }
    });


    /**
     *
     *
     */
    $(d).delegate('#add_atribute', 'click', function (event) {
        event.preventDefault();

        var tbody = $(el + " tbody").append(
            '<tr>' +
            '<td colspan="2">' +
            '<div class="row">' +
            '<div class="form-inline">' +
            '<div class="form-group col-xs-6 col-sm-4" style="margin:0px; padding-right:3px;">' +
            '<select name="attributes[]" class="form-control">' + attributes + '</select>' +
            '</div>' +
            '<div class="form-group col-xs-12 col-sm-8" style="margin:0px; padding-right:3px;">' +
            '<select name="attribute_value[]" class="form-control" multiple="multiple" onfocus="blur();"></select>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</td>' +
            '<td style="padding-left:8px; padding-right:8px;">' +
            '<div class="form-group" style="margin:0px;">' +
            '<a href="#" class="btn btn-raised btn-fab btn-fab-sm mdb" role="_delete"><i class="material-icons">clear</i></a>' +
            '</div>' +
            '</td>' +
            '</tr>');

        var tr = $(tbody).find('tr').last();
        var attribute = $(tr).find('select[name="attributes[]"]');
        var attribute_value = $(tr).find('select[name="attribute_value[]"]');


        $(attribute).select2({
            tags: true,
            minimumResultsForSearch: Infinity,
            placeholder: 'Selecciona un atributo de la lista',
        }).val(1).trigger('change');


        $(attribute_value).select2({
            tags: true,
            multiple: "multiple",
            minimumResultsForSearch: -1,
            placeholder: "Selecciona los valores para este atributo",
        });
    });


    /**
     *
     *
     */
    var verifyCheck = function (checkbox) {

        if ($(checkbox).is(':checked')) {
            return true;
        }
        return false;
    }


    $("input#check-variants").click(function (event) {

        verifyCheck(this) ? $("#btn-show-variants").show() : $("#btn-show-variants").hide();
        loadAttributes();
    });


    $(d).delegate(el, "change", function (event) {
        event.preventDefault();
        loadAttributes();
    });


    $(el).delegate("a[role='_delete']", "click", function (event) {
        event.preventDefault();
        var row = event.target.closest('tr');

        $(row).fadeOut("normal", function () {
            $(this).remove();
            loadAttributes();
        });
    });


    // init
    // $(".select2").select2({
    // 	language: "es"
    // });

    getValues();
    getAttributes();


    $("#check-variants").change(function (event) {
        event.preventDefault();

    });


    function autoVariants() {
        var group = $('#variants .form-group');
        var attributesArray = [];

        $(group).each(function (index) {

            var variants = $(this).find('input[name="attributeValue[]"]');
            var array = [];

            $(variants).each(function (index) {
                if ($(this).is(':checked')) {

                    array.push($(this).val());
                }
            });

            if (array.length != 0)
                attributesArray.push(array);
        });

        $('form[role="edit"]').addClass('editing');

        loadAttributes(attributesArray);
    }


    $('#btn-handle-attributes').click(function (event) {
        event.preventDefault();

        var modal = $('#handle-attributes');

        modal.modal({
            backdrop: 'static',
            keyboard: false
        });


        $('#btnAutoVariants').click(function (event) {

            event.preventDefault();
            var checked = Array.prototype.slice.call(document.querySelectorAll('#handle-attributes input.attributeValue:checked'));

            if (checked.length) {
                autoVariants();
                modal.modal('hide');
            }
        });
    });


    $(document).delegate('form[role="edit"] input[type="checkbox"]', 'change', function (event) {

        event.preventDefault();

        btnGroupHandleAttributes();

    });
})(document);


function btnGroupHandleAttributes() {

    var form = $('form[role="edit"]');


    if (!document.getElementById('save-attributes')) {

        var btn_group = document.createElement('DIV');

        btn_group.classList.add('btn-group', 'btn-group-sm', 'pull-right');
        btn_group.setAttribute('id', 'save-attributes');

        var btn_cancel = document.createElement('BUTTON');
        btn_cancel.classList.add('btn', 'mdb');
        btn_cancel.appendChild(document.createTextNode('Cancelar'));
        btn_cancel.setAttribute('type', 'button');
        btn_cancel.setAttribute('role', 'cancel');


        var btn_save = document.createElement('BUTTON');
        btn_save.classList.add('btn', 'mdb', 'btn-primary', 'btn-raised');
        btn_save.appendChild(document.createTextNode('Guardar Cambios'));
        btn_save.setAttribute('role', 'save');

        btn_group.appendChild(btn_cancel);
        btn_group.appendChild(btn_save);

        form.append(btn_group);

        $(document).delegate('form[role="edit"] button[role="cancel"]', 'click', function (event) {
            event.preventDefault();
            event.stopPropagation();

            location.reload();
        });

        $(document).delegate('form[role="edit"] button[role="save"]', 'click', function (event) {
            event.preventDefault();
            event.stopPropagation();

            $('form[role="edit"]').submit();
        });
    }
}



