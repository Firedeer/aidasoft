// resibe como parametro el ID del elemnto HTML
function bodegas(id){
    var datos = $('#'+id+'');
    var route = 'bodegas';
    $.get( route, function(res){
        $(res).each(function(key, value){
            datos.append('<option value='+value.id+'>'+ value.bodega +'</option>');
        });
        // $('select').selectpicker('refresh'); 
        // disabled_selected ()
    });
}   

function brands(){
    var datos = $('#brand');
    var route = 'brands';
    
    $.get( route, function(res){
        $(res).each(function(key, value){
            datos.append('<option value='+value.id+'>'+ value.brand+'</option>');
        });
    });
}

function classes(){
    var datos = $('#class');
    var route = 'classes';
    
    $.get( route, function(res){
        $(res).each(function(key, value){
            datos.append('<option value='+value.id+'>'+ value.class+'</option>');
        });
    });
}

function classes(){
    var datos = $('#class');
    var route = 'classes';
    
    $.get( route, function(res){
        $(res).each(function(key, value){
            datos.append('<option value='+value.id+'>'+ value.class+'</option>');
        });
    });
}

function brandsToProviders( provider ){
    var item = $( provider ); 

    $.get( 'brand/provider', function(res){
        $(res).each(function(key, value){
            item.append('<option value='+value.provider_id+'>'+value.provider+'</option>');
        });
        item.val('')
         // $('select').selectpicker('refresh'); 
     });
}


// Select dinamicos 
$('select[id="provider"]').change(function(event){
console.log(this)
    event.preventDefault();
    var item  = $('#brand');
    var route = "brands/provider/"+event.target.value+"";

    if( event.target.value == 'any' ){
        item.empty();
        item.attr('disabled', 'true');
    } else {    
        item.empty();
        item.removeAttr("disabled");
        item.focus();

        select_data(route, item)
    }
})

// Select dinamicos 
$('#category').change(function(event){
    event.preventDefault();
    var item  = $('#class');
    var route = "classes/category/"+event.target.value+"";

    if( event.target.value == 'any' ){
        item.empty();
        item.attr('disabled', 'true');
    } else {    
        item.empty();
        item.removeAttr("disabled");
        item.focus();

        select_data(route, item)
    }
})

function select_data(route, item ){

    $.get( route, function(res){
        $(res).each(function(key, value){
            item.append('<option value='+value.id+'>'+ value.description +'</option>');
        });
         // $('select').selectpicker('refresh'); 
     });
}

// JavaScript ECMASCRIPT 6 
// $(`#provider`).change(event => {
//     var data = $(`#brand`);
//     var route = `/brands/provider/`+event.target.value+``;

//     $.get(route, function(res, status){
//         data.empty();
//         res.forEach(element => {
//             data.append(`<option value= ${element.id} > ${element.brand} </option>`)
//         })
//     })
// })


function select_movements(){
    var item = $('select[name="movement"]');
    var route = "movement/";
    var entry = "<optgroup label='Entrada'>";
    var exit = "</optgroup><optgroup label='Salida'>";

    $.get( route, function(res){
        $(res).each(function(key, value){
            if( value.movement === 'Entrada'){
                entry += '<option value='+value.type+' data-subtext='+value.code+'>'+ value.description +'</option>';


            } else if ( value.movement === 'Salida'){
                exit += '<option value='+value.type+' data-subtext='+value.code+'>'+ value.description +'</option>';
            }

        });

        var n  =  entry + exit + "</optgroup>";
        item.append(n)
                // console.log(n)
                // $(item).selectpicker('refresh')
            });
}

function select_bodegas( item ){
    var item = $('select[name="bodega"]');
    var route = "bodegas/user/_";

    $.get( route, function(res){
        $(res).each(function(key, value){
            item.append('<option value='+value.id+'>'+ value.description +'</option>')
        });
        item.val('')
            // $(item).selectpicker('refresh')
        });
}

function select_articles( $id ){
    var item = $('select[id='+$id+']');
    var route = "articles";

    $.get( route, function(res){
        $(res).each(function(key, value){
            item.append('<option value='+value.id+' data-subtext='+value.name+'>'+value.name+'</option>')
        });
            // $(item).selectpicker('refresh',{
            //  liveSearchPlaceholder: 'true'
            // })
        });
}


    function show_user(){
        var item = $('#user');
        var route = "users";

        $.get( route, function(res){
            $(res).each( function(key, value){
                item.append('<option value='+value.id+'>'+value.name+'</option>')
            });

            // $(item).selectpicker('refresh')
        }); 
    }