function crear_grafico(a) {
    var dato = JSON.parse("[" + a[0] + "]");
console.log(dato)
    var data = {
        labels: ["Enero", "febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        datasets: dato // json con la data 
    }; // data

    var options = {
        // Boolean - whether or not the chart should be responsive and resize when the browser does.
        responsive: true,

    };

    var ctx = document.getElementById('myChart').getContext("2d");
    var myChart = new Chart(ctx).Line(data, options);
    document.getElementById('js-legend').innerHTML = myChart.generateLegend();
} // fin de crear grafico


function traer_data() {
    //    var datos = $('#brand');
    var route = 'annual_inventory';
    //    var res;
    var a = [];
    var _json = '';
    $.get(route, function (res) {
        $(res).each(function (key, value) {

            _json += '{ "label": "' + value.description + '",' +
                '"fillColor": "rgba(' + ConvertToRGB(value.colour) + ',0.2)",' +
                '"strokeColor": "rgba(' + ConvertToRGB(value.colour) + ',1)",' +
                '"pointColor": "rgba(' + ConvertToRGB(value.colour) + ',1)", ' +
                '"pointStrokeColor": "#fff", ' +
                '"pointHighlightFill": "#fff", ' +
                '"pointHighlightStroke": "rgba(' + ConvertToRGB(value.colour) + ',1)",' +

                ' "data": [ "' + value.january + '","' + value.february + '","' + value.march + '","' + value.april + '","' + value.may + '","' + value.june + '","' + value.july + '","' + value.august + '","' + value.september + '","' + value.october + '","' + value.november + '","' + value.december + '"]}';

            if (key < (res.length - 1)) {
                _json += ',';

            }

        });

        a[0] = _json
console.log(a)
        crear_grafico(a);

    });
}