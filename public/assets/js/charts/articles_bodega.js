function chart_article_bodega(a, b, bodega, colour) {
    var jsonLabels = JSON.parse("[" + a[0] + "]");
    var jsonData = JSON.parse("[" + b[0] + "]");

    // DATA
    var data = {
        labels: jsonLabels,
        datasets: [
            {
                label: bodega,
                backgroundColor: "rgba("+colour+",0.3)",
                borderColor: "rgba("+colour+",1)",
                data: jsonData
            }
        ]
    };

    // OPTIONS
    var options = {
        responsive: true,
        elements: {
            rectangle: {
                borderWidth: 1
            }
        },
        animation: {
            onComplete: function () {
                var chartInstance = this.chart;
                var ctx = chartInstance.ctx;
                ctx.textAlign = "center";

                Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                    var meta = chartInstance.controller.getDatasetMeta(i);
                    Chart.helpers.each(meta.data.forEach(function (bar, index) {
                        ctx.fillText(dataset.data[index], bar._model.x, bar._model.y - 10);
                    }), this)
                }), this);
            }
        }
    }

    var ctx = document.getElementById('myChart').getContext("2d");
    window.myHorizontalBar = new Chart(ctx, {
        type: 'horizontalBar',
        data: data,
        options: options
    });
}