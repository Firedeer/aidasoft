var app=new Vue({

    el:'#app',
    data:{ 
        message:sessionStorage.getItem("referencia_item"),
        helados: [],
      
    },
    methods:{
        calcula(valor)
        {
     
           return valor;
        },
        created: function(){
            var that = this;
            axios.get('https://raw.githubusercontent.com/fernandoggaitan/vuejs_axios/master/response.json')
            .then(function (response) {
               that.helados = response.data;
            })
            .catch(function (error) {
               console.log('Error: ' + error);
            }); 
         },
        formSubmit(e) {
            e.preventDefault();
            let currentObj = this;
            axios.post('/article/referencia/AERO22', {
                name: this.name,
                description: this.description
            })
            .then(function (response) {
                currentObj.output = response.data;
            })
            .catch(function (error) {
                currentObj.output = error;
            });
        }
    }
});
