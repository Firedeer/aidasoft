function findById(items, id) {

    for (var i in items) {
        if (items[i].id == id) {
            return items[i];
        }
    }

    return null;
}



Vue.filter('_article', function (id) {
    // if (vm) {
    //     var article = findById(vm.articleList, id);
    //     return article != null ? article.description : '';
    // }

    var article = findById(this.$parent.article_list, id);

    return article != null ? article.description : '';
});




Vue.component('select-article', {
    template: "#select_article",
    props: ['article_list', 'id', 'errors']
});



Vue.component('quantity-article', {
   template: "#quantity_article",
    props: ['quantity', 'errors']
});



Vue.component('article-row', {
    template: "#article_row",
    props: ['article', 'article_list'],
    data: function () {
        return {
            editing: false,
            errors: [],
            draft: []
        }
    },
    methods: {
        edit: function () {

            this.errors = [];

            // Clone the article object in a draft object
            this.draft = JSON.parse(JSON.stringify(this.article));

            // Vue.set(article, 'editing', true);
            this.editing = true;
        },

        update: function () {

            this.errors = [];

            $.ajax({
                url: '/transfer/article/validate',
                type: 'GET',
                dataType: 'json',
                data: this.draft,
                success: function (response) {

                    if (response.valid == true) {

                        this.$parent.articles.$set(this.$parent.articles.indexOf(this.article), response.article);

                        // article.editing = false;
                        this.editing = false;
                    }
                }.bind(this),
                error: function (jqXHR) {
                    this.errors = jqXHR.responseJSON;
                }.bind(this)

            });
        },

        cancel: function(){
            this.editing = false;
        },

        remove: function () {
            // var index = this.articles.indexOf(this.article);
            //
            // this.$parent.articles.splice(index, 1);

            this.$parent.articles.$remove(this.article); // deprecated in Vue v2
        }
    }
});


var vm = new Vue({
    el: '#element',

    data: {
        articles: [],

        article_list: [],

        errors: []
    },

    filters: {},

    methods: {
        addArticle: function () {

            this.errors = [];

            $.ajax({
                url: '/transfer/article/validate',
                type: 'GET',
                dataType: 'json',
                data: {
                    article_id: vm.add_article.article_id,
                    quantity: vm.add_article.quantity
                },
                success: function (response) {
                    if (response.valid == true) {

                        vm.articles.push(vm.add_article);

                        vm.add_article = {article_id: '', quantity: ''};
                    }
                },
                error: function (jqXHR) {
                    vm.errors = jqXHR.responseJSON;
                }

            });
        },

    },

    created: function () {
        $.getJSON('/articles/all', function (data) {
            vm.article_list = data;
        });


        $.ajax({
            url: '/transfer/1',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                vm.articles = data;
            },
            error: function (jqXHR) {

                console.log(jqXHR.responseJSON);
                vm.errors = jqXHR.responseJSON;
            }

        });
    }
});

