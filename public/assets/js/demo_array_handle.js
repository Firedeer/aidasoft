Array.prototype.unique = function (a) {
    return function () {
        return this.filter(a)
    }
}(function (a, b, c) {
    return c.indexOf(a, b + 1) < 0
});


function compareTwoArray(array1, array2) {

    var same = true;

    for (var i = 0; i < array1.length; i++) {

        if (!array1[i].includes(array2[i])) {
            same = false;
            break;
        }
    }

    return same;
}



function multipleArrayFilter(array1, array2) {

    for (var x = 0; x < array1.length; x++) {

        for (var y = 0; y < array2.length; y++) {

            if (array2[y].length == array1[x].length) {

                // if (array1[x].toString() == array2[y].toString()) {
                //     console.log(true)
                // }
                if (compareTwoArray(array1[x], array2[y])) {

                    var index = array2.indexOf(array2[y]);
                    array2.splice(index, 1);
                }
            }
        }
    }

    return array2;
}




/**
 * function arraySortAsc(array) {
 *   return array.sort(function (a, b) {
 *       return a - b
 *   });
 * }
 */
function sortAsc(a, b){
    return a - b
}

function sortDesc(a, b){
    return b - a
}