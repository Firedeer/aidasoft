'use strict';

(function ($) {

    $.fn.imageUpload = function (options) {

        var spin = $("#spin.load-rotate");
        var modal = $("#cropImage");
        var base64 = $("textarea[name='image']");
        var btnReset = $("#btn-reset");
        var imgPreview = $(".__img-preview");
        var imgDefault = '<i class="material-icons">photo_size_select_actual</i>';
        var uploadCrop = $("#upload-demo");
        var uploadContainer = $('label[for="upload"]');
        var imageName = $('#image_name');

        var methods = {

            init: function () {

                uploadCrop.croppie({
                    enableExif: true,
                    viewport: {
                        width: 250,
                        height: 250
                    },
                    boundary: {
                        width: 300,
                        height: 300
                    },
                    enableOrientation: true
                });
            },

            modal: function () {

                modal.modal({
                    'backdrop': 'static',
                    'show': true
                });

                modal.on('shown.bs.modal', function () {
                    uploadCrop.croppie('bind');
                });
            },

            rotate: function (input) {
                uploadCrop.croppie('rotate', parseInt($(input).data('deg')));
            },

            renderImage: function (input) {

                var reader = new FileReader();

                reader.onload = function (e) {
                    uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        // console.log('jQuery bind complete');
                    });
                }

                reader.readAsDataURL(input.files[0]);
                this.modal();

            },

            checkFileType: function (input) {
                var value = $(input).val();
                var file = value.toLowerCase();
                var extension = file.substring(file.lastIndexOf('.') + 1);
                return ($.inArray(extension, options.allowedExtensions) == -1) ? false : true;
            },

            reset: function () {
                options.fileUpload[0].value = '';
                imgPreview.find('img').remove();
                imgPreview.html(imgDefault);
                $(imageName).val('');

                $(imgPreview).parent().hide();
                $(uploadContainer).show();
            }
        };


        var defaults = {
            allowedExtensions: ['jpg', 'jpeg', 'png'],
            fileUpload: '',
            success: function () {
            },
            error: function () {
                console.log('Error: el fichero seleccionado no es correcto.');
            }
        };


        options = $.extend(defaults, options);

        return this.each(function () {

            options.fileUpload = $(this);

            options.fileUpload.on('change', function (event) {

                var arrayPath = this.value.split('\\');

                $(imageName).val( arrayPath[arrayPath.length - 1] );

                if (methods.checkFileType(this)) {
                    methods.renderImage(this);
                } else {
                    options.error();
                    $(this).focus();
                }
            });


            $('.rotate').on('click', function (event) {

                event.preventDefault();
                methods.rotate(this);
            });

            $('#cancel-upload').on('click', function () {
                methods.reset();
            });

            $('#upload-result').on('click', function (event) {

                event.preventDefault();

                uploadCrop.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (resp) {

                    spin.css('display', 'block');

                    setTimeout(function () {

                        imgPreview.html('<span class="img-remove" id="btn-reset"></span><img src="' + resp + '" style="width:100%;" />');
                        $(imgPreview).parent().show();
                        $(uploadContainer).hide();


                        base64.val(resp);
                        modal.modal('hide');
                        spin.css('display', 'none');
                    }, 2000);

                });
            });


            $(".__image_container-preview").on('click', btnReset, function (event) {
                methods.reset();
            });


            // imgPreview.html(imgDefault);
            methods.init();
        });
    };

})(jQuery);


$(function () {
    $('#upload').imageUpload();
});