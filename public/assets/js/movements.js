function show_movement_entry(){
	var item = $('#movement_e>tbody');
	var route = "movement/article/show";

	$.get( route, function(res){
		$(res).each(function(key, value){
			item.append(
				'<tr>'+
				'<td>'+value.date+'</td>'+
				'<td class="text-uppercase"><strong>'+value.barcode+'</strong></td>'+
				'<td><a href="article/'+value.article_id+'">'+value.article+'</a></td>'+
				'<td>'+value.quantity+'</td>'+
				'<td>'+value.unit_cost+'</td>'+
				'<td>'+value.tax+'</td>'+
				'<td><a href="bodega/'+value.bodega_id+'">'+value.bodega+'</a></td>'+
				'<td><a href="movement/'+value.type_movement_id+'">'+value.type_movement+'</a></td>'+
				'<td>'+value.movement+'</td>'+
				'</tr>'
				)
		});
		dataTableConfig ('#movement_e')
	}); 
}

function show_movements( id ){
	var item = $('#movements>tbody');
	var route = "movement/article/show/";


	$.get( route+id, function(res){

		$(res).each(function(key, value){

			if ( value.movement === 'Entrada'){

				entryValue = '';
				entryValue += '<tr>'+
				'<td>'+value.date+'</td>'+
				'<td><a href="movement/'+value.type_movement_id+'">'+value.type_movement+'<input type="hidden" name="initial[]" value="'+value.initial+'"></a></td>'+
				'<td><a href="bodega/'+value.bodega_id+'">'+value.bodega+'</a></td>'+
				'<td class="success" name="entry">'+value.quantity+'</td>'+
				'<td class="warning" name=""></td>'+
				'<td></td>';
				if ( $.isNumeric(value.unit_cost) ){

					entryValue += '<td>'+value.unit_cost+'</td>';
				} else entryValue += '<td>0.00</td>';
				if ( $.isNumeric(value.tax) ){

					entryValue += '<td>'+value.tax+'</td>';
				} else entryValue += '<td>0.00</td>';

				entryValue += '<td class="info"></td></tr>';

				item.append( entryValue )

			} else if ( value.movement === 'Salida' ){
				exitValue  = '';
				exitValue  += 
				'<tr>'+
				'<td>'+value.date+'</td>'+
				'<td><a href="movement/'+value.type_movement_id+'">'+value.type_movement+'<input type="hidden" name="initial[]" value="'+value.initial+'"></a></td>'+
				'<td><a href="bodega/'+value.bodega_id+'">'+value.bodega+'</a></td>'+
				'<td class="success" name=""></td>'+
				'<td class="warning" name="exit">'+value.quantity+'</td>'+
				'<td></td>';

				if ( $.isNumeric(value.unit_cost) ){
					exitValue  +='<td>'+value.unit_cost+'</td>';
				} else  exitValue  += '<td>0.00</td>';

				if ( $.isNumeric(value.unit_cost) ){
					exitValue  += '<td>'+value.unit_cost+'</td>';
				} else  exitValue  += '<td>0.00</td>';

				exitValue  += '<td class="info"></td></tr>';

				item.append( exitValue );																																
			}

		});

		var table          = document.getElementById('movements'),
		rows           = table.getElementsByTagName('tr'),
		total 	       = rows[rows.length - 1].getElementsByTagName('th'),
		entryColumn    = 3,
		exitColumn     = 4,
		stockColumn    = 5,
		costColumn     = 6,
		taxColumn  	   = 7,
		// discountColumn = 8, 
		amountColumn   = 8,
		amount         = 0.00,
		totalEntry     = 0.00,
		totalExit      = 0.00,
		totalStocks    = 0,
		totalAmount    = 0.00;


		for (i = 1; i<rows.length; i++){

			var column  = rows[i].getElementsByTagName('td');

			if ( $.isNumeric( column[entryColumn].innerHTML ) ){

				entry = column[ entryColumn ].innerHTML;
				

				if( $("input[name='initial[]']")[i-1].value  == 1) {
					totalEntry = parseInt(entry);
					totalStocks = parseInt(entry)
					
				} else {
					
					totalEntry += parseInt(entry);
					totalStocks += parseInt(entry) ;

				} 
				


				column[ stockColumn ].innerHTML = totalStocks;

				var tax = parseFloat(column[ taxColumn ].innerHTML );

				amount = ( tax  !== 0 ) ? parseFloat( ( entry * parseFloat( column[ costColumn ].innerHTML)) + parseFloat( ( entry * parseFloat( column[ costColumn ].innerHTML)) * tax ) ) : parseFloat( entry * parseFloat( column[ costColumn ].innerHTML) );
				// if( tax  !== 0 ){
				// 	var totalTax = parseFloat( ( entry * parseFloat( column[ costColumn ].innerHTML)) * tax )
				// 	amount  =  parseFloat( ( entry * parseFloat( column[ costColumn ].innerHTML)) + totalTax );	
				// } else {
				// 	amount  = parseFloat( entry * parseFloat( column[ costColumn ].innerHTML) );	
				// }

				// amount -= parseFloat( column[ discountColumn ].innerHTML )

				totalAmount += amount;

				column[ amountColumn ].innerHTML = amount.toFixed(2);

			} else if ( $.isNumeric( column[exitColumn].innerHTML ) ) {

				exit = column[ exitColumn ].innerHTML;
				totalExit += parseInt(exit);

				totalStocks -= parseInt(exit);

				column[ stockColumn ].innerHTML = totalStocks;
			} 
			

			total[1].innerHTML = totalEntry; 
			total[2].innerHTML = totalExit;  
			total[3].innerHTML = totalStocks; 
			total[6].innerHTML = totalAmount.toFixed(2);
		}


	});
}



