'use strict';

(function ($) {


    $.fn.textAreaHandler = function (options) {

        var textArea = this;
        var counterElement;
        var container;

        var defaults = {
            max: 500
        };

        var methods = {
            count: function (count) {

                if (count < 0) {
                    console.log(container);
                    container[0].classList.add('has-error');
                }
                return count;
            }

        };

        options = $.extend(defaults, options);

        return this.each(function () {

            textArea[0].setAttribute('min', 0);
            textArea[0].setAttribute('max', options.max);
            textArea[0].insertAdjacentHTML('afterend', '<span id="text-counter" class="text-muted">' + options.max + '</span>');
            counterElement = document.getElementById('text-counter');
            container = textArea.closest('.form-group');

            counterElement.textContent = methods.count(options.max - textArea[0].value.length);


            $(document).delegate(counterElement, 'keyup', function (event) {

                counterElement.textContent = methods.count(options.max - textArea[0].value.length);
            });
        });
    }
})(jQuery);