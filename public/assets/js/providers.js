(function(d){

	var provider = d.querySelector('.deleteProvider');

	$(d).on('click', '.deleteProvider', function(event){
		event.preventDefault();
		var route = this.getAttribute("href").split('/provider')[1];
		var element = this.parentNode.parentNode.parentNode.parentNode.parentNode;
		deleteProvider('/provider'+route, element);

	});

	function deleteProvider(route, elem){
		
		var token = Array.prototype.slice.apply( d.getElementsByName('_token') )[0].value;
		console.log(route)
		
		$.ajax({
			url: route,
			headers: {'X-CSRF-TOKEN': token },
			type: 'DELETE',
			dataType: 'json',
			success: function(res){
				console.log( res );
				console.log(elem);
				elem.remove(elem);
				// searchProviders();
			},
			error: function(res){
				console.log(res );
			}
		});
	}
})(document);