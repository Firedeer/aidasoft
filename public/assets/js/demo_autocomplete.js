
	$(document).ready(function(){

		
		var options = {
			// data: ['Color', 'Talla', 'Tamano', 'Sexo']
			url: "autocomplete/attribute",

			getValue: "description",

			template: {
				type: "description",
				fields: {
					description: "description"
				}
			},


			list: {
				match: {
					enabled: true
				}
			},

			delay: 10,

			ajaxSettings: {
				dataType: "json",
				method: "GET",
				data:{},
				// header: {'X-CSRF-TOKEN': $("#_token").val() },

			},


			preparePostData: function(data) {

				data.term = $("#attribute").val();
				return data;
			},	
	};

	$("#attribute").easyAutocomplete(options);
});
	