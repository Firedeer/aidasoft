function ConvertToRGB( c ) {
    var can  = document.createElement( 'canvas' ),
        ctx  = can.getContext( '2d' );
    can.width = can.height = 1;
    ctx.fillStyle = c;
//    console.log( ctx.fillStyle ); //always css 6 digit hex color string, e.g. '#ffffff'
    ctx.fillRect( 0, 0, 1, 1 ); //paint the canvas
    var img  = ctx.getImageData( 0, 0, 1, 1 ),
        data = img.data,
        rgba = {
            r: data[ 0 ], //0-255 red
            g: data[ 1 ], //0-255 green
            b: data[ 2 ], //0-255 blue
            a: data[ 3 ]  //0-255 opacity (0 being transparent, 255 being opaque)
        };
       
    return data[0]+','+data[1]+','+data[2];
}


function convertRgbToHex(rgb) {
  rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
  return  (rgb && rgb.length === 4) ? "#" +
          ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
          ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
          ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
}