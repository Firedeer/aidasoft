<?php

use Illuminate\Database\Seeder;

class BodegaTableSeeder extends Seeder {


	public function run(){

		$bodegas = [
		'Bodega Bocas del Toro',
		'Bodega Cocle',
		'Bodega Colon',
		'Bodega Chiriqui',
		'Bodega Darien',
		'Bodega Herrera',
		'Bodega Los Santos',
		'Bodega Panama',
		'Bodega Santiago',
		'Bodega Panama Oeste'
		];


		$colours = [
		'#5e4934',
		'#495e34',
		'#3f345e',
		'#5e3449',
		'#5e5e34',
		'#5e3434',
		'#34495e',
		'#222f3d',
		'#345e53',
		'#7795b4'
		];


		$sucursals = [
		10, 
		55, 
		15, 
		11, 
		48, 
		12, 
		45, 
		4, 
		57, 
		43
		];

		foreach ($bodegas as $key => $value) {
			\DB::table('tmk_bodegas')->insertGetId([
				'description' => $bodegas[$key],
				'colour' 	  => $colours[$key],
				'sucursal_id' => $sucursals[$key],
				'created_at'  => date('Y-m-d H:i:s', time()),
				'updated_at'  => date('Y-m-d H:i:s', time()),
                'created_by'        => 1,
                'updated_by'      	=> 1,

				]);

		}
	}

}