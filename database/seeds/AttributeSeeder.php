<?php

use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



    	$attributes = ['Color', 'Talla', 'Sexo'];
    	$attributeValues = [
    	['Blanco', 'Amarillo', 'Verde', 'Azul', 'Fucsia', 'Negro'],
    	['Small', 'Medium', 'Large', 'xLarge'],
    	['Hombre', 'Mujer'],
    	];


    	for ($x=0; $x < count($attributes); $x++) { 

    		$attr_id = \DB::table('tmk_attributes')->insertGetId([
    			'description' => $attributes[$x],
    			'created_at'  => date('Y-m-d H:i:s', time()),
    			'updated_at'  => date('Y-m-d H:i:s', time()),
                'created_by'        => 1,
                'updated_by'      	=> 1,
    			]);

    		for ($y=0; $y < count($attributeValues[$x]) ; $y++) { 
    			# code...
    			\DB::table('tmk_attribute_values')->insert([
    				'attribute_id' => $attr_id,
    				'description'  => $attributeValues[$x][$y],
    				'value'  	   => $attributeValues[$x][$y],
    				'created_at'   => date('Y-m-d H:i:s', time()),
    				'updated_at'   => date('Y-m-d H:i:s', time()),
                    'created_by'        => 1,
                    'updated_by'      	=> 1,
    				]);
    		}
    	}


    }
}
