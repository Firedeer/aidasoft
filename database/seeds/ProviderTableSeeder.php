<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProviderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// DB::table('tmk_type_providers')->truncate();

    	$faker = Faker::create();
    	$provider = ['Bienes', 'Servicios', 'Recursos'];
        $type =  Config('enums.provider_types');

    	foreach ( $provider as $value) {
    		DB::table('tmk_type_providers')->insert(array (
    			'description' => $value,
    			'created_at'  => $faker->dateTimeThisYear($max = 'now'),
	            'updated_at'  => $faker->dateTimeThisYear($max = 'now'),
    		));
    	}
    	// DB::table('tmk_providers')->truncate();

	    for($i = 0; $i<10; $i++){

	       	$name =  $faker->firstName;

	        DB::table('tmk_providers')->insert(array (
	            'firstname'         => $name,
	            'lastname'        	=> $faker->lastName, 
                'company'           => $faker->company,
	            'email'           	=> $name.'@'.$faker->freeEmailDomain,
	            'address'         	=> $faker->address,  
	            'phone'           	=> $faker->phoneNumber,                          
	            // 'type_provider_id'  => rand(1, count($provider)),
                // 'type'              => $type[rand(1, count($type))],
                'type'              => $faker->randomElement($type),
	            'created_at'        => $faker->dateTimeThisYear($max = 'now'),
	            'updated_at'      	=> $faker->dateTimeThisYear($max = 'now'),
                'created_by'        => 1,
                'updated_by'      	=> 1,
	        ));
	    }
    }
}
