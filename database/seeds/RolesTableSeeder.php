<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // DB::table('tmk_roles')->truncate();

        DB::table('tmk_roles')->insert(array (
            // 'id'          => 4,
            'description' => 'superadmin',
            'read'        => 1,
            'write'       => 1,
            'modify'      => 1,
            'created_at'  => date('Y-m-d H:i:s', time()),
            'updated_at'  => date('Y-m-d H:i:s', time()),
        ));


        DB::table('tmk_roles')->insert(array (
            // 'id'          => 3,
            'description' => 'admin',
            'read'        => 1,
            'write'       => 1,
            'modify'      => 1,
            'created_at'  => date('Y-m-d H:i:s', time()),
            'updated_at'  => date('Y-m-d H:i:s', time()),
        ));


        DB::table('tmk_roles')->insert(array (
            // 'id'          => 1,
            'description' => 'user',
            'read'        => 1,
            'write'       => 1,
            'modify'      => 0,
            'created_at'  => date('Y-m-d H:i:s', time()),
            'updated_at'  => date('Y-m-d H:i:s', time()),
        ));


        DB::table('tmk_roles')->insert(array (
            // 'id'          => 2,
            'description' => 'editor',
            'read'        => 1,
            'write'       => 0,
            'modify'      => 1,
            'created_at'  => date('Y-m-d H:i:s', time()),
            'updated_at'  => date('Y-m-d H:i:s', time()),
        ));


    }
}
