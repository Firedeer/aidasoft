<?php 

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BrandTableSeeder extends Seeder {

	public function run (){

		$faker = Faker::create();
		$array = array( 'desconocido', 'Adidas', 'Marca 2', 'Marca 3', 'Marca 4', 'Marca 5' );

    	foreach ( $array as $value ) {
    		DB::table('tmk_brands')->insert(array (
    			'description' => $value,
    			'created_at'  => $faker->dateTimeThisYear($max = 'now'),
	            'updated_at'  => $faker->dateTimeThisYear($max = 'now'),
    		));
    	}

    	// $json = DB::table('tmk_providers')
    	// ->select('tmk_providers.id')
    	// ->get();

    	// $providers = json_encode($json);
    	// $providers = json_decode($providers, true);


    	for($i = 1; $i<=10; $i++) {
    		$company =  $faker->company;

    		DB::table('tmk_provider_brands')->insert(array(
				'provider_id' => $i,
				'brand_id'	  => rand(2, count($array) ),
				// 'company' 	  => $company,
				// 'email'       => $company.'@'.$faker->freeEmailDomain,
				// 'address'     => $faker->address,  
	   //          'phone'       => $faker->tollFreePhoneNumber,
	            'created_at'  => $faker->dateTimeThisYear($max = 'now'),
	            'updated_at'  => $faker->dateTimeThisYear($max = 'now'),
    		));
     	}

	}
} 