<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_purchase_status', function (Blueprint $table) {
            // $table->increments('id');
            $table->integer('status_id')->default(2);
            //$table->foreign('status_id')->references('id')->on('tmk_order_status')->onDelete('cascade');
            
            $table->integer('order_id');
            //$table->foreign('order_id')->references('id')->on('tmk_purchase_orders')->onDelete('cascade');
            
            $table->integer('user_id');
           // $table->foreign('user_id')->references('id')->on('tmk_users')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_purchase_status');
    }
}
