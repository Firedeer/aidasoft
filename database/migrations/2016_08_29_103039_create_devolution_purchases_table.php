<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevolutionPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_devolution_purchases', function (Blueprint $table) {
            $table->increments('id');

            // $table->integer('user_id')->unsigned();
            // $table->foreign('user_id')->references('id')->on('tmk_users')->onDeletes('cascade');
            
            $table->date('date');
            $table->decimal('total', 10, 2);
            $table->string('description');
            
            // $table->integer('article_id')->unsigned();
            // $table->foreign('article_id')->references('id')->on('tmk_article_bodegas')->onDeletes('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_devolution_purchases');
    }
}
