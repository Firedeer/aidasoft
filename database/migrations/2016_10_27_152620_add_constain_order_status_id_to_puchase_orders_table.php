<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstainOrderStatusIdToPuchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('tmk_purchase_orders', function (Blueprint $table) {
    		$table->integer('order_status_id')->unsigned()->default(1);
    	//	$table->foreign('order_status_id')->references('id')->on('tmk_order_status');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('tmk_purchase_orders', function (Blueprint $table) {
    		$table->dropForeign('tmk_purchase_orders_order_status_id_foreign');
    	});
    }
}
