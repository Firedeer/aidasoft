<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_article_warehouses', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('tmk_articles')->onDelete('cascade');
//            $table->integer('warehouse_id')->unsigned();
//            $table->foreign('warehouse_id')->references('id')->on('tmk_bodegas')->onDelete('cascade');
//            $table->integer('article_master_id')->unsigned();

//            $table->index(['article_id', 'warehouse_id', 'article_master_id'])->unique();

            $table->integer('stock')->default(0)->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_article_warehouses');
    }
}
