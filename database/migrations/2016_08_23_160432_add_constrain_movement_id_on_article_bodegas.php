<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstrainMovementIdOnArticleBodegas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tmk_article_bodegas', function (Blueprint $table) {
            $table->integer('movement_id')->unsigned();
            $table->foreign('movement_id')->references('id')->on('tmk_movements')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmk_article_bodegas', function (Blueprint $table) {
            $table->dropForeign('tmk_article_bodegas_movement_id_foreign');
        });
    }
}
