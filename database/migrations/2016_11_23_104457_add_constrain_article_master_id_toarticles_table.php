<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstrainArticleMasterIdToarticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tmk_articles', function (Blueprint $table) {
            $table->integer('article_master_id')->unsigned();
            $table->foreign('article_master_id')->references('id')->on('tmk_article_masters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmk_articles', function (Blueprint $table) {
         
            $table->dropForeign('tmk_articles_article_master_id_foreign');
        });
    }
}
