<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tmk_inventory_details', function(Blueprint $table)
		{
			$table->increments('id');  
//
			// $table->integer('article_id')->unsigned();
//            
			// $table->integer('bodega_id')->unsigned();
			
			// $table->foreign(['article_id', 'bodega_id'])->references(['article_id', 'bodega_id'])->on('tmk_article_bodegas')->onDelete('cascade');
//            
			$table->integer('article_bodega_id')->unsigned();
            $table->foreign('article_bodega_id')->references('id')->on('tmk_article_bodegas')->onDelete('cascade');
			
			$table->integer('inventory_id')->unsigned();
			$table->foreign('inventory_id')->references('id')->on('tmk_inventories')->onDelete('cascade');
			
			$table->bigInteger('quantity');
			$table->decimal('unit_cost', 10, 2);

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tmk_inventory_details');
	}

}
