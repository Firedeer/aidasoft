<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferArticleBodegaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_transfer_article_bodegas', function (Blueprint $table) {
            $table->integer('transfer_id')->unsigned();
            $table->foreign('transfer_id')->references('id')->on('tmk_transfers')->onDelete('cascade');
            $table->integer('article_bodega_id')->unsigned();
            $table->foreign('article_bodega_id')->references('id')->on('tmk_article_bodegas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_transfer_article_bodegas');
    }
}
