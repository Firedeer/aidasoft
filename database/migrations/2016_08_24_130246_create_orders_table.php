<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_num')->nullable();

            $table->string('subject', 100)->nullable();

            $table->string('original_reference')->nullable();
            $table->string('order_file')->nullable();

            $table->integer('from_user_id')->nullable();
            $table->integer('for_user_id')->nullable();

//            $table->integer('from_user_id')->unsigned();
//            $table->foreign('from_user_id')->references('id')->on('tmk_users')->onDeletes('cascade');

//            $table->integer('for_user_id')->unsigned();
//            $table->foreign('for_user_id')->references('id')->on('tmk_users')->onDeletes('cascade');

            $table->timestamp('require_date')->nullable()->default(null);

            $table->string('observation')->nullable();

            $table->integer('sucursal_destination')->unsigned()->nullable();

            $table->integer('warehouse_origen')->unsigned()->nullable();

            $table->integer('warehouse_destination')->unsigned()->nullable();

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('canceled_by')->nullable();

            $table->timestamp('ordered_at')->nullable();
            $table->timestamp('canceled_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_orders');
    }
}
