<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleMasterWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_article_master_warehouses', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('article_master_id')->unsigned();
            //$table->foreign('article_master_id')->references('id')->on('tmk_article_masters');

            $table->integer('warehouse_id')->unsigned();
           // $table->foreign('warehouse_id')->references('id')->on('tmk_bodegas');

           // $table->index(['article_master_id', 'warehouse_id'])->unique();

            $table->integer('min_stock')->unsigned()->nullable();

            $table->integer('max_stock')->unsigned()->nullable();

            $table->boolean('active')->default(true); // true = activo, false = inactivo
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_article_master_warehouses');
    }
}
