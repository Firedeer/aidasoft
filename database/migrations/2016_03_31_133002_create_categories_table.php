<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tmk_categories', function(Blueprint $table)
		{
			$table->increments('id');

			// $table->integer('user_id')->unsigned();
            // $table->foreign('user_id')->references('id')->on('tmk_users')->onDeletes('cascade');

            $table->string('description');
            $table->boolean('active')->default(true); // true = activo, false = inactivo


            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

			$table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tmk_categories');
	}

}
