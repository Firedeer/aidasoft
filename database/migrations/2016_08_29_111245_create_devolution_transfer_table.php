<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevolutionTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_devolution_transfers', function (Blueprint $table) {
            $table->increments('id');

            // $table->integer('devolution_id')->unsigned();
            // $table->foreign('devolution_id')->references('id')->on('tmk_devolutions')->onDelete('cascade');

            $table->integer('transfer_id')->unsigned();
            $table->foreign('transfer_id')->references('id')->on('tmk_transfers')->onDelete('cascade');

            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('tmk_article_bodegas');

            $table->decimal('quantity', 10, 2);
            $table->string('observation');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_devolution_transfers');
    }
}
