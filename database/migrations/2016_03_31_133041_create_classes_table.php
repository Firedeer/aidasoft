<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tmk_classes', function(Blueprint $table)
		{
			$table->increments('id');
            
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('tmk_categories')->onDelete('cascade');
			
			// $table->integer('user_id')->unsigned();
            // $table->foreign('user_id')->references('id')->on('tmk_users')->onDeletes('cascade');

			$table->string('description');
            $table->boolean('active')->default(true); // true = activo, false = inactivo
            
			
            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tmk_classes');
	}

}
