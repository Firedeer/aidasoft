<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropConstrainBrandToProviderId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tmk_brands', function (Blueprint $table) {
            //
            $table->dropForeign('tmk_brands_provider_id_foreign');
            $table->dropColumn('provider_id');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmk_brands', function (Blueprint $table) {
        });
    }
}
