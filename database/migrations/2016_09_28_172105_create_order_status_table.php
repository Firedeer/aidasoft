<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_order_status', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('acronym', 20)->nullable();
            $table->string('description', 50);
            $table->integer('value')->default(1);
            $table->string('observation', 250)->nullable();
            $table->string('colour', 10)->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_order_status');
    }
}
