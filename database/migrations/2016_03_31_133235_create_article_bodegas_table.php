<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleBodegasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_article_bodegas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('tmk_articles')->onDelete('cascade');

            $table->integer('bodega_id')->unsigned();
            $table->foreign('bodega_id')->references('id')->on('tmk_bodegas')->onDelete('cascade');

            // $table->integer('user_id')->unsigned();
            // $table->foreign('user_id')->references('id')->on('tmk_users')->onDeletes('cascade');

            // $table->primary(['article_id', 'bodega_id']);

            // $table->date('date'); 
            $table->bigInteger('quantity')->default(0);
            $table->bigInteger('quantity_exit')->default(0);


            $table->boolean('active')->default(true); // true = activo, false = inactivo 

            $table->boolean('initial')->default(false);

            $table->integer('movementable_id')->nullable();
            $table->string('movementable_type')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_article_bodegas');
    }

}
