<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->integer('read')->default(0);
            $table->integer('write')->default(0);
            $table->integer('modify')->default(0);
            // $table->integer('role_id');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_roles');
    }
}
