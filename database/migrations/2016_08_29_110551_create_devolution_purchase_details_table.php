<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevolutionPurchaseDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_devolution_purchase_details', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('devolution_id')->unsigned();
            $table->foreign('devolution_id')->references('id')->on('tmk_devolution_purchases');
            
            // $table->integer('invoice_id')->unsigned();
            // $table->foreign('invoice_id')->references('id')->on('tmk_invoices');

            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('tmk_articles');

             $table->integer('reference_id')->unsigned();
            $table->foreign('reference_id')->references('id')->on('tmk_article_bodegas');

            $table->decimal('quantity', 10, 2);
            $table->string('observation');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_devolution_purchase_details');
    }
}
