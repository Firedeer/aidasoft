<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleWarehouseDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_article_warehouse_details', function (Blueprint $table) {
//            $table->increments('id');

            $table->integer('article_warehouse_id')->unsigned();
            //$table->foreign('article_warehouse_id')->references('id')->on('tmk_article_warehouses')->onDelete('cascade');
            $table->index('article_warehouse_id');

            $table->integer('stock')->unsigned()->default(0);

            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_article_warehouse_details');
    }
}
