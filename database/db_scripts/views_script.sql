--MYSQL


CREATE VIEW tmk_article_bodegas_view AS
 SELECT DISTINCT tmk_article_masters.id AS article_master_id, ab2.article_id, ab2.bodega_id, tmk_article_masters.min_stock, tmk_article_masters.max_stock, SUM(ab2.quantity) AS entry, ( SELECT SUM(quantity) FROM tmk_article_bodegas AS ab1 INNER JOIN tmk_movements ON tmk_movements.id = ab1.movement_id WHERE ab1.article_id = ab2.article_id AND ab1.bodega_id = ab2.bodega_id AND tmk_movements.id = 2 ) AS 'exit' FROM tmk_article_bodegas AS ab2 INNER JOIN tmk_movements ON tmk_movements.id = ab2.movement_id INNER JOIN tmk_articles ON tmk_articles.id = ab2.article_id INNER JOIN tmk_article_masters ON tmk_article_masters.id = tmk_articles.article_master_id WHERE tmk_movements.id = 1 GROUP BY article_id, bodega_id, tmk_article_masters.id, tmk_article_masters.min_stock,tmk_article_masters.max_stock

 
 CREATE VIEW tmk_stock_bodegas_view AS
SELECT DISTINCT ab2.article_id , ab2.bodega_id , tmk_article_masters.barcode, tmk_article_masters.name AS article, tmk_article_masters.min_stock, tmk_article_masters.max_stock, SUM(ab2.quantity) AS entry, ( SELECT SUM(quantity) FROM tmk_article_bodegas AS ab1 INNER JOIN tmk_movements ON tmk_movements.id = ab1.movement_id WHERE ab1.article_id = ab2.article_id AND ab1.bodega_id = ab2.bodega_id AND tmk_movements.id = 2 ) AS 'exit' FROM tmk_article_bodegas AS ab2 INNER JOIN tmk_movements ON tmk_movements.id = ab2.movement_id INNER JOIN tmk_articles ON tmk_articles.id = ab2.article_id INNER JOIN tmk_article_masters ON tmk_article_masters.id = tmk_articles.article_master_id WHERE tmk_movements.id = 1 GROUP BY article_id, bodega_id, tmk_article_masters.name, tmk_article_masters.barcode, tmk_article_masters.min_stock,tmk_article_masters.max_stock

CREATE VIEW tmk_inventario_general
AS
select 
tmk_article_masters.id As article_master_id,
tmk_article_masters.barcode,
tmk_article_masters.name As article_master,
tmk_article_masters.image As image,
tmk_articles.id AS article_id,
tmk_articles.internal_reference,
CONCAT(tmk_article_masters.name, ' ', tmk_articles.description) as article, 
tmk_brands.description AS brand,
tmk_categories.description AS category,
tmk_measurement_units.description AS measurement_unit,
tmk_bodegas.id AS warehouse_id,
tmk_bodegas.description AS warehouse,
tmk_article_master_warehouses.min_stock,
tmk_article_master_warehouses.max_stock,
tmk_article_warehouses.stock,
(
	SELECT SUM(quantity) 
	FROM tmk_orders AS query
	INNER JOIN tmk_order_details AS query2 ON query2.order_id = query.id
	WHERE query.status_id = 1 AND query.warehouse_destination = tmk_bodegas.id AND query2.article_id = tmk_articles.id
) AS reserved,

tmk_article_warehouses.stock AS available_quantity,

tmk_articles.active,
tmk_article_warehouses.updated_at as last_movement,
(select CONCAT(query.firstname,' ',query.lastname) from tmk_users as query where query.id = tmk_article_masters.created_by) As created_by,
tmk_article_masters.created_at,
(select CONCAT(query.firstname,' ',query.lastname) from tmk_users as query where query.id = tmk_article_masters.updated_by) As updated_by,
tmk_article_masters.updated_at,
(select CONCAT(query.firstname,' ',query.lastname) from tmk_users as query where query.id = tmk_articles.deleted_by) As deleted_by,
tmk_articles.deleted_at
from tmk_article_masters
left join tmk_article_master_warehouses ON tmk_article_master_warehouses.article_master_id = tmk_article_masters.id
left join tmk_article_warehouses ON tmk_article_warehouses.article_master_warehouse_id = tmk_article_master_warehouses.id
left join tmk_brands on tmk_brands.id = tmk_article_masters.brand_id
left join tmk_categories on tmk_categories.id = tmk_article_masters.category_id
left join tmk_measurement_units on tmk_measurement_units.id = tmk_article_masters.measurement_unit_id
left join tmk_bodegas ON tmk_bodegas.id = tmk_article_master_warehouses.warehouse_id
left join tmk_articles ON tmk_articles.id = tmk_article_warehouses.article_id

CREATE VIEW tmk_article_master_warehouses_view
AS
SELECT        article_master_id, barcode, article_master, image, article_id, internal_reference, article, brand, category, measurement_unit, warehouse_id, warehouse, min_stock, 
                         max_stock, stock - IFNULL
                             ((SELECT        quantity
                                 FROM            tmk_reserva_salida_extra AS rs
                                 WHERE        (warehouse_id = tmk_inventario_general.warehouse_id) AND (article_id = tmk_inventario_general.article_id)), 0) AS stock, IFNULL
                             ((SELECT        quantity
                                 FROM            tmk_reserva_salida_extra AS rs
                                 WHERE        (warehouse_id = tmk_inventario_general.warehouse_id) AND (article_id = tmk_inventario_general.article_id)), 0) AS s, reserved, 
                         available_quantity - IFNULL
                             ((SELECT        quantity
                                 FROM            tmk_reserva_salida_extra AS rs
                                 WHERE        (warehouse_id = tmk_inventario_general.warehouse_id) AND (article_id = tmk_inventario_general.article_id)), 0) AS available_quantity, 
                         active, last_movement, created_by, created_at, updated_by, updated_at, deleted_by, deleted_at
FROM            tmk_inventario_general


CREATE VIEW tmk_reserva_salida_extra
AS
SELECT        A.status_id, A.warehouse_id, SUM(B.quantity) AS quantity, B.article_id
FROM           tmk_external_movement_details AS B INNER JOIN
                        tmk_external_movements AS A ON B.external_movement_id = A.id
GROUP BY A.status_id, A.warehouse_id, B.article_id
HAVING        (A.status_id = 2) OR
                         (A.status_id = 2)
						 
						 
						 
	CREATE VIEW tmk_articles_view
	AS
	SELECT 
	tmk_articles.id AS article_id,
	tmk_articles.barcode,
	tmk_articles.internal_reference,
	tmk_articles.description,
	tmk_article_masters.id As article_master_id,
	tmk_article_masters.name AS article_master,
	tmk_article_masters.barcode AS article_master_barcode,
	tmk_article_masters.internal_reference AS article_master_internal_reference,
	tmk_article_masters.image, 
	tmk_article_masters.long_description,  
	tmk_article_masters.min_stock, 
	tmk_article_masters.max_stock,
	tmk_brands.description AS brand, 
	tmk_categories.description AS category, 
	tmk_measurement_units.description AS measurement_unit,
	  IFNULL( 
	 (SELECT CAST(SUM( query.amount ) /  SUM(query.quantity)  AS decimal (6,2)) 
	  FROM tmk_purchase_order_details AS query 
	  WHERE query.article_id = tmk_articles.id AND query.unit_cost > 0 AND query.unit_cost IS NOT NULL 
	  ), 0.00)
	 AS avg_unit_cost,
	tmk_article_masters.created_by,
	tmk_article_masters.updated_by,
	tmk_articles.deleted_by,
	tmk_article_masters.created_at,
	tmk_article_masters.updated_at,
	tmk_articles.deleted_at
	FROM tmk_articles 
	INNER JOIN tmk_article_masters ON tmk_article_masters.id = tmk_articles.article_master_id
	LEFT JOIN tmk_brands ON tmk_brands.id =  tmk_article_masters.brand_id
	LEFT JOIN tmk_categories ON tmk_categories.id = tmk_article_masters.category_id
	LEFT JOIN tmk_measurement_units ON tmk_measurement_units.id = tmk_article_masters.measurement_unit_id
						 
						 
CREATE VIEW tmk_movement_references_view
AS
SELECT        tmk_transfers.id AS reference_id, tmk_transfers.transfer_num AS reference, tmk_transfer_details.article_id, tmk_transfer_details.quantity, NULL AS unit_cost,
                             (
							 SELECT        CONCAT(tmk_users.firstname, ' ', tmk_users.lastname)
                               FROM            tmk_users
                               WHERE        tmk_users.id = tmk_transfers.transferred_by) AS desde,
                             (SELECT        CONCAT(tmk_users.firstname, ' ', tmk_users.lastname)
                               FROM            tmk_users
                               WHERE        tmk_users.id = tmk_transfers.transferred_for) AS para, 'transfer' AS movement_tag
FROM            tmk_transfers INNER JOIN
                         tmk_transfer_details ON tmk_transfer_details.transfer_id = tmk_transfers.id
UNION all
SELECT        tmk_purchase_orders.id, tmk_purchase_orders.order, tmk_purchase_order_details.article_id, tmk_purchase_order_details.quantity, 
                         tmk_purchase_order_details.unit_cost, '', '', 'purchaseorder' AS movement_tag
FROM            tmk_purchase_orders INNER JOIN
                         tmk_purchase_order_details ON tmk_purchase_order_details.order_id = tmk_purchase_orders.id
UNION all
SELECT        x.id, x.external_movement_num AS ordern, y.article_id, y.quantity, NULL AS unit_cost, '', '', 'ExternalMovement' AS movement_tag
FROM            tmk_external_movements x INNER JOIN
                         tmk_external_movement_details y ON x.id = y.external_movement_id
						 


CREATE VIEW tmk_movements_view AS 

select 
tmk_article_bodegas.id,  
tmk_article_bodegas.created_at, 
tmk_type_movements.description AS movement_type, 
tmk_movements.description AS movement, 
tmk_movements.code AS movement_code,
referencia.movement_tag,
tmk_article_masters.id AS article_master_id,
tmk_article_masters.name AS article_master,
tmk_article_masters.image,
tmk_article_masters.barcode,
tmk_articles.id as article_id, 
CONCAT(tmk_article_masters.name, ' ', tmk_articles.description) as article, 
tmk_articles.internal_reference, 
tmk_measurement_units.description as unity,
tmk_article_bodegas.quantity,  
tmk_article_bodegas.quantity_exit,  
referencia.unit_cost,
referencia.reference_id,
referencia.reference,
referencia.desde,
referencia.para,
tmk_article_bodegas.initial,
tmk_bodegas.id AS warehouse_id,
tmk_bodegas.description as warehouse

 from   tmk_movement_references_sub_view  as referencia

right join tmk_article_bodegas on tmk_article_bodegas.movementable_id = referencia.reference_id
AND  REVERSE(LEFT(REVERSE(tmk_article_bodegas.movementable_type), locate('sledoM', REVERSE(tmk_article_bodegas.movementable_type))-1 ))  = referencia.movement_tag collate utf8_general_ci
and  tmk_article_bodegas.article_id = referencia.article_id 

inner JOIN tmk_bodegas ON tmk_bodegas.id = tmk_article_bodegas.bodega_id
inner JOIN tmk_articles ON tmk_articles.id = tmk_article_bodegas.article_id

INNER JOIN tmk_article_masters ON tmk_article_masters.id = tmk_articles.article_master_id
INNER JOIN tmk_measurement_units ON tmk_measurement_units.id =  tmk_article_masters.measurement_unit_id

inner JOIN tmk_movements ON tmk_movements.id = tmk_article_bodegas.movement_id
inner JOIN tmk_type_movements ON tmk_type_movements.id = tmk_movements.type_movement_id

						 

 CREATE VIEW tmk_order_details_sub_view AS 
select 
tmk_order_details.id, 
tmk_orders.id AS order_id, 
tmk_orders.order_num,  
tmk_order_details.article_id, 
tmk_order_details.quantity, 
tmk_order_details.quantity - IFNULL((SELECT SUM(subquery.quantity) FROM tmk_transfer_details AS subquery
where subquery.transfer_id = tmk_transfers.id AND  subquery.article_id = tmk_order_details.article_id), 0) AS pending,
tmk_transfers.id AS transfer_id,
tmk_transfers.transfer_num, 
(SELECT SUM(subquery.quantity) 
FROM tmk_transfer_details AS subquery
where subquery.transfer_id = tmk_transfers.id AND  subquery.article_id = tmk_order_details.article_id) AS transferred, 
(SELECT SUM(subquery.received_quantity) 
FROM tmk_transfer_details AS subquery
where subquery.transfer_id = tmk_transfers.id AND  subquery.article_id = tmk_order_details.article_id) AS received
from tmk_orders 
inner join tmk_order_details on tmk_order_details.order_id = tmk_orders.id
left join tmk_transfers on tmk_transfers.order_id = tmk_orders.id
left join tmk_transfer_details on tmk_transfer_details.transfer_id = tmk_transfers.id


create VIEW tmk_order_details_view AS 
select tmk.id, tmk.order_id, tmk.order_num, tmk.article_id, tmk.quantity, tmk.pending, tmk.transferred, tmk.received
from  `tmk_order_details_sub_view` as tmk
group by
id, 
order_id, 
order_num,  
article_id, 
quantity, 
pending,
transferred, 
received









--ESTE AREA ES SOLO DE LA VISTA DE PURCHASE_ORDER_DETALS CON SUS RESPECTIVAS SUB VISTAS

create view a_view as 
SELECT `id`, `order` as ordenes, `provider_id`, `require_date`, `warehouse_id`, `subtotal`, `tax`, `discount`, `observation`, `created_by`, `updated_by`, `deleted_by`, `confirmed_at`, `canceled_at`, `created_at`, `updated_at`, `deleted_at`, `order_status_id` FROM `tmk_purchase_orders`

create bx_view as 
SELECT BX.article_id,BX.purchase_order_id, SUM( BX.quantity ) AS quantity 
				FROM tmk_purchase_receipts BX

				GROUP BY BX.article_id,BX.purchase_order_id
				

create view cx_view as 
SELECT CX.article_id,purchase_order_id, SUM( CX.quantity ) AS Cantpend
				FROM tmk_purchase_receipts CX
				GROUP BY CX.article_id,purchase_order_id

create view dx_view as 
SELECT SUM( query.quantity ) cantrec,purchase_order_id  
			FROM tmk_purchase_receipts AS query GROUP BY purchase_order_id
			

create view ex_view as 
SELECT SUM( query.quantity ) cantpen2,query.movementable_id 
	FROM tmk_article_bodegas AS query 
	INNER JOIN tmk_purchase_orders
	ON tmk_purchase_orders.id = query.movementable_id  
	AND query.movementable_type = 'TradeMarketing\Models\PurchaseOrder'
	group by 
	query.movementable_id


create view fx_view as 
SELECT FX.article_id, SUM( FX.quantity ) AS quantityBod,FX.movementable_id
	FROM tmk_article_bodegas FX
	WHERE 
	REVERSE(LEFT(REVERSE
	(FX.movementable_type), 
	locate('sledoM', REVERSE(FX.movementable_type))-1 )) = 'PurchaseOrder'
	GROUP BY FX.article_id,FX.movementable_id


create view gx_view as
SELECT MAX(created_at) creado,article_id,purchase_order_id
	FROM tmk_purchase_receipts AS query 
	group by
	article_id,purchase_order_id



create view tmk_purchase_order_details_view as

SELECT
	
	tmk_purchase_order_details.id,
	A.id AS order_id, 
	A.ordenes,
	tmk_purchase_order_details.article_id,
	tmk_articles.internal_reference,
	CONCAT(tmk_article_masters.name,' ',tmk_articles.description) AS article,
	tmk_measurement_units.description AS measuremnt_unit,

		IFNULL( 
		tmk_purchase_order_details.quantity, 0
	) AS quantity,
		IFNULL( 
		tmk_purchase_order_details.unit_cost, 0.00 
	) AS unit_cost,
		IFNULL( 
		CAST( 
			tmk_purchase_order_details.unit_cost * tmk_purchase_order_details.quantity AS decimal (10,2) 
		), 0.00
	) AS amount,
		IFNULL( 
		tmk_purchase_order_details.tax, 0.00 
	) AS tax,
		BX1.quantity received,
		IFNULL((tmk_purchase_order_details.quantity -CX1.Cantpend),0) pending,
		0 AS returned,
		(IFNULL(DX1.cantrec,0)-IFNULL(EX1.cantpen2,0)) AS unassigned,
		IFNULL(FX1.quantityBod,0) AS in_bodega,
		tmk_purchase_order_details.complete,
	(GX1.creado) AS last_receipt,
	tmk_purchase_order_details.created_at,
	tmk_purchase_order_details.updated_at,
	tmk_purchase_order_details.deleted_at
FROM 
a_view  A
	LEFT JOIN tmk_purchase_order_details ON A.id = tmk_purchase_order_details.order_id
	LEFT JOIN tmk_article_bodegas ON tmk_article_bodegas.movementable_id = A.id 
	AND REVERSE(LEFT(REVERSE(tmk_article_bodegas.movementable_type), LOCATE('sledoM', REVERSE(tmk_article_bodegas.movementable_type))-1 ))  = 'PurchaseOrder'
	INNER JOIN tmk_articles ON tmk_articles.id = tmk_purchase_order_details.article_id
	INNER JOIN tmk_article_masters ON tmk_article_masters.id = tmk_articles.article_master_id
	INNER JOIN tmk_measurement_units ON tmk_measurement_units.id = tmk_article_masters.measurement_unit_id
	LEFT JOIN bx_view AS BX1
	on BX1.purchase_order_id = A.id and BX1.article_id=tmk_purchase_order_details.article_id
	LEFT JOIN
	cx_view as CX1
	ON  CX1.purchase_order_id = A.id and  CX1.article_id=tmk_purchase_order_details.article_id
	LEFT JOIN
	dx_view AS DX1
	ON DX1.purchase_order_id= A.id

	LEFT JOIN
	ex_view  AS EX1
	ON EX1.movementable_id=A.id
	LEFT JOIN
	fx_view AS FX1
	ON FX1.movementable_id=A.id AND FX1.article_id=tmk_purchase_order_details.article_id
	LEFT JOIN
	gx_view AS GX1
	ON GX1.purchase_order_id=A.id AND GX1.article_id=tmk_purchase_order_details.article_id

WHERE tmk_purchase_order_details.deleted_at IS NULL 
GROUP BY 
	tmk_purchase_order_details.id,
	A.id, 
	A.ordenes,
	BX1.quantity,
	CX1.Cantpend,
	DX1.cantrec,
	EX1.cantpen2,
	FX1.quantityBod,
	GX1.creado,
	tmk_purchase_order_details.article_id,
	tmk_articles.internal_reference,
	tmk_article_masters.name,
	tmk_articles.description,
	tmk_purchase_order_details.quantity,
	tmk_purchase_order_details.unit_cost,
	tmk_purchase_order_details.tax,
	tmk_measurement_units.unity,
	tmk_measurement_units.description,
	tmk_purchase_order_details.complete,
	tmk_purchase_order_details.created_at,
	tmk_purchase_order_details.updated_at,
	tmk_purchase_order_details.deleted_at





--FIN DE PURCHASE_ORDER_DETAIL  
--------------------------------------------------------------

CREATE VIEW tmk_purchase_orders_view AS 
SELECT 
	tmk_purchase_orders.id,
	tmk_purchase_orders.order,
	tmk_purchase_orders.require_date,
	tmk_purchase_orders.subtotal,
	tmk_purchase_orders.tax,
	tmk_purchase_orders.discount,
	( select SUM(amount) from tmk_purchase_order_details where tmk_purchase_order_details.order_id =  tmk_purchase_orders.id ) AS total,
	tmk_purchase_orders.observation,
	tmk_bodegas.id AS warehouse_id,
	tmk_bodegas.description As warehouse,
	tmk_purchase_orders.provider_id,
	CONCAT( tmk_providers.firstname, ' ', tmk_providers.lastname ) AS provider,
	tmk_providers.address AS provider_addres, 
	tmk_providers.email AS provider_email, 
	tmk_providers.phone AS provider_phone, 
	tmk_purchase_orders.created_by, 
	tmk_users.agent_id, 
	tmk_purchase_orders.order_status_id,
	tmk_order_status.description AS order_status,
	tmk_order_status.acronym AS order_status_acronym,
	tmk_purchase_orders.created_at,
	tmk_purchase_orders.updated_at,
	tmk_purchase_orders.confirmed_at,
	tmk_purchase_orders.canceled_at,
	tmk_purchase_orders.deleted_at,
	(   
		SELECT COUNT( IFNULL( query.id, 0 ) )
		FROM tmk_purchase_order_details AS query
		WHERE ( SELECT IFNULL ( 
				sum( tmk_purchase_order_details.quantity
				), 0
			) FROM tmk_purchase_order_details WHERE tmk_purchase_order_details.id = query.id
		) > ( SELECT IFNULL ( 
				sum( tmk_purchase_receipts.quantity
				), 0
			) FROM tmk_purchase_receipts WHERE tmk_purchase_receipts.purchase_order_id = query.id 
		) AND query.order_id = tmk_purchase_orders.id AND query.deleted_at IS NULL
	) AS missing

FROM tmk_purchase_orders
LEFT JOIN tmk_providers ON tmk_providers.id = tmk_purchase_orders.provider_id
INNER JOIN tmk_users ON tmk_users.id =  tmk_purchase_orders.created_by
INNER JOIN tmk_bodegas ON tmk_bodegas.id = tmk_purchase_orders.warehouse_id
INNER JOIN tmk_order_status ON tmk_order_status.id = tmk_purchase_orders.order_status_id






create VIEW tmk_traking AS
SELECT 
tmk_orders.id AS order_id, 
tmk_orders.order_num, 
(
    SELECT tmk_bodegas.description 
    FROM tmk_bodegas 
    WHERE tmk_bodegas.id = tmk_orders.warehouse_origen 
)AS order_warehouse_origen, 
(
    SELECT tmk_bodegas.description 
    FROM tmk_bodegas 
    WHERE tmk_bodegas.id = tmk_orders.warehouse_destination
)AS order_warehouse_destination,
tmk_orders.ordered_at, 
(
    select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) FROM tmk_users WHERE tmk_users.id = tmk_orders.created_by
) AS ordered_by,
(
    select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) FROM tmk_users WHERE tmk_users.id = tmk_orders.for_user_id
) AS ordered_for,
 
tmk_orders.canceled_at AS order_canceled_at,
 
(
select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) FROM tmk_users WHERE tmk_users.id = tmk_orders.canceled_by
) AS order_canceled_by,
 
tmk_transfers.id AS transfer_id, 
tmk_transfers.transfer_num, 
tmk_transfers.transfer_reference,
(
    select tmk_bodegas.description 
    from tmk_bodegas 
    where tmk_bodegas.id = tmk_transfers.bodega_id 
)AS transfer_warehouse_origen, 
(
    select tmk_bodegas.description 
    from tmk_bodegas 
    where tmk_bodegas.id = tmk_transfers.bodega_id_end 
)AS transfer_warehouse_destination,
tmk_transfers.created_at, 
(
    select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.created_by
) AS created_by,
tmk_transfers.updated_at, 
(
    select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.updated_by
) AS updated_by,
tmk_transfers.transferred_at,
(
    select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.transferred_by
) AS transferred_by,
(
    select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.transferred_for
) AS transferred_for,
 
tmk_transfers.received_at, 
(
    select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.received_by
) AS received_by,
tmk_transfers.canceled_at, 
(
    select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.canceled_by
) AS canceled_by,
tmk_transfers.requested_at, 
(
    select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.requested_by
) AS requested_by,
( 
    select tmk_order_status.description from tmk_order_status where tmk_order_status.id =  tmk_orders.status_id 
) As order_status,
( 
    select tmk_order_status.description from tmk_order_status where tmk_order_status.id =  tmk_transfers.status_id 
) As transfer_status
FROM tmk_orders
LEFT JOIN tmk_transfers ON tmk_transfers.order_id = tmk_orders.id 


create view tmk_tracking_orders_view as
select
ordered_at,
ordered_by,
transfer_id, 
transfer_num, 
order_id,
order_num,
order_warehouse_origen as warehouse,
order_warehouse_origen as warehouse_origen,
order_warehouse_destination as warehouse_destination,
CONCAT(ordered_by,' creó un pedido con número ', order_num, '.') as description
from tmk_traking
where ordered_at is not null

union all
	


select
requested_at,
requested_by,
transfer_id, 
transfer_num, 
order_id,
order_num,
order_warehouse_origen as warehouse,
order_warehouse_origen as warehouse_origen,
order_warehouse_destination as warehouse_destination,
CONCAT(requested_by, ' creó una solicitud de transferencia para ', order_warehouse_origen, ' ',  order_num, '.') as description
from tmk_traking
where requested_at is not null

union all

	
select
transferred_at,
transferred_by,
transfer_id, 
transfer_num, 
order_id,
order_num,
order_warehouse_origen as warehouse,
order_warehouse_origen as warehouse_origen,
order_warehouse_destination as warehouse_destination,
CONCAT(transferred_by, ' creó una transferencia hacia ', order_warehouse_destination, '.') as description
from tmk_traking
where transferred_at is not null

union all

select
received_at,
received_by,
transfer_id, 
transfer_num, 
order_id,
order_num,
order_warehouse_origen as warehouse,
order_warehouse_origen as warehouse_origen,
order_warehouse_destination as warehouse_destination,
CONCAT(received_by, ' recibió la transferencia.') as description
from tmk_traking
where received_at is not null

union all
select
canceled_at,
canceled_by,
transfer_id, 
transfer_num, 
order_id,
order_num,
order_warehouse_origen as warehouse,
order_warehouse_origen as warehouse_origen,
order_warehouse_destination as warehouse_destination,
CONCAT(ordered_by, ' canceló la transferencia.') as description
from tmk_traking
where canceled_at is not null

union all

select
order_canceled_at,
order_canceled_by,
transfer_id, 
transfer_num, 
order_id,
order_num,
order_warehouse_origen as warehouse,
order_warehouse_origen as warehouse_origen,
order_warehouse_destination as warehouse_destination,
CONCAT(order_canceled_by, ' canceló la transferencia.') as description
from tmk_traking
where order_canceled_at is not null
order by order_id



create VIEW tmk_transfers_view AS 
SELECT  
tmk_transfers.id, 
tmk_transfers.transfer_num, 
tmk_transfers.transfer_reference,
(
	select tmk_bodegas.description 
	from tmk_bodegas 
	where tmk_bodegas.id = tmk_transfers.bodega_id 
)AS transfer_warehouse_origen, 
(
	select tmk_bodegas.description 
	from tmk_bodegas 
	where tmk_bodegas.id = tmk_transfers.bodega_id_end 
)AS transfer_warehouse_destination,
( 
	select tmk_order_status.description from tmk_order_status where tmk_order_status.id =  tmk_transfers.status_id 
) As status,

tmk_transfers.created_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.created_by
) AS created_by,
tmk_transfers.updated_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.updated_by
) AS updated_by,
tmk_transfers.transferred_at,
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.transferred_by
) AS transferred_by,
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.transferred_for
) AS transferred_for,

tmk_transfers.received_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.received_by
) AS received_by,
tmk_transfers.canceled_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.canceled_by
) AS canceled_by,
tmk_orders.id AS order_id, 
tmk_orders.order_num, 
(
	select tmk_bodegas.description 
	from tmk_bodegas 
	where tmk_bodegas.id = tmk_orders.warehouse_origen 
)AS order_warehouse_origen, 
(
	select tmk_bodegas.description 
	from tmk_bodegas 
	where tmk_bodegas.id = tmk_orders.warehouse_destination
)AS order_warehouse_destination,
tmk_orders.ordered_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_orders.created_by
) AS ordered_by,
tmk_transfers.requested_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.requested_by
) AS requested_by
from tmk_orders
inner join tmk_transfers on tmk_orders.id = tmk_transfers.order_id






























--SQL



USE  BI_COMERCIAL
GO 


/**
*
*
*/
IF OBJECT_ID ('tmk_article_bodegas_view') IS NOT NULL
BEGIN
	DROP VIEW tmk_article_bodegas_view
END
GO

CREATE VIEW tmk_article_bodegas_view AS 
SELECT DISTINCT
tmk_article_masters.id AS article_master_id, 
ab2.article_id,
ab2.bodega_id, 
tmk_article_masters.min_stock,
tmk_article_masters.max_stock,
SUM (ab2.quantity) AS entry, 
( 
	SELECT SUM (quantity)  
	FROM tmk_article_bodegas AS ab1 
	INNER JOIN tmk_movements ON tmk_movements.id = ab1.movement_id 
	WHERE ab1.article_id  = ab2.article_id 
	AND ab1.bodega_id =  ab2.bodega_id 
	AND tmk_movements.id = 2 
) AS 'exit'
FROM tmk_article_bodegas AS ab2 
INNER JOIN tmk_movements ON tmk_movements.id = ab2.movement_id 
INNER JOIN tmk_articles ON tmk_articles.id =  ab2.article_id
INNER JOIN tmk_article_masters ON tmk_article_masters.id = tmk_articles.article_master_id 
WHERE tmk_movements.id = 1 
GROUP BY article_id, bodega_id, tmk_article_masters.id, tmk_article_masters.min_stock,tmk_article_masters.max_stock

GO


/**
*
*
*/
IF OBJECT_ID ('tmk_stock_bodegas_view') IS NOT NULL
BEGIN
	DROP VIEW tmk_stock_bodegas_view
END
GO

CREATE VIEW tmk_stock_bodegas_view AS 
SELECT DISTINCT 
ab2.article_id ,
ab2.bodega_id , 
tmk_article_masters.barcode,
tmk_article_masters.name AS article,
tmk_article_masters.min_stock,
tmk_article_masters.max_stock,
SUM (ab2.quantity) AS entry, 
( 
	SELECT SUM (quantity)  
	FROM tmk_article_bodegas AS ab1 
	INNER JOIN tmk_movements ON tmk_movements.id = ab1.movement_id 
	WHERE ab1.article_id  = ab2.article_id 
	AND ab1.bodega_id =  ab2.bodega_id 
	AND tmk_movements.id = 2 
) AS 'exit'
FROM tmk_article_bodegas AS ab2 
INNER JOIN tmk_movements ON tmk_movements.id = ab2.movement_id 
INNER JOIN tmk_articles ON tmk_articles.id =  ab2.article_id
INNER JOIN tmk_article_masters ON tmk_article_masters.id = tmk_articles.article_master_id 
WHERE tmk_movements.id = 1 
GROUP BY article_id, bodega_id, tmk_article_masters.name, tmk_article_masters.barcode, tmk_article_masters.min_stock,tmk_article_masters.max_stock
GO


/**
*
*
*/
/*
IF OBJECT_ID('tmk_inventory_view') IS NOT NULL 
BEGIN
    DROP VIEW tmk_inventory_view 
END 
GO

CREATE VIEW tmk_inventory_view AS 

SELECT 
inv.article_id, 
inv.article, 
inv.article_master_id, 
inv.bodega_id,
inv.movement_id,
inv.category_id,  
inv.min_stock, 
inv.max_stock, 
inv.initial, 
inv.[entry], 
inv.[exit], 
inv.stock,
inv.order_id,
inv.[date]
FROM (
	SELECT 
	tmk_article_bodegas.article_id, 
	tmk_articles.internal_reference,
	tmk_articles.description AS article, 
	tmk_articles.article_master_id,
	tmk_article_masters.min_stock,
	tmk_article_masters.max_stock,
	tmk_article_bodegas.bodega_id,
	tmk_article_bodegas.movement_id,
	tmk_article_masters.category_id,
	ISNULL( (
		SELECT TOP 1  quantity 
		FROM tmk_article_bodegas AS subquery
		WHERE subquery.article_id = tmk_article_bodegas.article_id AND initial = 1
		ORDER BY  id DESC 
	) , 0) as initial, 
	SUM (tmk_article_bodegas.quantity) AS [entry],
	( 
		SELECT ISNULL( 
			SUM (quantity), 0
		)  
		FROM tmk_article_bodegas AS subquery 
		INNER JOIN tmk_movements ON tmk_movements.id = subquery.movement_id 
		WHERE subquery.article_id  = tmk_article_bodegas.article_id AND subquery.bodega_id = tmk_article_bodegas.bodega_id AND tmk_movements.id = 2 
	) AS [exit],	
	SUM (tmk_article_bodegas.quantity) - ( 
		SELECT ISNULL( 
			SUM (quantity), 0
		)  
		FROM tmk_article_bodegas AS subquery 
		INNER JOIN tmk_movements ON tmk_movements.id = subquery.movement_id 
		WHERE subquery.article_id  = tmk_article_bodegas.article_id AND subquery.bodega_id = tmk_article_bodegas.bodega_id AND tmk_movements.id = 2 
	) AS stock,
	tmk_purchase_order_details.order_id

	, CONCAT(CAST(YEAR(tmk_article_bodegas.created_at) AS nvarchar(4)) ,'-',  CAST(MONTH(tmk_article_bodegas.created_at) AS nvarchar(4)) ) AS date
	FROM tmk_article_bodegas
	INNER JOIN tmk_type_movements ON tmk_type_movements.id = tmk_article_bodegas.movement_id 
	INNER JOIN tmk_articles ON tmk_articles.id =  tmk_article_bodegas.article_id
	INNER JOIN tmk_article_masters ON tmk_article_masters.id = tmk_articles.article_master_id
	INNER JOIN tmk_bodegas ON tmk_bodegas.id =  tmk_article_bodegas.bodega_id
	INNER JOIN tmk_purchase_article_bodegas ON tmk_purchase_article_bodegas.article_bodega_id = tmk_article_bodegas.id
	INNER JOIN tmk_purchase_order_details ON tmk_purchase_order_details.id = tmk_purchase_article_bodegas.purchase_detail_id

	GROUP BY tmk_article_bodegas.article_id, 
	tmk_article_bodegas.bodega_id, 
	tmk_articles.internal_reference, 
	tmk_articles.description, 
	tmk_article_masters.max_stock, 
	tmk_article_masters.min_stock, 
	tmk_articles.article_master_id, 
	tmk_purchase_order_details.order_id,
	tmk_article_bodegas.created_at,
	tmk_article_bodegas.movement_id,
	tmk_article_masters.category_id
) AS inv
GROUP BY 
inv.article_id, 
inv.article, 
inv.article_master_id, 
inv.bodega_id, 
inv.min_stock, 
inv.max_stock, 
inv.initial, 
inv.[entry], 
inv.[exit],
inv.stock, 
inv.[date],
inv.order_id,
inv.movement_id,
inv.category_id
GO
--SELECT 
--tmk_article_bodegas.article_id , 
--tmk_article_bodegas.bodega_id , 
--tmk_article_masters.barcode,
--tmk_article_masters.name AS article, 
--tmk_article_masters.min_stock,
--tmk_article_masters.max_stock,
--tmk_bodegas.description AS bodega,
--ISNULL( (
--    SELECT TOP 1  quantity 
--    FROM tmk_article_bodegas AS subquery
--    WHERE subquery.article_id = tmk_article_bodegas.article_id AND initial = 1
--    ORDER BY  id DESC 
--) , 0) as initial, 
--SUM (tmk_article_bodegas.quantity) AS [entry],
--( 
--    SELECT ISNULL( 
--		SUM (quantity), 0
--	)  
--    FROM tmk_article_bodegas AS subquery 
--    INNER JOIN tmk_movements ON tmk_movements.id = subquery.movement_id 
--    WHERE subquery.article_id  = tmk_article_bodegas.article_id AND subquery.bodega_id = tmk_article_bodegas.bodega_id AND tmk_movements.id = 2 
--) AS [exit],	
--SUM (tmk_article_bodegas.quantity) - ( 
--    SELECT ISNULL( 
--		SUM (quantity), 0
--	)  
--    FROM tmk_article_bodegas AS subquery 
--    INNER JOIN tmk_movements ON tmk_movements.id = subquery.movement_id 
--    WHERE subquery.article_id  = tmk_article_bodegas.article_id AND subquery.bodega_id = tmk_article_bodegas.bodega_id AND tmk_movements.id = 2 
--) AS stock,
--(
--    SELECT TOP 1 subquery.created_at 
--    FROM tmk_article_bodegas AS subquery
--    WHERE subquery.article_id = tmk_article_bodegas.article_id AND subquery.movement_id = 1
--    ORDER BY id DESC 
--) AS last_purchase,
--( 
--    SELECT MAX(updated_at) FROM tmk_article_bodegas AS subquery WHERE subquery.article_id = tmk_article_bodegas.article_id
--    GROUP BY article_id 
--) AS last_update
--FROM tmk_article_bodegas
--INNER JOIN tmk_type_movements ON tmk_type_movements.id = tmk_article_bodegas.movement_id 
--INNER JOIN tmk_articles ON tmk_articles.id =  tmk_article_bodegas.article_id
--INNER JOIN tmk_article_masters ON tmk_article_masters.id = tmk_articles.article_master_id
--INNER JOIN tmk_bodegas ON tmk_bodegas.id =  tmk_article_bodegas.bodega_id
----WHERE tmk_type_movements.movement_id = 1 AND tmk_article_bodegas.created_at >= ( 
----    SELECT TOP 1 subquery.created_at FROM tmk_article_bodegas AS subquery
----    WHERE subquery.article_id = tmk_article_bodegas.article_id AND initial = 1
----    ORDER BY created_at DESC 
----    )
--GROUP BY article_id, bodega_id, tmk_bodegas.description
--, tmk_article_masters.barcode, tmk_article_masters.name, tmk_article_masters.max_stock, tmk_article_masters.min_stock
GO
*/




/**
 *
 */
IF OBJECT_ID('tmk_movement_references_view') IS NOT NULL 
BEGIN
    DROP VIEW tmk_movement_references_view 
END 
GO

/*
CREATE VIEW tmk_movement_references_view 
AS  

SELECT article_bodega_id, 
tmk_transfers.id AS reference_id, 
tmk_transfers.transfer_num AS reference, 
(
SELECT CONCAT(tmk_users.firstname ,' ', tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.transferred_by
) AS [from], 
(
SELECT CONCAT(tmk_users.firstname ,' ', tmk_users.lastname) 
from tmk_users 
where tmk_users.id = tmk_transfers.transferred_for
) AS [for],


tmk_transfers.created_at, 
'0.00' AS unit_cost, 
'transfer' AS 'movement_tag'
from tmk_article_bodegas 
inner join tmk_transfer_article_bodegas on tmk_transfer_article_bodegas.article_bodega_id  = tmk_article_bodegas.id 
inner join tmk_transfers on tmk_transfers.id = tmk_transfer_article_bodegas.transfer_id
union
select article_bodega_id, 
tmk_purchase_orders.id, 
tmk_purchase_orders.[order],  
CONCAT(tmk_users.firstname ,' ', tmk_users.lastname), 
tmk_providers.firstname, 
tmk_purchase_orders.created_at, 
tmk_purchase_order_details.unit_cost,
'purchase'
from tmk_article_bodegas 
inner join tmk_purchase_article_bodegas on tmk_purchase_article_bodegas.article_bodega_id = tmk_article_bodegas.id 
inner join tmk_purchase_order_details on tmk_purchase_order_details.id = tmk_purchase_article_bodegas.purchase_detail_id
inner join tmk_purchase_orders on tmk_purchase_orders.id = tmk_purchase_order_details.order_id
inner join tmk_providers on tmk_providers.id =  tmk_purchase_orders.provider_id
inner join tmk_users on tmk_users.id = tmk_purchase_orders.created_by 
GO
*/



/**
*
*
*/
IF OBJECT_ID('tmk_movements_view') IS NOT NULL 
BEGIN
    DROP VIEW tmk_movements_view 
END 
GO

CREATE VIEW tmk_movements_view AS 

select 
tmk_article_bodegas.id,  
tmk_article_bodegas.created_at, 
tmk_type_movements.description AS movement_type, 
tmk_movements.description AS movement, 
tmk_movements.code AS movement_code,
tmk_movement_references.movement_tag,
tmk_article_masters.id AS article_master_id,
tmk_article_masters.name AS article_master,
tmk_article_masters.image,
tmk_article_masters.barcode,
tmk_articles.id as article_id, 
CONCAT(tmk_article_masters.name, ' ', tmk_articles.description) as article, 
tmk_articles.internal_reference, 
tmk_measurement_units.description as unity,
tmk_article_bodegas.quantity,  
tmk_article_bodegas.quantity_exit,  
tmk_movement_references.unit_cost,
tmk_movement_references.reference_id,
tmk_movement_references.reference,
tmk_movement_references.[from],
tmk_movement_references.[for],
tmk_article_bodegas.initial,
tmk_bodegas.id AS warehouse_id,
tmk_bodegas.description as warehouse

 from (
select 
tmk_transfers.id AS reference_id, 
tmk_transfers.transfer_num as reference,  
tmk_transfer_details.article_id,
tmk_transfer_details.quantity,
NULL as unit_cost,

	(
		SELECT CONCAT(tmk_users.firstname ,' ', tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.transferred_by
	) AS [from], 
	(
		SELECT CONCAT(tmk_users.firstname ,' ', tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.transferred_for
	) AS [for],

'transfer' as movement_tag
from tmk_transfers 
inner join tmk_transfer_details on tmk_transfer_details.transfer_id =tmk_transfers.id
union 
select 
tmk_purchase_orders.id, 
tmk_purchase_orders.[order], 
tmk_purchase_order_details.article_id, 
tmk_purchase_order_details.quantity, 
tmk_purchase_order_details.unit_cost, 
'','',
'purchaseorder' as movement_tag
from tmk_purchase_orders 
inner join tmk_purchase_order_details on tmk_purchase_order_details.order_id = tmk_purchase_orders.id
union 

select
tmk_external_movements.id, 
tmk_external_movements.external_movement_num, 
article_id, 
quantity, 
NULL,
'',
'',
'externalmovement' AS movement_tag

from tmk_external_movements inner join tmk_external_movement_details 
on tmk_external_movement_details.external_movement_id = tmk_external_movements.id

) as tmk_movement_references 

right join tmk_article_bodegas on tmk_article_bodegas.movementable_id = tmk_movement_references.reference_id
AND  REVERSE(LEFT(REVERSE(tmk_article_bodegas.movementable_type), CHARINDEX('\', REVERSE(tmk_article_bodegas.movementable_type))-1 ))  = tmk_movement_references.movement_tag
and  tmk_article_bodegas.article_id = tmk_movement_references.article_id 

inner JOIN tmk_bodegas ON tmk_bodegas.id = tmk_article_bodegas.bodega_id
inner JOIN tmk_articles ON tmk_articles.id = tmk_article_bodegas.article_id

INNER JOIN tmk_article_masters ON tmk_article_masters.id = tmk_articles.article_master_id
INNER JOIN tmk_measurement_units ON tmk_measurement_units.id =  tmk_article_masters.measurement_unit_id

inner JOIN tmk_movements ON tmk_movements.id = tmk_article_bodegas.movement_id
inner JOIN tmk_type_movements ON tmk_type_movements.id = tmk_movements.type_movement_id
GO





/**
*
* 
*/
IF OBJECT_ID('tmk_purchase_orders_view') IS NOT NULL 
BEGIN
    DROP VIEW tmk_purchase_orders_view 
END 
GO

CREATE VIEW tmk_purchase_orders_view
AS
SELECT 
	tmk_purchase_orders.id,
	tmk_purchase_orders.[order],
	tmk_purchase_orders.require_date,
	tmk_purchase_orders.subtotal,
	tmk_purchase_orders.tax,
	tmk_purchase_orders.discount,
	( select SUM(amount) from tmk_purchase_order_details where tmk_purchase_order_details.order_id =  tmk_purchase_orders.id ) AS total,
	tmk_purchase_orders.observation,
	tmk_bodegas.id AS warehouse_id,
	tmk_bodegas.description As warehouse,
	tmk_purchase_orders.provider_id,
	CONCAT( tmk_providers.firstname, ' ', tmk_providers.lastname ) AS provider,
	tmk_providers.address AS provider_addres, 
	tmk_providers.email AS provider_email, 
	tmk_providers.phone AS provider_phone, 
	tmk_purchase_orders.created_by, 
	tmk_users.agent_id, 
	tmk_purchase_orders.order_status_id,
	tmk_order_status.description AS order_status,
	tmk_order_status.acronym AS order_status_acronym,
	tmk_purchase_orders.created_at,
	tmk_purchase_orders.updated_at,
	tmk_purchase_orders.confirmed_at,
	tmk_purchase_orders.canceled_at,
	tmk_purchase_orders.deleted_at,
	(   
		SELECT COUNT ( ISNULL( query.id, 0 ) )
		FROM tmk_purchase_order_details AS query
		WHERE ( SELECT ISNULL ( 
				sum( tmk_purchase_order_details.quantity
				), 0
			) FROM tmk_purchase_order_details WHERE tmk_purchase_order_details.id = query.id
		) > ( SELECT ISNULL ( 
				sum( tmk_purchase_receipts.quantity
				), 0
			) FROM tmk_purchase_receipts WHERE tmk_purchase_receipts.purchase_order_id = query.id 
		) AND query.order_id = tmk_purchase_orders.id AND query.deleted_at IS NULL
	) AS missing

FROM tmk_purchase_orders
LEFT JOIN tmk_providers ON tmk_providers.id = tmk_purchase_orders.provider_id
INNER JOIN tmk_users ON tmk_users.id =  tmk_purchase_orders.created_by
INNER JOIN tmk_bodegas ON tmk_bodegas.id = tmk_purchase_orders.warehouse_id
INNER JOIN tmk_order_status ON tmk_order_status.id = tmk_purchase_orders.order_status_id
GO



/**
*
* 
*/



IF OBJECT_ID('tmk_purchase_order_details_view') IS NOT NULL 
BEGIN
    DROP VIEW tmk_purchase_order_details_view 
END 
GO

CREATE VIEW tmk_purchase_order_details_view
AS
SELECT
	tmk_purchase_order_details.id,
	tmk_purchase_orders.id AS order_id, 
	tmk_purchase_orders.[order],
	tmk_purchase_order_details.article_id,
	tmk_articles.internal_reference,
	CONCAT(tmk_article_masters.name,' ',tmk_articles.description) AS article,
	tmk_measurement_units.description AS measuremnt_unit,
	ISNULL( 
		tmk_purchase_order_details.quantity, 0
	) AS quantity,
	ISNULL( 
		tmk_purchase_order_details.unit_cost, 0.00 
	) AS unit_cost,
	ISNULL( 
		CAST( 
			tmk_purchase_order_details.unit_cost * tmk_purchase_order_details.quantity AS decimal (10,2) 
		), 0.00
	) AS amount,
	ISNULL( 
		tmk_purchase_order_details.tax, 0.00 
	) AS tax,
	ISNULL( (
			SELECT _table.quantity 
			FROM (
				SELECT tmk_purchase_receipts.article_id, SUM( tmk_purchase_receipts.quantity ) AS quantity 
				FROM tmk_purchase_receipts
				WHERE tmk_purchase_receipts.purchase_order_id = tmk_purchase_orders.id 
				GROUP BY tmk_purchase_receipts.article_id
			) AS _table WHERE _table.article_id = tmk_purchase_order_details.article_id

	), 0 ) AS received,  
		ISNULL( (


			SELECT tmk_purchase_order_details.quantity - _table.quantity 
			FROM (
				SELECT tmk_purchase_receipts.article_id, SUM( tmk_purchase_receipts.quantity ) AS quantity 
				FROM tmk_purchase_receipts
				WHERE tmk_purchase_receipts.purchase_order_id = tmk_purchase_orders.id 
				GROUP BY tmk_purchase_receipts.article_id
			) AS _table WHERE _table.article_id = tmk_purchase_order_details.article_id
			) , 0 
		)  AS pending,
	0 AS returned,  -- aqui va la cantidad devuelta
	ABS( 
		ISNULL( ( 
			SELECT SUM( query.quantity )  
			FROM tmk_purchase_receipts AS query 
			WHERE query.purchase_order_id = tmk_purchase_orders.id 
			) , 0 
		) - ISNULL( ( 
				SELECT SUM( query.quantity )  
				FROM tmk_article_bodegas AS query 
				INNER JOIN tmk_purchase_orders ON tmk_purchase_orders.id = query.movementable_id  AND query.movementable_type = 'TradeMarketing\Models\PurchaseOrder'
				--WHERE tmk_purchase_article_bodegas.purchase_detail_id = tmk_purchase_order_details.id 
			) , 0 
		) 
	) AS unassigned,
	ISNULL(
	( SELECT _table.quantity FROM (
			SELECT tmk_article_bodegas.article_id, SUM( tmk_article_bodegas.quantity ) AS quantity
			FROM tmk_article_bodegas
			WHERE tmk_article_bodegas.movementable_id = tmk_purchase_orders.id 	
			AND REVERSE(LEFT(REVERSE(tmk_article_bodegas.movementable_type), CHARINDEX('\', REVERSE(tmk_article_bodegas.movementable_type))-1 ))  = 'PurchaseOrder'
			GROUP BY tmk_article_bodegas.article_id
	) AS _table WHERE _table.article_id = tmk_purchase_order_details.article_id ) , 0 
	) AS in_bodega,
	tmk_purchase_order_details.complete,
	(
		SELECT MAX(created_at) 
		FROM tmk_purchase_receipts AS query 
		WHERE query.purchase_order_id = tmk_purchase_orders.id	
	) AS last_receipt, 
	tmk_purchase_order_details.created_at,
	tmk_purchase_order_details.updated_at,
	tmk_purchase_order_details.deleted_at
FROM tmk_purchase_orders 
	LEFT JOIN tmk_purchase_order_details ON tmk_purchase_orders.id = tmk_purchase_order_details.order_id
	LEFT JOIN tmk_article_bodegas ON tmk_article_bodegas.movementable_id = tmk_purchase_orders.id 
	AND REVERSE(LEFT(REVERSE(tmk_article_bodegas.movementable_type), CHARINDEX('\', REVERSE(tmk_article_bodegas.movementable_type))-1 ))  = 'PurchaseOrder'
	INNER JOIN tmk_articles ON tmk_articles.id = tmk_purchase_order_details.article_id
	INNER JOIN tmk_article_masters ON tmk_article_masters.id = tmk_articles.article_master_id
	INNER JOIN tmk_measurement_units ON tmk_measurement_units.id = tmk_article_masters.measurement_unit_id
WHERE tmk_purchase_order_details.deleted_at IS NULL
GROUP BY 
	tmk_purchase_order_details.id,
	tmk_purchase_orders.id, 
	tmk_purchase_orders.[order],
	tmk_purchase_order_details.article_id,
	tmk_articles.internal_reference,
	tmk_article_masters.name,
	tmk_articles.description,
	tmk_purchase_order_details.quantity,
	tmk_purchase_order_details.unit_cost,
	tmk_purchase_order_details.tax,
	tmk_measurement_units.unity,
	tmk_measurement_units.description,
	tmk_purchase_order_details.complete,
	tmk_purchase_order_details.created_at,
	tmk_purchase_order_details.updated_at,
	tmk_purchase_order_details.deleted_at

GO



	/*
	*
	*
	*/
	IF OBJECT_ID('tmk_articles_view') IS NOT NULL
	BEGIN
		DROP VIEW tmk_articles_view
	END
	GO

	CREATE VIEW tmk_articles_view 
	AS
	SELECT 
	tmk_articles.id AS article_id,
	tmk_articles.barcode,
	tmk_articles.internal_reference,
	tmk_articles.description,
	tmk_article_masters.id As article_master_id,
	tmk_article_masters.name AS article_master,
	tmk_article_masters.barcode AS article_master_barcode,
	tmk_article_masters.internal_reference AS article_master_internal_reference,
	tmk_article_masters.image, 
	tmk_article_masters.long_description,  
	tmk_article_masters.min_stock, 
	tmk_article_masters.max_stock,
	tmk_brands.description AS brand, 
	tmk_categories.description AS category, 
	tmk_measurement_units.description AS measurement_unit,
	  ISNULL( 
	 (SELECT CAST ( SUM( query.amount ) /  SUM(query.quantity)  AS decimal (6,2)) 
	  FROM tmk_purchase_order_details AS query 
	  WHERE query.article_id = tmk_articles.id AND query.unit_cost > 0 AND query.unit_cost IS NOT NULL 
	  ), 0.00)
	 AS avg_unit_cost,
	tmk_article_masters.created_by,
	tmk_article_masters.updated_by,
	tmk_articles.deleted_by,
	tmk_article_masters.created_at,
	tmk_article_masters.updated_at,
	tmk_articles.deleted_at
	FROM tmk_articles 
	INNER JOIN tmk_article_masters ON tmk_article_masters.id = tmk_articles.article_master_id
	LEFT JOIN tmk_brands ON tmk_brands.id =  tmk_article_masters.brand_id
	LEFT JOIN tmk_categories ON tmk_categories.id = tmk_article_masters.category_id
	LEFT JOIN tmk_measurement_units ON tmk_measurement_units.id = tmk_article_masters.measurement_unit_id
	GO
/*
ISNULL(tmk_article_masters.min_stock, 1) AS min_stock,

ISNULL(tmk_article_masters.max_stock, 0) AS max_stock,

ISNULL((
	SELECT TBL_STOCK.[entry] - TBL_STOCK.[exit] 
	FROM ( 
		SELECT tmk_article_bodegas.article_id, SUM(tmk_article_bodegas.quantity) AS [entry], ISNULL( (
			SELECT SUM(query.quantity) 
			FROM tmk_article_bodegas AS query INNER JOIN tmk_movements ON tmk_movements.id = query.movement_id 
			WHERE tmk_movements.type_movement_id = 2
			AND query.article_id = tmk_article_bodegas.article_id
		), 0) AS [exit]
	FROM tmk_article_bodegas INNER JOIN tmk_movements ON tmk_movements.id = tmk_article_bodegas.movement_id 
	GROUP BY tmk_article_bodegas.article_id ) TBL_STOCK WHERE TBL_STOCK.article_id = tmk_articles.id
), 0) AS stock,

ISNULL ((
	SELECT TBL_pendeing.pending FROM (
		SELECT article_id, SUM(quantity) - SUM(received) As pending FROM (
		SELECT DISTINCT tmk_purchase_order_details.article_id, tmk_purchase_order_details.quantity, 
		ISNULL((select SUM (query.quantity) FROM tmk_purchase_receipts AS query WHERE query.purchase_detail_id = tmk_purchase_order_details.id), 0) AS received, 
		tmk_purchase_orders.order_status_id
		FROM tmk_purchase_order_details 
		INNER JOIN tmk_purchase_orders ON tmk_purchase_orders.id = tmk_purchase_order_details.order_id
		WHERE tmk_purchase_orders.order_status_id = 5
	) AS _table 
	GROUP BY article_id
) AS TBL_pendeing WHERE TBL_pendeing.article_id = tmk_articles.id ), 0) AS pendeing,
*/
GO






/*
*
*
*/
IF OBJECT_ID('tmk_article_master_warehouses_view') IS NOT NULL
BEGIN
	DROP VIEW tmk_article_master_warehouses_view
END
GO

CREATE VIEW tmk_article_master_warehouses_view 
AS
select 
tmk_article_masters.id As article_master_id,
tmk_article_masters.barcode,
tmk_article_masters.name As article_master,
tmk_article_masters.image As image,
tmk_articles.id AS article_id,
tmk_articles.internal_reference,
CONCAT(tmk_article_masters.name, ' ', tmk_articles.description) as article, 
tmk_brands.description AS brand,
tmk_categories.description AS category,
tmk_measurement_units.description AS measurement_unit,
tmk_bodegas.id AS warehouse_id,
tmk_bodegas.description AS warehouse,
tmk_article_master_warehouses.min_stock,
tmk_article_master_warehouses.max_stock,
tmk_article_warehouses.stock,
(
	SELECT SUM(quantity) 
	FROM tmk_orders AS query
	INNER JOIN tmk_order_details AS query2 ON query2.order_id = query.id
	WHERE query.status_id = 1 AND query.warehouse_destination = tmk_bodegas.id AND query2.article_id = tmk_articles.id
) AS reserved,
(
	tmk_article_warehouses.stock - ISNULL((
		SELECT SUM(tmk_transfer_details.quantity) 
		FROM tmk_transfers INNER JOIN tmk_transfer_details ON tmk_transfer_details.transfer_id = tmk_transfers.id
		WHERE status_id = 1  /*draft*/ AND bodega_id = tmk_bodegas.id and article_id = tmk_articles.id
	), 0)
) AS available_quantity,

tmk_articles.active,
tmk_article_warehouses.updated_at as last_movement,
(select CONCAT(query.firstname,' ',query.lastname) from tmk_users as query where query.id = tmk_article_masters.created_by) As created_by,
tmk_article_masters.created_at,
(select CONCAT(query.firstname,' ',query.lastname) from tmk_users as query where query.id = tmk_article_masters.updated_by) As updated_by,
tmk_article_masters.updated_at,
(select CONCAT(query.firstname,' ',query.lastname) from tmk_users as query where query.id = tmk_articles.deleted_by) As deleted_by,
tmk_articles.deleted_at
from tmk_article_masters
left join tmk_article_master_warehouses ON tmk_article_master_warehouses.article_master_id = tmk_article_masters.id
left join tmk_article_warehouses ON tmk_article_warehouses.article_master_warehouse_id = tmk_article_master_warehouses.id
left join tmk_brands on tmk_brands.id = tmk_article_masters.brand_id
left join tmk_categories on tmk_categories.id = tmk_article_masters.category_id
left join tmk_measurement_units on tmk_measurement_units.id = tmk_article_masters.measurement_unit_id
left join tmk_bodegas ON tmk_bodegas.id = tmk_article_master_warehouses.warehouse_id
left join tmk_articles ON tmk_articles.id = tmk_article_warehouses.article_id
GO



/**
*
*/

IF OBJECT_ID ('tmk_order_details_view') IS NOT NULL
BEGIN
	DROP VIEW tmk_order_details_view
END
GO

CREATE VIEW tmk_order_details_view AS 

select tmk.id, tmk.order_id, tmk.order_num, tmk.article_id, tmk.quantity, tmk.pending, tmk.transferred, tmk.received from (
select 
tmk_order_details.id, 
tmk_orders.id AS order_id, 
tmk_orders.order_num,  
tmk_order_details.article_id, 
tmk_order_details.quantity, 
tmk_order_details.quantity - ISNULL((SELECT SUM(subquery.quantity) 
FROM tmk_transfer_details AS subquery
where subquery.transfer_id = tmk_transfers.id AND  subquery.article_id = tmk_order_details.article_id), 0) AS pending,
tmk_transfers.id AS transfer_id,
tmk_transfers.transfer_num, 
(SELECT SUM(subquery.quantity) 
FROM tmk_transfer_details AS subquery
where subquery.transfer_id = tmk_transfers.id AND  subquery.article_id = tmk_order_details.article_id) AS transferred, 
(SELECT SUM(subquery.received_quantity) 
FROM tmk_transfer_details AS subquery
where subquery.transfer_id = tmk_transfers.id AND  subquery.article_id = tmk_order_details.article_id) AS received
from tmk_orders 
inner join tmk_order_details on tmk_order_details.order_id = tmk_orders.id
left join tmk_transfers on tmk_transfers.order_id = tmk_orders.id
left join tmk_transfer_details on tmk_transfer_details.transfer_id = tmk_transfers.id
) AS tmk 
group by
id, 
order_id, 
order_num,  
article_id, 
quantity, 
pending,
transferred, 
received
GO




/**
* TRANSFERS VIEW
*
*/
IF OBJECT_ID ('tmk_transfers_view') IS NOT NULL
BEGIN
	DROP VIEW tmk_transfers_view
END
GO

CREATE VIEW tmk_transfers_view AS 
SELECT  
tmk_transfers.id, 
tmk_transfers.transfer_num, 
tmk_transfers.transfer_reference,
(
	select tmk_bodegas.description 
	from tmk_bodegas 
	where tmk_bodegas.id = tmk_transfers.bodega_id 
)AS transfer_warehouse_origen, 
(
	select tmk_bodegas.description 
	from tmk_bodegas 
	where tmk_bodegas.id = tmk_transfers.bodega_id_end 
)AS transfer_warehouse_destination,
( 
	select tmk_order_status.description from tmk_order_status where tmk_order_status.id =  tmk_transfers.status_id 
) As [status],

tmk_transfers.created_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.created_by
) AS created_by,
tmk_transfers.updated_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.updated_by
) AS updated_by,
tmk_transfers.transferred_at,
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.transferred_by
) AS transferred_by,
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.transferred_for
) AS transferred_for,

tmk_transfers.received_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.received_by
) AS received_by,
tmk_transfers.canceled_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.canceled_by
) AS canceled_by,
tmk_orders.id AS order_id, 
tmk_orders.order_num, 
(
	select tmk_bodegas.description 
	from tmk_bodegas 
	where tmk_bodegas.id = tmk_orders.warehouse_origen 
)AS order_warehouse_origen, 
(
	select tmk_bodegas.description 
	from tmk_bodegas 
	where tmk_bodegas.id = tmk_orders.warehouse_destination
)AS order_warehouse_destination,
tmk_orders.ordered_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_orders.created_by
) AS ordered_by,
tmk_transfers.requested_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.requested_by
) AS requested_by
from tmk_orders
inner join tmk_transfers on tmk_orders.id = tmk_transfers.order_id
GO 







/*
--
Select 
tmk_transfers.id, transfer_num, 
tmk_orders.order_num AS [order], 
tmk_transfers.subject AS [subject],
(select description from tmk_bodegas where id =  bodega_id) AS warehouse_origen,
(select description from tmk_bodegas where id =  bodega_id_end) AS warehouse_destination,
(Select CONCAT(tmk_users.firstname ,' ', tmk_users.lastname) from tmk_users where id = transferred_by)  AS transferred_by, 
(Select CONCAT(tmk_users.firstname ,' ', tmk_users.lastname) from tmk_users where id = transferred_for)  AS transferred_for, 
(Select CONCAT(tmk_users.firstname ,' ', tmk_users.lastname) from tmk_users where id = tmk_transfers.created_by)  AS created_by, 
(Select CONCAT(tmk_users.firstname ,' ', tmk_users.lastname) from tmk_users where id = tmk_transfers.updated_by)  AS updated_by, 
[status], 
transferred_at, 
tmk_transfers.received_at, 
tmk_transfers.created_at, 
tmk_transfers.updated_at
from tmk_transfers 
inner join tmk_orders on tmk_orders.id  = tmk_transfers.order_id 
where   bodega_id_end in (9, 6) or transferred_by = 1
order by id desc, transferred_at desc
*/




