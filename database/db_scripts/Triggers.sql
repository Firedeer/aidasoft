USE BI_COMERCIAL
GO


/** SQL
 * Actualiza la tabla article_warehouses para mantener el stock actualizado,
 * cuando se genera una insercion en la tabla article_bodegas
 */		
IF OBJECT_ID ('tmk_article_warehouses_update_stock') IS NOT NULL
BEGIN
	DROP TRIGGER tmk_article_warehouses_update_stock
END
GO	
CREATE TRIGGER tmk_article_warehouses_update_stock ON tmk_article_bodegas  
AFTER INSERT  
AS  

DECLARE @article_id AS INT
DECLARE @warehouse_id AS INT
DECLARE @quantity_entry AS INT = 0 
DECLARE @quantity_exit AS INT = 0
DECLARE @updated_at AS DATETIME
DECLARE @stock AS INT = 0
DECLARE @article_master AS INT
DECLARE @article_master_warehouse_id AS INT = 0

SET @article_id		= ( SELECT article_id FROM INSERTED )
SET @warehouse_id	= ( SELECT bodega_id FROM INSERTED )
SET @quantity_entry = ( SELECT quantity FROM INSERTED )
SET @quantity_exit	= ( SELECT quantity_exit FROM INSERTED )
SET @updated_at		= ( SELECT updated_at FROM INSERTED)
SET @stock			= ( SELECT stock	
						FROM tmk_article_warehouses INNER JOIN tmk_article_master_warehouses 
						ON tmk_article_master_warehouses.id = tmk_article_warehouses.article_master_warehouse_id 
						WHERE article_id = @article_id AND warehouse_id =  @warehouse_id )

SET @article_master = ( SELECT TOP 1 article_master_id FROM tmk_articles WHERE id = @article_id )

SET @article_master_warehouse_id  =  ( SELECT TOP 1 id FROM tmk_article_master_warehouses WHERE warehouse_id = @warehouse_id AND article_master_id = @article_master )
									
IF EXISTS (SELECT * FROM tmk_article_warehouses WHERE article_id = @article_id AND  article_master_warehouse_id = @article_master_warehouse_id )	
	BEGIN  
	IF @quantity_entry = 0 

	UPDATE tmk_article_warehouses 
	SET stock = @stock - @quantity_exit, updated_at = @updated_at 
	WHERE article_id = @article_id AND article_master_warehouse_id = @article_master_warehouse_id
	
		ELSE   
	UPDATE tmk_article_warehouses 
	SET stock = @stock + @quantity_entry, updated_at = @updated_at 
	WHERE article_id = @article_id AND  article_master_warehouse_id = @article_master_warehouse_id
	
	END

ELSE
	BEGIN   
		IF @quantity_entry > 0 
		BEGIN	
	 
		IF EXISTS (SELECT * FROM tmk_article_master_warehouses WHERE article_master_id = @article_master AND warehouse_id = @warehouse_id)
			BEGIN
				SET @article_master_warehouse_id = (SELECT id FROM tmk_article_master_warehouses WHERE article_master_id = @article_master AND warehouse_id = @warehouse_id)
		
				INSERT INTO tmk_article_warehouses (article_id, article_master_warehouse_id, stock, created_at, updated_at) 
				VALUES ( @article_id, @article_master_warehouse_id, @quantity_entry, @updated_at, @updated_at)
			END
		ELSE
			BEGIN
				INSERT INTO tmk_article_master_warehouses (article_master_id, warehouse_id) VALUES (@article_master, @warehouse_id) 
				
				SET @article_master_warehouse_id =  (SELECT @@IDENTITY AS 'Identity')
				
				INSERT INTO tmk_article_warehouses (article_id, article_master_warehouse_id, stock, created_at, updated_at) 
				VALUES ( @article_id, @article_master_warehouse_id, @quantity_entry, @updated_at, @updated_at)
			END
		END
	
	ELSE 
		RAISERROR ('No es posible crear un movimiento con stock 0', 16, 1)
	END	
GO  

-- TEST 
--select * from tmk_article_warehouses
--Begin transaction
--INSERT [dbo].[tmk_article_bodegas] ([article_id], [bodega_id], [quantity], [quantity_exit], [active], [initial], [created_at], [updated_at], [deleted_at], [movement_id]) 
--VALUES (1, 8, 0, 100, 1, 0, CAST(N'2016-12-10 22:29:05.000' AS DateTime), CAST(N'2016-12-10 22:29:05.000' AS DateTime), NULL, 1)
--select * from tmk_article_bodegas order by id desc
--select * from tmk_article_warehouses

--rollback
--select * from tmk_article_bodegas 
--select * from tmk_article_warehouses


-- MYSQL




SET @article_id		:= NEW.article_id
,@warehouse_id	:= NEW.bodega_id
,@quantity_entry := NEW.quantity
,@quantity_exit	:= NEW.quantity_exit
,@updated_at		:= NEW.updated_at
,@stock			:= ( SELECT stock FROM tmk_article_warehouses AS A INNER JOIN tmk_article_master_warehouses AS B
						ON B.id = A.article_master_warehouse_id 
                        WHERE a.article_id=NEW.article_id and b.warehouse_id=NEW.bodega_id)
,@article_master = (SELECT article_master_id FROM tmk_articles WHERE id = NEW.article_id LIMIT 1)
,@article_master_warehouse_id  =  ( SELECT  id FROM tmk_article_master_warehouses WHERE warehouse_id = NEW.bodega_id AND article_master_id = @article_master LIMIT 1)


IF (SELECT IF( EXISTS( SELECT * FROM tmk_article_warehouses WHERE article_id = NEW.article_id AND article_master_warehouse_id = @article_master_warehouse_id), 1, 0) AS RESP)=1 THEN

	IF NEW.quantity= 0 THEN

		UPDATE tmk_article_warehouses 
		SET stock = @stock - NEW.quantity_exit, updated_at = NEW.updated_at 
		WHERE article_id = NEW.article_id AND article_master_warehouse_id = @article_master_warehouse_id

	ELSE
		UPDATE tmk_article_warehouses 
		SET stock = @stock + NEW.quantity_exit, updated_at = NEW.updated_at 
		WHERE article_id = NEW.article_id AND article_master_warehouse_id = @article_master_warehouse_id
	END IF;

ELSE
	IF NEW.quantity> 0 THEN
		IF (SELECT IF( EXISTS(SELECT * FROM tmk_article_master_warehouses WHERE article_master_id = @article_master AND warehouse_id = NEW.bodega_id), 1, 0) AS RESP)=1 THEN
			SET @article_master_warehouse_id = (SELECT id FROM tmk_article_master_warehouses WHERE article_master_id = @article_master AND warehouse_id = NEW.bodega_id)
			INSERT INTO tmk_article_warehouses (article_id, article_master_warehouse_id, stock, created_at, updated_at) 
			VALUES ( NEW.article_id, @article_master_warehouse_id,NEW.quantity, NEW.updated_at, NEW.updated_at)
		ELSE
			INSERT INTO tmk_article_master_warehouses (article_master_id, warehouse_id) VALUES (@article_master, @warehouse_id) 
				
			SET @article_master_warehouse_id =  (SELECT LAST_INSERT_ID())
				
			INSERT INTO tmk_article_warehouses (article_id, article_master_warehouse_id, stock, created_at, updated_at) 
			VALUES ( NEW.article_id, @article_master_warehouse_id, NEW.quantity,  NEW.updated_at, NEW.updated_at)

		END IF;
	ELSE


	END IF;

 END IF;

 
 
DROP TRIGGER IF EXISTS `TransactNotifycation`;

CREATE DEFINER=`cpses_r1jirvnpgq`@`localhost` TRIGGER `TransactNotifycation` AFTER INSERT ON `tmk_article_bodegas` FOR EACH ROW IF EXISTS(SELECT count(*) FROM tmk_article_warehouses
                      WHERE article_id = NEW.article_id AND article_master_warehouse_id = 
              ( SELECT  id FROM tmk_article_master_warehouses WHERE warehouse_id = NEW.bodega_id AND article_master_id = @article_master LIMIT 1)
             
             ) THEN
IF NEW.quantity= 0 THEN

UPDATE tmk_article_warehouses 
SET stock = ( SELECT stock FROM tmk_article_warehouses AS A INNER JOIN tmk_article_master_warehouses AS B
						ON B.id = A.article_master_warehouse_id 
                        WHERE a.article_id=NEW.article_id and b.warehouse_id=NEW.bodega_id)- NEW.quantity_exit
, updated_at = NEW.updated_at 
WHERE article_id = NEW.article_id AND article_master_warehouse_id = ( SELECT  id FROM tmk_article_master_warehouses WHERE warehouse_id = NEW.bodega_id AND article_master_id = @article_master LIMIT 1);
 
ELSE
UPDATE tmk_article_warehouses 
SET stock = ( SELECT stock FROM tmk_article_warehouses AS A INNER JOIN tmk_article_master_warehouses AS B
						ON B.id = A.article_master_warehouse_id 
                        WHERE a.article_id=NEW.article_id and b.warehouse_id=NEW.bodega_id)- NEW.quantity_exit
, updated_at = NEW.updated_at 
WHERE article_id = NEW.article_id AND article_master_warehouse_id = ( SELECT  id FROM tmk_article_master_warehouses WHERE warehouse_id = NEW.bodega_id AND article_master_id = @article_master LIMIT 1);
          
END IF;

ELSE

IF NEW.quantity> 0 THEN

IF EXISTS(SELECT COUNT(*) FROM tmk_article_master_warehouses WHERE article_master_id = (SELECT article_master_id FROM tmk_articles WHERE id = NEW.article_id) AND warehouse_id = NEW.bodega_id) THEN



			INSERT INTO tmk_article_warehouses (article_id, article_master_warehouse_id, stock, created_at, updated_at) 
			VALUES ( NEW.article_id, (SELECT id FROM tmk_article_master_warehouses WHERE article_master_id = @article_master AND warehouse_id = NEW.bodega_id),NEW.quantity, NEW.updated_at, NEW.updated_at);

ELSE
INSERT INTO tmk_article_master_warehouses (article_master_id, warehouse_id) VALUES ((SELECT article_master_id FROM tmk_articles WHERE id = NEW.article_id LIMIT 1), NEW.bodega_id);
				
INSERT INTO tmk_article_warehouses (article_id, article_master_warehouse_id, stock, created_at, updated_at) 
			VALUES ( NEW.article_id, (SELECT MAX(ID) FROM tmk_article_bodegas), NEW.quantity,  NEW.updated_at, NEW.updated_at);
            
END IF;




END IF;
 
 
 
END IF